################################################################
# directory trees
################################################################

# search prefix
#   additional path to search for required libraries
#   (e.g., su3lib and eigen)
search_prefix := $(BOOST_ROOT) $(MASCOT_ROOT)
search_dirs_include  := /usr/local/include/eigen3/
search_dirs_lib := /usr/local/gfortran/lib/x86_64 

# install prefix
install_prefix := $(current-dir)
# Note: You should reset to /user/local to do a systemwide 
# installation.  This is analagous to the --prefix= option of 
# autoconf installations.

################################################################
# machine-specific library configuration
################################################################

# SU3LIB numerical precision
#   Set flag SU3DBL for double precision or SU3QUAD for quad precision.
#   Note: quad precision requires ifort compiler

FFLAGS += -DSU3DBL
##FFLAGS += -DSU3QUAD

# machine-specific numerical library
# Gnu Scientific library
LDLIBS += -lgsl 
LDLIBS += -lgslcblas 

# binary output using hdf5 
LDLIBS += -lmascot -lz -lhdf5
#LDLIBS += -lz 

# Intel Math Kernel library for MFDn eigensolver
# Needed when compiling with intel C++ & Intel Math Kernel library for MFDn eigensolver
LDLIBS += -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lguide -lpthread -openmp
LDLIBS += -lboost_mpi -lboost_serialization -lboost_system -lboost_chrono
#LDLIBS += -lacml
#LDLIBS += -lirc

################################################################
# C++ compiler-specific configuration
################################################################

# C++ compiler
CXX := mpicxx 
#CXX := icpc

# C++ compiler optimization and debugging
#CXXFLAGS += -cxx=g++  -DNDEBUG -DCPP0X_STD_TR1 -fopenmp -O2 #-Wall -W
CXXFLAGS += -cxx=icpc -std=c++0x -shared-intel -DCPP0X_STD_TR1 -DMPICH_IGNORE_CXX_SEEK -DHAVE_INLINE -openmp -O3

# parallel C++ compiler
#   used in module.mk files as
#   program program.o: CXX := $(MPICXX)
#MPICXX := mpicxx -cxx=$(CXX)
MPICXX := mpicxx 

################################################################
# FORTRAN compiler-specific configuration
################################################################

# FORTRAN compiler
# Example values:
#   for GCC 3.x: f77
#   for GCC 4.x: gfortran
#   for Intel: ifort
#FC := gfortran
#FC := ifort 
FC := mpif77 -f77=ifort -recursive
#FC := mpif77 -f77=/sw/bin/gfortran
#FC := mpif77 -f77=gfortran -m64 -frecursive #frecursive needed for multithread safe su3lib
#FC := mpif90

# FORTRAN compiler optimization and debugging
FFLAGS += -O3 
#FFLAGS += -g

################################################################
# C++/FORTRAN linking 
#    with C++ main()
################################################################

# FORTRAN object libraries (added to LDLIBS)
# Example values, depending on the compiler you are using to compile
# the FORTRAN objects:
#   for GCC 3.x f77: -lg2c
#   for GCC 4.x gfortran: -lgfortran
#   for Intel ifort: -lifport -lifcore -limf
#fortran_libs := -lgfortran
fortran_libs += -lifport -lifcore -limf

# FORTRAN linking flags (added to LDFLAGS)
# Not yet needed but provided as hook.
fortran_flags := 

################################################################
# directory trees
################################################################

# search prefix
#   additional path to search for required libraries
#   (e.g., su3lib and eigen)
search_prefix := $(HOME)/local $(BOOST_ROOT) $(MASCOT_ROOT) /usr/common/usg/gsl/1.13 $(HDF5_PAR_DIR) $(GSL_DIR)
search_dirs_include  := $(HOME)/local/include/eigen3.1.1
search_dirs_lib := $(MKL_LIBDIR)

# install prefix
install_prefix := $(HOME)/$(NERSC_HOST)/local/su3shell
# Note: You should reset to /user/local to do a systemwide 
# installation.  This is analagous to the --prefix= option of 
# autoconf installations.

################################################################
# machine-specific library configuration
################################################################

# SU3LIB numerical precision
#   Set flag SU3DBL for double precision or SU3QUAD for quad precision.
#   Note: quad precision requires ifort compiler

FFLAGS += -DSU3DBL
##FFLAGS += -DSU3QUAD

# machine-specific numerical library
# Gnu Scientific library
LDLIBS += -lgsl 
LDLIBS += -lgslcblas 

# binary output using hdf5 
LDLIBS += -lmascot #-lz -lhdf5
#LDLIBS += -lz 

# Intel Math Kernel library for MFDn eigensolver
# Needed when compiling with intel C++ & Intel Math Kernel library for MFDn eigensolver
#LDLIBS += -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lguide -lpthread
LDLIBS += -fopenmp
LDLIBS += -lboost_mpi -lboost_serialization -lboost_chrono -lboost_system
#LDLIBS += -lacml
#LDLIBS += -lirc

################################################################
# C++ compiler-specific configuration
################################################################

# C++ compiler
CXX := cc 
CXXFLAGS += -O3 -std=c++11 -DNDEBUG -DHAVE_INLINE -ffast-math -funroll-loops -fopenmp

# parallel C++ compiler
#   used in module.mk files as
#   program program.o: CXX := $(MPICXX)
MPICXX := cc -std=c++11

################################################################
# FORTRAN compiler-specific configuration
################################################################

# FORTRAN compiler
# Example values:
#   for GCC 3.x: f77
#   for GCC 4.x: gfortran
#   for Intel: ifort
#FC := gfortran
#FC := ifort 
#FC := mpif77 -f77=ifort
FC := ftn

FFLAGS += -O3 -ffast-math -funroll-loops

################################################################
# C++/FORTRAN linking 
#    with C++ main()
################################################################

# FORTRAN object libraries (added to LDLIBS)
# Example values, depending on the compiler you are using to compile
# the FORTRAN objects:
#   for GCC 3.x f77: -lg2c
#   for GCC 4.x gfortran: -lgfortran
#   for Intel ifort: -lifport -lifcore -limf
#fortran_libs := -lgfortran
#fortran_libs := -lifport -lifcore -limf

# FORTRAN linking flags (added to LDFLAGS)
# Not yet needed but provided as hook.
fortran_flags := 

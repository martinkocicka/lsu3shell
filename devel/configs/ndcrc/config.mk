################################################################
# directory trees
################################################################

# search prefix
#   additional path to search for required libraries
#   (e.g., su3lib and eigen)
search_prefix := ${HOME}/local ${HOME}/opt/mascot-111006/gnu ${HOME}/opt/hdf5-1.8.7/gnu
search_dirs_include  := ${HOME}/local/include/eigen3 ${BOOST_ROOT}
search_dirs_lib := 

# install prefix
install_prefix := $(HOME)/local
# Note: You should reset to /user/local to do a systemwide 
# installation.  This is analagous to the --prefix= option of 
# autoconf installations.

################################################################
# machine-specific library configuration
################################################################

# SU3LIB numerical precision
#   Set flag SU3DBL for double precision or SU3QUAD for quad precision.
#   Note: quad precision requires ifort compiler

FFLAGS += -DSU3DBL
##FFLAGS += -DSU3QUAD

# machine-specific numerical library
# Intel Math Kernel library for MFDn eigensolver
# Needed when compiling with intel C++ & Intel Math Kernel library for MFDn eigensolver
##LDLIBS += -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lguide -lpthread
#LDLIBS += -lacml
#LDLIBS += -lirc
# ugly fix to force static linking of ACML
LDLIBS += /afs/crc.nd.edu/x86_64_linux/scilib/acml/current/gfortran/gfortran64/lib/libacml.a




################################################################
# C++ compiler-specific configuration
################################################################

# C++ compiler
CXX := g++
#CXX := icpc

# C++ compiler optimization and debugging
CXXFLAGS += -O3  -DNDEBUG
#CXXFLAGS += -g


# parallel C++ compiler
#   used in module.mk files as
#   program program.o: CXX := $(MPICXX)
MPICXX := mpicxx -cxx=$(CXX)

################################################################
# FORTRAN compiler-specific configuration
################################################################

# FORTRAN compiler
# Example values:
#   for GCC 3.x: f77
#   for GCC 4.x: gfortran
#   for Intel: ifort
#FC := gfortran
#FC := ifort 
#FC := mpif77 -f77=ifort
#FC := mpif77 -f77=/sw/bin/gfortran
#FC := mpif77 -f77=gfortran
FC := mpif90

# FORTRAN compiler optimization and debugging
FFLAGS += -O3  -DNDEBUG
#FFLAGS += -g

################################################################
# C++/FORTRAN linking 
#    with C++ main()
################################################################

# FORTRAN object libraries (added to LDLIBS)
# Example values, depending on the compiler you are using to compile
# the FORTRAN objects:
#   for GCC 3.x f77: -lg2c
#   for GCC 4.x gfortran: -lgfortran
#   for Intel ifort: -lifport -lifcore -limf
fortran_libs := -lgfortran
#fortran_libs += -lifport -lifcore -limf

# C++/FORTRAN linkage naming convention
#   Note: This will normally be automatically determined.
#   Sometimes it might be necessary to provide a directive below to overrride.
CXXFLAGS := -DNDCRCGNU

# FORTRAN linking flags (added to LDFLAGS)
# Not yet needed but provided as hook.
fortran_flags := 

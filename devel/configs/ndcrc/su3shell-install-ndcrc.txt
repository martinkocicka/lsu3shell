****************************************************************
Local installation notes for su3shell package for

      University of Notre Dame CRC Linux cluster

Last modified: 11/5/11 (mac)
****************************************************************

GNU Make -- OK

MPICH2
  -- load module for latest version of MPICH2 for the compiler suite being used 
  -- these instructions assume gnu

  module load mpich2/1.4.0-gnu

boost
  -- load module for latest version (tested with 1.43)

  module load boost

HDF5
  -- ND CRC hdf5 module is apparently based on static binaries from
  HDF5 site and does not have parallel support enabled
  -- therefore, must do own installation

  Installation of HDF5-1.8.7 (11/4/11):
    module load mpich2/1.4.0-gnu
    setenv CC mpicc
    ./configure --enable-parallel --prefix=$HOME/opt/hdf5-1.8.7/gnu

Mascot

  Installation of version 111006 (11/6/11):

  Untar and move the Mascot root directory to $HOME/opt/mascot-111006/gnu. 

    Edit make.inc to set COMPILER=g++.

    module load mpich2/1.4.0-gnu
    module load boost
    setenv MASCOT_ROOT $HOME/opt/mascot-111006/gnu
    setenv HDF5_DIR $HOME/opt/hdf5-1.8.7/gnu
    make

LAPACK
  -- two versions are installed: ACML for AMD machines, MKL for Intel machines
  -- the shared object libraries produce a linking error
  -- force use of static libraries in the su3shell makefile -- the ugly way is

  LDFLAGS += /afs/crc.nd.edu/x86_64_linux/scilib/acml/current/gfortran/gfortran64/lib/libacml.a

Eigen
  -- install manually as $HOME/usr/local/include/eigen3/Eigen/...

----------------------------------------------------------------

See ndcrc/config.mk for mods to configuration file.

The build with:
    module load mpich2/1.4.0-gnu
    module load boost
    cd su3shell
    make all
    make install

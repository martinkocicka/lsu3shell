#ifndef BROADCAST_DATA_CONTAINER_H
#define BROADCAST_DATA_CONTAINER_H
#include <boost/chrono.hpp> 
#include <boost/mpi.hpp>
#include <boost/archive/binary_iarchive.hpp> 
#include <boost/archive/binary_oarchive.hpp> 
template<class T>
void BroadcastDataContainer(T& data_container, const std::string& container_name)
{
	boost::chrono::system_clock::time_point start;
	boost::mpi::communicator mpi_comm_world;
	int my_rank(mpi_comm_world.rank());

	std::string buffer;
	int buffer_length;

	if (my_rank == 0)
	{
		cout << "BroadcastDataContainer:\t\tBeggining broadcasting container '" << container_name << "'. ";
		start = boost::chrono::system_clock::now();

		std::ostringstream ss;
		boost::archive::binary_oarchive oa(ss);
		oa << data_container;
		buffer = ss.str();
		buffer_length = buffer.size();
		cout << "Size of serialized class: " << sizeof(char)*buffer_length/(1024.0*1024.0) << " MB." << endl;
	}

	MPI_Bcast((void*)&buffer_length, 1, MPI_INT, 0, mpi_comm_world);

	if (my_rank != 0)
	{
		buffer.resize(buffer_length);
	}

	MPI_Bcast((void*)buffer.data(), buffer_length, MPI_CHAR, 0, mpi_comm_world);

	if (my_rank != 0)
	{
		std::istringstream ss(buffer);
		boost::archive::binary_iarchive oa(ss);
		oa >> data_container;
	}
	else
	{
		cout << "BroadcastDataContainer:\t\tProcess 0 is done! It took: " << boost::chrono::duration<double>(boost::chrono::system_clock::now() - start) << endl;
	}
}
#endif

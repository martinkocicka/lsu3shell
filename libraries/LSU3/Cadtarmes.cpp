#include <LSU3/Cadtarmes.h>
#include <LSU3/Cadta_table.h>

namespace lsu3
{

Cadtarmes::RMES_RECORD Cadtarmes::empty_rms_record_(0, 0, 0);

void Cadtarmes::add(const uint32_t bra_index, const uint32_t ket_index, const std::vector<uint32_t>& tensor_ids, const std::vector<float>& rmes)
{
	data_[key(bra_index, ket_index)] = RMES_RECORD(tensor_ident_numbers_.size(), rmes_.size(), tensor_ids.size());
	rmes_.insert(rmes_.end(), rmes.begin(), rmes.end());
	tensor_ident_numbers_.insert(tensor_ident_numbers_.end(), tensor_ids.begin(), tensor_ids.end());
}


size_t GetRelevantTensorGroups( const std::vector<CTensorGroup_ada*>& tensorGroups,
								const std::vector<unsigned char>& Bra_confs, 
								const std::vector<unsigned char>& Ket_confs, 
								const std::vector<unsigned char>& hoShells, 
								std::vector<CTensorGroup_ada*>& selectedTensorGroups, 
								std::vector<int>& phase_tensor_group) 
{
	std::vector<unsigned char> tensor(hoShells.size());
	size_t i, j;

//	Iterate over all families of TensorGroups (.i.e. SingleShell, TwoShellsdAZero, TwoShells, ThreeShells, FourShells)
	for (j = 0; j < tensorGroups.size(); ++j)	// iterate over all TensorGroups in the family ...
	{
		CTensorGroup_ada* pTG = tensorGroups[j];
		if (pTG->Couple(Bra_confs, Ket_confs, hoShells)) // Does TensorGroup connet bra and ket configurations ?
		{
			tensor.assign(tensor.size(), 0);	//	tensor is an array whose element [i] hold the number of a+/a operators in HO shell n == hoShells[i]
			const std::vector<char>& n_adta_InSST = pTG->n_adta_InSST(); 
			const std::vector<unsigned char>& ShellsT = pTG->ShellsT(); 
			for(size_t i = 0; i < hoShells.size(); ++i)	// iterate HO shells with non-zero number of fermions 
			{
				std::vector<unsigned char>::const_iterator cit = std::find(ShellsT.begin(), ShellsT.end(), hoShells[i]);	// *cit == HO shell n == hoShells[i]
				if (cit != ShellsT.end()) // there is a single-shell tensor acting on HO shell n == hoShells[i]
				{
					size_t index = cit - ShellsT.begin();
					tensor[i] = n_adta_InSST[index];	// store number of a+/a operators ==> needed for phase calculation
				}
			}
			phase_tensor_group.push_back(rme_uncoupling_phase(Bra_confs, tensor, Ket_confs));	//	calculate and store a phase due to the exchanges of fermion creation/annihilation operators [see relation (5.6)]
			selectedTensorGroups.push_back(pTG);
		}
	}
}

//	transform distr_bra and distr_ket and augment gamma_bra and omega_bra according 
void TransformDistributions_SelectByDistribution(
										const std::vector<CTensorGroup_ada*>& tensorGroups,
										SingleDistribution& distr_bra, 
										UN::SU3xSU2_VEC& gamma_bra,
										SU3xSU2_VEC& omega_bra,
										SingleDistribution& distr_ket, 
										std::vector<unsigned char>& hoShells, unsigned char& num_vacuums_ket_distr,
										std::vector<int>& phase_pn, std::vector<CTensorGroup_ada*>& selectedTensorGroups)
{
	unsigned char num_vacuums_bra_distr;
	SingleDistribution transformed_distribution_bra, transformed_distribution_ket;

	Transform(distr_bra, distr_ket, hoShells, transformed_distribution_bra, transformed_distribution_ket);
	distr_bra.swap(transformed_distribution_bra);
	distr_ket.swap(transformed_distribution_ket);

	num_vacuums_bra_distr = std::count(distr_bra.begin(), distr_bra.end(), 0);
	num_vacuums_ket_distr = std::count(distr_ket.begin(), distr_ket.end(), 0);

	phase_pn.resize(0); selectedTensorGroups.resize(0);
	
	GetRelevantTensorGroups(tensorGroups, distr_bra, distr_ket, hoShells, selectedTensorGroups, phase_pn);

	if (!selectedTensorGroups.empty())
	{
		UN::SU3xSU2_VEC gamma_bra_vacuum_augmented;
		//	augment gamma_bra and omega_bra
		if (num_vacuums_bra_distr > 0)	// ==>	we must to add a vacuum shells into bra
		{
			AugmentGammaByVacuumShells(distr_bra, gamma_bra, gamma_bra_vacuum_augmented);
			AugmentOmegaByVacuumShells(distr_bra, gamma_bra_vacuum_augmented, omega_bra);	// input: omega_bra output: omega_bra !!!
			gamma_bra.swap(gamma_bra_vacuum_augmented);
		}
	}
}

void TransformGammaKet_SelectByGammas(
//	input
									const std::vector<unsigned char>& hoShells,
									const SingleDistribution& distr_ket, 
									const unsigned char num_vacuums_ket_distr, 
									const std::vector<int>& phase_pn, 
									const std::vector<CTensorGroup_ada*>& tensorGroups_pn, 
									const UN::SU3xSU2_VEC& gamma_bra_vacuum_augmented, 
//	output:									
									UN::SU3xSU2_VEC& gamma_ket, 
									std::vector<std::pair<CRMECalculator*, unsigned int> >& selected_tensors_pn)
{
	UN::SU3xSU2_VEC gamma_ket_vacuum_augmented;
	if (num_vacuums_ket_distr > 0)
	{
		AugmentGammaByVacuumShells(distr_ket, gamma_ket, gamma_ket_vacuum_augmented);
		gamma_ket.swap(gamma_ket_vacuum_augmented);
	}

	for (size_t i = 0; i < tensorGroups_pn.size(); ++i)
	{
		tensorGroups_pn[i]->SelectTensorsByGamma(gamma_bra_vacuum_augmented, gamma_ket, distr_ket, hoShells, phase_pn[i], selected_tensors_pn);
	}
}

void TransformOmegaKet_CalculateRME(
//	input:
										const SingleDistribution& distr_ket,										
										const UN::SU3xSU2_VEC& gamma_bra,
										const SU3xSU2_VEC& omega_bra,
										const UN::SU3xSU2_VEC& gamma_ket,
										const unsigned char num_vacuum_shells_bra,
										const std::vector<std::pair<CRMECalculator*, unsigned int> >& tensors_pn,
//	output:										
										SU3xSU2_VEC& omega_ket, 
										std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_pn)
{
	if (num_vacuum_shells_bra > 0)
	{
		AugmentOmegaByVacuumShells(distr_ket, gamma_ket, omega_ket);
	}

	for (size_t i = 0; i < tensors_pn.size(); ++i)
	{
		SU3xSU2::RME* pRME_pn = tensors_pn[i].first->CalculateRME(gamma_bra, omega_bra, gamma_ket, omega_ket);
		if (pRME_pn)
		{
			rme_index_pn.push_back(std::make_pair(pRME_pn, tensors_pn[i].second));
		}
	}
}


void Calculate_adta_rmes( 
				const int32_t nucleon_type, 
				const CBaseSU3Irreps& baseSU3Irreps,
				const std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC> adta_tensors, // assumption: SU3xSU2_VEC are sorted
				const std::vector<std::pair<uint32_t, uint32_t> > ij_pairs,
				const CncsmSU3xSU2Basis& bra_basis,
				const CncsmSU3xSU2Basis& ket_basis, 
				const bool generate_missing_rme_files, const bool log_is_on,
				const Cadta_table& adta_table, 
				Cadtarmes& rmes)
{	
	unsigned char max_hoshell = baseSU3Irreps.GetLastShell();

	std::ofstream null_log_file("/dev/null");
	std::ostream& log_file = (log_is_on) ? std::cout : null_log_file; 

	CSSTensorRMELookUpTablesContainer rmeLookUpTableContainer(baseSU3Irreps);
//	Step #1: create vector of pointer to CTensorGroup_ada - structure underpinning calculation of multi-shell rmes
	std::vector<CTensorGroup_ada*> tensorGroups;
	uint16_t nTensors;
	uint32_t firstLabelId(0);
	std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC>::const_iterator it = adta_tensors.begin();
	for (; it != adta_tensors.end(); ++it)
	{
		SU3xSU2_VEC tensorLabels(1);	//	a+a is a one-body interaction ==> only one SU3xSU2 label is needed 
		std::vector<char> structure(3);
		std::vector<std::vector<char> > vSingleShell_structures;	//	array of structures describing construction of the single-shell tensors
		std::vector<unsigned char> ShellsT;							//	array of HO shells of all single-shell tensors present in a general tensor
		char nShellsT;												//	number of shells in tensor => for one-body interaction max(nShellsT) = 2
		std::vector<std::vector<SU3xSU2::LABELS*> > vSingleShellTensorLabels; 	
		std::vector<SU3xSU2::LABELS*> vOmega;

		structure[0] = it->first.first;
		structure[1] = it->first.second;
		structure[2] = 0;

		if ((abs(structure[0]) - 1) > max_hoshell || (abs(structure[1]) - 1) > max_hoshell) // ==> this operator is not needed
		{
			continue;
		}
	
	//	transform structure describing a general one-body tensor and its
	//	SU3xSU2 label into two possible cases:
	//	(A) Input: structure = {1 -3 0}
	//	TensorLabels = {IR0}
	//	Output:
	//	ShellsT = {0, 2}
	//	vSingleShell_structures = [ {1}, {-3}]
	//	vSingleShellTensorLabels = [empty, empty]
	//	vOmega = {&IR0}
	//
	//	(B) Input: structure = {3 -3 0}
	//	TensorLabels = {IR0}
	//	Output:
	//	ShellsT = {2}
	//	vSingleShell_structures = [{3 -3 0}]
	//	vSingleShellTensorLabels = [{&IR0}]
	//	vOmega = empty
		StructureToSingleShellTensors(structure, ShellsT, tensorLabels, vSingleShell_structures, vSingleShellTensorLabels, vOmega);
		nShellsT = ShellsT.size();
		assert(nShellsT == 1 || nShellsT == 2);

		std::vector<char> dA(ShellsT.size(), 0);				//	case (A): dA = {+1, -1} or {-1, +1}; case (B): dA = {0}
		std::vector<unsigned char> nAnnihilators(nShellsT, 0);	//	case (A): nAnnihilators = {0, 1} or {1, 0}; case (B): nAnnihilators = {1}
		CTensorGroup_ada* pCTensorGroup(NULL);  

		for (int i = 0; i < nShellsT; ++i)
		{
			nAnnihilators[i] = std::count_if(vSingleShell_structures[i].begin(), vSingleShell_structures[i].end(), std::bind2nd(std::less<char>(), 0));
			dA[i] = std::count_if(vSingleShell_structures[i].begin(), vSingleShell_structures[i].end(), std::bind2nd(std::greater<char>(), 0)) // #creations - #annihilations
					- nAnnihilators[i];
		}
		assert(std::accumulate(dA.begin(), dA.end(), 0) == 0);

		//	use structure, ShellsT, dA, nAnnihilators to create class CTensorGroup_ada
		tensorGroups.push_back(new CTensorGroup_ada(structure, ShellsT, dA, nAnnihilators, firstLabelId));
		pCTensorGroup = tensorGroups.back();

		nTensors = it->second.size();
		pCTensorGroup->m_Tensors.reserve(nTensors);
		for (size_t itensor = 0; itensor < nTensors; ++itensor)
		{
			//	either vOmega has one element ---> &tensorLabels[0]	[if na != nb] && vSingleShell_structures = {na irrep, nb irrep}
			//	or vSingleShell_structures has one element ----> &tensorLabels[0]	[if na == nb] && vOmega={empty}
			tensorLabels[0] = it->second[itensor];
			std::vector<CrmeTable*> SingleShellTensorRmeTables(ShellsT.size(), NULL); // number of single-shell tensors = number of active shells
			//	Generate a vector of pointers to single-shell tensors rme tables
			for (int j = 0; j < ShellsT.size(); ++j)
			{
//	use global class rmeLookUpTableContainer to pointer to rme for a given single shell tensor
				SingleShellTensorRmeTables[j] = rmeLookUpTableContainer.GetRMETablePointer(ShellsT[j], nAnnihilators[j], dA[j], vSingleShell_structures[j], vSingleShellTensorLabels[j], generate_missing_rme_files, log_is_on, log_file);
				assert(SingleShellTensorRmeTables[j]); //	since GetRMETablePointer should never return NULL !
			}
			pCTensorGroup->AddTensor(SingleShellTensorRmeTables, vOmega);
		}
		firstLabelId += nTensors;
	}
//	sort CTensorGroup_ada according to structure_ arrays ==> I am able to use binary search to find a particular structure	
	std::sort(tensorGroups.begin(), tensorGroups.end(), CTensorGroup_adaLess());

	SingleDistribution distr_bra, distr_ket; 
	UN::SU3xSU2_VEC gamma_bra, gamma_ket; 
	SU3xSU2_VEC omega_bra, omega_ket;

	std::vector<unsigned char> hoShells; 
	unsigned char num_vacuums_ket_distr;
	std::vector<int> phase_pn; 
	std::vector<CTensorGroup_ada*> selectedTensorGroups;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_pn;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_pn;

	for (int i = 0; i < ij_pairs.size(); ++i)
	{
		uint32_t bra_index = ij_pairs[i].first;
		uint32_t ket_index = ij_pairs[i].second;

		hoShells.resize(0);
		phase_pn.resize(0);
		selectedTensorGroups.resize(0);
		selected_tensors_pn.resize(0);
		rme_index_pn.resize(0);

		if (nucleon_type == nucleon::PROTON)
		{
			distr_bra.resize(0); bra_basis.getDistr_p(bra_index, distr_bra);
			gamma_bra.resize(0); bra_basis.getGamma_p(bra_index, gamma_bra);
			omega_bra.resize(0); bra_basis.getOmega_p(bra_index, omega_bra);

			distr_ket.resize(0); ket_basis.getDistr_p(ket_index, distr_ket); 
			gamma_ket.resize(0); ket_basis.getGamma_p(ket_index, gamma_ket);
			omega_ket.resize(0); ket_basis.getOmega_p(ket_index, omega_ket);
		}
		else if (nucleon_type == nucleon::NEUTRON)
		{
			distr_bra.resize(0); bra_basis.getDistr_n(bra_index, distr_bra);
			gamma_bra.resize(0); bra_basis.getGamma_n(bra_index, gamma_bra);
			omega_bra.resize(0); bra_basis.getOmega_n(bra_index, omega_bra);

			distr_ket.resize(0); ket_basis.getDistr_n(ket_index, distr_ket); 
			gamma_ket.resize(0); ket_basis.getGamma_n(ket_index, gamma_ket);
			omega_ket.resize(0); ket_basis.getOmega_n(ket_index, omega_ket);
		}

		TransformDistributions_SelectByDistribution(tensorGroups, distr_bra, gamma_bra, omega_bra, distr_ket, hoShells, num_vacuums_ket_distr, phase_pn, selectedTensorGroups);
		TransformGammaKet_SelectByGammas(hoShells, distr_ket, num_vacuums_ket_distr, phase_pn, selectedTensorGroups, gamma_bra, gamma_ket, selected_tensors_pn);
		TransformOmegaKet_CalculateRME( distr_ket, gamma_bra, omega_bra, gamma_ket, num_vacuums_ket_distr, selected_tensors_pn, omega_ket, rme_index_pn);
		
		if (!selected_tensors_pn.empty())
		{
			std::for_each(selected_tensors_pn.begin(), selected_tensors_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
		}
		if (!rme_index_pn.empty())
		{
			std::vector<float> rmes_values;
			std::vector<uint32_t> tensor_ids;

			for (size_t i = 0; i < rme_index_pn.size(); ++i)
			{
				tensor_ids.push_back(adta_table.getId(rme_index_pn[i].first->Tensor));

				uint32_t nrme = rme_index_pn[i].first->m_ntotal;
				for (int j = 0; j < nrme; ++j)
				{
					rmes_values.push_back(rme_index_pn[i].first->m_rme[j]);
				}

				delete []rme_index_pn[i].first->m_rme;	//	allocated in CRMECalculator::CalculateRME
				delete rme_index_pn[i].first;
			}
			rmes.add(bra_index, ket_index, tensor_ids, rmes_values);
		}
/*		
		else
		{
			cout << "(" << bra_index << " " << ket_index << ")" << endl;	
			if (nucleon_type == nucleon::PROTON)
			{
				bra_basis.Show_Proton_Conf(bra_index);
				cout << "\t";
				ket_basis.Show_Proton_Conf(ket_index);
			}
		}
*/		
	}

	for (size_t j = 0; j < tensorGroups.size(); ++j)
	{
		delete tensorGroups[j];
	}
	cout << "#rmes : " << rmes.rmes_size() << endl;
}

}

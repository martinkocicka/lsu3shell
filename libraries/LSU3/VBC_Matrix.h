#ifndef VBC_MATRIX_H
#define VBC_MATRIX_H

#include <fstream>
#include <string>
#include <vector>

namespace lsu3 
{

    struct VBC_Matrix 
    {
        std::vector<int> rowind;
        std::vector<int> colind;
        std::vector<int> rownnz;
        std::vector<int> colnnz;

        std::vector<float> vals;

        size_t irow;
        size_t icol;
        size_t nrows;
        size_t ncols;
        size_t nnz;

        VBC_Matrix() : nnz(0) { }
		size_t memory_usage() const
		{
			return (rowind.size() + colind.size() + rownnz.size() + colnnz.size())*sizeof(int) + vals.size()*sizeof(float);
		}

        void save_matrix_market(const std::string& filename)
        {
            std::ofstream ofs(filename.c_str());

            ofs << "%%MatrixMarket matrix coordinate real symmetric" << std::endl;

            uintmax_t nnz_ = 0;
            uintmax_t n_vals = vals.size();
            for (uintmax_t k = 0; k < n_vals; ++k)
                if (vals[k] != 0.0)
                    nnz_++;
            ofs << nrows << " " << ncols << " " << nnz_ << std::endl;

            uintmax_t l = 0;
            uintmax_t n_blocks = rowind.size();
            for (uintmax_t k = 0; k < n_blocks; ++k) {
                for (uintmax_t i = 0; i < rownnz[k]; ++i) {
                    for (uintmax_t j = 0; j < colnnz[k]; ++j) {
                        if (vals[l] != 0.0) {
                            uintmax_t row = rowind[k] + i;
                            uintmax_t col = colind[k] + j;
                            ofs << (row+1) << " " << (col+1) << " " << vals[l] << std::endl;
                        }
                        
                        l++;
                    }
                }
            }
        }
    };
}

#endif

$(eval $(begin-module))

################################################################
# unit definitions
################################################################

module_units_h := CInteractionPN_MPI std types BroadcastDataContainer VBC_Matrix \

module_units_cpp-h := ncsmSU3xSU2Basis CInteractionPN_MPI Cadtarmes Cadta_table \


# module_units_f := 
# module_programs_cpp :=


################################################################
# library creation flag
################################################################

$(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################


$(eval $(end-module))





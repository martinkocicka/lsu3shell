#include <LSU3/ncsmSU3xSU2Basis.h>

namespace lsu3
{

/******************************************************************
 			*** Functions facilitating debugging ****
 ******************************************************************/

void Set(CTuple<int, 4>& lmS, const UN::SU3xSU2& gamma)
{
	lmS[0] = gamma.lm; lmS[1] = gamma.mu; lmS[2] = gamma.S2; lmS[3] = gamma.mult;
}
void Set(CTuple<int, 4>& lmS, const SU3xSU2::LABELS& omega)
{
	lmS[0] = omega.lm; lmS[1] = omega.mu; lmS[2] = omega.S2; lmS[3] = omega.rho;
}

template<class T>
void Output(const T& gamma, const SU3xSU2_VEC& omega, std::vector<CTuple<int, 4> >& Labels)
{
	CTuple<int, 4> lmS;

	Set(lmS, gamma[0]); Labels.push_back(lmS); 

	if (omega.empty())
	{
		return;
	}

	Set(lmS, gamma[1]); Labels.push_back(lmS);

	for (size_t iomega = 0; iomega < omega.size() - 1; ++iomega)
	{
		Set(lmS, omega[iomega]); Labels.push_back(lmS);
		Set(lmS, gamma[iomega+2]); Labels.push_back(lmS);
	}
	Set(lmS, (omega.back()));
	Labels.push_back(lmS); 
}

void Print(const CTuple<int, 4>& lmS)
{
	cout << lmS[3] << "(" << lmS[0] << " " << lmS[1] << ")" << lmS[2];
}


void PrintState(const std::vector<CTuple<int, 4> >& Labels)
{
	cout << "[";
	size_t index;
	size_t nOmegas = (Labels.size() - 1)/2;
	for (size_t i = 0; i < nOmegas; ++i)
	{
		cout << "{";
	}
	Print(Labels[0]);
	if (nOmegas > 0)
	{
		cout << " x ";
		Print(Labels[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas - 1; ++i)
		{
			Print(Labels[index]); cout << " x ";
			Print(Labels[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels[index]);
	}

	cout << "]"; 
}

void Print( const SingleDistribution& distr)
{
	cout << "[";
	for (size_t i = 0; i < distr.size() - 1; ++i)
	{
		cout << (int)distr[i] << " ";
	}
	cout << (int)distr.back() << "]";
}

void Print(UN::SU3xSU2_VEC& gamma, SU3xSU2_VEC& omega)
{
	std::vector<CTuple<int, 4> > Labels;

	Output(gamma, omega, Labels);

	PrintState(Labels);
}

void CncsmSU3xSU2Basis::ShowProtonConf(uint32_t proton_irrep_index) const
{
	SingleDistribution distr_p; 
	UN::SU3xSU2_VEC gamma_p; 
	SU3xSU2_VEC omega_p;

	getDistr_p(proton_irrep_index, distr_p);
	getGamma_p(proton_irrep_index, gamma_p);
	getOmega_p(proton_irrep_index, omega_p);

	Print(distr_p);
	Print(gamma_p, omega_p);
}

void CncsmSU3xSU2Basis::ShowNeutronConf(uint32_t neutron_irrep_index) const
{
	SingleDistribution distr_n;
	UN::SU3xSU2_VEC gamma_n;
	SU3xSU2_VEC omega_n;

	getDistr_n(neutron_irrep_index, distr_n); 
	getGamma_n(neutron_irrep_index, gamma_n);
	getOmega_n(neutron_irrep_index, omega_n);

	Print(distr_n);
	Print(gamma_n, omega_n);
}

/********************************************************************************
 			*** Constructors and their helper functions   ****
 ********************************************************************************/
struct Compare_UNSU3xSU2_VEC
{
	struct Compare_UNSU3xSU2
	{
		inline bool operator() (const UN::SU3xSU2& a, const UN::SU3xSU2& b) const 
		{ 
			return 	(a.lm < b.lm || (((a.lm == b.lm) && a.mu < b.mu) ||  \
					((a.lm == b.lm && a.mu == b.mu && a.S2 < b.S2) || \
					(a.lm == b.lm && a.mu == b.mu && a.S2 == b.S2 && a.mult < b.mult))));
		}
	};
	inline bool operator() (const UN::SU3xSU2_VEC& a, const UN::SU3xSU2_VEC& b) const 
	{ 
		return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(), Compare_UNSU3xSU2()); 
	}
};

struct Compare_SU3_VEC
{
//	SU3::LABELS::operator< does not compare rho maximal multiplicity!
//	and hence I have to define my own operator< which takes into account
//	also multiplicities: Compare_SU3_With_Rho
	struct Compare_SU3_With_Rho
	{
		inline bool operator() (const SU3::LABELS& lhs, const SU3::LABELS& rhs) const 
		{
			return (lhs.lm < rhs.lm || (((lhs.lm == rhs.lm) && lhs.mu < rhs.mu) || ((lhs.lm == rhs.lm && lhs.mu == rhs.mu && lhs.rho < rhs.rho)))); 
		}
	};
	inline bool operator() (const SU3_VEC& a, const SU3_VEC& b) const 
	{ 
		return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(), Compare_SU3_With_Rho()); 
	}
};

void CncsmSU3xSU2Basis::GenerateDistrGammaOmega_SortedVectors(	const CncsmSU3Basis& basis,
																std::vector<SingleDistribution>& distribs_p,
																std::vector<SingleDistribution>& distribs_n,
																std::vector<UN::SU3xSU2_VEC>& gammas,
																std::vector<SU3_VEC>& omegas_su3,
																std::vector<SU2_VEC>& omegas_spin)

{
	std::set<SingleDistribution> distribs_p_set, distribs_n_set;
	std::set<UN::SU3xSU2_VEC, Compare_UNSU3xSU2_VEC> gammas_set;
	std::set<SU3_VEC, Compare_SU3_VEC> omegas_su3_set;
	std::set<SU2_VEC> omegas_spin_set;

	SingleDistribution distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_VEC omega_p, omega_n;

	for (IdentFermConfIterator protonConfIter = basis.firstProtonConf(); protonConfIter.hasDistr(); protonConfIter.nextDistr())
	{
		distr_p.resize(0); 
		protonConfIter.getDistr(distr_p);
		distribs_p_set.insert(distr_p);

		for (protonConfIter.rewindGamma(); protonConfIter.hasGamma(); protonConfIter.nextGamma())
		{
			gamma_p.resize(0); 
			protonConfIter.getGamma(gamma_p);
			gammas_set.insert(gamma_p);

			for (protonConfIter.rewindOmega(); protonConfIter.hasOmega(); protonConfIter.nextOmega())
			{
				omega_p.resize(0); 
				protonConfIter.getOmega(omega_p);
				omegas_su3_set.insert(SU3_VEC(omega_p.begin(), omega_p.end())); 
				omegas_spin_set.insert(SU2_VEC(omega_p.begin(), omega_p.end()));
			}
		}
	}

	for (IdentFermConfIterator neutronConfIter = basis.firstNeutronConf(); neutronConfIter.hasDistr(); neutronConfIter.nextDistr())
	{
		distr_n.resize(0); 
		neutronConfIter.getDistr(distr_n);
		distribs_n_set.insert(distr_n);

		for (neutronConfIter.rewindGamma(); neutronConfIter.hasGamma(); neutronConfIter.nextGamma())
		{
			gamma_n.resize(0); 
			neutronConfIter.getGamma(gamma_n);
			gammas_set.insert(gamma_n);

			for (neutronConfIter.rewindOmega(); neutronConfIter.hasOmega(); neutronConfIter.nextOmega())
			{
				omega_n.resize(0); 
				neutronConfIter.getOmega(omega_n);
				omegas_su3_set.insert(SU3_VEC(omega_n.begin(), omega_n.end())); 
				omegas_spin_set.insert(SU2_VEC(omega_n.begin(), omega_n.end()));
			}
		}
	}

	distribs_p.reserve(distribs_p_set.size());
	distribs_n.reserve(distribs_n_set.size());

	distribs_p  .insert(distribs_p.end() , distribs_p_set.begin() , distribs_p_set.end());
	distribs_n  .insert(distribs_n.end() , distribs_n_set.begin() , distribs_n_set.end());
	gammas		.insert(gammas.end()     , gammas_set.begin()     , gammas_set.end());
	omegas_su3  .insert(omegas_su3.end() , omegas_su3_set.begin() , omegas_su3_set.end());
	omegas_spin .insert(omegas_spin.end(), omegas_spin_set.begin(), omegas_spin_set.end());
}

void CncsmSU3xSU2Basis::Reshuffle(const proton_neutron::ModelSpace& ncsmModelSpace, const uint16_t idiag, const uint16_t ndiag)
{
	uint32_t number_of_states(0);

	idiag_ = idiag; 
	ndiag_ = ndiag;

	dims_.clear();
	dims_.insert(dims_.begin(), ndiag, 0);

	max_states_in_block_ = 0;
	pnbasis_ipin_.resize(0);
	first_state_in_block_.resize(0);
	block_end_.resize(0);
	wpn_.resize(0);
//	iterate over all possible (ip in) pairs and generate 
//	wpn_
//	wpn_irreps_container_
//	pnbasis_
	SU3xSU2_VEC wpn_su3xsu2;
	wpn_su3xsu2.reserve(1024);

	SU3_VEC wpn_su3;
	wpn_su3.reserve(1024);

	SU2_VEC allowed_spins; 
	allowed_spins.reserve(50);

	std::vector<uint32_t> wpn_container_indices(wpn_su3xsu2.size());
	wpn_container_indices.reserve(1024);

	proton_neutron::ModelSpace::const_iterator nhwSubspace;
	int ipin_pair(0);

	for (int index_p = 0; index_p < pconfs_.size(); ++index_p)
	{
		int Nphw = nhw_p(index_p);
		int ap_max = getMult_p(index_p);
		SU3xSU2::LABELS wp(getProtonSU3xSU2(index_p)); 
		for (int index_n = 0; index_n < nconfs_.size(); ++index_n)
		{
			int Nhw = Nphw + nhw_n(index_n);
			if (Nhw > Nmax())
			{
				break; // fermionic irreps are stored in an increasing order according to the number of oscillator quanta
			}
			nhwSubspace = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);
			if (nhwSubspace == ncsmModelSpace.end())	// => given Nhw subspace does not belong to ncsmModelSpace model space
			{
				continue;
			}
			int an_max = getMult_n(index_n);
			SU3xSU2::LABELS wn(getNeutronSU3xSU2(index_n));

			int idiag_current = (ipin_pair % ndiag_);	// current (ip, in) pair belongs to idiag_current section of basis
			allowed_spins.resize(0);
			std::vector<SU3_VEC> allowed_su3;
//	for a given {Sp, Sn}, find an array of allowed total spins {S, S', ... } and
//	return vector of allowed SU3 labels for each allowed spin: 
//	{{(lm1 mu1), (lm2 mu2), ... }, {(lm1' mu1'), (lm2' mu2'), .... }, ...}
			nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(wp.S2, wn.S2), allowed_spins, allowed_su3);
			uint32_t nwpn_spin = allowed_spins.size();
			if (nwpn_spin) // ==> (Sp Sn) are allowed
			{
				wpn_su3.resize(0);
				wpn_su3xsu2.resize(0);
				SU3::Couple(wp, wn, wpn_su3);
				SU3_VEC::const_iterator last_wpn_su3 = wpn_su3.end();
//	iterate over allowed spins and create vector of allowed SU3xSU2 irreps
				for (uint32_t ispin = 0; ispin < nwpn_spin; ++ispin)
				{
					if (!allowed_su3[ispin].empty())	//	==> I need to select only allowed (lm mu) 
					{
						for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin(); current_su3 < last_wpn_su3; ++current_su3)
						{
							SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
							if (current_allowed_su3 != allowed_su3[ispin].end() && (current_allowed_su3->lm == current_su3->lm && current_allowed_su3->mu == current_su3->mu))
							{
								wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
							}
						}
					}
					else	//	if array of allowed SU(3) irreps is empty ==> include ALL SU(3) irreps listed in wpn_su3
					{
						for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin(); current_su3 < last_wpn_su3; ++current_su3)
						{
							wpn_su3xsu2.push_back(SU3xSU2::LABELS(*current_su3, allowed_spins[ispin]));
						}
					}
				}

				wpn_container_indices.resize(0);	// store indices pointing to wpn_irreps_container_ so we don't have to perform search
				for (int i = 0; i < wpn_su3xsu2.size(); ++i)
				{
					uint32_t index = wpn_irreps_container_.getIndexInTable(wpn_su3xsu2[i]);
					dims_[idiag_current] += ap_max*an_max*wpn_su3xsu2[i].rho*wpn_irreps_container_[index].dim();
					wpn_container_indices.push_back(index);
				}

				if (idiag_ == idiag_current) // the wpn irreps stored in wpn_su3xsu2 will be stored
				{
					first_state_in_block_.push_back(number_of_states);

					for (int i = 0; i < wpn_su3xsu2.size(); ++i)
					{
						uint16_t dim = wpn_irreps_container_[wpn_container_indices[i]].dim();
						if (dim > 0)
						{
							wpn_.push_back(wpn_container_indices[i]);
							number_of_states += ap_max*an_max*wpn_su3xsu2[i].rho*dim;
						}
					}
					uint16_t nstates_in_block = number_of_states - first_state_in_block_.back();
					if (nstates_in_block > max_states_in_block_)
					{
						max_states_in_block_ = nstates_in_block;
					}
					pnbasis_ipin_.push_back(index_p); 
					pnbasis_ipin_.push_back(index_n); 
					block_end_.push_back(wpn_.size());
				}
				ipin_pair++;
			}
		}
	}
}

CncsmSU3xSU2Basis::CncsmSU3xSU2Basis(const proton_neutron::ModelSpace& ncsmModelSpace, const uint16_t idiag, const uint16_t ndiag): 
									idiag_(idiag), ndiag_(ndiag),
									dims_(ndiag, 0), 
									min_ho_quanta_p_(255),
									min_ho_quanta_n_(255), JJ_(ncsmModelSpace.JJ()), nprotons_(ncsmModelSpace.Z()), nneutrons_(ncsmModelSpace.N())
{
	CncsmSU3Basis basis(ncsmModelSpace);
	nmax_ = ncsmModelSpace.back().N();

//	sorted vector of gammas; common for protons & neutrons
	std::vector<UN::SU3xSU2_VEC> gammas;
//	sorted vector of inter-shell su3 coupling; common for protons & neutrons
	std::vector<SU3_VEC> omegas_su3;
//	sorted vector of inter-shell spin coupling; common for protons & neutrons
	std::vector<SU2_VEC> omegas_spin;

	GenerateDistrGammaOmega_SortedVectors(basis, distribs_p_, distribs_n_, gammas, omegas_su3, omegas_spin);

	std::vector<uint16_t> gamma_positions(gammas.size(), 0), omega_su3_positions(omegas_su3.size(), 0), omega_spin_positions(omegas_spin.size(), 0);
	
//	Suppose that vector of gamma vectors, vector of omega_su3 vectors, and
//	vector of omega_spin vectors are stored in continuous arrays.

//	Here we calculate position of a first element of each gamma vector
	for (int i = 1; i < gammas.size(); ++i)
	{
		gamma_positions[i] = gamma_positions[i-1] + gammas[i-1].size();
	}
//	Here we calculate position of a first element of each omegas_su3 vector
	for (int i = 1; i < omegas_su3.size(); ++i)
	{
		omega_su3_positions[i] = omega_su3_positions[i-1] + omegas_su3[i-1].size();
	}
//	Here we calculate position of a first element of each omegas_spin vector
	for (int i = 1; i < omegas_spin.size(); ++i)
	{
		omega_spin_positions[i] = omega_spin_positions[i-1] + omegas_spin[i-1].size();
	}
	
	occupied_shells_distr_p_.resize(distribs_p_.size());
	// loop over proton distributions and count the number of occupied shells for each distribution
	// and also calculate minimal number of HO quanta for proton distributions
	for (int i = 0; i < distribs_p_.size(); ++i)
	{
		uint8_t num_quanta(0);
		for (int j = 0; j < distribs_p_[i].size(); ++j)
		{
			num_quanta += distribs_p_[i][j]*j;
		}	
		if (num_quanta < min_ho_quanta_p_)
		{
			std::swap(num_quanta, min_ho_quanta_p_);
		}
		occupied_shells_distr_p_[i] = std::count_if(distribs_p_[i].begin(), distribs_p_[i].end(), std::bind2nd(std::greater<uint8_t>(), (uint8_t)0));
	}

	occupied_shells_distr_n_.resize(distribs_n_.size());
	// loop over proton distributions and count the number of occupied shells for each distribution
	// and also calculate minimal number of HO quanta for neutron distributions
	for (int i = 0; i < distribs_n_.size(); ++i)
	{
		uint8_t num_quanta(0);
		for (int j = 0; j < distribs_n_[i].size(); ++j)
		{
			num_quanta += distribs_n_[i][j]*j;
		}	
		if (num_quanta < min_ho_quanta_n_)
		{
			std::swap(num_quanta, min_ho_quanta_n_);
		}
		occupied_shells_distr_n_[i] = std::count_if(distribs_n_[i].begin(), distribs_n_[i].end(), std::bind2nd(std::greater<uint8_t>(), (uint8_t)0));
	}

//	variables needed for construction of pconfs_ and nconfs_ vectors
	SingleDistribution distr;
	UN::SU3xSU2_VEC gamma;
	SU3xSU2_VEC omega;
	uint32_t iw_su3, iw_spin, igamma;
	NUCLEON_BASIS_INDICES  new_index_p, new_index_n;
//	Constructs elements of pconfs_ vector
//	Elements are pointers to 
//	[0] = dp 
//	[1] = gamma 
//	[2] = omega_su3 
//	[3] = omega_spin
	for (IdentFermConfIterator protonConfIter = basis.firstProtonConf(); protonConfIter.hasDistr(); protonConfIter.nextDistr())
	{
		distr.resize(0); 
		protonConfIter.getDistr(distr);
		cpp0x::get<kDistr>(new_index_p) = std::distance(distribs_p_.begin(), std::lower_bound(distribs_p_.begin(), distribs_p_.end(), distr));

		for (protonConfIter.rewindGamma(); protonConfIter.hasGamma(); protonConfIter.nextGamma())
		{
			gamma.resize(0); 
			protonConfIter.getGamma(gamma);
			igamma = std::distance(gammas.begin(), std::lower_bound(gammas.begin(), gammas.end(), gamma, Compare_UNSU3xSU2_VEC()));

			cpp0x::get<kGamma>(new_index_p) = gamma_positions[igamma];

			for (protonConfIter.rewindOmega(); protonConfIter.hasOmega(); protonConfIter.nextOmega())
			{
				omega.resize(0); 
				protonConfIter.getOmega(omega);

				iw_su3 = std::distance(omegas_su3.begin(), std::lower_bound(omegas_su3.begin(), omegas_su3.end(), SU3_VEC(omega.begin(), omega.end()), Compare_SU3_VEC()));
				cpp0x::get<kOmegaSu3>(new_index_p) = omega_su3_positions[iw_su3];

				iw_spin = std::distance(omegas_spin.begin(), std::lower_bound(omegas_spin.begin(), omegas_spin.end(), SU2_VEC(omega.begin(), omega.end())));
				cpp0x::get<kOmegaSpin>(new_index_p) = omega_spin_positions[iw_spin];

				pconfs_.push_back(new_index_p);
			}
		}
	}

//	This loop fills nconfs_ vector with pointers to 
//	[0] = dp 
//	[1] = gamma 
//	[2] = omega_su3 
//	[3] = omega_spin
	for (IdentFermConfIterator neutronConfIter = basis.firstNeutronConf(); neutronConfIter.hasDistr(); neutronConfIter.nextDistr())
	{
		distr.resize(0); 
		neutronConfIter.getDistr(distr);
		cpp0x::get<kDistr>(new_index_n) = std::distance(distribs_n_.begin(), std::lower_bound(distribs_n_.begin(), distribs_n_.end(), distr));

		for (neutronConfIter.rewindGamma(); neutronConfIter.hasGamma(); neutronConfIter.nextGamma())
		{
			gamma.resize(0); 
			neutronConfIter.getGamma(gamma);
			igamma = std::distance(gammas.begin(), std::lower_bound(gammas.begin(), gammas.end(), gamma, Compare_UNSU3xSU2_VEC()));

			cpp0x::get<kGamma>(new_index_n) = gamma_positions[igamma];

			for (neutronConfIter.rewindOmega(); neutronConfIter.hasOmega(); neutronConfIter.nextOmega())
			{
				omega.resize(0); 
				neutronConfIter.getOmega(omega);

				iw_su3 = std::distance(omegas_su3.begin(), std::lower_bound(omegas_su3.begin(), omegas_su3.end(), SU3_VEC(omega.begin(), omega.end()), Compare_SU3_VEC()));
				cpp0x::get<kOmegaSu3>(new_index_n) = omega_su3_positions[iw_su3];

				iw_spin = std::distance(omegas_spin.begin(), std::lower_bound(omegas_spin.begin(), omegas_spin.end(), SU2_VEC(omega.begin(), omega.end())));
				cpp0x::get<kOmegaSpin>(new_index_n) = omega_spin_positions[iw_spin];

				nconfs_.push_back(new_index_n);
			}
		}
	}

	gammas_.reserve(gamma_positions.back() + gammas.back().size());	
	for (int i = 0; i < gammas.size(); ++i)
	{
		gammas_.insert(gammas_.end(), gammas[i].begin(), gammas[i].end());
	}

	omegas_su3_.reserve(omega_su3_positions.back() + omegas_su3.back().size());	
	for (int i = 0; i < omegas_su3.size(); ++i)
	{
		omegas_su3_.insert(omegas_su3_.end(), omegas_su3[i].begin(), omegas_su3[i].end());
	}

	omegas_spin_.reserve(omega_spin_positions.back() + omegas_spin.back().size());	
	for (int i = 0; i < omegas_spin.size(); ++i)
	{
		omegas_spin_.insert(omegas_spin_.end(), omegas_spin[i].begin(), omegas_spin[i].end());
	}

//	now when both pconfs_ and nconfs_ have been generated we could generate list of final SU3xSU2 irreps
//  NOTE: ***This is the most time consuming part of building a basis***
	GenerateFinal_SU3xSU2Irreps_Container(ncsmModelSpace);

	Reshuffle(ncsmModelSpace, idiag, ndiag);
}


CncsmSU3xSU2Basis::CncsmSU3xSU2Basis(const proton_neutron::ModelSpace& ncsmModelSpace, const CncsmSU3xSU2Basis& basis, const uint16_t idiag, const uint16_t ndiag)
:wpn_irreps_container_(basis.wpn_irreps_container_), JJ_(ncsmModelSpace.JJ()), nprotons_(ncsmModelSpace.Z()), nneutrons_(ncsmModelSpace.N())
{
	min_ho_quanta_p_ = basis.min_ho_quanta_p_; 
	min_ho_quanta_n_ = basis.min_ho_quanta_n_;
	nmax_ = basis.nmax_;

	// occupied_shells_distr_p_[i] = the number of occupied shells for ith proton distribution
	occupied_shells_distr_p_ = basis.occupied_shells_distr_p_;
	occupied_shells_distr_n_ = basis.occupied_shells_distr_n_;

	//	arrays storing quantum labels required for a complete definition of
	//	proton/neutron configurations in a given model space	
	distribs_p_ = basis.distribs_p_;
	distribs_n_ = basis.distribs_n_;
	gammas_ = basis.gammas_;
	omegas_su3_ = basis.omegas_su3_; 
	omegas_spin_ = basis.omegas_spin_;
	pconfs_ = basis.pconfs_;
	nconfs_ = basis.nconfs_;

	Reshuffle(ncsmModelSpace, idiag, ndiag); // takes care of idiag_ ndiag_ and dims_
}

SU3xSU2::LABELS CncsmSU3xSU2Basis::getOmega_pn(const uint32_t proton_irrep_index, const uint32_t neutron_irrep_index, const uint32_t pn_irrep_index) const
{
	SU3xSU2::LABELS wpn(wpn_irreps_container_[wpn_[pn_irrep_index]]);
	wpn.rho = SU3::mult(getProtonSU3xSU2(proton_irrep_index), getNeutronSU3xSU2(neutron_irrep_index), wpn);
	return wpn;
}

//	Creates a table of allowed SU3xSU2 irreps with their basis for all possible basis states
void CncsmSU3xSU2Basis::GenerateFinal_SU3xSU2Irreps_Container(const proton_neutron::ModelSpace& ncsmModelSpace)
{
	proton_neutron::ModelSpace::const_iterator nhwSubspace;

	SU3_VEC wpn_su3;
	wpn_su3.reserve(1024);

	SU2_VEC allowed_spins; 
	allowed_spins.reserve(50);
	
	std::set<SU3xSU2::LABELS> finalSU3xSU2Irreps;
	for (int index_p = 0; index_p < pconfs_.size(); ++index_p)
	{
		int Nphw = nhw_p(index_p);
		SU3xSU2::LABELS wp(getProtonSU3xSU2(index_p)); 
		for (int index_n = 0; index_n < nconfs_.size(); ++index_n)
		{
			int Nhw = Nphw + nhw_n(index_n);
			if (Nhw > Nmax())
			{
				break; // fermionic irreps are stored in an increasing order according to the number of oscillator quanta
			}
			nhwSubspace = std::find(ncsmModelSpace.begin(), ncsmModelSpace.end(), Nhw);
			if (nhwSubspace == ncsmModelSpace.end())
			{
				continue;
			}
			SU3xSU2::LABELS wn(getNeutronSU3xSU2(index_n));

			allowed_spins.resize(0);
			std::vector<SU3_VEC> allowed_su3;
//	for a given {Sp, Sn}, find an array of allowed total spins {S, S', ... } and
//	return vector of allowed SU3 labels for each allowed spin: 
//	{{(lm1 mu1), (lm2 mu2), ... }, {(lm1' mu1'), (lm2' mu2'), .... }, ...}
			nhwSubspace->GetAllowedSpinsAndSU3(std::make_pair(wp.S2, wn.S2), allowed_spins, allowed_su3);
			uint32_t nwpn_spin = allowed_spins.size();
			if (nwpn_spin)
			{
				wpn_su3.resize(0);
				SU3::Couple(wp, wn, wpn_su3);
				SU3_VEC::const_iterator last_wpn_su3 = wpn_su3.end();
//	iterate over allowed spins ...
				for (uint32_t ispin = 0; ispin < nwpn_spin; ++ispin)
				{
					if (!allowed_su3[ispin].empty())	//	==> I need to select only allowed (lm mu) 
					{
						for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin(); current_su3 < last_wpn_su3; ++current_su3)
						{
							SU3_VEC::const_iterator current_allowed_su3 = std::lower_bound(allowed_su3[ispin].begin(), allowed_su3[ispin].end(), *current_su3);
							if (current_allowed_su3 != allowed_su3[ispin].end() && (current_allowed_su3->lm == current_su3->lm && current_allowed_su3->mu == current_su3->mu))
							{
								finalSU3xSU2Irreps.insert(SU3xSU2::LABELS(1, current_su3->lm, current_su3->mu, allowed_spins[ispin]));
							}
						}
					}
					else	//	if array of allowed SU(3) irreps is empty ==> include ALL SU(3) irreps listed in wpn_su3
					{
						for (SU3_VEC::const_iterator current_su3 = wpn_su3.begin(); current_su3 < last_wpn_su3; ++current_su3)
						{
							finalSU3xSU2Irreps.insert(SU3xSU2::LABELS(1, current_su3->lm, current_su3->mu, allowed_spins[ispin]));
						}
					}
				}
			}
		}
	}
	SU3xSU2_VEC irrep_list(finalSU3xSU2Irreps.size());
	std::copy(finalSU3xSU2Irreps.begin(), finalSU3xSU2Irreps.end(), irrep_list.begin());
	wpn_irreps_container_.CreateContainer(irrep_list, ncsmModelSpace.JJ());
}


void CncsmSU3xSU2Basis::ShowMemoryRequirements() const
{
	cout << "#distribs_p: " << distribs_p_.size() << endl;
	cout << "#distribs_n: " << distribs_n_.size() << endl;
	
	cout << "#(U(N)>SU(3)xSU(2)) irreps stored in gamma_: ";
	cout << gammas_.size() << "  memory: " << sizeof(UN::SU3xSU2_VEC::value_type)*gammas_.size()/(1024.0*1024.0) << " mb" << endl;	

	cout << "# rho_max(l m) irreps in omegas_su3_: " << omegas_su3_.size() << "  memory: " << sizeof(SU3_VEC::value_type)*omegas_su3_.size()/(1024.0*1024.0) << " mb" << endl;	

	cout << "# 2S spins in omegas_spin_: " << omegas_spin_.size() << "  memory: " << sizeof(SU2_VEC::value_type)*omegas_spin_.size()/(1024.0*1024.0) << " mb" << endl;	

	cout << "#elements in pconfs: " << pconfs_.size() << "  memory: " \
			<< sizeof(std::vector<NUCLEON_BASIS_INDICES>::value_type)*pconfs_.size()/(1024.0*1024.0) << " mb" << endl;	
	cout << "#elements in nconfs: " << nconfs_.size() << "  memory: " \
			<< sizeof(std::vector<NUCLEON_BASIS_INDICES>::value_type)*nconfs_.size()/(1024.0*1024.0) << " mb" << endl;	

	cout << "#element in pnbasis_ipin_: " << pnbasis_ipin_.size();
	cout << " memory: " << sizeof(std::vector<uint32_t>::value_type)*pnbasis_ipin_.size()/(1024.0*1024) << " mb" << endl;
	cout << "#elements in wpn: " << wpn_.size() << " memory: " << sizeof(std::vector<uint16_t>::value_type)*wpn_.size()/(1024.0*1024) << " mb" << endl;
}

/**************************************************************
				*** PROTON INTERFACE ***
 **************************************************************/
uint8_t CncsmSU3xSU2Basis::HOquanta_p(const uint32_t proton_irrep_index) const
{
	uint8_t num_quanta(0);
	uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);

	for (int i = 0; i < distribs_p_[idistr].size(); ++i)
	{
		num_quanta += distribs_p_[idistr][i]*i;
	}
	return num_quanta;
}


void CncsmSU3xSU2Basis::getOmega_p(uint32_t proton_irrep_index, SU3xSU2_VEC& omega_p) const
{
	assert(omega_p.empty());

	const SU3::LABELS* omega_su3 = &omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index])];
	const SU2::LABEL* omega_spin = &omegas_spin_[cpp0x::get<kOmegaSpin>(pconfs_[proton_irrep_index])];
	uint32_t number_omegas = occupied_shells_distr_p_[cpp0x::get<kDistr>(pconfs_[proton_irrep_index])] - 1;

	omega_p.resize(number_omegas);
	for (int i = 0; i < number_omegas; ++i)
	{
		omega_p[i].rho = omega_su3[i].rho;
		omega_p[i].lm  = omega_su3[i].lm;
		omega_p[i].mu  = omega_su3[i].mu;
		omega_p[i].S2  = omega_spin[i]; 
	}
}

void CncsmSU3xSU2Basis::getGamma_p(uint32_t proton_irrep_index, UN::SU3xSU2_VEC& gamma) const 
{
	assert(gamma.empty()); 
	uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);
	uint32_t igamma = cpp0x::get<kGamma>(pconfs_[proton_irrep_index]);
	//	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
	gamma.insert(gamma.end(), &gammas_[igamma], &gammas_[igamma] + occupied_shells_distr_p_[idistr]);
	assert(gamma.size() == occupied_shells_distr_p_[idistr]);
	assert(gamma.size());
}

void CncsmSU3xSU2Basis::getDistr_p(uint32_t proton_irrep_index, SingleDistribution& distr) const 
{
	assert(distr.empty()); 
	uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);
	distr.insert(distr.begin(), distribs_p_[idistr].begin(), distribs_p_[idistr].end());
}

SU3xSU2::LABELS CncsmSU3xSU2Basis::getProtonSU3xSU2(uint32_t proton_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return SU3xSU2::LABELS(omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]) + ilast_irrep], omegas_spin_[cpp0x::get<kOmegaSpin>(pconfs_[proton_irrep_index]) + ilast_irrep]);
	}
	else
	{
		assert(number_shells == 1);
		const UN::SU3xSU2* gamma = &gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])];
		return SU3xSU2::LABELS(1, gamma->lm, gamma->mu, gamma->S2);
	}
}

uint16_t CncsmSU3xSU2Basis::getMult_p(const uint32_t proton_irrep_index) const
{
	uint16_t max_mult = 1;
	uint32_t idistr = cpp0x::get<kDistr>(pconfs_[proton_irrep_index]);
	uint32_t igamma = cpp0x::get<kGamma>(pconfs_[proton_irrep_index]);
//	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
	for (int i = 0; i < occupied_shells_distr_p_[idistr]; ++i)
	{
		max_mult *= gammas_[igamma + i].mult;
	}

	uint32_t iomega_su3 = cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]);
	for (int i = 0; i < occupied_shells_distr_p_[idistr] - 1; ++i)
	{
		max_mult *= omegas_su3_[iomega_su3 + i].rho;
	}
	return max_mult;
}

int32_t CncsmSU3xSU2Basis::getLambda_p(const uint32_t proton_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]) + ilast_irrep].lm;
	}
	else
	{
		assert(number_shells == 1);
		return gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])].lm;
	}
}

int32_t CncsmSU3xSU2Basis::getMu_p(const uint32_t proton_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return omegas_su3_[cpp0x::get<kOmegaSu3>(pconfs_[proton_irrep_index]) + ilast_irrep].mu;
	}
	else
	{
		assert(number_shells == 1);
		return gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])].mu;
	}
}

int32_t CncsmSU3xSU2Basis::getSS_p(const uint32_t proton_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_p(proton_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return omegas_spin_[cpp0x::get<kOmegaSpin>(pconfs_[proton_irrep_index]) + ilast_irrep];
	}
	else
	{
		assert(number_shells == 1);
		return gammas_[cpp0x::get<kGamma>(pconfs_[proton_irrep_index])].S2;
	}
}

void CncsmSU3xSU2Basis::ShowProtonIrreps()
{
	for (int i = 0; i < pconfs_.size(); ++i)
	{
		SingleDistribution distr;
		int nhw = nhw_p(i);

		getDistr_p(i, distr);
		cout << "ip: " << i << "\tnhw:" << nhw << "\t[";
		for (int j = 0; j < distr.size() - 1; ++j)
		{
			cout << (int)distr[j] << " ";
		}
		cout << (int)distr.back() << "]\t";
		UN::SU3xSU2_VEC gamma;
		getGamma_p(i, gamma);
		cout << "gamma:";
		for (int j = 0; j < gamma.size() - 1; ++j)
		{
			cout << gamma[j] << " ";
		}
		cout << gamma.back();
		cout << "\tomega:";

		SU3xSU2_VEC omega;
		getOmega_p(i, omega);
		if (omega.size())
		{
			for (int j = 0; j < omega.size() - 1; ++j)
			{
				cout << (int)omega[j].rho << "(" << (int)omega[j].lm << " " << (int)omega[j].mu << ")" << (int)omega[j].S2 << " ";
			}
			cout << (int)omega.back().rho << "(" << (int)omega.back().lm << " " << (int)omega.back().mu << ")" << (int)omega.back().S2 << " ";
		}
		else
		{
			cout << gamma.back();
		}
		cout << "\tmax_mult_p: " << getMult_p(i);
		cout << endl;
	}
}



/**************************************************************
				*** NEUTRON INTERFACE ***
 **************************************************************/
void CncsmSU3xSU2Basis::getOmega_n(uint32_t neutron_irrep_index, SU3xSU2_VEC& omega_n) const
{
	assert(omega_n.empty());

	const SU3::LABELS* omega_su3 = &omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index])];
	const SU2::LABEL* omega_spin = &omegas_spin_[cpp0x::get<kOmegaSpin>(nconfs_[neutron_irrep_index])];
	uint32_t number_omegas = occupied_shells_distr_n_[cpp0x::get<kDistr>(nconfs_[neutron_irrep_index])] - 1;

	omega_n.resize(number_omegas);
	for (int i = 0; i < number_omegas; ++i)
	{
		omega_n[i].rho = omega_su3[i].rho;
		omega_n[i].lm  = omega_su3[i].lm;
		omega_n[i].mu  = omega_su3[i].mu;
		omega_n[i].S2  = omega_spin[i]; 
	}
}

uint8_t CncsmSU3xSU2Basis::HOquanta_n(const uint32_t neutron_irrep_index) const
{
	uint8_t num_quanta(0);
	uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);

	for (int i = 0; i < distribs_n_[idistr].size(); ++i)
	{
		num_quanta += distribs_n_[idistr][i]*i;
	}
	return num_quanta;
}

void CncsmSU3xSU2Basis::getDistr_n(uint32_t neutron_irrep_index, SingleDistribution& distr) const 
{
	assert(distr.empty()); 
	uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);
	distr.insert(distr.begin(), distribs_n_[idistr].begin(), distribs_n_[idistr].end());
}

void CncsmSU3xSU2Basis::getGamma_n(uint32_t neutron_irrep_index, UN::SU3xSU2_VEC& gamma) const 
{
	assert(gamma.empty()); 
	uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);
	uint32_t igamma = cpp0x::get<kGamma>(nconfs_[neutron_irrep_index]);
	//	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
	gamma.insert(gamma.end(), &gammas_[igamma], &gammas_[igamma] + occupied_shells_distr_n_[idistr]);
	assert(gamma.size() == occupied_shells_distr_n_[idistr]);
	assert(gamma.size());
}

SU3xSU2::LABELS CncsmSU3xSU2Basis::getNeutronSU3xSU2(uint32_t neutron_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return SU3xSU2::LABELS(omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]) + ilast_irrep], omegas_spin_[cpp0x::get<kOmegaSpin>(nconfs_[neutron_irrep_index]) + ilast_irrep]);
	}
	else
	{
		assert(number_shells == 1);
		const UN::SU3xSU2* gamma = &gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])];
		return SU3xSU2::LABELS(1, gamma->lm, gamma->mu, gamma->S2);
	}
}

uint16_t CncsmSU3xSU2Basis::getMult_n(const uint32_t neutron_irrep_index) const
{
	uint16_t max_mult = 1;
	uint32_t idistr = cpp0x::get<kDistr>(nconfs_[neutron_irrep_index]);
	uint32_t igamma = cpp0x::get<kGamma>(nconfs_[neutron_irrep_index]);
//	each occupied shell defines one UN>SU3xSU2 irrep, i.e. element of gamma
	for (int i = 0; i < occupied_shells_distr_n_[idistr]; ++i)
	{
		max_mult *= gammas_[igamma + i].mult;
	}

	uint32_t iomega_su3 = cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]);
	for (int i = 0; i < occupied_shells_distr_n_[idistr] - 1; ++i)
	{
		max_mult *= omegas_su3_[iomega_su3 + i].rho;
	}
	return max_mult;
}

int32_t CncsmSU3xSU2Basis::getLambda_n(const uint32_t neutron_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]) + ilast_irrep].lm;
	}
	else
	{
		assert(number_shells == 1);
		return gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])].lm;
	}
}

int32_t CncsmSU3xSU2Basis::getMu_n(const uint32_t neutron_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return omegas_su3_[cpp0x::get<kOmegaSu3>(nconfs_[neutron_irrep_index]) + ilast_irrep].mu;
	}
	else
	{
		assert(number_shells == 1);
		return gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])].mu;
	}
}

int32_t CncsmSU3xSU2Basis::getSS_n(const uint32_t neutron_irrep_index) const
{
	uint8_t number_shells = getNumberOfOccupiedShells_n(neutron_irrep_index);
	if (number_shells > 1)
	{
		uint32_t ilast_irrep = number_shells - 2;
		return omegas_spin_[cpp0x::get<kOmegaSpin>(nconfs_[neutron_irrep_index]) + ilast_irrep];
	}
	else
	{
		assert(number_shells == 1);
		return gammas_[cpp0x::get<kGamma>(nconfs_[neutron_irrep_index])].S2;
	}
}

void CncsmSU3xSU2Basis::ShowNeutronIrreps()
{
	for (int i = 0; i < nconfs_.size(); ++i)
	{
		SingleDistribution distr;
		int nhw = nhw_n(i);

		getDistr_n(i, distr);
		cout << "in: " << i << "\tnhw:" << nhw << "\t[";
		for (int j = 0; j < distr.size() - 1; ++j)
		{
			cout << (int)distr[j] << " ";
		}
		cout << (int)distr.back() << "]\t";
		UN::SU3xSU2_VEC gamma;
		getGamma_n(i, gamma);
		cout << "gamma:";
		for (int j = 0; j < gamma.size() - 1; ++j)
		{
			cout << gamma[j] << " ";
		}
		cout << gamma.back();
		cout << "\tomega:";

		SU3xSU2_VEC omega;
		getOmega_n(i, omega);
		if (omega.size())
		{
			for (int j = 0; j < omega.size() - 1; ++j)
			{
				cout << (int)omega[j].rho << "(" << (int)omega[j].lm << " " << (int)omega[j].mu << ")" << (int)omega[j].S2 << " ";
			}
			cout << (int)omega.back().rho << "(" << (int)omega.back().lm << " " << (int)omega.back().mu << ")" << (int)omega.back().S2 << " ";
		}
		else
		{
			cout << gamma.back();
		}
		cout << "\tmax_mult_n: " << getMult_n(i);
		cout << endl;
	}
}




}

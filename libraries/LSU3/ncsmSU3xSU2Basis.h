#ifndef NCSMSU3xSU2BASIS_H
#define NCSMSU3xSU2BASIS_H
#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/CTuple.h>
#include <SU3ME/ModelSpaceExclusionRules.h>
#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <LSU3/std.h>

namespace lsu3
{
class CncsmSU3xSU2Basis
{
	public:
//	[0] ... idistr: distribution index in vector distribs_(p/n)_. The number of shells that
//			    	are occupied is equal to number_of_active_shells_(p/n)_[idistr]
//	[1] ... iwspin:	index in vector of SU2::LABELS omegas_spin_.
//					The number of elements = number_of_active_shells_(p/n)_[idistr] - 1
//	[2] ... igamma: index in vector of UN::SU3xSU2 labels gammas_. 
//					The number of elements = number_of_active_shells_(p/n)_[idistr]
//	[3] ... iwsu3:	index in vector of SU3::LABELS omegas_su3_.
//					The number of elements = number_of_active_shells_(p/n)_[idistr] - 1
	enum Nucleon_Basis_Indices {kDistr = 0, kOmegaSpin = 1, kGamma = 2, kOmegaSu3 = 3};  
	// Here we assume that one can use unsigned 16 bit integers for distributions and spins,
	// that is, distribs_p_.size() <= 0xFFFF & omegas_spin_.size() <= 0xFFFF
	typedef	cpp0x::tuple<uint16_t, uint16_t, uint32_t, uint32_t>  NUCLEON_BASIS_INDICES;
	CncsmSU3xSU2Basis(const proton_neutron::ModelSpace& ncsmModelSpace, const uint16_t idiag, const uint16_t ndiag);
	CncsmSU3xSU2Basis(const proton_neutron::ModelSpace& ncsmModelSpace, const CncsmSU3xSU2Basis& basis, const uint16_t idiag, const uint16_t ndiag);

	inline uint8_t Nmax() const {return nmax_;}
	uint8_t nhw_p(const uint32_t index_irrep) {return HOquanta_p(index_irrep) - min_ho_quanta_p_;}
	uint8_t nhw_n(const uint32_t index_irrep) {return HOquanta_n(index_irrep) - min_ho_quanta_n_;}

	inline uint64_t getFirstStateId(const uint16_t idiag) const  {return std::accumulate(dims_.begin(), dims_.begin() + idiag, (uint64_t)0);}
	inline uint64_t getFirstStateId() const  {return std::accumulate(dims_.begin(), dims_.begin() + idiag_, (uint64_t)0);}
	inline uint64_t getModelSpaceDim() const {return std::accumulate(dims_.begin(), dims_.end(), (uint64_t)0);}
	inline uint64_t JJ() const {return JJ_;}
	inline uint64_t NProtons() const {return nprotons_;}
	inline uint64_t NNeutrons() const {return nneutrons_;}
	
	inline uint32_t dim() const {return dims_[idiag_];}
	inline uint32_t dim(const uint16_t idiag) const {return dims_[idiag];}

	inline uint16_t SegmentNumber() const {return idiag_;}
	inline uint16_t ndiag() const {return ndiag_;} 

	inline uint16_t omega_pn_dim(const uint32_t pn_irrep_index) const {return wpn_irreps_container_[wpn_[pn_irrep_index]].dim();}
	inline const IRREPBASIS& Get_Omega_pn_Basis(const uint32_t pn_irrep_index) const {return wpn_irreps_container_[wpn_[pn_irrep_index]];}

	void Reshuffle(const proton_neutron::ModelSpace& ncsmModelSpace, const uint16_t idiag, const uint16_t ndiag);

	void getDistr_p(uint32_t proton_irrep_index, SingleDistribution& distr) const;
	void getDistr_n(uint32_t neutron_irrep_index, SingleDistribution& distr) const;
	
	void getGamma_p(uint32_t proton_irrep_index, UN::SU3xSU2_VEC& gamma) const;
	void getGamma_n(uint32_t neutron_irrep_index, UN::SU3xSU2_VEC& gamma) const;
	
	void getOmega_p(uint32_t proton_irrep_index, SU3xSU2_VEC& omega_p) const;
	void getOmega_n(uint32_t neutron_irrep_index, SU3xSU2_VEC& omega_n) const;
	
	SU3xSU2::LABELS getOmega_pn(const uint32_t proton_irrep_index, const uint32_t neutron_irrep_index, const uint32_t pn_irrep_index) const;

	template<size_t N> typename cpp0x::tuple_element<N, NUCLEON_BASIS_INDICES>::type getIndex_p(const uint32_t proton_irrep_index) const {return cpp0x::get<N>(pconfs_[proton_irrep_index]);}
	template<size_t N> typename cpp0x::tuple_element<N, NUCLEON_BASIS_INDICES>::type getIndex_n(const uint32_t neutron_irrep_index) const {return cpp0x::get<N>(nconfs_[neutron_irrep_index]);}

	SU3xSU2::LABELS getProtonSU3xSU2(uint32_t proton_irrep_index) const;
	uint16_t getMult_p(const uint32_t proton_irrep_index) const;
	int32_t getLambda_p(const uint32_t proton_irrep_index) const;
	int32_t getMu_p(const uint32_t proton_irrep_index) const;
	int32_t getSS_p(const uint32_t proton_irrep_index) const;

	SU3xSU2::LABELS getNeutronSU3xSU2(uint32_t neutron_irrep_index) const;
	uint16_t getMult_n(const uint32_t neutron_irrep_index) const;
	int32_t getLambda_n(const uint32_t neutron_irrep_index) const;
	int32_t getMu_n(const uint32_t neutron_irrep_index) const;
	int32_t getSS_n(const uint32_t neutron_irrep_index) const;

	inline const NUCLEON_BASIS_INDICES* pconf_begin() const {return &pconfs_[0];}
	inline const uint32_t pconf_size() const {return pconfs_.size();}

	inline const NUCLEON_BASIS_INDICES* nconf_begin() const {return &nconfs_[0];}
	inline uint32_t nconf_size() const {return nconfs_.size();}
	inline uint16_t MaxNumberOfStatesInBlock() const {return max_states_in_block_;}
	inline uint16_t NumberOfStatesInBlock(uint32_t iblock) const 
	{
		assert(iblock <= NumberOfBlocks() - 1);
		return (iblock == NumberOfBlocks() - 1) ? (dim() - first_state_in_block_[iblock]) : first_state_in_block_[iblock+1] - first_state_in_block_[iblock];
	}

	inline uint32_t NumberOfBlocks() const {return first_state_in_block_.size();}
	inline uint32_t BlockPositionInSegment(uint32_t iblock) const {return first_state_in_block_[iblock];}

	inline uint32_t blockBegin(uint32_t iblock) const 
	{
		return (iblock) ? block_end_[iblock - 1] : 0;
	}
	inline uint32_t blockEnd(uint32_t iblock) const
	{
		return block_end_[iblock];
	}
	inline uint16_t NumberPNIrrepsInBlock(uint32_t iblock) const 
	{
		return blockEnd(iblock) - blockBegin(iblock);
	}
	inline const uint32_t getProtonIrrepId(uint32_t iblock) const {return pnbasis_ipin_[2*iblock];}
	inline const uint32_t getNeutronIrrepId(uint32_t iblock) const {return pnbasis_ipin_[2*iblock + 1];}

	
	void ShowMemoryRequirements() const;
	void ShowProtonIrreps();
	void ShowNeutronIrreps();

	void ShowProtonConf(uint32_t proton_irrep_index) const;
	void ShowNeutronConf(uint32_t neutron_irrep_index) const;
	private:
	uint8_t HOquanta_p(const uint32_t index_irrep) const;
	uint8_t HOquanta_n(const uint32_t index_irrep) const;
	void GenerateFinal_SU3xSU2Irreps_Container(const proton_neutron::ModelSpace& ncsmModelSpace);
	uint8_t getNumberOfOccupiedShells_p(const uint32_t index_irrep) const { return occupied_shells_distr_p_[cpp0x::get<kDistr>(pconfs_[index_irrep])];}
	uint8_t getNumberOfOccupiedShells_n(const uint32_t index_irrep) const {return occupied_shells_distr_n_[cpp0x::get<kDistr>(nconfs_[index_irrep])];}
	void GenerateDistrGammaOmega_SortedVectors(	const CncsmSU3Basis& basis,
												std::vector<SingleDistribution>& distribs_p,
												std::vector<SingleDistribution>& distribs_n,
												std::vector<UN::SU3xSU2_VEC>& gammas,
												std::vector<SU3_VEC>& omegas_su3,
												std::vector<SU2_VEC>& omegas_spin);
	private:
	SU2::LABEL JJ_;
	uint8_t nprotons_, nneutrons_;

	uint8_t min_ho_quanta_p_, min_ho_quanta_n_;
	uint8_t nmax_;

	uint16_t idiag_, ndiag_;

	std::vector<uint32_t> dims_;

	/////////////// Data structures independ on values of ndiag and idiag   /////////////////////////////
	
	// occupied_shells_distr_p_[i] = the number of occupied shells for ith proton distribution
	std::vector<uint8_t> occupied_shells_distr_p_;

	// occupied_shells_distr_n_[i] = the number of occupied shells for ith neutron distribution
	std::vector<uint8_t> occupied_shells_distr_n_;

	//	arrays storing quantum labels required for a complete definition of
	//	proton/neutron configurations in a given model space	
	std::vector<SingleDistribution> distribs_p_; 
	std::vector<SingleDistribution> distribs_n_;
	UN::SU3xSU2_VEC gammas_;
	SU3_VEC omegas_su3_; 
	SU2_VEC omegas_spin_;

	//	arrays with proton/neutron configurations. NUCLEON_BASIS_INDICES is a
	//	tuple of indices of distribs_p/n_, omegas_spin_, gammas_, and
	//	omegas_su3_ arrays
	std::vector<NUCLEON_BASIS_INDICES> pconfs_;
	std::vector<NUCLEON_BASIS_INDICES> nconfs_;
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	//	array of (ip in) indices
	std::vector<uint32_t> pnbasis_ipin_;		
	//	array of indices pointing to wpn_irreps_container_
	//	NOTE: we assume wpn_irreps_container_ has less than 65536 (lm mu)S irreps
	std::vector<uint16_t> wpn_;				
	SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container_;

	std::vector<uint32_t> first_state_in_block_; // full position ==   first_state_in_block_[i] + first_state_in_this_segment_
	std::vector<uint32_t> block_end_;
	uint16_t max_states_in_block_;
};

}
#endif

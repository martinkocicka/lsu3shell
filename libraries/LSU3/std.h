#ifndef LSU3_STD_H
#define LSU3_STD_H

#if defined CPP0X_BOOST

#include <boost/tr1/tuple.hpp>
#include <boost/tr1/array.hpp>
#include <boost/tr1/unordered_map.hpp>
#include <boost/tr1/unordered_set.hpp>

#include <boost/cstdint.hpp>

namespace cpp0x = std::tr1;

/*
#include <boost/tuple/tuple.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include <boost/cstdint.hpp>

namespace cpp0x = boost;
*/

typedef boost::uint8_t  uint8_t ;
typedef boost::uint16_t uint16_t;
typedef boost::uint32_t uint32_t;
typedef boost::uint64_t uint64_t;

typedef boost::int8_t  int8_t ;
typedef boost::int16_t int16_t;
typedef boost::int32_t int32_t;
typedef boost::int64_t int64_t;

typedef boost::uintmax_t uintmax_t;

#elif defined CPP0X_STD_TR1

#include <tr1/tuple>
#include <tr1/array>
#include <tr1/unordered_map>
#include <tr1/unordered_set>

//#include <stdint.h>

namespace cpp0x = std::tr1;

#else // CPP0X_STD

#include <tuple>
#include <array>
#include <unordered_map>
#include <unordered_set>

#include <cstdint>

namespace cpp0x = std;

#endif

#endif

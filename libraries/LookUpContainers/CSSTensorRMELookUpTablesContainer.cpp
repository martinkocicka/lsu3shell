#include <LookUpContainers/CSSTensorRMELookUpTablesContainer.h>

std::vector<CrmeTable*> CSSTensorRMELookUpTablesContainer::rmeTables_;

struct EQUAL_TO
{
    const SU3xSU2_VEC& to_find;
	const std::vector<char>& m_structure;
	public:
	EQUAL_TO(const SU3xSU2_VEC& labels, const std::vector<char>& structure): to_find(labels), m_structure(structure) {};
    inline bool operator()(const CrmeTable* RMETable) const
    {
        return RMETable->IsTensorEqual(m_structure, to_find);
    }
};

void CSSTensorRMELookUpTablesContainer::PrepareRMETable(	const unsigned char n, 				// 	HO shell
															const size_t nAnnihilators, 		//	number of annihilation operators in single shell tensor
															const int dA, 				
															const std::vector<char>& structure,	// 	structure of the single shell tensor. For each 0 \in array structure
																								//	there is one element in array TensorLabels
															const std::vector<SU3xSU2::LABELS*> TensorLabels, 	// labels describing creation of the single shell tensor
															bool log_is_on, std::ostream& log_file)
{
	SU3xSU2_VEC Labels;

//	The size of the input vector TensorLabels is equal to the number of zeros
//	in structure. And hence for a+ or ta operator it is empty.  However,
//	CrmeTable constructor does not permit empty TensorLabels => if we have
//	TensorLabels.empty() we have to add either (n 0)1/2 for a+ or (0 n)1/2 for
//	ta
	if (TensorLabels.empty())  // this is either a+ or ta, because if Tensor = a+, or ta, structure = {+n}, or {-n}. Since it contain no zeros, TensorLabels is empty.
	{
		assert(structure.size() == 1);
		assert(fabs(dA) == 1);

		if (dA == 1)
		{
			Labels.push_back(SU3xSU2::LABELS(1, n, 0, 1));
		}
		else
		{
			Labels.push_back(SU3xSU2::LABELS(1, 0, n, 1));
		}
	}
	else
	{	
		assert(TensorLabels.size() == std::count(structure.begin(), structure.end(), 0)); //	each zero in structure must correspond to one element in TensorLabel 
//		create SU3xSU2_VEC from vector<SU3xSU2::LABELS*>
		for (size_t i = 0; i < TensorLabels.size(); ++i)
		{
			Labels.push_back(*TensorLabels[i]);
		}
	}

	if (std::find_if(rmeTables_.begin(), rmeTables_.end(), EQUAL_TO(Labels, structure)) == rmeTables_.end()) // rme table of Tensor with Labels and nAnnihilators is not not stored in rmeTables_ ==> add it!
	{
		//	true ==> if file with rme does not exist, it will be created
		rmeTables_.push_back(new CrmeTable(structure, Labels, BaseSU3Irreps_, true, log_is_on, log_file));
	}
}


CrmeTable* CSSTensorRMELookUpTablesContainer::GetRMETablePointer(	const unsigned char n, 		// 	HO shell
																	const size_t nAnnihilators, //	number of annihilation operators in single shell tensor
																	const int dA, 				
																	const std::vector<char>& structure, // 	structure of the single shell tensor. For each 0 \in array structure
																					//	there is one element in array TensorLabels
																	// labels describing creation of the single shell tensor
																	const std::vector<SU3xSU2::LABELS*> TensorLabels) const
{
	SU3xSU2_VEC Labels;

//	The size of the input vector TensorLabels is equal to the number of zeros
//	in structure. And hence for a+ or ta operator it is empty.  However,
//	CrmeTable constructor does not permit empty TensorLabels => if we have
//	TensorLabels.empty() we have to add either (n 0)1/2 for a+ or (0 n)1/2 for
//	ta
	if (TensorLabels.empty())  // this is either a+ or ta, because if Tensor = a+, or ta, structure = {+n}, or {-n}. Since it contain no zeros, TensorLabels is empty.
	{
		assert(structure.size() == 1);
		assert(fabs(dA) == 1);

		if (dA == 1)
		{
			Labels.push_back(SU3xSU2::LABELS(1, n, 0, 1));
		}
		else
		{
			Labels.push_back(SU3xSU2::LABELS(1, 0, n, 1));
		}
	}
	else
	{	
		assert(TensorLabels.size() == std::count(structure.begin(), structure.end(), 0)); //	each zero in structure must correspond to one element in TensorLabel 
//		create SU3xSU2_VEC from vector<SU3xSU2::LABELS*>
		for (size_t i = 0; i < TensorLabels.size(); ++i)
		{
			Labels.push_back(*TensorLabels[i]);
		}
	}
//	We need to find whether a single-shell tensor, uniquely defined by
//	TensorLabels and structure, is already located in vector of pointers
//	CrmeTable* rmeTables_
	std::vector<CrmeTable*>::const_iterator it = std::find_if(rmeTables_.begin(), rmeTables_.end(), EQUAL_TO(Labels, structure)); // find a rme for tensor = {TensorLabels, structure}
//	Note that elements of rmeTables_ are not sorted and we use search
//	algorithm std::find_if that scales linearly with the number of single-shell
//	tensors rme.
	if (it == rmeTables_.end())
	{
		throw std::runtime_error("Assumption that all rme tables are loaded in memory was violated [CSSTensorRMELookUpTablesContainer::GetRMETablePointer assumes that all rme tables are loaded in memory].");
	}


	return *it;	//	rme table found, return pointer to class CrmeTable, which holds it.
}


//	This method tries to find in rmeTables_ a rme table for a single-shell
//	tensor described by HO shell n, construction structure, and array of
//	TensorLabels. The size of TensorLabels is equal to the number of zeros in
//	structure. If rme Table does not exist in rmeTables_, a new table is
//	constucted and stored in rmeTables_. This construction requires SU(3) base
//	irreps for a given model space. Therefore, BaseSU3Irreps must be also
//	provided on input.
//	Although,
//	nAnnihilators
//	dA
//	could be calculated from the structure inside of GetRMETablePointer, this
//	would slow the execution as GetRMETablePointer is called multiple times
//	with the same structure, but different inner couplings TesorLabels.
CrmeTable* CSSTensorRMELookUpTablesContainer::GetRMETablePointer(	const unsigned char n, 		// 	HO shell
																	const size_t nAnnihilators, //	number of annihilation operators in single shell tensor
																	const int dA, 				
																	const std::vector<char>& structure, // 	structure of the single shell tensor. For each 0 \in array structure
																					//	there is one element in array TensorLabels
																	const std::vector<SU3xSU2::LABELS*> TensorLabels, // labels describing creation of the single shell tensor
																	bool generate_missing_rme_files, bool log_is_on, std::ostream& log_file)
{
	size_t i;
	std::vector<CrmeTable*>::iterator it;
	SU3xSU2_VEC Labels;

//	The size of the input vector TensorLabels is equal to the number of zeros
//	in structure. And hence for a+ or ta operator it is empty.  However,
//	CrmeTable constructor does not permit empty TensorLabels => if we have
//	TensorLabels.empty() we have to add either (n 0)1/2 for a+ or (0 n)1/2 for
//	ta
	if (TensorLabels.empty())  // this is either a+ or ta, because if Tensor = a+, or ta, structure = {+n}, or {-n}. Since it contain no zeros, TensorLabels is empty.
	{
		assert(structure.size() == 1);
		assert(fabs(dA) == 1);

		if (dA == 1)
		{
			Labels.push_back(SU3xSU2::LABELS(1, n, 0, 1));
		}
		else
		{
			Labels.push_back(SU3xSU2::LABELS(1, 0, n, 1));
		}
	}
	else
	{	
		assert(TensorLabels.size() == std::count(structure.begin(), structure.end(), 0)); //	each zero in structure must correspond to one element in TensorLabel 
//		create SU3xSU2_VEC from vector<SU3xSU2::LABELS*>
		for (i = 0; i < TensorLabels.size(); ++i)
		{
			Labels.push_back(*TensorLabels[i]);
		}
	}
//	We need to find whether a single-shell tensor, uniquely defined by
//	TensorLabels and structure, is already located in vector of pointers
//	CrmeTable* rmeTables_
	it = std::find_if(rmeTables_.begin(), rmeTables_.end(), EQUAL_TO(Labels, structure)); // find a rme for tensor = {TensorLabels, structure}
//	Note that elements of rmeTables_ are not sorted and we use search
//	algorithm std::find_if that scales linearly with the number of single-shell
//	tensors rme.

	if (it == rmeTables_.end()) // rme table of Tensor with Labels and nAnnihilators is not not stored in rmeTables_ ==> add it!
	{
		rmeTables_.push_back(new CrmeTable(structure, Labels, BaseSU3Irreps_, generate_missing_rme_files, log_is_on, log_file));
		return rmeTables_.back();	// return pointer to the end of rmeTables_, where the newly created rme table has been just stored.
	}
	else
	{
		return *it;	//	rme table found, return pointer to class CrmeTable, which holds it.
	}
}

size_t CSSTensorRMELookUpTablesContainer::GetMemorySize()
{
	size_t memory_size = 0;
	for (size_t i = 0; i < rmeTables_.size(); ++i)
	{
		memory_size += rmeTables_[i]->CalculateMemorySize();
	}
	return memory_size;
}

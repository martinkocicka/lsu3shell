#ifndef SINGLE_SHELL_RME_LOOK_UP_TABLE_CONTAINER_H
#define SINGLE_SHELL_RME_LOOK_UP_TABLE_CONTAINER_H
#include <SU3ME/rmeTable.h>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>

//	This class contains a static [i.e. global] container of single shell
//	tensors rme look up tables.
class CSSTensorRMELookUpTablesContainer
{
	private:
    friend class boost::serialization::access;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & rmeTables_;
    }

	//	this vector stores rme of all single-shell tensor T^{Gamma} that occurs in the PP and NN interaction 
	//	elements of m_RMETables are not sorted, since there is no need for a fast look up. All single-shell
	//	tensors in CTensorStructure are represented by their pointers to CrmeTable, which are globally
	//	stored in m_RMETables and hence can be properly descructed by CInteractionPPNN when they are not needed.
	static std::vector<CrmeTable*> rmeTables_;
	const CBaseSU3Irreps& BaseSU3Irreps_;

	public:
	CSSTensorRMELookUpTablesContainer(const CBaseSU3Irreps& BaseSU3Irreps):BaseSU3Irreps_(BaseSU3Irreps) {};
	static void ReleaseMemory()
	{
		for (size_t i = 0; i < rmeTables_.size(); ++i)
		{
			delete rmeTables_[i];
		}
		rmeTables_.clear();
	}
	inline uint32_t size() const {return rmeTables_.size();}
//	This method tries to find in rmeTables_ data structure a rme table for a
//	single-shell tensor described by the input parameters n, nAnnihilators, dA,
//	structure, and TensorLabels. If rme table for such a single-shell tensor
//	does not exist in rmeTables_, a new table is constructed, stored in
//	rmeTables_, and its address in memory is returned. This construction
//	requires SU(3) base irreps for a given model space. Therefore,
//	BaseSU3Irreps must be also provided on input.
/*!
  \param[in] BaseSU3Irreps set of unique seed irreps dest 
  \param[in] generate_missing_rme_files = true, then if file with rme does not exist, it will be calculated on-the-fly and saved. If false, program stops whenever rme file does not exist.
  \param[in] log_is_on if true, detailed information will be logged into an output stream
  \param[in] log_file output stream in case log_is_on is true.
*/
	CrmeTable* GetRMETablePointer(	const unsigned char n, 								// 	HO shell
									const size_t nAnnihilators, 						//	number of annihilation operators in single shell tensor
									const int dA, 				
									const std::vector<char>& structure, 				// 	structure of the single shell tensor. For each 0 \in array structure
																						//	there is one element in array TensorLabels
									const std::vector<SU3xSU2::LABELS*> TensorLabels, 	// labels describing creation of the single shell tensor
									bool generate_missing_rme_files, bool log_is_on, std::ostream& log_file);

	void PrepareRMETable(	const unsigned char n, 								// 	HO shell
							const size_t nAnnihilators, 						//	number of annihilation operators in single shell tensor
							const int dA, 				
							const std::vector<char>& structure, 				// 	structure of the single shell tensor. For each 0 \in array structure
																						//	there is one element in array TensorLabels
							const std::vector<SU3xSU2::LABELS*> TensorLabels, 	// labels describing creation of the single shell tensor
							bool log_is_on, std::ostream& log_file);

	CrmeTable* GetRMETablePointer(	const unsigned char n, 		// 	HO shell
									const size_t nAnnihilators, //	number of annihilation operators in single shell tensor
									const int dA, 				
 									// 	structure of the single shell tensor. For each 0 \in array structure
									//	there is one element in array TensorLabels
									const std::vector<char>& structure,
								 	// labels describing creation of the single shell tensor
									const std::vector<SU3xSU2::LABELS*> TensorLabels) const;
	size_t GetMemorySize();
};
#endif

#ifndef HASH_FIXED_H
#define HASH_FIXED_H

#include <assert.h>
#include <LookUpContainers/lock.h>

template<typename Key, typename Value>
struct NoDeletePolicy
{
  void on_delete (Key &k, Value& v){}
};

/**
 * This class implements a HashMap of fixed size to store (Key, Value)
 * tuples. A (Key,Value) pair will be stored according to its
 * hash-value computed by the Hasher object (using Hasher::operator()
 * similarly to boost::unordered_map). When the HashFixed is full,
 * subsequent insertions lead to an undefined behavior (most likely
 * segmentation fault). When the HasFixed is destructed,
 * Policy::on_delete(Key&, Value&) will be called on each (Key,Value)
 * pair that has been inserted. Key must have an equality operator
 * defined (Key::operator==).
 *
 * Implementation note: The hashmap is implemented with a bucket data
 * structure (an array of single linked list). To avoid memory
 * operations, all the memory needed is allocated in the storage
 * array. This implies that Key and Value must have a defaut
 * constructor and an affectation operator defined (Key(),
 * Key::operator=(), Value() and Value::operator=()), also Keys needs
 * to be comparable (Key must define Key::operator==()) NOTE: this
 * could be fixed using placement new but did not seem useful at
 * first.
 *
 * The Hasher and the Policy are constructed using the default
 * constructor. But that can be hacked to construct them by copy.
 *
 * The HashFixed can not be resized and the number of bucket is the
 * maximum number of element in the HashMap. There is no use of
 * pointers internally, but indexing in the storage array with 32-bit
 * integers. If a HashFixed larger than 2**32 elements is required,
 * the data structure will need to be adapted (most likely just
 * changing the type of index). If a HashFixed smaller than 2**16
 * elements is useful, the index could be changed to 16-bit integers
 * to gain space.
 *
 * If the HashFixed is believed to have bugs, turn on the DEBUG
 * template parameter and activate assertions. That might not detect all
 * the bugs, but should catch some. In particular DEBUG verifies how
 * many elements are added in the HasFixed.
 **/

 /**
  * I have no idea why the following code does not work properly on hopper. It causes segmentation fault. _OPENMP seems to be defined
  *
#ifdef _OPENMP 
template <typename Key, typename Value, typename Hasher, typename DeletePolicy = NoDeletePolicy<Key,Value>, typename Locking = openmp_locking, bool DEBUG=false , bool PROFILE=false>
#else
template <typename Key, typename Value, typename Hasher, typename DeletePolicy = NoDeletePolicy<Key,Value>, typename Locking = no_locking, bool DEBUG=false , bool PROFILE=false>
#endif
**/
template <typename Key, typename Value, typename Hasher, typename DeletePolicy = NoDeletePolicy<Key,Value>, typename Locking = openmp_locking, bool DEBUG=false , bool PROFILE=false>
class HashFixed
{
  typedef int index;
public:
  typedef struct iterator {index i; HashFixed* c;
    iterator(){}
    iterator(index i, HashFixed* c):i(i),c(c){}

    const Key& k()const{return c->storage[i].k;}
    Value& v(){return c->storage[i].v;}

    Value& operator* (){return this->v();}

    bool operator== (const iterator& it) const {return i == it.i && c == it.c;}
    bool operator!= (const iterator& it) const {return ! this->operator==( it);}

    iterator& operator++ () { //Pre increment
      i = c->next_in_order(i);
      return *this;
    }

    iterator operator++(int)  // Post increment
    {
      iterator tmp(*this);
      this->operator++(); // Call pre-increment on this.
      return tmp;
    }
  } iterator;

  typedef struct const_iterator {
    index i; const HashFixed* c;
    const_iterator(){}
    const_iterator(index i, const HashFixed* c):i(i),c(c){}

    const Key& k()const{return c->storage[i].k;}
    const Value& v()const{return c->storage[i].v;}

    const Value& operator* () const{return this->v();}
    bool operator== (const iterator& it) const {return i == it.i && c == it.c;}
    bool operator!= (const iterator& it) const {return ! this->operator==( it);}

    bool operator== (const const_iterator& it) const {return i == it.i && c == it.c;}
    bool operator!= (const const_iterator& it) const {return ! this->operator==( it);}


    const_iterator& operator++ () { //Pre increment
      i = c->next_in_order(i);
      return *this;
    }

    const_iterator operator++(int)  // Post increment
    {
      iterator tmp(*this);
      this->operator++(); // Call pre-increment on this.
      return tmp;
    }
    
  } const_iterator;

  /**
   *  provides the memory requirements for HashFixed<Key, Value,
   *  ... >. This assumes that Key and Value are static type that do
   *  not contain dynamically allocated memory. This one is for a
   *  particular instance of the HashFixed.
   **/
  size_t static_memory_usage() const
  {
    return size*sizeof(element) + nb_bucket*sizeof(index) + sizeof(*this);
  }
  
  /**
   * Computes the memory usage of a particular instance of
   * HashFixed. It uses a functor passed as a parameter to compute the
   * size of all the objects contained within the Cache. It calls
   * size_t SizeOf::operator (const Key&, const Value&) function to
   * obtain the size of the dynamically allocated memory of a given
   * (Key,Value) pair. The returned value also take into
   * consideration the "static" footprint of the cache.
   *
   * See the end of this file for a simple example on how to use it.
   **/
  template < typename SizeOf >
  size_t dynamic_memory_usage(SizeOf& so) const
  {
    size_t actsize = this->static_memory_usage();
    for (int buck = 0; buck < nb_bucket; ++buck)
      {
	index ind = bucket[buck];
	while (ind >= 0) {
	  element& e = storage[ind];
	  actsize += so (e.k, e.v);
	  ind = e.bucket_next;
	}
      }
    return actsize;
  }

private:
  struct element
  {
    index bucket_next; //-1 => tail of bucket 
    Key k;
    Value v;
  };

  element* storage;
  index empty; //-1 for none; points to single linked list
  index* bucket; //give head of bucket

  int nb_bucket;
  int size;

  Hasher h;
  Locking lock;

  int nb_find;
  int nb_insert;
  /// chose which bucket a key is affected to.
  int which_bucket(const Key& k) const {
    return h(k)%nb_bucket;
  }    

  DeletePolicy p;

  ///allows to traverse all the element of the structure. returns the
  ///first index to consider. returns -1 if empty.
  index first_in_order() const {
    for (int i=0;i<nb_bucket;++i)
      {
	if (bucket[i] >= 0)
	  return bucket[i];
      }
    return -1;
  }

  ///allows to traverse all the element of the structure. returns the
  ///index after i in a particular order. returns -1 on over.
  index next_in_order(index i) const {
      if (this->storage[i].bucket_next != -1)
	{ //if there are still elements in the bucket, pull them
	  i = this->storage[i].bucket_next;
	}
      else 
	{ //otherwise, compute the next bucket and pull from it.
	  int buck = this->which_bucket(this->storage[i].k);
	  buck ++;
	  while (this->bucket[buck] < 0 && buck < this->nb_bucket)
	    buck++;
	  
	  if (buck == nb_bucket) 
	    { //end of cache
	      i = -1;
	    }
	  else
	    { //non empty bucket found
	      i = this->bucket[buck];
	    }
	}
      return i;
    }

public:
  ///iterating over the elements is NOT thread safe
  iterator end(){
    return iterator(-1,this);
  }

  ///iterating over the elements is NOT thread safe
  const_iterator end() const{
    return const_iterator(-1,this);
  }

  ///iterating over the elements is NOT thread safe
  iterator begin(){
    return iterator(first_in_order(), this);
  }

  ///iterating over the elements is NOT thread safe
  const_iterator begin() const{
    return const_iterator(first_in_order(), this);
  }
  
  /// initializes a HashFixed that stores size objects. Subsequently added object will cause an undefined behavior
  HashFixed (int size)
  {
    assert (size > 0);
    storage = new element[size];
    this->size = size;

	 if (PROFILE) nb_find = 0;
	 if (PROFILE) nb_insert = 0;

    empty = 0;

    //init buckets

    nb_bucket = size;
    bucket = new index[nb_bucket];
    for (int i=0; i<nb_bucket; i++) {
      bucket[i] = -1;
    }

    if (DEBUG) assert(coherent());
  }


	~HashFixed()
	{ 
	  ReleaseMemory();
	}

  /// calls the policy on all the objetcs in the cache
	void ReleaseMemory() 
	{ 
	  if (storage) 
	    {
	      //	call eviction policy
	      for (index i = 0; i != empty; i++) 
		{ 
		  p.on_delete (storage[i].k, storage[i].v);
      		}
	      if (PROFILE) 
		{ 
		  std::cerr<<"HashFixed : "<<this<<std::endl; 
		  std::cerr<<"nb_find : "<<nb_find<<std::endl; 
		  std::cerr<<"nb_insert : "<<nb_insert<<std::endl; 
		  std::cerr<<"hit_ratio : "<<((double)(nb_find-nb_insert))/(nb_find)<<std::endl<<std::endl;
		}
	    }
	  //	free own memory
	  if (bucket) {delete[] bucket; bucket = 0;}
	  if (storage) {delete[] storage; storage = 0;}
	}

	size_t nelems() 
	{
		return empty;
	}

  /// never invalidate interators
  void insert (const Key& k, const Value & v)
  {
    lock.acquire();
    if (DEBUG) assert (coherent());

	 if (PROFILE) nb_insert++;

    if (empty >= size) {std::cerr<<"HashFixed is full. size is "<<size<<". empty is "<<empty<<"."<<std::endl; exit(0); return;}

    index newelem = empty;
    if (DEBUG) assert (newelem != -1);
    //move empty further
    empty++;

    //affect values
    storage[newelem].k = k;
    storage[newelem].v = v;  

    //insert newelem in bucket at the head
    index wbuck = which_bucket(k);
    if (DEBUG) assert (wbuck >= 0 && wbuck < nb_bucket);

    storage[newelem].bucket_next = bucket[wbuck];
    bucket[wbuck] = newelem;

    if (DEBUG) assert(coherent());
    lock.release();
  }

  /// does not invalidate iterators. Returns end() if the key is not
  /// found
  iterator find (const Key& k)
  {
    index wbuck = which_bucket(k);
    if (DEBUG) assert (wbuck >= 0 && wbuck < nb_bucket);

    //    lock.acquire();
	 if (PROFILE) nb_find++;

    index current = bucket[wbuck];

    if (DEBUG) assert(coherent());

    while (current != -1) {
      element& current_elem = storage[current];

      if (current_elem.k == k)
		  {

			 //return appropriate value
		    //			 lock.release();
			 return iterator(current, this);
		  }

      current = current_elem.bucket_next;
    }

    //if I reach here, the object is not found
    //    lock.release();
    return end();
  }


  /// Calling this function outputs the internal structure of the
  /// cache to stdout. Useful for debugging only.
  void show_state()
  {
    std::cout<<"--- raw ---"<<std::endl;
    std::cout<<"empty: "<<empty<<std::endl;

    std::cout<<"storage"<<std::endl;
    for (index i=0; i<size; i++)
      {
		  element & e = storage [i];
		  std::cout<<i<<" : "<<e.bucket_next<<" "<<e.k<<" "<<e.v<<std::endl;
      }

    std::cout<<"bucket"<<std::endl;
    for (int i=0; i<nb_bucket; i++)
      {
		  std::cout<<i<<" : "<<bucket[i]<<std::endl;
      }
    std::cout<<"--- pretty ---"<<std::endl;
    std::cout<<"buckets"<<std::endl;
    for (int j=0; j< nb_bucket; j++)
      {
		  index i;
		  index old_i = -1;
		  int nbcount = 0;
		  for (i = bucket[j]; i >=0 && nbcount < size; old_i = i, i = storage[i].bucket_next, nbcount++)
			 {
				assert (old_i != i);
				std::cout<<i<<" ";
			 }
		  i = old_i;
		  std::cout<<std::endl;
      }
  }

private:
  /// does not actually check much.
  bool coherent()
  {
	 
    {
		for (int j=0; j< nb_bucket; j++)
		  {
			 index i;
			 int nbcount = 0;
			 for (i = bucket[j]; i >=0 ; nbcount++, i = storage[i].bucket_next)
				{
				  if (j != which_bucket(storage[i].k)) //is the element in the right bucket
					 return false;
				  if (nbcount > size) //no loop
					 return false;
				}
		  }      
    }
	 
    return true;
  }

};

#endif


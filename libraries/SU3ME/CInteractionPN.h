#ifndef CINTERACTIONPN_H
#define CINTERACTIONPN_H
#include <SU3ME/CRMECalculator.h>
#include <SU3ME/CTensorStructure.h>
#include <LookUpContainers/CSSTensorRMELookUpTablesContainer.h>

#include <LSU3/std.h>
		  
struct CTensorGroup_adaLess;

class CTensorGroup_ada
{
	friend struct CTensorGroup_adaLess;
	CTensorGroup_ada(const CTensorGroup_ada& TG) {}
	private:
	std::vector<char> n_adta_InSST_; 	//	needed for a proper calculation of the overall phase in SU(3)xSU(2) reduction formula
										//	this phase depends besides distribution of bra and ket fermions over HO shells also
										//	number of a+/ta operators in a shell with SST.
//	Note that the following three vectors can be easily obtained from m_structure
	std::vector<unsigned char> m_nAnnihilators;
	std::vector<char> m_dA;
	std::vector<unsigned char> m_ShellsT;	//	HO shells with active single-shell tensors.
	public:
	unsigned int firstLabelId_;									
 	CTuple<char, 2> structure_;
	struct DeleteCRMECalculatorPtrs: public std::unary_function<const std::pair<CRMECalculator*, unsigned int>&, void>
	{
		void operator()(const std::pair<CRMECalculator*, unsigned int>& item) const
		{
			delete item.first;
		}
	};
	void SelectTensorsByGamma(	const UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, const std::vector<unsigned char>& Ket_confs,
								const std::vector<unsigned char>& hoShells, 
								const int phase, std::vector<std::pair<CRMECalculator*, unsigned int> >& selected_tensors_pn);
	const std::vector<char>& n_adta_InSST() {return n_adta_InSST_;}
	const std::vector<unsigned char>& ShellsT() {return m_ShellsT;}

	unsigned int firstTensorId;
	std::vector<CTensorStructure*> m_Tensors;

	CTensorGroup_ada(	const std::vector<char>& structure, 
						const std::vector<unsigned char>& Shells, 
						const std::vector<char>& dA, 
						const std::vector<unsigned char> nAnnihilators, const unsigned int firstLabelId);
	~CTensorGroup_ada();

	inline const std::vector<unsigned char>& TensorShells() const {return m_ShellsT;}
	void AddTensor(const std::vector<CrmeTable*>& SingleShellTensorRmeTables, const std::vector<SU3xSU2::LABELS*> vOmega);
//	Does ket_confs x TensorGroup --> bra_confs ???
	bool Couple(const std::vector<unsigned char>& Bra_confs, const std::vector<unsigned char>& Ket_confs, const std::vector<unsigned char>& hoShells);

	size_t SelectTensorsByGamma(const	UN::SU3xSU2_VEC& gamma_bra, const UN::SU3xSU2_VEC& gamma_ket, const std::vector<unsigned char>& Ket_confs,
								const std::vector<unsigned char>& BraKetShells, const int phase, std::vector<CRMECalculator*>& Tensors);
	void Show(const CBaseSU3Irreps& BaseSU3Irreps);
	void ShowTensorStructure();
	void ShowTensors();
};

struct CTensorGroup_adaLess
{
	inline bool operator()(const CTuple<char, 2>& structure, const CTensorGroup_ada* p2) const
	{
		return structure < p2->structure_;
	}
	inline bool operator()(const CTensorGroup_ada* p2, const CTuple<char, 2>& structure) const
	{
		return p2->structure_ < structure;
	}
	inline bool operator()(const CTensorGroup_ada* p1, const CTensorGroup_ada* p2) const
	{
		return p1->structure_ < p2->structure_;
	}
	inline bool operator()(const CTensorGroup_ada& r1, const CTensorGroup_ada& r2) const
	{
		return r1.structure_ < r2.structure_;
	}
};


struct PNInteractionData
{
	void operator=(const PNInteractionData& pndata);
	public:
	std::pair<unsigned int, unsigned int> id_;
	
	SU3xSU2::LABELS* tensor_labels_;
	TENSOR_STRENGTH* coeffs_;
	WigEckSU3SO3CGTable** su3so3cgTables_;

	size_t nTensors_, ncoeffs_;

	inline bool operator<(const std::pair<unsigned int, unsigned int>& id) const {return id_ < id;}
	inline bool operator<(const PNInteractionData& pnIntData) const {return id_ < pnIntData.id_;}
	inline bool operator==(const PNInteractionData& pnIntData) const {return id_ == pnIntData.id_;}
	inline bool IsKeyEqualTo(const std::pair<unsigned int, unsigned int>& id) {return id_ == id;}
	
	PNInteractionData(const PNInteractionData& pndata);

	PNInteractionData(	const std::pair<unsigned int, unsigned int>& id, const std::vector<TENSOR_STRENGTH>& coeffs, const SU3xSU2_VEC& tensor_labels);
	
	~PNInteractionData()
	{
		delete []coeffs_;
		delete []tensor_labels_;
		delete []su3so3cgTables_;
	}
};

class CInteractionPN
{
	typedef std::vector<std::pair<cpp0x::tuple<SU3xSU2::LABELS, SU3xSU2::LABELS, SU3xSU2::LABELS>, std::vector<TENSOR_STRENGTH> > > TENSORS_WITH_STRENGTHS;
	typedef std::map<CTuple<int8_t, 4>, TENSORS_WITH_STRENGTHS> PNInteractionTensors;

	const CBaseSU3Irreps& BaseSU3Irreps_;

	CWigEckSU3SO3CGTablesLookUpContainer wigEckSU3SO3CGTablesLookUpContainer_;
	CSSTensorRMELookUpTablesContainer rmeLookUpTableContainer_;

	private:
	std::vector<CTensorGroup_ada*> tensorGroups_;
	std::vector<PNInteractionData> pnInteraction_;
	bool generate_missing_rme_files_;
	bool log_is_on_;
	std::ostream& log_file_;
	public:
	inline bool log_is_on() const {return log_is_on_;}
	inline bool generate_missing_rme_files() const {return generate_missing_rme_files_;}

	void ShowTensorGroupsAndTheirTensorStructures();
	void ShowInteractionStructures() const;
	~CInteractionPN();

/*!
  Load proton-neutron two-body interaction terms 
  \param[in] BaseSU3Irreps set of unique seed irreps dest 
  \param[in] generate_missing_rme_files if true, then file with rme does not exist, it will be calculated on-the-fly and saved. If false, program stops whenever rme file does not exist.
  \param[in] log_is_on if true, detailed information will be logged into an output stream
  \param[in] log_file output stream in case log_is_on is true.
  */
	CInteractionPN(const CBaseSU3Irreps& BaseSU3Irreps, bool generate_missing_rme_files = true, bool log_is_on = true, std::ostream& log_file = std::cout):BaseSU3Irreps_(BaseSU3Irreps),
														rmeLookUpTableContainer_(BaseSU3Irreps),
														wigEckSU3SO3CGTablesLookUpContainer_(),
														generate_missing_rme_files_(generate_missing_rme_files), log_is_on_(log_is_on), log_file_(log_file)
														{}

	std::pair<uint32_t, uint32_t> LoadPNOperatorsIntoDataStructures(const std::vector<std::pair<std::string, TENSOR_STRENGTH> >& fileNameStrength, PNInteractionTensors& unique_Tensors);
	void AddOperators(int my_rank, const std::vector<std::pair<std::string, TENSOR_STRENGTH> >& filaNameStrength);
	size_t GetTensorsCoefficientsLabels(	const unsigned int index_proton, const unsigned int index_neutron, 
										SU3xSU2::LABELS*& tensor_labels, WigEckSU3SO3CGTable**& su3so3cgTables, TENSOR_STRENGTH*& coeffs) const;

	size_t GetRelevantTensorGroups(	const std::vector<unsigned char>& Bra_confs, 		
									const std::vector<unsigned char>& Ket_confs, 
									const std::vector<unsigned char>& hoShells, 
									std::vector<CTensorGroup_ada*>& tensorGroups, 
									std::vector<int>& phase_tensor_group) const;
};
#endif

#include <SU3ME/CRMECalculator.h>

///////////////////////////////////////////////////////////
///				CRMECalculator methods					///
///////////////////////////////////////////////////////////

//	Note that CRMECalculator::Couple returns true in case when omega_bra = empty and omega_ket = empty
//	At this point we can assume omega_bra.size() == omega_ket.size()
bool CRMECalculator::Couple(const SU3xSU2_VEC& omega_bra, const SU3xSU2_VEC& omega_ket) const
{
	size_t nOmegas = omega_bra.size();
	for (size_t i = 0; i < nOmegas; ++i)
	{
		if (!SU3::mult(omega_ket[i], m_Omega_t[i], omega_bra[i]))
		{
			return false;
		}
		if (!SU2::mult(omega_ket[i].S2, m_Omega_t[i].S2, omega_bra[i].S2))
		{
			return false;
		}
	}
	return true;
}

void CRMECalculator::Show() const
{
	cout << "Gamma_t = ";
	for (size_t i = 0; i < m_Gamma_t.size(); ++i)
	{
		cout << m_Gamma_t[i]->Tensor << "  ";
	}
	cout << "\tOmega_t = ";
	for (size_t i = 0; i < m_Omega_t.size(); ++i)
	{
		cout << m_Omega_t[i] << "  ";
	}
}

void CRMECalculator::ShowUserFriendly() const
{
	for (size_t i = 0; i < m_Omega_t.size(); ++i)
	{
		cout << "{";
	}

	if (m_Omega_t.empty())
	{
		SU3xSU2::LABELS Gamma(m_Gamma_t[0]->Tensor);
		if (m_Gamma_t[0]->m_operatorType == SU3xSU2::RME::Identity)
		{
			cout << "I";
		}
		else
		{
			cout << "T^"; 
			cout << (int)Gamma.rho << "(" << (int)Gamma.lm << " " << (int)Gamma.mu << ")" << (int)Gamma.S2;
		}
		return;
	}


	SU3xSU2::LABELS Gamma0(m_Gamma_t[0]->Tensor);
	SU3xSU2::LABELS Gamma1(m_Gamma_t[1]->Tensor);

	if (m_Gamma_t[0]->m_operatorType == SU3xSU2::RME::Identity)
	{
		cout << "I x ";
	}
	else
	{
		cout << "T^";
		cout << (int)Gamma0.rho << "(" << (int)Gamma0.lm << " " << (int)Gamma0.mu << ")" << (int)Gamma0.S2 << " x ";
	}

	if (m_Gamma_t[1]->m_operatorType == SU3xSU2::RME::Identity)
	{
		cout << "I}";
	}
	else
	{
		cout << "T^";
		cout << (int)Gamma1.rho << "(" << (int)Gamma1.lm << " " << (int)Gamma1.mu << ")" << (int)Gamma1.S2 << "}";
	}

	for (size_t iomega = 0; iomega < m_Omega_t.size() - 1; ++iomega)
	{
		cout << (int)m_Omega_t[iomega].rho << "(" << (int)m_Omega_t[iomega].lm << " " << (int)m_Omega_t[iomega].mu << ")" << (int)(int)m_Omega_t[iomega].S2 << " x ";
		SU3xSU2::LABELS Gamma(m_Gamma_t[iomega + 2]->Tensor);
		if (m_Gamma_t[iomega + 2]->m_operatorType == SU3xSU2::RME::Identity)	// this could be a identity operator if <bra| == |ket>
		{
			cout << "I" << "}";
		}
		else
		{
			cout << "T^";
			cout << (int)Gamma.rho << "(" << (int)Gamma.lm << " " << (int)Gamma.mu << ")" << (int)Gamma.S2 << "}";
		}
	}
	SU3xSU2::LABELS last_omega(m_Omega_t.back());

	cout << (int)last_omega.rho << "(" << (int)last_omega.lm << " " << (int)last_omega.mu << ")" << (int)last_omega.S2;
}

#ifndef CRMECALCULATOR_H
#define CRMECALCULATOR_H
#include <SU3ME/RME.h>
#include <SU3ME/rmeTable.h>
#include <LookUpContainers/WigEckSU3SO3CGTable.h>

#include<algorithm>
class CRMECalculator 
{
	public:
	SU3xSU2_VEC m_Omega_t;
	std::vector<SU3xSU2::RME*> m_Gamma_t;
	char phase_;
	WigEckSU3SO3CGTable* m_pSU3SO3CGTable;

	CRMECalculator(const std::vector<SU3xSU2::RME*>& Gamma_t, const SU3xSU2_VEC& Omega_t, WigEckSU3SO3CGTable* pSU3SO3CGTable, const int phase)
	:m_Omega_t(Omega_t), m_Gamma_t(Gamma_t), phase_(phase),  m_pSU3SO3CGTable(pSU3SO3CGTable) {}
	//	omega_ket x m_Omega_t ---> omega_bra
	//	Note that m_Omega_t = empty (e.g. in case of <0 0 6 0 | T | 0 0 6 >), then omega_bra and omega_ket must be also empty
	//	and method Couple returns true
	bool Couple(const SU3xSU2_VEC& omega_bra, const SU3xSU2_VEC& omega_ket) const;

	SU3xSU2::RME::DOUBLE* CalculateRME(	const UN::SU3xSU2_VEC& gamma_bra, const SU3xSU2_VEC& omega_bra, 
										const UN::SU3xSU2_VEC& gamma_ket, const SU3xSU2_VEC& omega_ket, size_t& nrme) 
	{
		if (m_Omega_t.empty())	//	no multi-shell coupling ==> the tensor is single-shell tensor and we can return rme rightaway
		{
			assert(phase_ == 1); // since the operator is a mere one single-shell tensor ==> no a+/a exchanging is needed ==> phase_1 == -1 should never occur
			assert(omega_ket.empty() && omega_bra.empty());
			assert(gamma_bra.size() == 1 && gamma_ket.size() == 1);
			SU3xSU2::RME::DOUBLE* prme = new SU3xSU2::RME::DOUBLE[m_Gamma_t[0]->m_ntotal];
			nrme = m_Gamma_t[0]->m_ntotal;
			memcpy(prme, m_Gamma_t[0]->m_rme, m_Gamma_t[0]->m_ntotal*sizeof(SU3xSU2::RME::DOUBLE));
			return prme;
		}

		SU3xSU2::RME resultingRME;
		if (Couple(omega_bra, omega_ket))
		{
			CalculateRME_2(gamma_bra, omega_bra, m_Gamma_t, m_Omega_t, gamma_ket, omega_ket, resultingRME);
			nrme = resultingRME.m_ntotal;
			if (phase_ == -1)
			{
				for (size_t i = 0; i < nrme; ++i)
				{
					resultingRME.m_rme[i] *= -1.0;
				}
			}
			return resultingRME.m_rme;
		}
		else
		{
			nrme = 0;
			return NULL;	//	does not couple omega_ket x Omega_t ---> omega_bra
		}
	}

	SU3xSU2::RME* CalculateRME(	const UN::SU3xSU2_VEC& gamma_bra, const SU3xSU2_VEC& omega_bra, 
								const UN::SU3xSU2_VEC& gamma_ket, const SU3xSU2_VEC& omega_ket) 
	{

		if (m_Omega_t.empty())	//	no multi-shell coupling ==> the tensor is single-shell tensor and we can return rme rightaway
		{
			assert(phase_ == 1); // since the operator is a mere one single-shell tensor ==> no a+/a exchanging is needed ==> phase_1 == -1 should never occur
			assert(omega_ket.empty() && omega_bra.empty());
			assert(gamma_bra.size() == 1 && gamma_ket.size() == 1);
			//	allocate memory for copy of rmes from m_Gamma_t[0]
			SU3xSU2::RME::DOUBLE* prme = new SU3xSU2::RME::DOUBLE[m_Gamma_t[0]->m_ntotal];
			//	copy m_Gamma_t[0] rmes into a allocated array
			memcpy(prme, m_Gamma_t[0]->m_rme, m_Gamma_t[0]->m_ntotal*sizeof(SU3xSU2::RME::DOUBLE));
			//	user needs to (1) delete SU3xSU3::RME->m_rme and then (2) delete SU3xSU2::RME 
			return new SU3xSU2::RME(gamma_bra[0], m_Gamma_t[0]->m_tensor_max, m_Gamma_t[0]->Tensor, gamma_ket[0], prme);
		}

		if (Couple(omega_bra, omega_ket))
		{
			SU3xSU2::RME *presultingRME = new SU3xSU2::RME();
			CalculateRME_2(gamma_bra, omega_bra, m_Gamma_t, m_Omega_t, gamma_ket, omega_ket, *presultingRME);
			if (phase_ == -1)
			{
				for (size_t i = 0; i < presultingRME->m_ntotal; ++i)
				{
					presultingRME->m_rme[i] *= -1.0;
				}
			}
			//	user needs to (1) delete presultingRME->m_rme (allocated inside of CalculateRME_2 and then (2) delete presultingRME itself
			return presultingRME;
		}
		else
		{
			return NULL;	//	does not couple omega_ket x Omega_t ---> omega_bra
		}
	}


	inline const SU3xSU2_VEC& GetOmega() const {return m_Omega_t;}
	void GetGamma(SU3xSU2_VEC& Gamma) const 
	{
		for (size_t i = 0; i < m_Gamma_t.size(); ++i)
		{
			Gamma.push_back(m_Gamma_t[i]->Tensor);
		}
	}

	~CRMECalculator()
	{
		for (size_t i = 0; i < m_Gamma_t.size(); ++i)
		{
			delete m_Gamma_t[i];	//	delete class SU3xSU2::RME
		}
	}

	void Show() const;
	void ShowUserFriendly() const;
};
#endif

#ifndef CSHELLCONFIGURATION_H
#define CSHELLCONFIGURATION_H
#include <vector>
#include <cstring>

typedef std::vector<unsigned> tSHELLSCONFIG;

class CShellConfigurations {
		size_t m_Nmax, m_A, m_LastShell;
		std::vector<std::vector<tSHELLSCONFIG> > m_ShellConfigurations;

		void GenerateShellConfigurations(const size_t Nmax, const unsigned A);
	public:
		CShellConfigurations(const size_t Nmax, const unsigned A);
		inline const std::vector<tSHELLSCONFIG>& GetConfigurations(const size_t N) {return m_ShellConfigurations[N];}; //vector of configurations at Nhw subspace
		inline size_t GetNumberOfConfigurations(const size_t N) const {return m_ShellConfigurations[N].size();}; //number of configurations at Nhw subspace
		inline size_t GetMaxShellNumber() { return m_LastShell;};

		void GetMinMaxFermionsShells(std::vector<std::pair<unsigned, unsigned> >& vMinMax) const;
		void GetMaxFermionsShells(std::vector<unsigned>& vMax) const;
		inline size_t GetLastHOShell() {return m_LastShell;}

		void ShowConfigurations(); // show all shell configurations
};

#endif

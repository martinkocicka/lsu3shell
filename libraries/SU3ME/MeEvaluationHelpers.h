#ifndef MEEVALUATIONHELPERS_H
#define MEEVALUATIONHELPERS_H

#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>

struct RmeCoeffsSU3SO3CGTablePointers
{
	SU3xSU2::RME* rmes_;
	TENSOR_STRENGTH* coeffs_;
	WigEckSU3SO3CGTable* pointer_SU3SO3CGTable_;
	RmeCoeffsSU3SO3CGTablePointers(SU3xSU2::RME* rmes, TENSOR_STRENGTH* coeffs, WigEckSU3SO3CGTable* SU3SO3CGs)
		:rmes_(rmes), pointer_SU3SO3CGTable_(SU3SO3CGs), coeffs_(coeffs) {}
	RmeCoeffsSU3SO3CGTablePointers(const RmeCoeffsSU3SO3CGTablePointers& rmeCoeffsCGTable)
		:rmes_(rmeCoeffsCGTable.rmes_),  pointer_SU3SO3CGTable_(rmeCoeffsCGTable.pointer_SU3SO3CGTable_), coeffs_(rmeCoeffsCGTable.coeffs_) {}
	RmeCoeffsSU3SO3CGTablePointers():rmes_(NULL), pointer_SU3SO3CGTable_(NULL), coeffs_(NULL) {}
};



unsigned char getDifferences(const std::vector<unsigned char>& distr_bra, const std::vector<unsigned char>& distr_ket);
//	transform distr_bra and distr_ket and augment gamma_bra and omega_bra according 
unsigned char TransformDistributions_SelectByDistribution(
										const CInteractionPPNN& interaction, 
										const CInteractionPN& interaction_pn, 
										SingleDistribution& distr_bra, 
										UN::SU3xSU2_VEC& gamma_bra,
										SU3xSU2_VEC& omega_bra,	//	
										SingleDistribution& distr_ket, 
										std::vector<unsigned char>& hoShells, unsigned char& num_vacuums_ket_distr,
										std::vector<int>& phase, std::vector<CTensorGroup*>& tensorGroups,
										std::vector<int>& phase_pn, std::vector<CTensorGroup_ada*>& tensorGroups_pn);
void TransformGammaKet_SelectByGammas(
//	input
									const std::vector<unsigned char>& hoShells,
									const SingleDistribution& distr_ket, 
									const unsigned char num_vacuums_ket_distr, 
									const nucleon::Type type,
									const std::vector<int>& phase, 
									const std::vector<CTensorGroup*>& tensorGroups, 
									const std::vector<int>& phase_pn, 
									const std::vector<CTensorGroup_ada*>& tensorGroups_pn, 
									const UN::SU3xSU2_VEC& gamma_bra_vacuum_augmented, 
//	output:									
									UN::SU3xSU2_VEC& gamma_ket, 
									std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> >& selected_tensors,
									std::vector<std::pair<CRMECalculator*, unsigned int> >& selected_tensors_pn);

void TransformOmegaKet_CalculateRME(
//	input:
										const SingleDistribution& distr_ket,										
										const UN::SU3xSU2_VEC& gamma_bra,
										const SU3xSU2_VEC& omega_bra,
										const UN::SU3xSU2_VEC& gamma_ket,
										const unsigned char num_vacuum_shells_bra,
										const std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> >& tensors,
										const std::vector<std::pair<CRMECalculator*, unsigned int> >& tensors_pn,
//	output:										
										SU3xSU2_VEC& omega_ket, 
										std::vector<RmeCoeffsSU3SO3CGTablePointers>& rmeCoeffs,
										std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_pn);

void Reset_rmeIndex(std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_pointer_index);
void Reset_rmeCoeffs(std::vector<RmeCoeffsSU3SO3CGTablePointers>& rmeCoeffs);
void Reset_rmeCoeffs(std::vector<MECalculatorData>& rmeCoeffs);

void InitializeIdenticalOperatorRME(SU3xSU2::RME& identityOperatorRME, const size_t ntotal_max = 1024);
void CreateIdentityOperatorRME(const SU3xSU2::LABELS& bra, const SU3xSU2::LABELS& ket, size_t amax, SU3xSU2::RME& identityOperatorRME);
void Calculate_Proton_x_Identity_MeData(	const SU3xSU2::LABELS& bra, 
											const SU3xSU2::LABELS& ket,
											const std::vector<RmeCoeffsSU3SO3CGTablePointers>& protonMeData, 
											const SU3xSU2::RME& neutronOperatorRME, 
											std::vector<MECalculatorData>& proton_neutronMeData);

void Calculate_Identity_x_Neutron_MeData(	const SU3xSU2::LABELS& bra, 
											const SU3xSU2::LABELS& ket,
											const SU3xSU2::RME& protonOperatorRME, 
											const std::vector<RmeCoeffsSU3SO3CGTablePointers>& neutronMeData, 
											std::vector<MECalculatorData>& proton_neutronMeData);
void CalculatePNInteractionMeData(	const CInteractionPN& interactionPN,
									const SU3xSU2::LABELS& bra, 
									const SU3xSU2::LABELS& ket,
									const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_p,
									const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_n, 
									std::vector<MECalculatorData>& rmeCoeffsPNPN);

void ShowMeCalcData(const std::vector<MECalculatorData>& rmeCoeffsPNPN);
#endif

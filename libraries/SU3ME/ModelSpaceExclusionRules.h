#ifndef MODEL_SPACE_EXCLUSION_RULES
#define MODEL_SPACE_EXCLUSION_RULES
#include <UNU3SU3/UNU3SU3Basics.h>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>

/** \f$S_{\pi}\f$, \f$S_{\nu}\f$ */
typedef std::pair<SU2::LABEL, SU2::LABEL> SpSn;

/** Contains vector of allowed spins and corresponding allowed SU(3) irreps.
*
* - Allowed_Spins_and_SU3s::first ... sorted vector of allowed total spins
*
* - Allowed_Spins_and_SU3s::second ... vector of sorted vectors of allowed SU(3) irreps
*/ 
typedef std::pair<SU2_VEC, std::vector<SU3_VEC> > Allowed_Spins_and_SU3s;

/** 
 * forward declaration that I need in order to declare proton_neutron::ModelSpace defined in include/proton_neutron_ncsmSU3Basis.h
 * as a friend of class CNhwSubspace
 */
// namespace proton_neutron
// {
// 	class ModelSpace;
// }

/** \brief Definition of \f$N\hbar\Omega\f$ subspace
*
* This class holds data structure with definition of \f$N\hbar\Omega\f$ model space.
*
* \todo implement constructor that will initialize CNhwSubspace and selection rules from the input file
* \todo implement destructor which will save model_space_definition_ into an output file
* \todo Implement the second version of GetAllowedSpinsAndSU3, whih would take as input parameter \f$S_{\pi}\f$, \f$S_{\nu}\f$, \f$(\lambda_{\pi}\;\mu_{\pi})\f$ and \f$(\lambda_{\nu}\;\mu_{\nu})\f$
*  and return a list of selected SU3xSU2 configurations. This will require reimplementation of 
* 	(a) void PNConfIterator::rewindPNOmega()
* 	(b)	void CncsmSU3Basis::GenerateFinal_SU3xSU2Irreps_and_CalculateDim()
*
*/ 
class CNhwSubspace
{
	private:
	friend class boost::serialization::access;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
   	{
       	ar & N_;
        ar & model_space_definition_;
   	}
	CNhwSubspace() {} // serialization requires this constructor

	public:
//	friend ModelSpace;
//	friend ModelSpaceAlias;
//! datatype which defines \f$N\hbar\Omega\f$  model space
	typedef std::vector<std::pair<SpSn, Allowed_Spins_and_SU3s> > Model_Space_Definition;

	// accessors to data vector
	Model_Space_Definition::size_type size() const {return model_space_definition_.size();};
	const Model_Space_Definition::value_type& operator[](Model_Space_Definition::size_type i) const {return model_space_definition_[i];};

	private:

	struct LessSpSn
	{
		inline bool operator() (const std::pair<SpSn, Allowed_Spins_and_SU3s>& l,  const std::pair<SpSn, Allowed_Spins_and_SU3s>& r) const {return l.first < r.first;}
		inline bool operator() (const SpSn l,  const std::pair<SpSn, Allowed_Spins_and_SU3s>& r) const {return l < r.first;}
		inline bool operator() (const std::pair<SpSn, Allowed_Spins_and_SU3s>& l,  const SpSn& r) const {return l.first < r;}
	};

	unsigned char N_;
	Model_Space_Definition model_space_definition_;

	public:
/** Construct the whole \f$N\hbar\Omega\f$ subspace 
*
* \param[in] N number of HO quanta above the allowed minimum
*/	
	CNhwSubspace(const unsigned char N): N_(N) {}

/** Construct the whole \f$N\hbar\Omega\f$ subspace 
* \param[in] N number of HO quanta above the allowed minimum
* \param[in] model_space_definition \f$N\hbar\Omega\f$ subspace definition 
*/ 
	CNhwSubspace(const unsigned char N, const Model_Space_Definition& model_space_definition): N_(N), model_space_definition_(model_space_definition) 
	{
//	sort {Sp Sn} pairs		
		std::sort(model_space_definition_.begin(), model_space_definition_.end(), LessSpSn());
		for (size_t i = 0; i < model_space_definition_.size(); ++i)
		{
//	check whether the vector of allowed S is sorted ....			
			SU2_VEC::iterator pos =  std::adjacent_find(model_space_definition_[i].second.first.begin(), model_space_definition_[i].second.first.end(), std::greater<SU2::LABEL>());               
			if (pos != model_space_definition_[i].second.first.end())
			{
				std::cerr << "Model space does not include sorted sequence of spin pairs {Sp Sn}" << std::endl;
				exit(EXIT_FAILURE);
//				we should rather throw exception here				
			}
			assert(pos == model_space_definition_[i].second.first.end()); // else not sorted
//	for each S sort a vector of allowed (lm mu) labels
			for (size_t j = 0; j < model_space_definition_[i].second.second.size(); ++j)
			{
				std::sort(model_space_definition_[i].second.second[j].begin(), model_space_definition_[i].second.second[j].end());
			}
		}
	}


/** 
 * \param[in] sp_sn \f$S_{\pi}\f$ and \f$S_{\nu}\f$
 * \param[out] allowed_spins  vector of allowed total spins.
 * \param[out] allowed_su3s  for each total spin it contains a vector of allowed SU(3) irrep labels.
 *
 *
 * Based on the content of CNhwSubspace::model_space_definition_ the following action takes place: 
 *
 *	(a) if empty ==> all spin/SU(3) configurations are allowed
 *
 *	(b) if {Sp, Sn} is not found ==> all configurations with Sp Sn are going to be excluded
 *
 *	(c) if {Sp, Sn} is found but corresponding vector of spins is empty ==> include all possible spins and their SU(3) irreps
 *
 *	(d) if {Sp, Sn} is found and vector of allowed spins exists 
 *
 *	- for each allowed spin S, if vector of SU(3) irreps is empty ==>  all configurations with Sp Sn S will be inluded
 *
 *	- for each allowed spin S, if vector of SU(3) irreps is non-empty ==> all configurations with Sp Sn S (lm mu) will be inluded, where (lm mu) belongs to a vector of allowed SU(3) irreps
 *
 * \sa This method is needed by PNConfIterator::rewindPNOmega() and CncsmSU3Basis::GenerateFinal_SU3xSU2Irreps_and_CalculateDim() methods.
 *
*/
	void GetAllowedSpinsAndSU3(const SpSn& sp_sn, SU2_VEC& allowed_spins, std::vector<SU3_VEC>& allowed_su3s) const
	{
		assert(allowed_su3s.empty() && allowed_spins.empty());
//	(a) if model_space_definition_ == empty ==> all spin/SU(3) configurations are allowed
		if (model_space_definition_.empty())	// ==> select all configurations ==> return Sp x Sn --> {S1, S2, ..., Sn}
		{
			SU2::Couple(sp_sn.first, sp_sn.second, allowed_spins);
			for (size_t ispin = 0; ispin < allowed_spins.size(); ++ispin)
			{
				allowed_su3s.push_back(SU3_VEC());	//	insert empty vector of SU(3) 
													//	if allowed_su3[ispin] is empty ==> for S[ispin] all SU(3) irreps are allowed
			}
		}
		else
		{
			std::vector<std::pair<SpSn, Allowed_Spins_and_SU3s> >::const_iterator it = std::lower_bound(model_space_definition_.begin(), model_space_definition_.end(), sp_sn, LessSpSn());
			if (it != model_space_definition_.end() && it->first == sp_sn)
			{
				if (it->second.first.empty())	//	array of allowed total spin is empty ==> include all possible spins
				{
					SU2::Couple(sp_sn.first, sp_sn.second, allowed_spins);
					for (size_t ispin = 0; ispin < allowed_spins.size(); ++ispin)
					{
						allowed_su3s.push_back(SU3_VEC());	//	insert empty vector of SU(3) 
													//	if allowed_su3[ispin] is empty ==> for S[ispin] all SU(3) irreps are allowed
					}
				}
				else
				{
					allowed_spins = it->second.first;
					allowed_su3s  = it->second.second;
				}
			}
//	if {Sp, Sn} pair is not found in model_space_definition_ ==> allowed_spins is going to be empty ==> no configuration with Sp Sn will be included
		}
	}
	bool operator==(const unsigned char N) const {return N == N_;}
/** \returns N number of harmonic oscillator quanta above the ground state [\f$N\hbar\Omega\f$] */
	unsigned char N() const {return N_;}
};
#endif

#include <SU3ME/OperatorLoader.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>


void COperatorLoader::Add_r2_mass_intrinsic(const int A)
{
	static std::string filename_rirjPN(su3shell_data_directory);
	filename_rirjPN += "/SU3_Interactions_Operators/r2/rirjme_2b_Nmax12_pshell.PN";
	

	static std::string filename_rirjPPNN(su3shell_data_directory);
	filename_rirjPPNN += "/SU3_Interactions_Operators/r2/rirjme_2b_Nmax12_pshell.PPNN";
	
	static std::string filename_1b_r2me(su3shell_data_directory);
	filename_1b_r2me += "/SU3_Interactions_Operators/r2/r2me_1b_Nmax12";

	AddOneBodyOperator(filename_1b_r2me, ((double)A - 1.0)/((double)A));
	AddTwoBodyOperatorPPNN(filename_rirjPPNN, -2.0/((double)(A)));
	AddTwoBodyOperatorPN(filename_rirjPN, -2.0/((double)(A)));
}

/* Load	(2hw/A)*Trel operator */	
void COperatorLoader::AddTrel(const unsigned A, const float hw)
{
	static std::string filename_TrelPPNN(su3shell_data_directory);
	filename_TrelPPNN += "/SU3_Interactions_Operators/Trel/Trel_2b_Nmax12_pshell.PPNN";

	static std::string filename_TrelPN(su3shell_data_directory);
	filename_TrelPN += "/SU3_Interactions_Operators/Trel/Trel_2b_Nmax12_pshell.PN";

	two_body_PN_.push_back(std::make_pair(filename_TrelPN, TrelCoeff(A, hw)));
	two_body_PPNN_.push_back(std::make_pair(filename_TrelPPNN, TrelCoeff(A, hw)));
}

/**	Load Vcoul operator */
void COperatorLoader::AddVcoul(const float hw)
{
//	Vcoul(hw) = sqrt(hw/10)*Vcoul(hw=10MeV)
	std::string filename_Vcoul(su3shell_data_directory);
	filename_Vcoul += "/SU3_Interactions_Operators/Vcoul_10MeV/Vcoul_2b_10MeV_Nmax12_pshell.PPNN";
	two_body_PPNN_.push_back(std::make_pair(filename_Vcoul, VcoulCoeff(hw)));
}

void COperatorLoader::AddVnn()
{
	static std::string filename_VPPNN(su3shell_data_directory);
	filename_VPPNN += "/SU3_Interactions_Operators/V_JISP16_bare_Jmax4/V_JISP16_bare_Jmax4_sq_14shl_hw15_vxx_r4_PNfmt_2011_SU3_Nmax12_pshell.PPNN";

	static std::string filename_VPN(su3shell_data_directory);
	filename_VPN += "/SU3_Interactions_Operators/V_JISP16_bare_Jmax4/V_JISP16_bare_Jmax4_sq_14shl_hw15_vxx_r4_PNfmt_2011_SU3_Nmax12_pshell.PN";

	two_body_PN_.push_back(std::make_pair(filename_VPN, 1.0));
	two_body_PPNN_.push_back(std::make_pair(filename_VPPNN, 1.0));
}

/**	lambda*N = lambda*(B+B) */
void COperatorLoader::AddBdB(const int A, const int lambda)
{
	static std::string filename_BdBPPNN(su3shell_data_directory);
	filename_BdBPPNN += "/SU3_Interactions_Operators/Ax[(B+).(B)]/b1+b2_Plus_b1b+2_2b_Nmax12_pshell.PPNN";

	static std::string filename_BdBPN(su3shell_data_directory);
	filename_BdBPN += "/SU3_Interactions_Operators/Ax[(B+).(B)]/b1+b2_Plus_b1b+2_2b_Nmax12_pshell.PN";

	static std::string filename_1b_BdB(su3shell_data_directory);
	filename_1b_BdB += "/SU3_Interactions_Operators/Ax[(B+).(B)]/b+b_1b_Nmax12";

	AddOperator(filename_BdBPN, filename_BdBPPNN, filename_1b_BdB, lambda*BdBCoeff(A));
}

void COperatorLoader::AddNcm(const uint8_t A, double lambda)
{
	AddBdB(A, lambda);
}

void COperatorLoader::AddAB00(double lambda)
{
	std::string ab00_ppnn_file_name(su3shell_data_directory);
	std::string ab00_pn_file_name(su3shell_data_directory);
	std::string ab00_1b_file_name(su3shell_data_directory);
	
	ab00_ppnn_file_name += "/SU3_Interactions_Operators/AB00/AB00_2b_Nmax8_pshell.PPNN";
	ab00_pn_file_name += "/SU3_Interactions_Operators/AB00/AB00_2b_Nmax8_pshell.PN";
	ab00_1b_file_name	+= "/SU3_Interactions_Operators/AB00/AB00_1b_Nmax8";

	AddTwoBodyOperatorPPNN(ab00_ppnn_file_name, lambda);
	AddTwoBodyOperatorPN(ab00_pn_file_name, lambda);
	AddOneBodyOperator(ab00_1b_file_name, lambda);
}


void COperatorLoader::AddOperator(const std::string& observable_2b_PN, const std::string& observable_2b_PPNN, const std::string& observable_1b, const float dcoeff /* = 1.0 */)
{
	AddTwoBodyOperatorPPNN(observable_2b_PPNN, dcoeff);
	AddTwoBodyOperatorPN(observable_2b_PN, dcoeff);
	AddOneBodyOperator(observable_1b, dcoeff);
}

void COperatorLoader::AddL2()
{
	std::string observable_2b_PN("operatorsSU3/L2/2LaLbme_2b_nmax8.PN");
	std::string observable_2b_PPNN("operatorsSU3/L2/2LaLbme_2b_nmax8.PPNN");
	std::string observable_1b("operatorsSU3/L2/L2me_1b_nmax8");

	AddOperator(observable_2b_PN, observable_2b_PPNN, observable_1b);
}


/** 
  This method reads interactions files and uses information stored in one-body
  operators (I need to implement a storage format supporting J0 M0 even in
  two-body interactions) to set MECalculatorData::JJ0_ and MECalculatorData::MM0_,
  where the former is used by CalculateMe_*nonScalar
 */
void COperatorLoader::Load(int my_rank, CInteractionPPNN& interactionPPNN, CInteractionPN& interactionPN, bool print_output)
{
	if (print_output == true && my_rank == 0)
	{
		std::cout << "Loading operators ... " << std::endl;
	}

	interactionPPNN.LoadTwoBodyOperators(my_rank, two_body_PPNN_);
	interactionPN.AddOperators(my_rank, two_body_PN_);
	interactionPPNN.AddOneBodyOperators(my_rank, one_body_, MECalculatorData::JJ0_, MECalculatorData::MM0_);

	if (print_output == true && my_rank == 0)
	{
		std::cout << "Done" << std::endl;
	}
}

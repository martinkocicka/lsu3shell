#ifndef RME_H
#define RME_H
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <cassert>
#include <iostream>


namespace SU3xSU2
{
		static float dIdentity = 1.0;
	//	structure RME holds reduced matrix elements of type
	//	<alpha_{f} (lmf muf)Sf ||| T^{alpha_{0} (lm0 mu0)}_{S0} ||| alpha_{i} (lmi mui)Si>_{rhot}
	// 	where alpha_{f/i/0} is multiplicity due to the inner and outer multiplicities.
	//	
	//	ibra == alpha_{f}
	//	iket == alpha_{i}
	//	itensor == alpha_{0}
	//
	//	Bra 	== rhof_max (lmf muf)Sf
	//	Ket 	== rhoi_max (lmi mui)Sf
	//	Tensor	== rho0_max (lm0 mu0)S0
	//
	//	rme are stored as an array with the following indexing:
	//	index(ibra, iket, itensor, rhot) = ibra*ket_max*tensor_max*rhot_max + iket*tensor_max*rhot_max + itensor*rhot_max + irhot
	struct RME
	{
		public:
		typedef float DOUBLE;
		public:
	// 	if RME represents rme of the identity operator, then RME::GetVector
	// 	returns address of the dIdentity variable
		enum OperatorType {Identity = 0, SingleShell = 1, MultipleShell = 2};
		OperatorType m_operatorType; 

		size_t m_ntotal;
		DOUBLE *m_rme;

		SU3xSU2::LABELS Bra, Ket;	//	for OperatorType == SingleShell, Bra and Ket should be UN::SU3xSU2 type, 
									//	nevetheless we store mult_{f}^{max} and mult_{i}^{max} in m_bra_max and m_ket_max
		SU3xSU2::LABELS Tensor;

		int m_bra_max; 	//	alpha_{f}^{\max}
		int m_tensor_max;	//	alpha_{0}^{\max}
		int m_ket_max;	//	alpha_{i}^{\max}
		int m_rhot_max;

		int m_n3;	//	ket_max*tensor_max*rhot_max
		int m_n2; //	tensor_max*rhot_max

		private:
		//	Input:
		//	su39lm: 
		//	array containing SU(3) 9lm coefficients for a given fixed set of
		//	rhot, rho0, rhoi, rhof. It is a matrix with irho1_max columns and
		//	irho2_max rows. 
		//	
		//	rme1:
		//	vector of rho1_max reduced matrix elements
		//	rme2:
		//	vector of rho2_max reduced matrix elements
		//
		//	ALGORITHM:
		//	STEP 1: Multiply matrix su39lm by the column vector rme1. 
		//
		//  a_{1 1} 		a_{1 2} 		... a_{1 irho1_max}                    < || || >_{rho1 = 1}   
		//	a_{2 1} 		a_{2 2} 		... a_{2 irho1_max}                    < || || >_{rho1 = 2}
		//	.                                                                x      .
		//	.                                                                       .
		//	.                                                                       . 
		//	a_{irho2_max 1} a_{irho2_max 2}	...	a_{irho2_max irho1_max}            < || || >_{rho1 = irho1_max}
		//
		//	resultig column vector has irho2_max elements:
		//	dsu3_x_rme1_{rho2 = 1}
		//	dsu3_x_rme1_{rho2 = 2}
		//	.
		//	.
		//	.
		//	dsu3_x_rme1_{irho2_max}
		//
		//	STEP 2: Multiply dsu3_x_rme1 by the row vector rme2
		DOUBLE rme2_x_su39lm_x_rme1(int irho1_max, int irho2_max, DOUBLE* su39lm, DOUBLE* rme1, DOUBLE* rme2);
		public:
		// 	Constructor for SINGLE-SHELL operator rme
		//	tensor_max_ = alpha_{\gamma_{1}}^{\max} * alpha_{\gamma_{2}}^{\max} * Tensor_.rho
		//	Constructor for the reduced matrix elements of the single-shell operator
		RME(const UN::SU3xSU2& Bra_, int tensor_max_, const SU3xSU2::LABELS& Tensor_, const UN::SU3xSU2& Ket_, DOUBLE* rme):
		m_operatorType(SingleShell), Bra(Bra_), Ket(Ket_), Tensor(Tensor_)
		{ 
			m_bra_max = Bra_.mult;
			m_tensor_max = tensor_max_;
			m_ket_max = Ket_.mult;
			m_rhot_max = SU3::mult(Ket, Tensor, Bra);
			m_n3 = m_ket_max*m_tensor_max*m_rhot_max; 
			m_n2 = m_tensor_max*m_rhot_max;
			m_ntotal = m_ket_max*m_bra_max*tensor_max_*m_rhot_max;
			m_rme = rme;
		}
		// 	Constructor for IDENTITY operator 
		//	reduced matrix elements of <a_{f} (lmf muf)Sf || I || a_{i} (lmi mui) Si>
		//	Obviously, (lmf muf) == (lmi mui)
		//	and <i (lm mu)S ||| I ||| j (lm mu)S> = delta_{ij}
		//	Constructor for the identity operator of the single-shell type
		RME( const UN::SU3xSU2& BraKet_): m_operatorType(Identity), Bra(BraKet_), Ket(BraKet_), Tensor(1, 0, 0, 0)
		{
			m_bra_max = BraKet_.mult; 
			m_ket_max = BraKet_.mult;
			m_tensor_max = 1; 
			m_rhot_max = 1; 
			m_ntotal = 0; 
			m_rme = NULL;
		}
		RME(): m_ntotal(0), m_rme(NULL) {};

		//	Constructor for MULTIPLE-SHELL operator rme 
		//	operator which are calculated through SU(3) rme reduction rule rme1
		//	is either single-shell operator or multipl-shell operator whereas
		//	rme2 is always the single shell operator
		RME(const SU3xSU2::LABELS& Bra_, const SU3xSU2::LABELS& Tensor_, const SU3xSU2::LABELS& Ket_, const SU3xSU2::RME* rme1, const SU3xSU2::RME* rme2, DOUBLE* buffer);

		//	if operator is IDENTITY ===> return address of dIdentity, which containt 1.0 or NULL if ibra != iket.
		//	otherwise return address of <ibra ||| T^itensor || iket>_{rhot=0} element
		inline DOUBLE* GetVector(size_t ibra, size_t iket, size_t itensor) const
		{
			return (m_operatorType == Identity) ? ((ibra == iket) ? &dIdentity : NULL) : &m_rme[ibra*m_n3 + iket*m_n2 + itensor*m_rhot_max];
		}
		inline void Set(size_t ibra, size_t iket, size_t itensor, size_t irhot, DOUBLE value) 
		{
			m_rme[ibra*m_n3 + iket*m_n2 + itensor*m_rhot_max + irhot] = value; 
		}
		~RME() {}; 

		void Show();
		void ShowVector(size_t ibra, size_t iket, size_t itensor);
		void ShowStructure();
	};
}; 


void ShowTable(	const UN::SU3xSU2_VEC& gamma_bra, const SU3xSU2_VEC& omega_bra, 
				const std::vector<SU3xSU2::RME*>& Gamma_t, const SU3xSU2_VEC& Omega_t, 
				const UN::SU3xSU2_VEC& gamma_ket, const SU3xSU2_VEC& omega_ket);

//	This function calculates total number of rme elements for a given SU(3) rme table
size_t MultTotal(	const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_VEC& omega_bra, 
					const std::vector<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_VEC& Omega_t, 
					const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_VEC& omega_ket);

//	The function CalculateRME_1 calculates rme. The resulting rmes are being stored in 
//	an empty SU3xSU2::RME structure, resultingRME. 
// 	NOTE: user is responsible for launching delete []resultingRME.m_rme when resultingRME is not needed
void CalculateRME_1(const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_VEC& omega_bra, 
					const std::vector<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_VEC& Omega_t, 
					const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_VEC& omega_ket, 
					SU3xSU2::RME& resultingRME);

//	The function CalculateRME_2 calculates rme. The resulting rmes are being stored in 
//	an empty SU3xSU2::RME structure, resultingRME. 
// 	NOTE: user is responsible for launching delete []resultingRME.m_rme when resultingRME is not needed
void CalculateRME_2(const UN::SU3xSU2_VEC& gamma_bra, 			const SU3xSU2_VEC& omega_bra, 
					const std::vector<SU3xSU2::RME*>& Gamma_t, 	const SU3xSU2_VEC& Omega_t, 
					const UN::SU3xSU2_VEC& gamma_ket, 			const SU3xSU2_VEC& omega_ket, 
					SU3xSU2::RME& resultingRME);

int rme_uncoupling_phase(const std::vector<unsigned char>& bra, const std::vector<unsigned char>& tensor, const std::vector<unsigned char>& ket);
#endif


#ifndef TENSORS_h
#define TENSORS_h
#include <SU3ME/global_definitions.h>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <SU3ME/CState.h>
#include <map>
#include <cassert>

#include <Eigen/Core>
#include <Eigen/LU>

namespace SU3xSU2 {

typedef short SPS_INDEX;

class Tensor: public SU3xSU2::LABELS 
{
protected:	
	inline static SPS_INDEX CreationTensorToSpsIndex(const SU3xSU2::CANONICAL& ad) {return ad.Sigma*((ad.LM * ad.LM - ad.M_LM)/2 + ad.LM + 1);}
	inline static SPS_INDEX AnnihilationTensorToSpsIndex(const SU3xSU2::CANONICAL& ta) {return -1*ta.Sigma*((ta.LM * ta.LM + ta.M_LM)/2 + ta.LM + 1);}
#ifdef HECHT
//	tilde{a}^{(0\,n)1/2}_{\epsilon \Lambda \M_{\Lambda} \sigma} = (-)^{\Lambda - \M_{\Lambda} + 1/2 + \sigma} a_{n -\epsilon \Lambda -M_{\Lambda} -\sigma}	
	inline static char AnnihilationPhase(const SU3xSU2::CANONICAL& ta) { return (((ta.LM - ta.M_LM + 1 + ta.Sigma)/2) % 2 ? -1 : 1);}
#elif defined DRAAYER
//	tilde{a}^{(0\,n)1/2}_{\epsilon \Lambda \M_{\Lambda} \sigma} = (-)^{\Lambda + \M_{\Lambda} + 1/2 + \sigma} a_{n -\epsilon \Lambda -M_{\Lambda} -\sigma}	
	inline static char AnnihilationPhase(const SU3xSU2::CANONICAL& ta) { return (((ta.LM + ta.M_LM + 1 + ta.Sigma)/2) % 2 ? -1 : 1);}
#endif	
protected:
//	TENSOR_COMPONENT_SINGLE_RHO is being used (as its name suggests) whenever
//	tensorial structure of a certain multiplicity is needed.  Class Tensor
//	stores (see TENSOR_COMPONENT_ALL_RHO) tensorial structure for all
//	multiplicities.
	typedef std::pair<const std::vector<double>*, const std::vector<SPS_INDEX>* > TENSOR_COMPONENT_SINGLE_RHO; 

//	TENSOR_COMPONENT_ALL_RHO.first contains vector of coefficients:
//	TENSOR_COMPONENT_ALL_RHO.first[irho][j] is coefficients that stands in
//	front of j-th term, whose sps indices ranges from
//	TENSOR_COMPONENT_ALL_RHO.second[m_nBasisOperators*j] up to
//	TENSOR_COMPONENT_ALL_RHO.second[m_nBasisOperators*j + m_nBasisOperators - 1]
	typedef std::pair<std::vector<std::vector<double> >, std::vector<SPS_INDEX> > TENSOR_COMPONENT_ALL_RHO; 

//	TENSOR_STORAGE is a map that associates SU3xSU2::CANONICAL labels with a TENSOR_COMPONENT_ALL_RHO structure.
//	Example: T^{rho}_{SU3xSU2::CANONICAL} 
//	coefficients are stored in m_TensorComponents[SU3xSU2::CANONICAL]->second.first[rho]
//	and indices at m_TensorComponents[SU3xSU2::CANONICAL]->second.second
	typedef std::map<SU3xSU2::CANONICAL, TENSOR_COMPONENT_ALL_RHO> TENSOR_STORAGE;

//	Used in construction of tensor for storing precalculated values of spin
//	CG's
	typedef std::pair<CTuple<U1::LABEL, 2> ,double> tSpinCG; // .first = {X.Sigma, Y.Sigma}; .second = dSpinCG

//	m_OperatorType == GENERAL if a given operator is composed out of both
//	creation and annihilation basis operators
	enum OperatorType {CREATION, ANNIHILATION, GENERAL} m_OperatorType;
protected:
//	maximal multiplicity of the tensor
	int m_max_rho;
//	number of creation/annihilation	operators that compose Tensor
	char m_nBasisOperators;
//	Contains a structure of tensor in terms of creation/annihilation operators
//	Example: ad_{n}ad_{n}ad_{n}ta_{n}ta_{n} would correspond to 
//	{n+1, n+1, n+1,	-(n+1), -(n+1)} array. Note that we add +1 in order do be 
//	able to distinguish between ad_{n=0} and ta_{n=0} operators.
	std::vector<char> m_Structure;
//	Position of the first annihilation operator in m_Structure.  If there are
//	no annihilation operators involved it is equal to m_annihilStart =	m_Structure.end();
	int m_annihilStart;
//	This structure holds tensorial components of a given operator
	TENSOR_STORAGE m_TensorComponents;
protected:
	Tensor(const SU3xSU2::LABELS& RhoLmMuS2, OperatorType Type); 

	void GenerateSpinCGs(const SU2::LABEL& S1, const SU2::LABEL& S2, const SU2::LABEL& S0, std::vector<std::vector<tSpinCG> >& SpinCGRepository) const;

	void Add(const Tensor& X, const size_t rhoX, const SU3xSU2::CANONICAL& Xlabels, 
			 const Tensor& Y, const size_t rhoY, const SU3xSU2::CANONICAL& Ylabels, 
			 const std::vector<double>& Coeff, 
			 TENSOR_COMPONENT_ALL_RHO& CurrentOperator) const; 

	TENSOR_COMPONENT_SINGLE_RHO Component(const size_t rho, const SU3xSU2::CANONICAL& Component) const;

	void GenerateComponents(const Tensor& X, const size_t rhoX, 	
							const Tensor& Y, const size_t rhoY, 
							const std::vector<SU3xSU2::CANONICAL>& Components2Costruct, 
							bool fDiagnostic = false);

	template <typename UN_BASIS_STATE> void Eliminator();

	template <typename UN_BASIS_STATE> void EvaluateME_new(	const SU3xSU2::CANONICAL& Component, 
														const CState<UN_BASIS_STATE>& Bra, 
														const CState<UN_BASIS_STATE>& Ket, 
														std::vector<double>& Me) const;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// NOTE that EvaluateME method assumes that Bra is sorted according to
// UN::CMP_BASIS_STATES<UN_BASIS_STATE>.operator(), which is apparent from the
// use of method lower_bound
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	Calculates full matrix elements of operator with tensorial component
//	"Component" between "Bra" and "Ket", for ALL multiplicities rho_{0} of the
//	tensor, ALL (outer) multiplicities of "Bra" and ALL (outer) multiplicities
//	of "Ket". The resulting vector "Me" thus shall be three-dimensional. But
//	instead we store results in one-dimensional array of the following
//	structure:
//
//	< mult_{bra} Bra| T^{rho_{0}}_{Component} | mult_{ket} Ket> is stored in
//	Me[ipos], where
//	ipos = rho_{0}*(max_mult_ket*max_mult_bra) + mult_{bra}*max_mult_ket + mult_{ket}
	template <typename UN_BASIS_STATE> void EvaluateME(	const SU3xSU2::CANONICAL& Component, 
														const CState<UN_BASIS_STATE>& Bra, 
														const CState<UN_BASIS_STATE>& Ket, 
														std::vector<double>& Me) const;
public:
//	Construct ALL COMPONENTS of the tensor ResultXY by coupling tensor X^{rhoX}
//	with Y^{rhoY}. Note that ALL MULTIPLICITIES that emerge from coupling (X x
//	Y) are produced and stored in m_TensorComponents.
	Tensor(	const Tensor& X, const size_t rhoX, 
			const Tensor& Y, const size_t rhoY, 
			const SU3xSU2::LABELS& ResultXY, 
			bool fDiagnostic = false);
//	Construct only a certain components of the tensor ResultXY (provided in
//	vector "Components2Costruct") by coupling tensor X^{rhoX} with Y^{rhoY}.
//	Note that ALL MULTIPLICITIES that emerge from coupling (X x Y) are produced
//	and stored in m_TensorComponents.
	Tensor(	const Tensor& X, const size_t rhoX, 
			const Tensor& Y, const size_t rhoY, 
			const SU3xSU2::LABELS& ResultXY, 
			const std::vector<SU3xSU2::CANONICAL>& Components2Costruct, 
			bool fDiagnostic = false); 

	inline unsigned char GetNBasisOperators() const {return m_nBasisOperators;}
	inline size_t GetHOShell() const {return abs(m_Structure[0])-1;}
	size_t GetdA() const;
	size_t GetNumberOfAnnihilators() const 
	{
		assert(m_Structure.front() >= m_Structure.back()); // stops program if we have e.g. {-n, -n, +n, +n}
		return (m_Structure.size() - m_annihilStart);
	}

	char operator[](size_t i) const {return m_Structure[i];}
	inline size_t GetMaxRho() { return m_max_rho;}

	void ShowTensorComponents();
	void ShowTensorComponent(const SU3xSU2::CANONICAL& TensorComponent);
	void ShowTermIndices(const SU3xSU2::CANONICAL& TensorComponent, size_t iterm);

	void EliminateRedundancy();
//	In the future one may consider to generate HWS using symmetrically coupled
//	creation operators. In such a case this method will become handy. Otherwise
//	it does not serve any purpose.
	void RenormalizeTensorComponents();
	inline bool IsPureType() // returns TRUE if the Tensor is composed exclusively out of "pure" type  == {a+a+ ... a+} or all {tata ... ta} creation/annihilation operators
	{
		return (m_nBasisOperators == std::count(m_Structure.begin(), m_Structure.end(), m_Structure[0]));
	}

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	Reduced matrix elements are stored in a two-dimensional array rme.
//	Example:
//  < mult_{bra} Bra ||| T^{rho_{0}} ||| mult_{ket} Ket>_{rho}
//  is stored in rme[ipos][rho], where
//	ipos = rho_{0}*(max_mult_ket*max_mult_bra) + mult_{bra}*max_mult_ket + mult_{ket}
//	where 
//		rho_{0} 	\in <0, ..., Tensor::m_max_rho) 
//		rho 		\in <0, ..., max_rho),	where max_rho is a maximal multiplicity in coupling (ket x Tensor) --> Bra 
//											and it is being returned by CalculateRME.
//		mult_{bra}	\in <0, ..., Bra::max_mult)
//		mult_{ket}	\in <0, ..., Ket::max_mult)
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	template <typename UN_BASIS_STATE>  size_t CalculateRME(	const SU3xSU2::LABELS& irBra, const CState<UN_BASIS_STATE>& BraHws, 
																const SU3xSU2::LABELS& irKet, const CState<UN_BASIS_STATE>& KetHws, 
																std::vector<std::vector<double> >& rme) const;
	template <typename UN_BASIS_STATE>  void CalculateRME(	const SU3xSU2::LABELS& irBra, const CState<UN_BASIS_STATE>& BraHws, 
															const SU3xSU2::LABELS& irKet, const CState<UN_BASIS_STATE>& KetHws, 
															std::vector<double>& rme) const;

	template <typename UN_BASIS_STATE>  size_t CalculateRME(const UN::SU3xSU2& irBra, const CState<UN_BASIS_STATE>& BraHws, 
															const UN::SU3xSU2& irKet, const CState<UN_BASIS_STATE>& KetHws,
															std::vector<double>& rme) const;
};


class Creation: public Tensor {
public:
	Creation(size_t n);
};

class Annihilation: public Tensor {
public:
	Annihilation(size_t n);
};


template <typename UN_BASIS_STATE> 
void Tensor::EvaluateME_new(const SU3xSU2::CANONICAL& Component, 
							const CState<UN_BASIS_STATE>& Bra, 
							const CState<UN_BASIS_STATE>& Ket, 
							std::vector<double>& Me) const
{
	size_t NSPS = Bra.m_State[0].first.size();
	size_t max_mult_bra = Bra.GetMult();
	size_t max_mult_ket = Ket.GetMult();
	size_t maxMultBraKet = max_mult_bra*max_mult_ket;
//	allocate space needed to store all matrix elements
	Me.resize(m_max_rho*maxMultBraKet, 0.0);
	
	TENSOR_STORAGE::const_iterator TensorComponent = m_TensorComponents.find(Component);
	if (TensorComponent == m_TensorComponents.end())
	{
		std::cerr << "Error in Tensor::EvaluateME ... tensorial component " << Component << " was not found." << std::endl;
		exit(EXIT_FAILURE);
	}
	UN::BASIS_STATES_EQUAL<UN_BASIS_STATE> BASIS_STATES_EQUAL;
	const std::vector<std::vector<double> >& TensorCoeffs = (TensorComponent->second).first;
	const std::vector<SPS_INDEX>& TensorIndices = (TensorComponent->second).second;
	typename std::vector<UN_BASIS_STATE>::const_iterator BraStatesBEGIN = Bra.m_State.begin();
	typename std::vector<UN_BASIS_STATE>::const_iterator BraStatesEND = Bra.m_State.end();
	typename std::vector<UN_BASIS_STATE>::const_iterator BraStateIter;
	size_t nTensorTerms = TensorIndices.size()/m_nBasisOperators;
	size_t nKetTerms = Ket.m_State.size();
//	ipos ... location of matrix elements in vector Me
//	it is calculated as: ipos = irho*max_mult_bra*max_mult_ket + ibra_term*max_mult_ket + iket_term;
	size_t itensor_term, iket_term, k, irho, ipos, isps, ibra_term, ibit, mult_ket, mult_bra;
	int phase;
	double dcoeff;
	UN_BASIS_STATE State;

//	iterate over terms in a given tensor components
	for (itensor_term = 0; itensor_term < nTensorTerms; ++itensor_term)
	{
//	iterates over states in Ket
		for (iket_term = 0; iket_term < nKetTerms; ++iket_term)
		{
			State = Ket.m_State[iket_term];
			phase = 0;
//	Iterate over the annihilation operators from the right to the left. Note
//	that k is not used to access arrays. It is a mere counting index.  if the
//	loop ends without "break" (i.e. without trying to annihilate unoccupied
//	sps), TensorIndices[isps] points to the most right creation operator.
			for (k = m_nBasisOperators, isps = (itensor_term + 1)*m_nBasisOperators - 1; k > m_annihilStart; --k, --isps)
			{
				if (TensorIndices[isps] > 0) // annihilation takes place in the spin up sector
				{
					ibit = TensorIndices[isps] - 1;
					if (State.first.test(ibit)) 
					{
#ifdef FRONT_CREATE
						phase += (typename UN_BASIS_STATE::first_type(State.first) <<= (NSPS - ibit)).count();
#elif defined(REAR_CREATE)
						phase +=  State.second.count() + (typename UN_BASIS_STATE::first_type(State.first) >>= (ibit + 1)).count();
#endif						
				        State.first.set(ibit, 0);
					} 
					else   // sps ibit is not occupied and hence the result is vaccum => we skip this state
					{
						break;
					}
				}
				else // annihilation takes place in the spin down sector
				{
					ibit = -1 - TensorIndices[isps];
					if (State.second.test(ibit))
					{
#ifdef FRONT_CREATE
//	When annihilating sps in spin-down segment we jump over all occupied
//	spin-up sps.  and hence "State.first.count()" needs to be added to the
//	phase.
						phase += (typename UN_BASIS_STATE::second_type(State.second) <<= (NSPS - ibit)).count() +  State.first.count();
#elif defined(REAR_CREATE)
						phase += (typename UN_BASIS_STATE::second_type(State.second) >>= (ibit + 1)).count();
#endif
				        State.second.set(ibit, 0);
					}
					else  // sps ibit is not occupied and hence the result is vaccum => we skip this state
					{
						break;
					}
				}
			}
// 	This condition is true only if the previous loop has been "broken" which
// 	happens when trying to annihilate unoccupied sps state
			if (m_OperatorType == GENERAL && k > m_annihilStart)
			{
				continue; // skip the rest of the current iket_term	loop iteration
			}
//	iterate over the creation operators from the right to the left
//	TensorIndices[isps] in the beginning points to the most right creation
//	operator
			for ( ; k > 0; --k, --isps)
			{
				if (TensorIndices[isps] > 0) // spin up
				{
					ibit = TensorIndices[isps] - 1;
					if (!State.first.test(ibit)) 
					{
#ifdef FRONT_CREATE
						phase += (typename UN_BASIS_STATE::first_type(State.first) <<= (NSPS - ibit)).count();
#elif defined(REAR_CREATE)
						phase +=  State.second.count() + (typename UN_BASIS_STATE::first_type(State.first) >>= (ibit + 1)).count();
#endif
				        State.first.set(ibit, 1);
					} 
					else 
					{
						break;
					}
				}
				else // spin down
				{
					ibit = -1 - TensorIndices[isps];
					if (!State.second.test(ibit))
					{
#ifdef FRONT_CREATE
						phase += (typename UN_BASIS_STATE::first_type(State.second) <<= (NSPS - ibit)).count() + State.first.count();
#elif defined(REAR_CREATE)
						phase += (typename UN_BASIS_STATE::first_type(State.second) >>= (ibit + 1)).count();
#endif
				        State.second.set(ibit, 1);
					}
					else 
					{
						break;
					}
				}
			}
			if (k == 0) // state has not been annihilated => calculate its contribution to the resulting me
			{
//	once again: here I assume that Bra is sorted according to
//	UN::CMP_BASIS_STATES<UN_BASIS_STATE>.operator(), which is apparent from the
//	use of method lower_bound
				BraStateIter = std::lower_bound(BraStatesBEGIN, BraStatesEND, State, UN::CMP_BASIS_STATES<UN_BASIS_STATE>());
//	The following condition is true if State was found between Bra states
				if (BraStateIter != BraStatesEND && BASIS_STATES_EQUAL(*BraStateIter, State))
				{
					ibra_term = BraStateIter - BraStatesBEGIN;
					phase = (phase % 2) ? -1 : 1;
					for (mult_ket = 0; mult_ket < max_mult_ket; ++mult_ket)
					{
						for (mult_bra = 0; mult_bra < max_mult_bra; ++mult_bra)
						{
							dcoeff = phase*Bra.m_Coeffs[mult_bra][ibra_term]*Ket.m_Coeffs[mult_ket][iket_term];
							for (irho = 0, ipos = mult_bra*max_mult_ket + mult_ket; irho < m_max_rho; ++irho, ipos += maxMultBraKet)
							{
								Me[ipos] += TensorCoeffs[irho][itensor_term]*dcoeff;
							}
						}
					}
				}
			}
		}
	}
}


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// NOTE that EvaluateME method assumes that Bra is sorted according to
// UN::CMP_BASIS_STATES<UN_BASIS_STATE>.operator(), which is apparent from the
// use of method lower_bound
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//	Structure of Me:
//
//	<mult_{bra} Bra | T^{rho} | mult_{ket} Ket> = Me[ipos], 
//	where
//	ipos = rho*(max_mult_bra * max_mult_ket) + mult_bra*max_mult_ket + mult_ket
//
template <typename UN_BASIS_STATE> 
void Tensor::EvaluateME(	const SU3xSU2::CANONICAL& Component, 
							const CState<UN_BASIS_STATE>& Bra, 
							const CState<UN_BASIS_STATE>& Ket, 
							std::vector<double>& Me) const
{
#ifdef REAR_CREATE	
	size_t NSPS = Bra.m_State[0].first.size();
#endif	
	size_t max_mult_bra = Bra.GetMult();
	size_t max_mult_ket = Ket.GetMult();
	size_t maxMultBraKet = max_mult_bra*max_mult_ket;
//	allocate space needed to store all matrix elements
	Me.resize(m_max_rho*maxMultBraKet, 0.0);
	
	TENSOR_STORAGE::const_iterator TensorComponent = m_TensorComponents.find(Component);
	if (TensorComponent == m_TensorComponents.end())
	{
		std::cerr << "Error in Tensor::EvaluateME ... tensorial component " << Component << " was not found." << std::endl;
		exit(EXIT_FAILURE);
	}
	UN::BASIS_STATES_EQUAL<UN_BASIS_STATE> BASIS_STATES_EQUAL;
	const std::vector<std::vector<double> >& TensorCoeffs = (TensorComponent->second).first;
	const std::vector<SPS_INDEX>& TensorIndices = (TensorComponent->second).second;
	typename std::vector<UN_BASIS_STATE>::const_iterator BraStatesBEGIN = Bra.m_State.begin();
	typename std::vector<UN_BASIS_STATE>::const_iterator BraStatesEND = Bra.m_State.end();
	typename std::vector<UN_BASIS_STATE>::const_iterator BraStateIter;
	size_t nTensorTerms = TensorIndices.size()/m_nBasisOperators;
	size_t nKetTerms = Ket.m_State.size();
//	ipos ... location of matrix elements in vector Me
//	it is calculated as: ipos = irho*max_mult_bra*max_mult_ket + ibra_term*max_mult_ket + iket_term;
	size_t itensor_term, iket_term, k, irho, ipos, isps, ibra_term, ibit, mult_ket, mult_bra;
	int phase;
	double dcoeff;
	UN_BASIS_STATE State;

//	iterate over terms in a given tensor components
	for (itensor_term = 0; itensor_term < nTensorTerms; ++itensor_term)
	{
//	iterates over states in Ket
		for (iket_term = 0; iket_term < nKetTerms; ++iket_term)
		{
			State = Ket.m_State[iket_term];
			phase = 0;
//	Iterate over the annihilation operators from the right to the left. Note
//	that k is not used to access arrays. It is a mere counting index.  if the
//	loop ends without "break" (i.e. without trying to annihilate unoccupied
//	sps), TensorIndices[isps] points to the most right creation operator.
			for (k = m_nBasisOperators, isps = (itensor_term + 1)*m_nBasisOperators - 1; k > m_annihilStart; --k, --isps)
			{
				if (TensorIndices[isps] > 0) // annihilation takes place in the spin up sector
				{
					ibit = TensorIndices[isps] - 1;
					if (State.first.test(ibit)) 
					{
//	One should realize that when annihilating sps in spin-up segment we jump
//	over all occupied spin-down sps.  and hence "State.second.count()" needs to
//	be added to the phase.
#ifdef FRONT_CREATE
						phase += (typename UN_BASIS_STATE::first_type(State.first) >>= (ibit + 1)).count();
#elif defined(REAR_CREATE)
						phase += (typename UN_BASIS_STATE::first_type(State.first) <<= (NSPS - ibit)).count() + State.second.count();
#endif						
				        State.first.set(ibit, 0);
					} 
					else   // sps ibit is not occupied and hence the result is vaccum => we skip this state
					{
						break;
					}
				}
				else // annihilation takes place in the spin down sector
				{
					ibit = -1 - TensorIndices[isps];
					if (State.second.test(ibit))
					{
#ifdef FRONT_CREATE
						phase += (typename UN_BASIS_STATE::second_type(State.second) >>= (ibit + 1)).count() +  State.first.count();
#elif defined(REAR_CREATE)
						phase += (typename UN_BASIS_STATE::second_type(State.second) <<= (NSPS - ibit)).count();
#endif
				        State.second.set(ibit, 0);
					}
					else  // sps ibit is not occupied and hence the result is vaccum => we skip this state
					{
						break;
					}
				}
			}
// 	This condition is true only if the previous loop has been "broken" which
// 	happens when trying to annihilate unoccupied sps state
			if (m_OperatorType == GENERAL && k > m_annihilStart)
			{
				continue; // skip the rest of the current iket_term	loop iteration
			}
//	iterate over the creation operators from the right to the left
//	TensorIndices[isps] in the beginning points to the most right creation
//	operator
			for ( ; k > 0; --k, --isps)
			{
				if (TensorIndices[isps] > 0) // spin up
				{
					ibit = TensorIndices[isps] - 1;
					if (!State.first.test(ibit)) 
					{
#ifdef FRONT_CREATE
						phase += (typename UN_BASIS_STATE::first_type(State.first) >>= (ibit + 1)).count();
#elif defined(REAR_CREATE)
						phase += (typename UN_BASIS_STATE::first_type(State.first) <<= (NSPS - ibit)).count() + State.second.count();
#endif
				        State.first.set(ibit, 1);
					} 
					else 
					{
						break;
					}
				}
				else // spin down
				{
					ibit = -1 - TensorIndices[isps];
					if (!State.second.test(ibit))
					{
#ifdef FRONT_CREATE
						phase += (typename UN_BASIS_STATE::first_type(State.second) >>= (ibit + 1)).count() + State.first.count();
#elif defined(REAR_CREATE)
						phase += (typename UN_BASIS_STATE::first_type(State.second) <<= (NSPS - ibit)).count();
#endif
				        State.second.set(ibit, 1);
					}
					else 
					{
						break;
					}
				}
			}
			if (k == 0) // state has not been annihilated => calculate its contribution to the resulting me
			{
//	once again: here I assume that Bra is sorted according to
//	UN::CMP_BASIS_STATES<UN_BASIS_STATE>.operator(), which is apparent from the
//	use of method lower_bound
				BraStateIter = std::lower_bound(BraStatesBEGIN, BraStatesEND, State, UN::CMP_BASIS_STATES<UN_BASIS_STATE>());
//	The following condition is true if State was found between Bra states
				if (BraStateIter != BraStatesEND && BASIS_STATES_EQUAL(*BraStateIter, State))
				{
					ibra_term = BraStateIter - BraStatesBEGIN;
					phase = (phase % 2) ? -1 : 1;
					for (mult_ket = 0; mult_ket < max_mult_ket; ++mult_ket)
					{
						for (mult_bra = 0; mult_bra < max_mult_bra; ++mult_bra)
						{
							dcoeff = phase*Bra.m_Coeffs[mult_bra][ibra_term]*Ket.m_Coeffs[mult_ket][iket_term];
							for (irho = 0, ipos = mult_bra*max_mult_ket + mult_ket; irho < m_max_rho; ++irho, ipos += maxMultBraKet)
							{
								Me[ipos] += TensorCoeffs[irho][itensor_term]*dcoeff;
							}
						}
					}
				}
			}
		}
	}
}

template <typename UN_BASIS_STATE>  
size_t Tensor::CalculateRME(const SU3xSU2::LABELS& irBra, const CState<UN_BASIS_STATE>& BraHws, 
							const SU3xSU2::LABELS& irKet, const CState<UN_BASIS_STATE>& KetHws,
							std::vector<std::vector<double> >& rme) const
{
//	std::cout << "Obsolete method Tensor::CalculateRME(SU3xSU2::LABELS, CState, SU3xSU2::LABELS, CState, vector<vector<double> >)" << std::endl;

	assert(m_OperatorType == GENERAL);
	if (!SU2::mult(irKet.S2, S2, irBra.S2))
	{
		std::cerr << "Error in Tensor::CalculateRME  intrinsic spin does not couple (";
		std::cerr << (int)irKet.S2 << "/2 x " << (int)S2 << "/2 -> " << (int)irBra.S2 << "/2)" << std::endl;
		exit(EXIT_FAILURE);
	}
	if (!SU3::mult(irKet, *this, irBra))
	{
		std::cerr << "Error in Tensor::CalculateRME - Coupling (";
		std::cerr << (int)irKet.lm << " " << (int)irKet.mu << ") x (" << (int)lm << " " << (int)mu << ") -> (";
		std::cerr << (int)irBra.lm << " " << (int)irBra.mu << ") is not possible." << std::endl;
		exit(EXIT_FAILURE);
	}
	size_t max_mult_bra = BraHws.GetMult();
	size_t max_mult_ket = KetHws.GetMult();
	size_t maxMultBraKet = max_mult_bra*max_mult_ket;

//	Calculate additive quantum labels of the tensor components whose full
//	matrix elements between BraHws and KetHws we need to evaluate in order to
//	get reduced matrix elements.
	U1::LABEL eps0 		= 2*irBra.lm + irBra.mu - (2*irKet.lm + irKet.mu);	//	eps0 = Bra::eps_{hw} - Ket::eps_{hw}
	U1::LABEL M_LM0 	= irBra.mu - irKet.mu;								//	M_LM0 = Bra::M_{LM hw} - Ket::M_{LM hw}
	U1::LABEL Sigma0 	= irBra.S2 - irKet.S2;								//	Sigma0 = Bra::Sigma_{hw} - Ket::Sigma_{hw}

//	Vector LM0s contains allowed values of \Lambda_{0} for a given eps0. Each
//	value gives rise to one linear equation.
	std::vector<SU2::LABEL> LM0s;

//	SU(3) CG coefficient <irKet_{hw} Tensor eps0 LM0 || irBra_{hw}>_{rho} is
//	stored as vSU3CGs[rho][irow], where LM0 = LM0s[irow]
	std::vector<std::vector<double> > vSU3CGs;
	CSU3CGMaster SU3CGFactory;
//	ncols is equal to the maximal multiplicity in coupling (Ket x Tensor) -> Bra
//	which in turn corresponds to maximal multiplicity rho in reduced matrix
//	elements < Bra ||| T ||| Ket>_{rho}
	size_t ncols = SU3CGFactory.RMESU2U1(irKet, *this, irBra, LM0s, vSU3CGs);  // calculate <Ket 0 || Bra>_{rho}
//	Number of columns must be less or equal to the number of LM_{0} quantum labels 
	assert(ncols <= LM0s.size());
//	Generally, nrow >= ncols, but to find rme's we need only ncols linear euqations
	size_t nrows = ncols; 

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	Reduced matrix elements are stored in a two-dimensional array rme.
//	Example:
//  < mult_{bra} Bra ||| T^{rho_{0}} ||| mult_{ket} Ket>_{rho}
//  is stored in rme[ipos][rho], where
//	ipos = rho_{0}*(max_mult_ket*max_mult_bra) + mult_{bra}*max_mult_ket + mult_{ket}
//	where 
//		rho_{0} 	\in <0, ..., m_max_rho) 
//		rho 		\in <0, ..., ncols) 
//		mult_{bra}	\in <0, ..., max_mult_bra)
//		mult_{ket}	\in <0, ..., max_mult_ket)
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//TODO: if ncols << maxMultBraKet*m_max_rho then I may consider to switch 
//		rme.resize(ncols, std::vector<double>(maxMultBraKet*m_max_rho, 0.0));
//		but right now I think this order is more "user friendly" and memory
//		may not be an issue
	rme.resize(maxMultBraKet*m_max_rho, std::vector<double>(ncols, 0.0));

	size_t irow, icol, mult_ket, mult_bra, irho, ipos;
	SU3xSU2::CANONICAL TensorComponent;
//	FullMe is a two-dimensional vector with full matrix elements.
//	Example:
//	< mult_{bra} Bra| T^{rho_{0}_{LM0s[irow]} | mult_{ket} Ket> = FullMe[irow][ipos], 
//	where
//	ipos = rho_{0}*(max_mult_ket*max_mult_bra) + mult_{bra}*max_mult_ket + mult_{ket}
	std::vector<std::vector<double> > FullMe(nrows);

	double dSU2CGs;
	double dSpinCG = clebschGordan(irKet.S2, irKet.S2, S2, Sigma0, irBra.S2, irBra.S2); // C_{Ket 0}^{Bra}
	if (Negligible(dSpinCG)) 
	{
		std::cerr << "Error: spin CG = 0" << std::endl;
		exit(EXIT_FAILURE);
	}

//	Create matrix A 
	Eigen::MatrixXd A((int)nrows, (int)ncols);
//	Prepare vectors
	Eigen::VectorXd x((int)nrows); 
	Eigen::VectorXd b((int)nrows); 
//	Construct matrix - it is the same for all irho, alpha_{bra}, alpha_{ket}
	for (irow = 0; irow < nrows; ++irow) 
	{
#if defined(HECHT)		
		dSU2CGs = clebschGordan(irKet.mu,  irKet.mu, LM0s[irow],  M_LM0, irBra.mu,  irBra.mu)*dSpinCG;
#elif defined(DRAAYER)
		dSU2CGs = clebschGordan(irKet.mu, -irKet.mu, LM0s[irow], -M_LM0, irBra.mu, -irBra.mu)*dSpinCG;
#endif		

		for (icol = 0; icol < ncols; ++icol) // icol corresponds to multiplicity rho  <bra 0 || ket>_{rho} 
		{
			A(irow, icol) = dSU2CGs*vSU3CGs[icol][irow]; 
		}
	}
//	perform LU decomposition of A
	Eigen::FullPivLU<Eigen::MatrixXd> luOfA(A);

	TensorComponent.eps	= eps0;
	TensorComponent.M_LM	= M_LM0;
	TensorComponent.Sigma	= Sigma0;
//	calculate full matrix elements which compose right hand side of the system
//	of linear equations
	for (irow = 0; irow < nrows; ++irow)
	{
		TensorComponent.LM = LM0s[irow];
		EvaluateME_new(TensorComponent, BraHws, KetHws, FullMe[irow]);
//		EvaluateME(TensorComponent, BraHws, KetHws, FullMe[irow]);
	}

//	For all outer multiplicities in Bra and Ket hws and for all rho_{0} of the
//	tensor calculate rme
//	iterate over outer multiplicities of Ket hws
	for (mult_ket = 0; mult_ket < max_mult_ket; ++mult_ket)
	{
//	iterate over outer multiplicities of Bra hws
		for (mult_bra = 0; mult_bra < max_mult_bra; ++mult_bra)
		{
//	iterate over all possible multiplicities rho_{0} of the tensor
			for (irho = 0, ipos = mult_bra*max_mult_ket + mult_ket; irho < m_max_rho; ++irho, ipos += maxMultBraKet)
			{
//	clean vector x				
				x.setZero((int)nrows);
//	generate right hand side of a system of linear equations (vector b)				
				for (irow = 0; irow < nrows; ++irow)
				{
					b(irow) = FullMe[irow][ipos];
				}
				x = luOfA.solve(b);
				for(icol = 0; icol < ncols; ++icol)
				{
					rme[ipos][icol] = x(icol);
				}
			}
		}
	}
	return ncols; // return maximal multiplicity in (ket x Tensor) ---> bra, i.e. <ket tensor || bra>_{rho}
}

template <typename UN_BASIS_STATE>  
void Tensor::CalculateRME(const SU3xSU2::LABELS& irBra, const CState<UN_BASIS_STATE>& BraHws, 
							const SU3xSU2::LABELS& irKet, const CState<UN_BASIS_STATE>& KetHws, 
							std::vector<double>& rme) const
{
//	std::cout << "Obsolete method Tensor::CalculateRME(SU3xSU2::LABELS, CState, SU3xSU2::LABELS, CState, vector<vector<double> >)" << std::endl;

	assert(m_OperatorType == CREATION || m_OperatorType == ANNIHILATION);
	if (!SU2::mult(irKet.S2, 1, irBra.S2))
	{
		std::cerr << "Error in Tensor::CalculateRME  intrinsic spin does not couple (";
		std::cerr << (int)irKet.S2 << "/2 x " << (int)S2 << "/2 -> " << (int)irBra.S2 << "/2)" << std::endl;
		exit(EXIT_FAILURE);
	}
	if (SU3::mult(irKet, *this, irBra) != 1)
	{
		std::cerr << "Error in Tensor::CalculateRME - Coupling. Either:" << std::endl;
		std::cerr << "(a)  (";
		std::cerr << (int)irKet.lm << " " << (int)irKet.mu << ") x (" << (int)lm << " " << (int)mu << ") -> (";
		std::cerr << (int)irBra.lm << " " << (int)irBra.mu << ") is not possible." << std::endl;
		std::cerr << "(b)  (";
		std::cerr << (int)irKet.lm << " " << (int)irKet.mu << ") x (" << (int)lm << " " << (int)mu << ") -> (";
		std::cerr << (int)irBra.lm << " " << (int)irBra.mu << ") has multiplicity > 1 " << std::endl;
		exit(EXIT_FAILURE);
	}
	size_t max_mult_bra = BraHws.GetMult();
	size_t max_mult_ket = KetHws.GetMult();
	size_t maxMultBraKet = max_mult_bra*max_mult_ket;
	size_t mult_ket, mult_bra, ipos;
//	Calculate full matrix elements:
//	(a) enumerate tensor component that connects Ket_hws with Bra_hws
	SU3xSU2::CANONICAL TensorComponent;
	TensorComponent.eps 	=  2*irBra.lm + irBra.mu - (2*irKet.lm + irKet.mu);	//	eps0 = Bra::eps_{hw} - Ket::eps_{hw}
	TensorComponent.LM 	= (((m_OperatorType == CREATION) ? (2*lm - TensorComponent.eps) :  (2*mu + TensorComponent.eps)))/3;
	TensorComponent.M_LM = irBra.mu - irKet.mu;								//	M_LM0 = Bra::M_{LM hw} - Ket::M_{LM hw}
	TensorComponent.Sigma = irBra.S2 - irKet.S2;								//	Sigma0 = Bra::Sigma_{hw} - Ket::Sigma_{hw}
//	(b) FullMe.size() == BraHws.GetMult()*KetHws.GetMult()
	std::vector<double> FullMe;
//	(c) Calculate <alpha_{bra} bra hws_bra| a^{dagger}_{TensorComponent} |alpha_{ket} ket hws_ket>
//
	EvaluateME_new(TensorComponent, BraHws, KetHws, FullMe);
//	EvaluateME(TensorComponent, BraHws, KetHws, FullMe);

	std::vector<std::vector<double> > vSU3CGs;
	std::vector<SU2::LABEL> LM0s;
	CSU3CGMaster SU3CGFactory;
// 	Get SU(3) C-G coefficients that are needed for r.m.e calculation, i.e.
//	< irKet epsKet_{hw} LMKet_{hw}; (n 0) eps0 LM0 || irBra epsBra_{hw} LMBra_{hw}>, where 
	SU3CGFactory.RMESU2U1(irKet, *this, irBra, LM0s, vSU3CGs);  // calculate <Ket 0 || Bra>_{rho}

	assert(LM0s.size() 			== 1);
	assert(LM0s[0] == TensorComponent.LM);
	assert(vSU3CGs.size() 		== 1);
	assert(vSU3CGs[0].size() 	== 1);

	double dSpinCG = clebschGordan(irKet.S2, irKet.S2, 1, TensorComponent.Sigma, irBra.S2, irBra.S2);
	
#if defined(HECHT)
	double dSU2CGs = clebschGordan(irKet.mu,  irKet.mu, TensorComponent.LM,  TensorComponent.M_LM, irBra.mu,  irBra.mu)*dSpinCG;
#elif defined(DRAAYER)
	double dSU2CGs = clebschGordan(irKet.mu, -irKet.mu, TensorComponent.LM, -TensorComponent.M_LM, irBra.mu, -irBra.mu)*dSpinCG;
#endif
	double dCoeff = dSU2CGs*vSU3CGs[0][0];

	rme.resize(maxMultBraKet); 
	for (ipos = 0, mult_bra = 0; mult_bra < max_mult_bra; ++mult_bra)
	{
		for (mult_ket = 0; mult_ket < max_mult_ket; ++mult_ket, ++ipos)
		{
			rme[ipos] = FullMe[ipos] / dCoeff;
		}
	}
}


template <typename UN_BASIS_STATE>  
size_t Tensor::CalculateRME(const UN::SU3xSU2& irBra, const CState<UN_BASIS_STATE>& BraHws, 
							const UN::SU3xSU2& irKet, const CState<UN_BASIS_STATE>& KetHws,
							std::vector<double>& rme) const
{
	if (!SU2::mult(irKet.S2, S2, irBra.S2))
	{
		std::cerr << "Error in Tensor::CalculateRME  intrinsic spin does not couple (";
		std::cerr << (int)irKet.S2 << "/2 x " << (int)S2 << "/2 -> " << (int)irBra.S2 << "/2)" << std::endl;
		exit(EXIT_FAILURE);
	}
	if (!SU3::mult(irKet, *this, irBra))
	{
		std::cerr << "Error in Tensor::CalculateRME - Coupling (";
		std::cerr << (int)irKet.lm << " " << (int)irKet.mu << ") x (" << (int)lm << " " << (int)mu << ") -> (";
		std::cerr << (int)irBra.lm << " " << (int)irBra.mu << ") is not possible." << std::endl;
		exit(EXIT_FAILURE);
	}

	size_t max_mult_bra = irBra.mult;
	assert(max_mult_bra == BraHws.GetMult());
	size_t max_mult_ket = irKet.mult; 
	assert(max_mult_ket == KetHws.GetMult());
	size_t maxMultBraKet = max_mult_bra*max_mult_ket;

//	SU(3) CG coefficient <irKet_{hw} Tensor eps0 LM0 || irBra_{hw}>_{rho} is
//	stored as vSU3CGs[rho][irow], where LM0 = LM0s[irow]
	std::vector<std::vector<double> > vSU3CGs;
	CSU3CGMaster SU3CGFactory;

	if (m_OperatorType == GENERAL)
	{
//	Calculate additive quantum labels of the tensor components whose full
//	matrix elements between BraHws and KetHws we need to evaluate in order to
//	get reduced matrix elements.
		U1::LABEL eps0 		= 2*irBra.lm + irBra.mu - (2*irKet.lm + irKet.mu);	//	eps0 = Bra::eps_{hw} - Ket::eps_{hw}
		U1::LABEL M_LM0 	= irBra.mu - irKet.mu;								//	M_LM0 = Bra::M_{LM hw} - Ket::M_{LM hw}
		U1::LABEL Sigma0 	= irBra.S2 - irKet.S2;								//	Sigma0 = Bra::Sigma_{hw} - Ket::Sigma_{hw}

//	Vector LM0s contains allowed values of \Lambda_{0} for a given eps0. Each
//	value gives rise to one linear equation.
		std::vector<SU2::LABEL> LM0s;
//	ncols is equal to the maximal multiplicity in coupling (Ket x Tensor) -> Bra
//	which in turn corresponds to maximal multiplicity rho in reduced matrix
//	elements < Bra ||| T ||| Ket>_{rho}
		size_t ncols = SU3CGFactory.RMESU2U1(irKet, *this, irBra, LM0s, vSU3CGs);  // calculate <Ket 0 || Bra>_{rho}
//	Number of columns must be less or equal to the number of LM_{0} quantum labels 
		assert(ncols <= LM0s.size());
//	Generally, nrow >= ncols, but to find rme's we need only ncols linear euqations
		size_t nrows = ncols; 

//	Reduced matrix elements are stored in a one-dimensional array rme.
//	Example:
//  < mult_{bra} Bra ||| T^{rho_{0}} ||| mult_{ket} Ket>_{rho}
//  is stored in rme[ipos][rho], where
//	index_rme = mult_{bra}*(max_mult_ket*rho0max*rhotmax) + mult_{ket}*(rho0max*rhotmax) + rho0*rhotmax + rhot
//                                          |         |
//                                          |         |
//                                          V         V
//                                     m_max_rho     ncols
//
//	where 
//		rho0	 	\in <0, ..., m_max_rho) 
//		rhot 		\in <0, ..., ncols) 
//		mult_{bra}	\in <0, ..., max_mult_bra)
//		mult_{ket}	\in <0, ..., max_mult_ket)
		rme.resize(maxMultBraKet*m_max_rho*ncols, 0.0);

		size_t irow, icol, mult_ket, mult_bra, irho, index_rme, index_full_me;
		SU3xSU2::CANONICAL TensorComponent;
//	FullMe is a two-dimensional vector with full matrix elements.
//	Example:
//	< mult_{bra} Bra| T^{rho0_{LM0s[irow]} | mult_{ket} Ket> = FullMe[irow][index_full_me], 
//	where
//	index_full_me = rho0*(max_mult_ket*max_mult_bra) + mult_{bra}*max_mult_ket + mult_{ket}
		std::vector<std::vector<double> > FullMe(nrows);

		double dSU2CGs;
		double dSpinCG = clebschGordan(irKet.S2, irKet.S2, S2, Sigma0, irBra.S2, irBra.S2); // C_{Ket 0}^{Bra}
		if (Negligible(dSpinCG))
		{
			std::cerr << "Error: spin CG = 0" << std::endl;
			exit(EXIT_FAILURE);
		}

//	Create matrix A 
		Eigen::MatrixXd A((int)nrows, (int)ncols);
//	Prepare vectors
		Eigen::VectorXd x((int)nrows); 
		Eigen::VectorXd b((int)nrows); 
//	Construct matrix - it is the same for all irho, alpha_{bra}, alpha_{ket}
		for (irow = 0; irow < nrows; ++irow) 
		{
#if defined(HECHT)			
			dSU2CGs = clebschGordan(irKet.mu,  irKet.mu, LM0s[irow],  M_LM0, irBra.mu,  irBra.mu)*dSpinCG;
#elif defined(DRAAYER)
			dSU2CGs = clebschGordan(irKet.mu, -irKet.mu, LM0s[irow], -M_LM0, irBra.mu, -irBra.mu)*dSpinCG;
#endif
			for (icol = 0; icol < ncols; ++icol) // icol corresponds to multiplicity rho  <bra 0 || ket>_{rho} 
			{
				A(irow, icol) = dSU2CGs*vSU3CGs[icol][irow]; 
			}
		}
//	perform LU decomposition of A
		Eigen::FullPivLU<Eigen::MatrixXd> luOfA(A);

		TensorComponent.eps		= eps0;
		TensorComponent.M_LM	= M_LM0;
		TensorComponent.Sigma	= Sigma0;
//	calculate full matrix elements which compose right hand side of the system
//	of linear equations
		for (irow = 0; irow < nrows; ++irow)
		{
			TensorComponent.LM = LM0s[irow];
			EvaluateME_new(TensorComponent, BraHws, KetHws, FullMe[irow]);
//			EvaluateME(TensorComponent, BraHws, KetHws, FullMe[irow]);
		}

//	Calculate rmes for all outer multiplicities in Bra and Ket hws, for all rho_{0} of the
//	tensor calculate rme, and for all rhot due to SU3::mult(Ket, Tensor, Bra)

//	index_rme = mult_bra*(max_mult_ket*rho0max*rhotmax) + mult_ket*(rho0max*rhotmax) + irho*rhotmax + rhot (== icol)
//	!!!!!!!!!!!!!!!!!!!!!!!!!!!WARNING:!!!!!!!!!!!!!!!!!!!!!!!!!
//	 ==> The order of loops is extremely important and mut not be changed!!!
//	!!!!!!!!!!!!!!!!!!!!!!!!!!!WARNING:!!!!!!!!!!!!!!!!!!!!!!!!!
		for (mult_bra = 0, index_rme = 0; mult_bra < max_mult_bra; ++mult_bra)
		{
//	iterate over outer multiplicities of Bra hws
			for (mult_ket = 0; mult_ket < max_mult_ket; ++mult_ket)
			{
//	iterate over all possible multiplicities rho_{0} of the tensor
				for (irho = 0, index_full_me = mult_bra*max_mult_ket + mult_ket; irho < m_max_rho; ++irho, index_full_me += maxMultBraKet)
				{
//	clean vector x				
					x.setZero((int)nrows);
//	generate right hand side of a system of linear equations (vector b)				
					for (irow = 0; irow < nrows; ++irow)
					{
						b(irow) = FullMe[irow][index_full_me];
					}
					x = luOfA.solve(b);
					for(icol = 0; icol < ncols; ++icol)
					{
						rme[index_rme++] = x(icol);
					}
				}
			}
		}
		return ncols; // return maximal multiplicity in (ket x Tensor) ---> bra, i.e. <ket tensor || bra>_{rho}
	}
	else
	{
//	(lmi mui) x (n 0)/(0 n) ---> (lmf muf) is always multiplicity free, we have rhotmax = 1
//	for creation/annihilation operator we have rho0max == 1
//
//	index_rme = mult_bra*(max_mult_ket*rho0max*rhotmax) + mult_ket*(rho0max*rhotmax) + irho*rhotmax + rhot (== icol)
//                                |        |                    |      |         |     |       |
//                                |        |                    |      |         |     |       |
//                                V        V                    V      V         V     V       V
//                                1        1                    1      1         0     1       0
//  index_rme = mult_bra*max_mult_ket                   + mult_ket                                 
		assert(m_OperatorType == CREATION || m_OperatorType == ANNIHILATION);
		size_t mult_ket, mult_bra, ipos;
//	Calculate full matrix elements:
//	(a) enumerate tensor component that connects Ket_hws with Bra_hws
		SU3xSU2::CANONICAL TensorComponent;
		TensorComponent.eps 	=  2*irBra.lm + irBra.mu - (2*irKet.lm + irKet.mu);	//	eps0 = Bra::eps_{hw} - Ket::eps_{hw}
		TensorComponent.LM 	= (((m_OperatorType == CREATION) ? (2*lm - TensorComponent.eps) :  (2*mu + TensorComponent.eps)))/3;
		TensorComponent.M_LM = irBra.mu - irKet.mu;								//	M_LM0 = Bra::M_{LM hw} - Ket::M_{LM hw}
		TensorComponent.Sigma = irBra.S2 - irKet.S2;								//	Sigma0 = Bra::Sigma_{hw} - Ket::Sigma_{hw}
//	(b) FullMe.size() == BraHws.GetMult()*KetHws.GetMult()
		std::vector<double> FullMe;
//	(c) Calculate <alpha_{bra} bra hws_bra| a^{dagger}_{TensorComponent} |alpha_{ket} ket hws_ket>
		EvaluateME_new(TensorComponent, BraHws, KetHws, FullMe);
//		EvaluateME(TensorComponent, BraHws, KetHws, FullMe);

		std::vector<SU2::LABEL> LM0s;
// 	Get SU(3) C-G coefficients that are needed for r.m.e calculation, i.e.
//	< irKet epsKet_{hw} LMKet_{hw}; (n 0) eps0 LM0 || irBra epsBra_{hw} LMBra_{hw}>, where 
		SU3CGFactory.RMESU2U1(irKet, *this, irBra, LM0s, vSU3CGs);  // calculate <Ket 0 || Bra>_{rho}

		assert(LM0s.size() 			== 1);
		assert(LM0s[0] == TensorComponent.LM);
		assert(vSU3CGs.size() 		== 1);
		assert(vSU3CGs[0].size() 	== 1);

		double dSpinCG = clebschGordan(irKet.S2, irKet.S2, 1, TensorComponent.Sigma, irBra.S2, irBra.S2);
		
#if defined(HECHT)
		double dSU2CGs = clebschGordan(irKet.mu,  irKet.mu, TensorComponent.LM,  TensorComponent.M_LM, irBra.mu,  irBra.mu)*dSpinCG;
#elif defined(DRAAYER)
		double dSU2CGs = clebschGordan(irKet.mu, -irKet.mu, TensorComponent.LM, -TensorComponent.M_LM, irBra.mu, -irBra.mu)*dSpinCG;
#endif

		double dCoeff = dSU2CGs*vSU3CGs[0][0];

		rme.resize(maxMultBraKet); // generally nrme = max_mult_bra*max_mult_ket*rho0max*rhotmax, but we have rh0max = rhotmax = 1 ==> nrme = maxMultBraKet
		for (mult_bra = 0, ipos = 0; mult_bra < max_mult_bra; ++mult_bra)
		{
			for (mult_ket = 0; mult_ket < max_mult_ket; ++mult_ket, ++ipos)
			{
				rme[ipos] = FullMe[ipos] / dCoeff;
			}
		}
		return 1;
	}
}

};
#endif 

#ifndef TENSORSSO3SU2_h
#define TENSORSSO3U2_h
#include <SU3ME/global_definitions.h>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <map>
#include <cassert>

namespace SU3xSU2 
{
namespace SO3SU2 
{
typedef short SPS_INDEX;

static const int OSCILATOR_MAX = 33;  // => Maximal excitation = 32 hw
SPS_INDEX GetIndex(const int n, const int l, const int jj, const int mm);
void GetStateLabels(unsigned short Index, int& N, int& l, int& j, int& mj);

class Tensor: public SU3xSU2::LABELS 
{
public:
//	TENSOR_COMPONENT_SINGLE_RHO is being used (as its name suggests) whenever
//	tensorial structure of a certain multiplicity is needed.  Class Tensor
//	stores (see TENSOR_COMPONENT_ALL_RHO) tensorial structure for all
//	multiplicities.
	typedef std::pair<const std::vector<double>*, const std::vector<SPS_INDEX>* > TENSOR_COMPONENT_SINGLE_RHO; 
	inline int n() const { assert(m_OperatorType != GENERAL); return (m_OperatorType == CREATION) ? lm : mu;}
	static SPS_INDEX CreationTensorToSpsIndex(const int n, const SU3xSU2::PHYSICALJ& ad) {return GetIndex(n, ad.ll/2, ad.jj, ad.mm);}
	static SPS_INDEX AnnihilationTensorToSpsIndex(const int n, const SU3xSU2::PHYSICALJ& ta) {return -1.0*GetIndex(n, ta.ll/2, ta.jj, -ta.mm);}

//	tilde{a}^{(0\,n)1/2}_{l j mj} = (-)^{-n + j + m}  a_{n l j -mj}, where n =	ta.MU
	inline static char AnnihilationPhase(const int n, const SU3xSU2::PHYSICALJ& ta) { return MINUSto(-n + (ta.jj + ta.mm)/2);}
protected:

//	TENSOR_COMPONENT_ALL_RHO.first contains vector of coefficients:
//	TENSOR_COMPONENT_ALL_RHO.first[irho][j] is coefficients that stands in
//	front of j-th term, whose sps indices ranges from
//	TENSOR_COMPONENT_ALL_RHO.second[m_nBasisOperators*j] up to
//	TENSOR_COMPONENT_ALL_RHO.second[m_nBasisOperators*j + m_nBasisOperators - 1]
	typedef std::pair<std::vector<std::vector<double> >, std::vector<SPS_INDEX> > TENSOR_COMPONENT_ALL_RHO; 

//	TENSOR_STORAGE is a map that associates SU3xSU2::PHYSICALJ labels with a TENSOR_COMPONENT_ALL_RHO structure.
//	Example: T^{rho}_{SU3xSU2::PHYSICALJ} 
//	coefficients are stored in m_TensorComponents[SU3xSU2::PHYSICALJ]->second.first[rho]
//	and indices at m_TensorComponents[SU3xSU2::PHYSICALJ]->second.second
	typedef std::map<SU3xSU2::PHYSICALJ, TENSOR_COMPONENT_ALL_RHO> TENSOR_STORAGE;

//	Used in construction of tensor for storing precalculated values of spin
//	CG's
	typedef std::pair<CTuple<U1::LABEL, 2> ,double> tSpinCG; // .first = {X.Sigma, Y.Sigma}; .second = dSpinCG

//	m_OperatorType == GENERAL if a given operator is composed out of both
//	creation and annihilation basis operators
	enum OperatorType {CREATION, ANNIHILATION, GENERAL} m_OperatorType;
protected:
//	maximal multiplicity of the tensor
	int m_max_rho;
//	number of creation/annihilation	operators that compose Tensor
	char m_nBasisOperators;
//	Contains a structure of tensor in terms of creation/annihilation operators
//	Example: ad_{n}ad_{n}ad_{n}ta_{n}ta_{n} would correspond to 
//	{n+1, n+1, n+1,	-(n+1), -(n+1)} array. Note that we add +1 in order do be 
//	able to distinguish between ad_{n=0} and ta_{n=0} operators.
	std::vector<char> m_Structure;
//	Position of the first annihilation operator in m_Structure.  If there are
//	no annihilation operators involved it is equal to m_annihilStart =	m_Structure.end();
	int m_annihilStart;
//	This structure holds tensorial components of a given operator
	TENSOR_STORAGE m_TensorComponents;
protected:
	Tensor(const SU3xSU2::LABELS& RhoLmMuS2, OperatorType Type); 

	void Add(const Tensor& X, const size_t rhoX, const SU3xSU2::PHYSICALJ& Xlabels, 
			 const Tensor& Y, const size_t rhoY, const SU3xSU2::PHYSICALJ& Ylabels, 
			 const std::vector<double>& Coeff, 
			 TENSOR_COMPONENT_ALL_RHO& CurrentOperator) const; 


	void GenerateComponents(const Tensor& X, const size_t rhoX, 	
							const Tensor& Y, const size_t rhoY, 
							const std::vector<SU3xSU2::PHYSICALJ>& Components2Costruct, 
							bool fDiagnostic = false);
public:
//	Construct ALL COMPONENTS of the tensor ResultXY by coupling tensor X^{rhoX}
//	with Y^{rhoY}. Note that ALL MULTIPLICITIES that emerge from coupling (X x
//	Y) are produced and stored in m_TensorComponents.
	Tensor(	const Tensor& X, const size_t rhoX, 
			const Tensor& Y, const size_t rhoY, 
			const SU3xSU2::LABELS& ResultXY, 
			bool fDiagnostic = false);
//	Construct only a certain components of the tensor ResultXY (provided in
//	vector "Components2Costruct") by coupling tensor X^{rhoX} with Y^{rhoY}.
//	Note that ALL MULTIPLICITIES that emerge from coupling (X x Y) are produced
//	and stored in m_TensorComponents.
	Tensor(	const Tensor& X, const size_t rhoX, 
			const Tensor& Y, const size_t rhoY, 
			const SU3xSU2::LABELS& ResultXY, 
			const std::vector<SU3xSU2::PHYSICALJ>& Components2Costruct, 
			bool fDiagnostic = false); 

	inline unsigned char GetNBasisOperators() const {return m_nBasisOperators;}
	inline size_t GetHOShell() const {return abs(m_Structure[0])-1;}
	size_t GetdA() const;
	size_t GetNumberOfAnnihilators() const 
	{
		assert(m_Structure.front() >= m_Structure.back()); // stops program if we have e.g. {-n, -n, +n, +n}
		return (m_Structure.size() - m_annihilStart);
	}

	char operator[](size_t i) const {return m_Structure[i];}
	inline size_t GetMaxRho() { return m_max_rho;}

	void ShowTensorComponents() const;
	void ShowTensorComponent(const SU3xSU2::PHYSICALJ& TensorComponent) const;
	void ShowTermIndices(const SU3xSU2::PHYSICALJ& TensorComponent, size_t iterm);

//	In the future one may consider to generate HWS using symmetrically coupled
//	creation operators. In such a case this method will become handy. Otherwise
//	it does not serve any purpose.
	void RenormalizeTensorComponents();
	inline bool IsPureType() // returns TRUE if the Tensor is composed exclusively out of "pure" type  == {a+a+ ... a+} or all {tata ... ta} creation/annihilation operators
	{
		return (m_nBasisOperators == std::count(m_Structure.begin(), m_Structure.end(), m_Structure[0]));
	}

	TENSOR_COMPONENT_SINGLE_RHO Component(const size_t rho, const SU3xSU2::PHYSICALJ& Component) const;
};


class Creation: public SU3xSU2::SO3SU2::Tensor {
public:
	Creation(size_t n);
};

class Annihilation: public SU3xSU2::SO3SU2::Tensor {
public:
	Annihilation(size_t n);
};

};	// namespace SO3SU2
};	// namespace SU3xSU2
#endif 

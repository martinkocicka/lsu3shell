#include <SU3ME/distr2gamma2omega.h>

//	for a given distribution of fermions [nferms] over HO shells [hoShells]
//	generate vector of all combinations of U(N)>SU(3)xSU(2) [vgammas] irreps where each element in the
//	combination correspond to a certain SU(3)xSU(2) spanning ho shell. For HO shells with
//	no fermions, supply gamma[j] = vgammas[*][j] == {VACUUM} ={1 (0 0)0}.
//	Example:
//	nferms = {0 6} hoShells = {0 1} ==> vgammas[0] = {VACUUM, 1(0 0)0} 
//
//	nferms = {2 4] hoShells = {0 1} ==> vgammas[0] = {1(0 0)0, 1(0 2)0} 
//										vgammas[1] = {1(0 0)0, 1(1 0)2} 
void distr2gammas(const CBaseSU3Irreps& BaseSU3Irreps, const std::vector<unsigned char>& nferms, std::vector<unsigned char>& hoShells, std::vector<UN::SU3xSU2_VEC>& vgammas)
{
	size_t n, A;
	assert(nferms.size() == hoShells.size());
	std::vector<UN::SU3xSU2_VEC> vAllowedBaseIrreps;

    for (size_t i = 0; i < hoShells.size(); ++i)		
    {
		A = nferms[i];// number of fermions in n-th shell
		if (A == 0) 
		{
//			vAllowedBaseIrreps.push_back(UN::SU3xSU2_VEC(1, UN::SU3xSU2::VACUUM)); // A == 0 ==> add {VACUUM}
			vAllowedBaseIrreps.push_back(UN::SU3xSU2_VEC(1, UN::SU3xSU2(+1, 0, 0, 0))); // A == 0 ==> add {VACUUM}
		}
		else	// A > 0 ==> add array of allowed UN::SU3xSU2 irreps for a given HO number n, and A number of fermions
		{
			n = hoShells[i];// principal HO number of the i-th occupied shell
			UN::SU3xSU2_VEC AllowedIrreps;
			BaseSU3Irreps.GetIrreps(n, A, AllowedIrreps);
			vAllowedBaseIrreps.push_back(AllowedIrreps);//
		}
	}

    // Now we generate all possible combinations of coupling out of vvSU3xSU2Irreps
    size_t NIrrepsSets = vAllowedBaseIrreps.size();
	std::vector<unsigned> vElemsPerChange(NIrrepsSets, 1); //Fill with 1 cause vElemsPerChange[0] = 1;
	std::vector<size_t> vNSizes(NIrrepsSets);
	// uNAllowedCombinations = number of all possible coupling combinations
	// uNAllowedCombinations = vvSU3xSU2Irreps[0].size()* ... *vvSU3xSU2Irreps[NIrrepsSets].size()
    unsigned uNAllowedCombinations = 1;

    for (size_t i = 0; i < NIrrepsSets; i++)
    {
		vNSizes[i] = vAllowedBaseIrreps[i].size();	
   		uNAllowedCombinations *= vNSizes[i];
   	}

    for (size_t i = 1; i < NIrrepsSets; i++)
   	{
   		// if i == 0 => vElemsPerChange[0]=1 since constructor
		vElemsPerChange[i] = vElemsPerChange[i-1]*vNSizes[i-1];
   	}

//	generate possible combinations
	vgammas.resize(uNAllowedCombinations);
    for (unsigned index = 0; index < uNAllowedCombinations; index++)
    {
		// each allowed combination corresponds to one gamma element in array of U(N)>SU(3)xSU(2)
		UN::SU3xSU2_VEC gamma;
		for (size_t i = 0; i < NIrrepsSets; i++)
		{
		    size_t iElement = (index / vElemsPerChange[i]) % vNSizes[i];
		    gamma.push_back(vAllowedBaseIrreps[i][iElement]);
   		}
		vgammas[index] = gamma;
  	}
}

void AugmentGammaByVacuumShells(const std::vector<unsigned char>& nferms, const UN::SU3xSU2_VEC& gamma_without_vacuum, UN::SU3xSU2_VEC& gamma_transformed)
{
	assert(gamma_transformed.empty());

//	gamma_transformed.assign(nferms.size(), UN::SU3xSU2::VACUUM);
	gamma_transformed.assign(nferms.size(), UN::SU3xSU2(+1, 0, 0, 0));
	for (size_t i = 0, index = 0; i < nferms.size(); ++i)
	{
		if (nferms[i])
		{
			gamma_transformed[i] = gamma_without_vacuum[index++];
		}
	}
}

/*
void Transform(	const tSHELLSCONFIG& BraConfs, 
				const tSHELLSCONFIG& KetConfs, 
				std::vector<unsigned char>& hoShells, 
				std::vector<unsigned char>& nferms_bra, 
				std::vector<unsigned char>& nferms_ket)
{
	assert(KetConfs.size() == BraConfs.size());
	size_t nmax = KetConfs.size();
	size_t n;
	for (n = 0; n < nmax; ++n)
	{
		if (BraConfs[n] || KetConfs[n])
		{
			hoShells.push_back(n);
			nferms_bra.push_back(BraConfs[n]);
			nferms_ket.push_back(KetConfs[n]);
		}
	}
}

void Transform(	const std::vector<unsigned char>& BraConfs, 
				const std::vector<unsigned char>& KetConfs, 
				std::vector<unsigned char>& hoShells, 
				std::vector<unsigned char>& nferms_bra, 
				std::vector<unsigned char>& nferms_ket)
{
	assert(KetConfs.size() == BraConfs.size());
	size_t nmax = KetConfs.size();
	size_t n;
	for (n = 0; n < nmax; ++n)
	{
		if (BraConfs[n] || KetConfs[n])
		{
			hoShells.push_back(n);
			nferms_bra.push_back(BraConfs[n]);
			nferms_ket.push_back(KetConfs[n]);
		}
	}
}
*/

// Since su3sell code is used also for calculation of matrix elements between
// bra and ket model spaces which carry different Nmax we need to implement
// case when BraConfs.size() != KetConfs.size()
void Transform(	const tSHELLSCONFIG& BraConfs, 
				const tSHELLSCONFIG& KetConfs, 
				std::vector<unsigned char>& hoShells, 
				std::vector<unsigned char>& nferms_bra, 
				std::vector<unsigned char>& nferms_ket)
{
	size_t nmin = std::min(BraConfs.size(), KetConfs.size());
	size_t n;

	for (n = 0; n < nmin; ++n)
	{
		if (BraConfs[n] || KetConfs[n])
		{
			hoShells.push_back(n);
			nferms_bra.push_back(BraConfs[n]);
			nferms_ket.push_back(KetConfs[n]);
		}
	}
	if (BraConfs.size() == KetConfs.size())
	{
		return;
	}
	
	if (BraConfs.size() < KetConfs.size())
	{
		for (; n < KetConfs.size(); ++n)
		{
			if (KetConfs[n])
			{
				hoShells.push_back(n);
				nferms_bra.push_back(0);
				nferms_ket.push_back(KetConfs[n]);
			}
		}
	}
	else	// if (KetConfs.size() < BraConfs.size())
	{
		for (; n < BraConfs.size(); ++n)
		{
			if (BraConfs[n])
			{
				hoShells.push_back(n);
				nferms_bra.push_back(BraConfs[n]);
				nferms_ket.push_back(0);
			}
		}
	}
}

// Since su3sell code is used also for calculation of matrix elements between
// bra and ket model spaces which carry different Nmax we need to implement
// case when BraConfs.size() != KetConfs.size()
void Transform(	const std::vector<unsigned char>& BraConfs, 
				const std::vector<unsigned char>& KetConfs, 
				std::vector<unsigned char>& hoShells, 
				std::vector<unsigned char>& nferms_bra, 
				std::vector<unsigned char>& nferms_ket)
{
	size_t nmin = std::min(BraConfs.size(), KetConfs.size());
	size_t n;

	for (n = 0; n < nmin; ++n)
	{
		if (BraConfs[n] || KetConfs[n])
		{
			hoShells.push_back(n);
			nferms_bra.push_back(BraConfs[n]);
			nferms_ket.push_back(KetConfs[n]);
		}
	}
	if (BraConfs.size() == KetConfs.size())
	{
		return;
	}
	
	if (BraConfs.size() < KetConfs.size())
	{
		for (; n < KetConfs.size(); ++n)
		{
			if (KetConfs[n])
			{
				hoShells.push_back(n);
				nferms_bra.push_back(0);
				nferms_ket.push_back(KetConfs[n]);
			}
		}
	}
	else	// if (KetConfs.size() < BraConfs.size())
	{
		for (; n < BraConfs.size(); ++n)
		{
			if (BraConfs[n])
			{
				hoShells.push_back(n);
				nferms_bra.push_back(BraConfs[n]);
				nferms_ket.push_back(0);
			}
		}
	}
}

void gamma2su3vec(const UN::SU3xSU2_VEC& gamma, std::vector<SU3_VEC>& omega_su3)
{
	assert(gamma.size() >= 2);

	size_t nColumns = gamma.size() - 1;
	SU3_VEC Row(nColumns);
	std::vector<SU3_VEC> vStack(nColumns);
	SU3::Couple(gamma[0], gamma[1], vStack[0]);

	for (int icol = 0; ;)
	{
		if (icol == nColumns - 1)
		{
			for (size_t i = 0; i < vStack[icol].size(); ++i)
			{
				Row[icol] = vStack[icol][i];
				omega_su3.push_back(Row);
			}
			vStack[icol].resize(0);
			if (icol > 0)
			{
				icol--;
			}
		}

		if (!vStack[icol].empty())
		{
			Row[icol] = vStack[icol].back();
			vStack[icol].pop_back();
			SU3::Couple(Row[icol], gamma[icol+2], vStack[icol+1]);
			icol++;
		}
		else
		{
			if (icol > 0)
			{
				icol--;
			}
			else	// at this point icol == 0 and vStack[icol == 0] is empty
			{
				break;
			}
		}
	}
}

void gamma2su2vec(const UN::SU3xSU2_VEC& gamma, std::vector<SU2_VEC>& omega_su2)
{
	assert(gamma.size() >= 2);

	size_t nColumns = gamma.size() - 1;
	SU2_VEC Row(nColumns);
	std::vector<SU2_VEC> vStack(nColumns);
	SU2::Couple(gamma[0].S2, gamma[1].S2, vStack[0]);

	for (int icol = 0; ; )
	{
		if (icol == nColumns - 1)
		{
			for (size_t i = 0; i < vStack[icol].size(); ++i)
			{
				Row[icol] = vStack[icol][i];
				omega_su2.push_back(Row);
			}
			vStack[icol].resize(0);
			if (icol > 0)
			{
				icol--;
			}
		}

		if (!vStack[icol].empty())
		{
			Row[icol] = vStack[icol].back();
			vStack[icol].pop_back();
			SU2::Couple(Row[icol], gamma[icol+2].S2, vStack[icol+1]);
			icol++;
		}
		else
		{
			if (icol > 0)
			{
				icol--;
			}
			else	// at this point icol == 0 and vStack[icol == 0] is empty
			{
				break;
			}
		}
	}
}

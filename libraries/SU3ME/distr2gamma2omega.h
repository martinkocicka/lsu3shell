#ifndef DISTR2GAMMA2OMEGA_H
#define DISTR2GAMMA2OMEGA_H
#include <UNU3SU3/UNU3SU3Basics.h>
#include <SU3ME/BaseSU3Irreps.h>

#include <cassert>

//	given a set of U(N) > SU(3)xSU(2) irreps [gamma] produce a vector of all
//	possible coupling results [omega_su3] 
void gamma2su3vec(const UN::SU3xSU2_VEC& gamma, std::vector<SU3_VEC>& omega_su3);

//	Only those coupling are selected that have the resulting coupling omega_su3[i].back()
//	satisfying su3selector rule [exclude_this_su3]
//	This is probably not very handful as in the most cases cut in
//	proton/neutron space part would make impossible to decouple CM motion from
//	the instrinsic degrees of freedom
template<class su3SelectionRule>
void gamma2su3vec(const UN::SU3xSU2_VEC& gamma, std::vector<SU3_VEC>& omega_su3, const su3SelectionRule& exclude_this_su3)
{
	assert(gamma.size() >= 2);

	size_t nColumns = gamma.size() - 1;
	SU3_VEC Row(nColumns);
	std::vector<SU3_VEC> vStack(nColumns);
	SU3::Couple(gamma[0], gamma[1], vStack[0]);

	for (int icol = 0; ;)
	{
		if (icol == nColumns - 1)
		{
			for (size_t i = 0; i < vStack[icol].size(); ++i)
			{
				if (!exclude_this_su3(vStack[icol][i]))
				{
					Row[icol] = vStack[icol][i];
					omega_su3.push_back(Row);
				}
			}
			vStack[icol].resize(0);
			if (icol > 0)
			{
				icol--;
			}
		}

		if (!vStack[icol].empty())
		{
			Row[icol] = vStack[icol].back();
			vStack[icol].pop_back();
			SU3::Couple(Row[icol], gamma[icol+2], vStack[icol+1]);
			icol++;
		}
		else
		{
			if (icol > 0)
			{
				icol--;
			}
			else	// at this point icol == 0 and vStack[icol == 0] is empty
			{
				break;
			}
		}
	}
}


void gamma2su2vec(const UN::SU3xSU2_VEC& gamma, std::vector<SU2_VEC>& omega_su2);

template<class su2SelectionRule>
void gamma2su2vec(const UN::SU3xSU2_VEC& gamma, std::vector<SU2_VEC>& omega_su2, const su2SelectionRule& su2selector)
{
	assert(gamma.size() >= 2);

	size_t nColumns = gamma.size() - 1;
	SU2_VEC Row(nColumns);
	std::vector<SU2_VEC> vStack(nColumns);
	SU2::Couple(gamma[0].S2, gamma[1].S2, vStack[0]);

	for (int icol = 0; ; )
	{
		if (icol == nColumns - 1)
		{
			for (size_t i = 0; i < vStack[icol].size(); ++i)
			{
				if (!su2selector(vStack[icol][i]))
				{
					Row[icol] = vStack[icol][i];
					omega_su2.push_back(Row);
				}
			}
			vStack[icol].resize(0);
			if (icol > 0)
			{
				icol--;
			}
		}

		if (!vStack[icol].empty())
		{
			Row[icol] = vStack[icol].back();
			vStack[icol].pop_back();
			SU2::Couple(Row[icol], gamma[icol+2].S2, vStack[icol+1]);
			icol++;
		}
		else
		{
			if (icol > 0)
			{
				icol--;
			}
			else	// at this point icol == 0 and vStack[icol == 0] is empty
			{
				break;
			}
		}
	}
}

template <class su3SelectionRule, class su2SelectionRule>
void gamma2omega(const UN::SU3xSU2_VEC& gamma, const su3SelectionRule& su3selector, const su2SelectionRule& su2selector, size_t& nSU3omegas, size_t& nSU2omegas, std::vector<SU3xSU2_VEC>& vomegas)
{
	std::vector<SU3_VEC> omega_su3;
	std::vector<SU2_VEC> omega_su2;

	gamma2su2vec(gamma, omega_su2, su2selector);
	nSU2omegas = omega_su2.size();

	gamma2su3vec(gamma, omega_su3, su3selector);
	nSU3omegas = omega_su3.size();


	size_t nomegas = nSU3omegas*nSU2omegas;		//	== omega.size()
	if (nomegas)
	{
		size_t nirreps = gamma.size() - 1;		//	== number of SU3xSU2 irreps in each element of omega
		size_t iomega = 0;

		assert(nirreps = omega_su3[0].size());

		vomegas.resize(nomegas);
		SU3xSU2_VEC omega(nirreps);
		for (std::vector<SU3_VEC>::const_iterator itsu3 = omega_su3.begin(); itsu3 != omega_su3.end(); ++itsu3)
		{
			for (size_t i = 0; i < nirreps; ++i)
			{
				omega[i].rho = (*itsu3)[i].rho;
				omega[i].lm  = (*itsu3)[i].lm;
				omega[i].mu  = (*itsu3)[i].mu;
			}
			for (std::vector<SU2_VEC>::const_iterator itsu2 = omega_su2.begin(); itsu2 != omega_su2.end(); ++itsu2, ++iomega)
			{
				for (size_t i = 0; i < nirreps; ++i)
				{
					omega[i].S2  = (*itsu2)[i];
				}
				vomegas[iomega] = omega;
			}
		}
	}
}

inline size_t gamma_mult(const UN::SU3xSU2_VEC& gamma)
{
	int ntot = 1;
	for (size_t i = 0; i < gamma.size(); ++i)
	{ 
		ntot *= gamma[i].mult;
	}
	return ntot;
}

//	calculate total multiplicity of a vector of SU(3) or SU(3)xSU(2) irreps
template<class T>
inline size_t omega_mult(const T& omega)
{
	int ntot = 1;
	for (size_t i = 0; i < omega.size(); ++i)
	{ 
		ntot *= omega[i].rho;
	}
	return ntot;
}

template<class T>
inline size_t total_mult(const UN::SU3xSU2_VEC& gamma, const T& omega)
{
	return gamma_mult(gamma)*omega_mult(omega);
}


//	for a given distribution of fermions (nferms) over HO shells (hoShells)
//	generate vector of all combinations of gamma_bra irreps. For HO shells with
//	no fermions, supply gamma[j] = vgammas[*][j] == {VACUUM} ={1 (0 0)0}.
//	Example:
//	nferms = {0 6} hoShells = {0 1} ==> vgammas[0] = {VACUUM, 1(0 0)0} 
//
//	nferms = {2 4] hoShells = {0 1} ==> vgammas[0] = {1(0 0)0, 1(0 2)0} 
//										vgammas[1] = {1(0 0)0, 1(1 0)2} 
void distr2gammas(const CBaseSU3Irreps& BaseSU3Irreps, const std::vector<unsigned char>& nferms, std::vector<unsigned char>& hoShells, std::vector<UN::SU3xSU2_VEC>& vgammas); 

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//	Program implements the thre Transform(...) functions that take as an input
//	data vectors structured by many-body basis state generator (see CnsmBasis).
//	This must be first transform into table that can be used to calculate SU3
//	RME reduction 1
/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
void Transform(	const tSHELLSCONFIG& BraConfs, 
				const tSHELLSCONFIG& KetConfs, 
				std::vector<unsigned char>& hoShells, 
				std::vector<unsigned char>& nferms_bra, 
				std::vector<unsigned char>& nferms_ket);

void Transform(	const std::vector<unsigned char>& BraConfs, 
				const std::vector<unsigned char>& KetConfs, 
				std::vector<unsigned char>& hoShells, 
				std::vector<unsigned char>& nferms_bra, 
				std::vector<unsigned char>& nferms_ket);
//	EXAMPLE:
//	nferms = {3, 			4, 		0, 		1}
//	gamma = {1 (1 1)1/2, 1(4, 2), 0(0 0)0, 1(7 0)1/2}
//
//	omega = {1(5 0)1/2, 1(4 4)0}
//	transforms into
//	omega = {1(5 0)1/2, 1(5 0)1/2, 1(4 4)0}
//
//	so that omega is ready for usage in SU3 RME reduction algorithm
template<class T>
void AugmentOmegaByVacuumShells(const std::vector<unsigned char>& nferms, const T& gamma, SU3xSU2_VEC& omega)
{
	assert(nferms.size() == gamma.size()); 	//	gamma contains SU3xSU2 labels of single-shell tensors. No single-shell
											//	operator is equal to the identity operator. gamma was transformed
											//	from CTensorStructure storage form into gamma with identity operators
											//	in CTensor::GetRMECalculator
	size_t nShells = nferms.size();
	SU3xSU2::LABELS identity_operator(+1, 0, 0, 0);
	SU3xSU2_VEC omega_new(nShells - 1);
	size_t i(1), j(0), n(2); // i ... index of gamma_new[], n ... index of nferms[], j ... index of omega[]
	unsigned char Anl(nferms[0]), Anr(nferms[1]);

//	Couple the two first single shell irreps ... gamma[0] x gamma[1] --> omega_new[0]
	if (Anl && Anr) // if gamma[0] and gamma[1] are not vacuum ===> omega_new[0] = omega[0], where omega[0] = gamma[0] x gamma[1]
	{
		omega_new[0] = omega[j++];	//	includes multiplicity rho
	}
	else
	{
		if (Anl == 0 && Anr == 0)
		{
			omega_new[0] = identity_operator;
		}                 
		else if (Anl && Anr == 0)
		{
			omega_new[0] = SU3xSU2::LABELS(SU3::LABELS(gamma[0].lm, gamma[0].mu), gamma[0].S2);
		}
		else if (Anl == 0 && Anr)
		{
			omega_new[0] = SU3xSU2::LABELS(SU3::LABELS(gamma[1].lm, gamma[1].mu), gamma[1].S2);
		}

		for (; n < nShells && (Anl == 0 ||  nferms[n]== 0); n++, i++) // condition Anl == 0 || nferms[n] == 0 is false if we are about to use omega_new[i] = omega[0]
		{
			Anl += Anr; 
			Anr = nferms[n];
			if (Anr == 0)// omega_new[i] = (omega_new[i - 1] x Identity) ==> obviously omega_new[i] == omega_new[i-1], except for omega_new[i].rho must be equal to 1!
			{
				omega_new[i].rho = 1;
				omega_new[i].lm = omega_new[i - 1].lm;
				omega_new[i].mu = omega_new[i - 1].mu;
				omega_new[i].S2 = omega_new[i - 1].S2;
			}
			else if (Anl == 0 && Anr != 0) 
			{
				omega_new[i] = SU3xSU2::LABELS(SU3::LABELS(gamma[n].lm, gamma[n].mu), gamma[n].S2); // if Anl == 0 ==> omega_new[i] = ((0 0) x gamma[n])
			}
			else
			{
				break;
			}
		} 
	}
	for (; n < nShells; n++, i++)
	{
		if (nferms[n] == 0)	 // omega_new[i] = (omega_new[i - 1] x VACUUM) ==> obviously omega_new[i] == omega_new[i-1], except for omega_new[i].rho must be equal to 1!!!
		{
			omega_new[i].rho = 1; // omega_new[i].rho is multiplicity resulting from coupling (lm mu) x (0 0) ==> must be equal to 1
			omega_new[i].lm = omega_new[i - 1].lm;
			omega_new[i].mu = omega_new[i - 1].mu;
			omega_new[i].S2 = omega_new[i - 1].S2;
		}
		else
		{ 
			omega_new[i] = omega[j++]; 
		}
	}
	omega_new.swap(omega);
	assert(omega.size() == nShells - 1);
}

void AugmentGammaByVacuumShells(const std::vector<unsigned char>& nferms, const UN::SU3xSU2_VEC& gamma_without_vacuum, UN::SU3xSU2_VEC& gamma_transformed);
#endif

/****************************************************************
  global_definitions.cpp

  Last modified 3/5/11.
 
****************************************************************/

#include <SU3ME/global_definitions.h>

#include <cstdio>
#include <cstdlib>

const char *su3shell_data_directory = getenv("SU3SHELL_DATA");

const unsigned nspsmax = 256;//sizeof(unsigned long)*8;
const unsigned nmax = 35;

namespace storage_limits
{
	size_t number_9lm_coeffs  = 30000004;
	size_t number_u6lm_coeffs = 8000002;
	size_t number_z6lm_coeffs = 1000001;

//	number_su3su3_coeffs:
//	Maximal number of SU(3) irreps (wf, wi) that can be connected by a tensor
//	w0 L0. Its value depend on model space and interaction used.
//	LSU3shell_analytics can be used to estimate its value.
	size_t number_su3su3_coeffs = 1003;
}

/****************************************************************
  global_definitions.h                       

  Defines global constants, enumerations, and parameter variables 
  shared by multiple SU3NCSM compilation units.

  3/5/11 (mac): Extract global variables and declare as extern.

****************************************************************/

#ifndef GLOBAL_DEFINITIONS_H
#define GLOBAL_DEFINITIONS_H
#include <SU3NCSMUtils/CTuple.h>
#include<cmath>
#include<vector>
#include<iostream>
#include<string>

#define FRONT_CREATE	//	a^{d}_{1} | 5 3 2 0 > = | 1 5 3 2 0 > = (-)^{3} | 5 3 2 1 0 >
#define DRAAYER
//#define REAR_CREATE	//	a^{d}_{1} |5 3 2 0> = |5 3 2 0 1> = (-)^{1} |5 3 2 1 0> & a_{1} |5 3 2 0 1> = |5 3 2 0>

//#define HECHT


extern const char *su3shell_data_directory;

typedef float TENSOR_STRENGTH;

namespace nucleon
{
	enum Type {PROTON = 0, NEUTRON = 1};
};


namespace jt_coupled_bra_ket {
	typedef CTuple<unsigned char, 6> TwoBodyLabels;
	enum TwoBodyLabelsStructure {I1 = 0, I2 = 1, I3 = 2, I4 = 3, J = 4, T = 5}; 
};

namespace j_coupled_bra_ket {
	typedef CTuple<unsigned char, 5> TwoBodyLabels;
	enum TwoBodyLabelsStructure {I1 = 0, I2 = 1, I3 = 2, I4 = 3, J = 4}; 
};

namespace mscheme_bra_ket {
	typedef CTuple<unsigned char, 4> TwoBodyLabels;
	typedef CTuple<unsigned char, 2> OneBodyLabels;
	enum TwoBodyLabelsStructure {I1 = 0, I2 = 1, I3 = 2, I4 = 3}; 
};

enum TwoNucleonSystemType {PP = 0, NN = 1, PN = 2};

inline bool Negligible35(double val) { return fabs(val) < 5.5e-3;}
inline bool Negligible3(double val) { return fabs(val) < 1.0e-3;}
inline bool Negligible4(double val) { return fabs(val) < 1.0e-4;}
inline bool Negligible5(double val) { return fabs(val) < 1.0e-5;}
inline bool Negligible55(double val) { return fabs(val) < 5.0e-5;}
inline bool Negligible6(double val) { return fabs(val) < 1.0e-6;}
inline bool Negligible7(double val) { return fabs(val) < 1.0e-7;}
inline bool Negligible8(double val) { return fabs(val) < 1.0e-8;}
inline bool Negligible(double val) { return Negligible6(val);}


inline double MINUSto(int iPhase) { return (iPhase % 2) ? -1.0 : 1.0; }
extern const unsigned nspsmax;
extern const unsigned nmax;
inline unsigned nSPS(const unsigned n) { return (n+1)*(n+2)/2; }

namespace storage_limits
{
	extern size_t number_9lm_coeffs;
	extern size_t number_u6lm_coeffs;
	extern size_t number_z6lm_coeffs;
	extern size_t number_su3su3_coeffs; //used in WigEckSu3Su3CGTable
}
///////////////////////////////////////////////////////////
// 					SHOW VECTORS
///////////////////////////////////////////////////////////
template<class VECTOR>
void ShowVector(const VECTOR& v)
{
	for (size_t i = 0; i < v.size(); ++i)
	{
		if (i == v.size() - 1)
		{
			std::cout << (int)v[i];
		}
		else
		{
			std::cout << (int)v[i] << " ";
		}
	}
}
template<class VECTOR>
void ShowVector(const VECTOR& v, size_t nElems)
{
	for (size_t i = 0; i < nElems; ++i)
	{
		if (i == nElems - 1)
		{
			std::cout << (int)v[i];
		}
		else
		{
			std::cout << (int)v[i] << " ";
		}
	}
}
#endif

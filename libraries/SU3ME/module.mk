$(eval $(begin-module))

################################################################
# unit definitions
################################################################

module_units_h := \
bdbme CHwsGen CState IdentFermConfIterator Interaction2SU3tensors \
JTCoupled2BMe_Hermitian \
JTCoupled2BMe_Hermitian_SingleOperator ModelSpaceExclusionRules \
OneBodyOperator2SU3Tensors OperatorDataStructure MeEvaluationHelpers \
proton_neutron_ncsmSU3Basis proton_neutron_ncsmSU3BasisFastIteration \
OperatorLoader CalculateMe_proton_neutron_ncsmSU3BasisJfixed \
CalculateMe_proton_neutron_ncsmSU3BasisJcut CWig9lmLookUpTable 

module_units_cpp-h := \
BaseSU3Irreps CInteractionPN CRMECalculator \
CShellConfigurations CTensorStructure \
CalculateMe_IdenticalFermionsNCSMBasis \
CalculateMe_proton_neutron_ncsmSU3BasisJcut InteractionPPNN RME \
SU3xSU2Basis SU3InteractionRecoupler ScalarOperators2SU3Tensors Tensor TensorSO3SU2 \
distr2gamma2omega global_definitions ncsmSU3Basis \
proton_neutron_ncsmSU3Basis proton_neutron_ncsmSU3BasisFastIteration MeEvaluationHelpers OperatorLoader \
CalculateMe_proton_neutron_ncsmSU3BasisJfixed rmeTable\
# module_units_f := 
# module_programs_cpp :=


################################################################
# library creation flag
################################################################

$(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################


$(eval $(end-module))





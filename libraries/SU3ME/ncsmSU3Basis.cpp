#include <SU3ME/ncsmSU3Basis.h>
#include <algorithm>
#include <stack>
#include <climits>

///////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////    IdenticalFermionsNCSMBasis    /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

//	size_t nOmegasInDistribution ... number of possible omegas for a given gamma
size_t IdenticalFermionsNCSMBasis::GenerateGammasAndGammaIds(	const tSHELLSCONFIG& distribution,
																const IdType firstStateId,
																const NhwSubspace& Nhwsubspace,
																std::vector<std::pair<UN::SU3xSU2_VEC, IdType> >& vgamma)
{
	size_t nmax = distribution.size();
	std::vector<UN::SU3xSU2_VEC> vAllowedBaseIrreps;

    for (size_t n = 0; n < nmax; ++n)		
    {
		unsigned A = distribution[n];// number of fermions in n-th shell
		if (A == 0) {
    		continue;
		}
		else
		{
			UN::SU3xSU2_VEC AllowedIrreps;
			GetIrreps(n, A, AllowedIrreps);
			vAllowedBaseIrreps.push_back(AllowedIrreps);
		}
	}
    // Now we generate all possible combinations of coupling out of vvSU3xSU2Irreps
    size_t NIrrepsSets = vAllowedBaseIrreps.size();
	std::vector<unsigned> vElemsPerChange(NIrrepsSets, 1); //Fill with 1 cause vElemsPerChange[0] = 1;
	std::vector<size_t> vNSizes(NIrrepsSets);
	// uNAllowedCombinations = number of all possible coupling combinations
	// uNAllowedCombinations = vvSU3xSU2Irreps[0].size()* ... *vvSU3xSU2Irreps[NIrrepsSets].size()
    unsigned uNAllowedCombinations = 1;

    for (size_t i = 0; i < NIrrepsSets; i++)
    {
		vNSizes[i] = vAllowedBaseIrreps[i].size();	
   		uNAllowedCombinations *= vNSizes[i];
   	}

    for (size_t i = 1; i < NIrrepsSets; i++)
   	{
   		// if i == 0 => vElemsPerChange[0]=1 since constructor
		vElemsPerChange[i] = vElemsPerChange[i-1]*vNSizes[i-1];
   	}

	IdType firstStateInGammaId = firstStateId;
	vgamma.reserve(uNAllowedCombinations);
    for (unsigned index = 0; index < uNAllowedCombinations; index++)
    {
		// in each step create a set of SU3xSU2 irreps to couple 
		UN::SU3xSU2_VEC gamma;
		for (size_t i = 0; i < NIrrepsSets; i++)
		{
		    size_t iElement = (index / vElemsPerChange[i]) % vNSizes[i];
		    gamma.push_back(vAllowedBaseIrreps[i][iElement]);
   		}

		IdType nStatesInGamma = GetNumberOfStatesInGamma(gamma, Nhwsubspace.SU3SelectionRule(), Nhwsubspace.SpinSelectionRule(), maxnSU3Omegas_, maxnSU2Omegas_, maxnOmegasInGamma_);
		if (nStatesInGamma)
		{
			vgamma.push_back(std::make_pair(gamma, firstStateInGammaId));
			firstStateInGammaId += nStatesInGamma;
		}
  	}
//	we reserved memory for vgamma to hold uNAllowedCombinations elements.
//	However, not all gamma yield non-zero number of basis states => I need to
//	trim that excess capacity.
	if (vgamma.size() < uNAllowedCombinations)
	{
		std::vector<std::pair<UN::SU3xSU2_VEC, IdType> >(vgamma).swap(vgamma);
	}	
	return firstStateInGammaId;	//	this is the first state of the next gamma. In case the distribution is the last
								//	distribution in ncsm model space ==> firstStateInGammaId will correspond to dimension of the basis.
}

IdenticalFermionsNCSMBasis::IdenticalFermionsNCSMBasis(const int A, const NCSMModelSpace& ncsmModelSpace, int Jcut): 	
																						CBaseSU3Irreps(A, A, ncsmModelSpace.back().N()), 
																						ncsmModelSpace_(ncsmModelSpace), 
																						maxnOmegasInGamma_(0), 
																						maxnSU3Omegas_(0), 
																						maxnSU2Omegas_(0)
{
	assert(!ncsmModelSpace.empty());

	SU3xSU2::BasisIdenticalFermsCompatible::SetJcut(Jcut);
	
	CShellConfigurations ShellConfs(Nmax(), A);
	IdType firstStateInGammaId = 0;

	for (NCSMModelSpace::const_iterator Nhwsubspace = ncsmModelSpace_.begin(); Nhwsubspace != ncsmModelSpace_.end(); ++Nhwsubspace)
	{
		const std::vector<tSHELLSCONFIG>& ConfsNhw  = ShellConfs.GetConfigurations(Nhwsubspace->N());
		for (std::vector<tSHELLSCONFIG>::const_iterator distribution = ConfsNhw.begin(); distribution != ConfsNhw.end(); ++distribution)
		{
			NhwSubspace* pNhwsubspace = &ncsmModelSpace_[0] + (Nhwsubspace - ncsmModelSpace_.begin());
			std::vector<std::pair<UN::SU3xSU2_VEC, IdType> > vgamma;

			size_t nOmegasInDistribution = 0;
			firstStateInGammaId = GenerateGammasAndGammaIds(*distribution, firstStateInGammaId, *Nhwsubspace, vgamma);
			if (!vgamma.empty())	
			{
				distributions_.push_back(Distr(pNhwsubspace, *distribution, vgamma));
			}
		}
	}
	dim_ = firstStateInGammaId;	// at this moment dim_ = 0, since 

	vOmega_.reserve(maxnOmegasInGamma_);
	omega_su3_.reserve(maxnSU3Omegas_);
	omega_su2_.reserve(maxnSU2Omegas_);
	rewindDistr();
}


IdenticalFermionsNCSMBasis::IdType IdenticalFermionsNCSMBasis::dimDistr() const
{
	assert(hasDistr());	//	make sure we are not located at the end of the basis

	if (currentDistribution_ < (distributions_.end() - 1))
	{
		return (currentDistribution_ + 1)->vgamma_.front().second - currentDistribution_->vgamma_.front().second;
	}
	else	// currentDistribution_ == distributions_.end() - 1
	{
		assert(currentDistribution_ == (distributions_.end()-1));
		return dim_ - currentDistribution_->vgamma_.front().second;
	}
}

IdenticalFermionsNCSMBasis::IdType IdenticalFermionsNCSMBasis::dimGamma() const
{
	assert(hasGamma());	//	make sure we are not located at the end of the current gammas

	if (currentGamma_ < (currentDistribution_->vgamma_.end() - 1))
	{
		return (currentGamma_ + 1)->second - currentGamma_->second;
	}
	else
	{
		assert(currentGamma_ == (currentDistribution_->vgamma_.end() - 1));
		if (currentDistribution_ < (distributions_.end() - 1))
		{
			return (currentDistribution_ + 1)->vgamma_.front().second - currentGamma_->second;
		}
		else
		{
			return dim_ - currentGamma_->second;
		}
	}
}

IdenticalFermionsNCSMBasis::IdType IdenticalFermionsNCSMBasis::dimOmega() const
{
	assert(hasOmega()); 

	if (currentOmega_ < (vOmega_.end() - 1))
	{
		return (currentOmega_ + 1)->second - currentOmega_->second;
	}
	else	//	we are located at the last omega ==> get the first state of the next gamma
	{
		assert(currentOmega_ == (vOmega_.end() - 1));
		if ((currentGamma_ + 1) < currentDistribution_->vgamma_.end()) 
		{
			return (currentGamma_ + 1)->second - currentOmega_->second;
		}
		else	// but we are already at the last gamma => look at the first gamma of the next distribution
		{
			if ((currentDistribution_ + 1) < distributions_.end())
			{
				return (currentDistribution_ + 1)->vgamma_.front().second - currentOmega_->second;
			}
			else	//	but we are already at the last distribution ...
			{
				return dim_ - currentOmega_->second;
			}
		}
	}
}

void IdenticalFermionsNCSMBasis::rewindOmega()
{
	//	WARNING: it is assumed that gamma is transformed *currentGamma_
	//	TODO: I need to implement assert(IsIdentical(gamma, currentGamma_));
	if (vOmega_.empty())
	{
		if (currentGamma_->first.size() >= 2)
		{
			size_t a = gamma_mult(currentGamma_->first);
			IdType Id = currentGamma_->second;

			gamma2su2vec(currentGamma_->first, omega_su2_, CurrentSpinSelectionRule());
			gamma2su3vec(currentGamma_->first, omega_su3_, CurrentSU3SelectionRule());

			size_t nomegas = omega_su3_.size()*omega_su2_.size();	//	== omega.size()
			size_t nirreps = omega_su3_[0].size();				//	== number of SU3xSU2 irreps in each element of omega
			size_t iomega = 0;

			SU3xSU2_VEC omega(nirreps);
			for (std::vector<SU3_VEC>::const_iterator itsu3 = omega_su3_.begin(); itsu3 != omega_su3_.end(); ++itsu3)
			{
				for (size_t i = 0; i < nirreps; ++i)
				{
					omega[i].rho = (*itsu3)[i].rho;
					omega[i].lm  = (*itsu3)[i].lm;
					omega[i].mu  = (*itsu3)[i].mu;
				}

				SU3xSU2::LABELS IR0(itsu3->back(), 0);
				size_t mult = a*omega_mult(*itsu3);
				for (std::vector<SU2_VEC>::const_iterator itsu2 = omega_su2_.begin(); itsu2 != omega_su2_.end(); ++itsu2, ++iomega)
				{
					IR0.S2 = itsu2->back();
//					IdType nstatesGammaOmega = mult*SU3xSU2::BasisIdenticalFermsCompatible::GetNumberOfBasisStates(IR0);
					IdType nstatesGammaOmega = mult*SU3xSU2::BasisIdenticalFermsCompatible::getDim(IR0);
					if (nstatesGammaOmega)
					{
						for (size_t i = 0; i < nirreps; ++i)
						{
							omega[i].S2  = (*itsu2)[i];
						}
						vOmega_.push_back(std::make_pair(omega, Id));
						Id += nstatesGammaOmega;
					}
				}
			}
		}
		else
		{
			vOmega_.push_back(std::make_pair(SU3xSU2_VEC(), currentGamma_->second));
		}
	}
	currentOmega_ = vOmega_.begin();
}

#ifndef PROTON_NEUTRON_NCSMSU3BASIS_FAST_ITERATION_H
#define PROTON_NEUTRON_NCSMSU3BASIS_FAST_ITERATION_H
#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/CTuple.h>
#include <SU3ME/ModelSpaceExclusionRules.h>
#include <SU3ME/proton_neutron_ncsmSU3Basis.h>

class CncsmSU3BasisFastIteration
{
	private:
	enum Nucleon_Basis_Indices {kDistr = 0, kGamma = 1, kOmegaSu3 = 2, kOmegaSpin = 3};  
	enum Nucleon_Proton_Basis_Indices {kProtonBasisIndex = 0, kNeutronBasisIndex = 1, kRhoSU3xSU2Index = 2};  

//	[0] ... distribution index in vector distribs_(p/n)_
//	[1] ... gamma index in vector gammas_
//	[2] ... omega_su3 index in vector omegas_su3_
//	[3] ... omega_spin index in vector omegas_spin_
	typedef CTuple<unsigned short, 4>  NUCLEON_BASIS_INDICES;

//	[0] ... proton_basis index in vector proton_basis_
//	[1] ... neutron basis index in vector neutron_basis_
//	[2] ... {rho, irrep_index} ... multiplicity rho irrep_index, which is the index in wpn_irreps_container_
	typedef CTuple<unsigned int, 3>  PROTON_NEUTRON_BASIS_INDICES;
	private:
// integer structure: bits 1 ... 32 {irrep_index} 33 ... 64 {rho}  
	inline unsigned int KEY_INTEGER(unsigned short rho, unsigned short irrep_basis_index) const {return (rho << 16) | irrep_basis_index;}
	inline unsigned short Get_irrep_index(unsigned int current_index) const {return pn_basis_[current_index][kRhoSU3xSU2Index] & 0xFFFF;}
	inline unsigned short Get_rho(unsigned int current_index) const {return pn_basis_[current_index][kRhoSU3xSU2Index] >> 16;}

	void GenerateDistrGammaOmega_SortedVectors(	const CncsmSU3Basis& Basis, unsigned int idiag, unsigned int ndiag, 	
												std::vector<SingleDistribution>& distribs_p, std::vector<SingleDistribution>& distribs_n, 
												std::vector<UN::SU3xSU2_VEC>& gammas, std::vector<SU3_VEC>& omegas_su3, std::vector<SU2_VEC>& omegas_spin);
 // construct: distribs_p_, distribs_n_, gammas_, omegas_su3_, omegas_spin_, proton_basis_, neutron_basis_
	void Generate_ProtonBasis_NeutronBasis(const CncsmSU3Basis& basis, unsigned int idiag, unsigned int ndiag);

	public:
	CncsmSU3BasisFastIteration(const proton_neutron::ModelSpace& ncsmModelSpace, const int idiag, const int ndiag);
	public:
	enum NextPNConfStatus {kNewPNOmega = 0, kNewOmega_n = 1, kNewOmega_p = 2, kNewGamma_n = 3, kNewGamma_p = 4, kNewDistr_n = 5, kNewDistr_p = 6};

	void ShowMemoryRequirements() const;
	void ShowBasis(std::ostream& output_device);

	inline unsigned int protonConfIndex(unsigned int current_index) const {return pn_basis_[current_index][kProtonBasisIndex];}
	inline unsigned int neutronConfIndex(unsigned int current_index) const {return pn_basis_[current_index][kNeutronBasisIndex];}
	
	inline const NUCLEON_BASIS_INDICES& protonConf(unsigned int current_index) const {return proton_basis_[pn_basis_[current_index][kProtonBasisIndex]];}
	inline const NUCLEON_BASIS_INDICES& neutronConf(unsigned int current_index) const { return neutron_basis_[pn_basis_[current_index][kNeutronBasisIndex]]; }

	inline unsigned long getFirstStateId() const  {return std::accumulate(dims_.begin(), dims_.begin() + idiag_, (unsigned long)0);}
	inline unsigned long getModelSpaceDim() const {return std::accumulate(dims_.begin(), dims_.end(), (unsigned long)0);}
	inline unsigned int size() const {return pn_basis_.size();}
	inline unsigned int dim() const {return dims_[idiag_];}

	unsigned char status(const unsigned int current_index, const unsigned int index_last) const;
	unsigned int getMult_p(unsigned int current_index) const;
	unsigned int getMult_n(unsigned int current_index) const;
	inline unsigned int getProtonBasisSize() const {return proton_basis_.size();}
	inline unsigned int getNeutronBasisSize() const {return neutron_basis_.size();}
	
	inline unsigned int rhomax_x_dim(unsigned int current_index) const { return Get_rho(current_index)*(wpn_irreps_container_[Get_irrep_index(current_index)]).dim(); }

	inline const SingleDistribution* getDistr_p(unsigned int current_index) const { return &distribs_p_[proton_basis_[(pn_basis_[current_index])[kProtonBasisIndex]][kDistr]]; }
	inline const SingleDistribution* getDistr_n(unsigned int current_index) const { return &distribs_n_[neutron_basis_[(pn_basis_[current_index])[kNeutronBasisIndex]][kDistr]]; }

	inline void getDistr_p(unsigned int current_index, SingleDistribution& distr_p) const
	{
		const SingleDistribution* distr_ptr = getDistr_p(current_index);
		distr_p.insert(distr_p.end(), distr_ptr->begin(), distr_ptr->end());
	}
	inline void getDistr_n(unsigned int current_index, SingleDistribution& distr_n) const
	{
		const SingleDistribution* distr_ptr = getDistr_n(current_index);
		distr_n.insert(distr_n.end(), distr_ptr->begin(), distr_ptr->end());
	}

	inline const UN::SU3xSU2_VEC* getGamma_p(unsigned int current_index) const { return &gammas_[proton_basis_[(pn_basis_[current_index])[kProtonBasisIndex]][kGamma]]; }
	inline void getGamma_p(unsigned int current_index, UN::SU3xSU2_VEC& gamma_p) const
	{
		const UN::SU3xSU2_VEC* gamma_ptr = getGamma_p(current_index);
		gamma_p.insert(gamma_p.end(), gamma_ptr->begin(), gamma_ptr->end());
	}

	inline const UN::SU3xSU2_VEC* getGamma_n(unsigned int current_index) const { return &gammas_[neutron_basis_[(pn_basis_[current_index])[kNeutronBasisIndex]][kGamma]]; }
	inline void getGamma_n(unsigned int current_index, UN::SU3xSU2_VEC& gamma_n) const
	{
		const UN::SU3xSU2_VEC* gamma_ptr = getGamma_n(current_index);
		gamma_n.insert(gamma_n.end(), gamma_ptr->begin(), gamma_ptr->end());
	}

	inline const SU3_VEC* getOmega_p_Su3(unsigned int current_index) const { return &omegas_su3_[proton_basis_[(pn_basis_[current_index])[kProtonBasisIndex]][kOmegaSu3]]; }
	inline const SU3_VEC* getOmega_n_Su3(unsigned int current_index) const { return &omegas_su3_[neutron_basis_[(pn_basis_[current_index])[kNeutronBasisIndex]][kOmegaSu3]]; }
	inline const SU2_VEC* getOmega_p_Spin(unsigned int current_index) const { return &omegas_spin_[proton_basis_[(pn_basis_[current_index])[kProtonBasisIndex]][kOmegaSpin]]; }
	inline const SU2_VEC* getOmega_n_Spin(unsigned int current_index)  const { return &omegas_spin_[neutron_basis_[(pn_basis_[current_index])[kNeutronBasisIndex]][kOmegaSpin]]; }

	//	return labels of SU3xSU2 irrep for the proton final configuration
	void getOmega_p(unsigned int current_index, SU3xSU2_VEC& omega_p) const;
	//	return labels of SU3xSU2 irrep for the proton final configuration
	SU3xSU2::LABELS getProtonSU3xSU2(unsigned int current_index) const;

	void getOmega_n(unsigned int current_index, SU3xSU2_VEC& omega_n) const;
	//	return labels of SU3xSU2 irrep for the neutron final configuration
	SU3xSU2::LABELS getNeutronSU3xSU2(unsigned int current_index) const;

	SU3xSU2::LABELS getOmega_pn(unsigned int current_index) const
	{
		unsigned short irrep_index = Get_irrep_index(current_index);
		return SU3xSU2::LABELS(Get_rho(current_index), wpn_irreps_container_[irrep_index].lm, wpn_irreps_container_[irrep_index].mu, wpn_irreps_container_[irrep_index].S2);
	}
//	get index pointing to a table of all possible SU3xSU2 final irreps and their basis
	inline const IRREPBASIS& Get_Omega_pn_Basis(unsigned int current_index) const {return wpn_irreps_container_[Get_irrep_index(current_index)];}
	private:
	unsigned int idiag_;
	std::vector<unsigned long> dims_;

	std::vector<SingleDistribution> distribs_p_; 
	std::vector<SingleDistribution> distribs_n_;
	std::vector<UN::SU3xSU2_VEC> gammas_;
	std::vector<SU3_VEC> omegas_su3_; 
	std::vector<SU2_VEC> omegas_spin_;
	SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container_;

	std::vector<NUCLEON_BASIS_INDICES> proton_basis_;
	std::vector<NUCLEON_BASIS_INDICES> neutron_basis_;
	std::vector<PROTON_NEUTRON_BASIS_INDICES> pn_basis_;
};
#endif

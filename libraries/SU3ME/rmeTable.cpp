#include <SU3ME/rmeTable.h>

#include <boost/archive/binary_iarchive.hpp> 
#include <boost/archive/binary_oarchive.hpp> 
#include <boost/archive/text_iarchive.hpp> 
#include <boost/archive/text_oarchive.hpp> 

#include <stdexcept>
#include <stack>

//	using structure of a tensor and its SU(3)xSU(2) tensor labels create a common root of the file name
std::string GetRMETableFileNameRoot(const std::vector<char>& structure, const SU3xSU2_VEC& tensorLabels)
{
	assert(!structure.empty() && !tensorLabels.empty());

	const char n = abs(structure[0]) - 1;
	size_t i;
	if (su3shell_data_directory == NULL) {
		std::cerr<< "SU3SHELL_DATA environment variable is not set." << std::endl;
		exit (-1);
	}
	std::string sRMETableFile(su3shell_data_directory);
	sRMETableFile += "/rme/";
	std::ostringstream sn;
	sn << (int)n;
	for (i = 0; i < structure.size(); ++i)
	{
		if (structure[i] > 0)
		{
			sRMETableFile += "a+";
		}
		else if (structure[i] < 0) 
		{
			sRMETableFile += "ta";
		}
	}
	sRMETableFile += "n";
	sRMETableFile += sn.str();

	for (i = 0; i < tensorLabels.size(); ++i)
	{
		std::ostringstream snumber;
		snumber << "__" << (int)tensorLabels[i].rho << "-" << (int)tensorLabels[i].lm << "-" << (int)tensorLabels[i].mu << "-" << (int)tensorLabels[i].S2;
		sRMETableFile += snumber.str();
	}
	sRMETableFile += ".rme";
	return sRMETableFile;
}

inline std::string GetRMETableFileName_bin(const std::vector<char>& structure, const SU3xSU2_VEC& tensorLabels)
{ 
	return std::string(GetRMETableFileNameRoot(structure, tensorLabels) + "_bin"); 
}

inline std::string GetRMETableFileName_text(const std::vector<char>& structure, const SU3xSU2_VEC& tensorLabels)
{ 
	return std::string(GetRMETableFileNameRoot(structure, tensorLabels) + "_text"); 
}


bool CrmeTable::IsTensorEqual(const std::vector<char>& structure, const SU3xSU2_VEC& TensorLabels) const
{ 
	if (tensorLabels_.size() != TensorLabels.size()) 
	{
		return false;
	}
	for (size_t i = 0; i < tensorLabels_.size(); ++i)
	{
		if ((tensorLabels_[i].rho != TensorLabels[i].rho) || (tensorLabels_[i].lm != TensorLabels[i].lm) || (tensorLabels_[i].mu != TensorLabels[i].mu) || (tensorLabels_[i].S2 != TensorLabels[i].S2))
//		if (!(tensorLabels_[i] == TensorLabels[i]))
		{
			return false;
		}
	}
	return (structure_ == structure);
}

size_t CrmeTable::CalculateMemorySize()
{
	size_t nBytes = sizeof(CrmeTable) + tensorLabels_.size()*sizeof(SU3xSU2::LABELS) + rmeLookUpTables_.size()*sizeof(RME_TABLE_ENTRY) + rmes_.size()*sizeof(SU3xSU2::RME::DOUBLE);
	for (int i = 0; i < rmeLookUpTables_.size(); ++i)
	{
		nBytes += rmeLookUpTables_[i].size() * sizeof(RME_TABLE_ENTRY);
	}
	return nBytes;
}

void CrmeTable::ShowTensorLabels()
{
	std::cout << "{";
	for (size_t i = 0; i < tensorLabels_.size() - 1; ++i)
	{
		std::cout << tensorLabels_[i] << ", ";
	}
	std::cout << tensorLabels_.back() << "}";
}

void CrmeTable::Show(const CBaseSU3Irreps& BaseSU3Irreps)
{
	char dA = GetdA();
	const unsigned char n = GetHOShellNumber();
	size_t i, j;
	UN::SU3xSU2 Bra, Ket;
	size_t multKet, multBra;
	size_t Ai, Af, nrme, irhot, irho0, ibra, iket, rhotmax, rho0max, index_rme;
	rho0max = abs(tensorLabels_.back().rho);

	ShowTensorLabels();

	for (i = 0; i < rmeLookUpTables_.size(); ++i)
	{
		Ai = i + Aimin_;
		Af = Ai + dA;
		std::cout << "Af = " << Af << "\t" << "Ai = " << Ai << std::endl;
		for (j = 0; j < rmeLookUpTables_[i].size(); ++j)
		{
			(rmeLookUpTables_[i][j].first).GetLabels(Bra, Ket);
			uint32_t ipos = rmeLookUpTables_[i][j].second;
			multKet = (Ai > 0) ? BaseSU3Irreps.GetMultiplicity(n, Ai, Ket.S2, Ket.lm, Ket.mu) : 1;
			multBra = (Af > 0) ? BaseSU3Irreps.GetMultiplicity(n, Af, Bra.S2, Bra.lm, Bra.mu) : 1;
			rhotmax = SU3::mult(Ket, tensorLabels_.back(), Bra);

			uint16_t nrme = GetNumberOfRmes(i, j);
			assert(nrme == multBra * multKet * rhotmax * rho0max);

			std::cout << (rmeLookUpTables_[i][j].first).m_Su3Labels << " " << (rmeLookUpTables_[i][j].first).m_Spin;
			std::cout << "\t\t<af = " << multBra << " (" << (int)Bra.lm << " " << (int)Bra.mu << ")" << (int)Bra.S2;
			std::cout << "||| T |||";
			std::cout << "ai = " << multKet << " (" << (int)Ket.lm << " " << (int)Ket.mu << ")" << (int)Ket.S2 << ">" << std::endl;
			for (ibra = 0; ibra < multBra; ++ibra)
			{
				for (iket = 0; iket < multKet; ++iket)
				{
					for (irho0 = 0; irho0 < rho0max; ++irho0)
					{
						for (irhot = 0; irhot < rhotmax; ++irhot)
						{
							index_rme = ibra*(multKet*rho0max*rhotmax) + iket*(rho0max*rhotmax) + irho0*rhotmax + irhot;
							cout << "\t\t\t\taf = " << ibra << " ai = " << iket << " rho0 = " << irho0 << " rhot = " << irhot << "\t\t" << rmes_[ipos++] << std::endl;
						}
					}
				}
			}
		}
	}
	std::cout << "This structure uses " << CalculateMemorySize()/1024.0 << "kb of memory" << endl;
}

SU3xSU2::RME::DOUBLE* CrmeTable::GetRME_Ai(const UN::SU3xSU2& omegaf, const UN::SU3xSU2& omegai, unsigned char Ai)
{
	RMEKey Key(omegaf, omegai);
	size_t index = Ai - Aimin_;
	assert(index < rmeLookUpTables_.size());
	RME_TABLE_ENTRY::const_iterator cit = std::lower_bound(rmeLookUpTables_[index].begin(), rmeLookUpTables_[index].end(), Key, RMEKEY_EQUAL());

	if (cit != rmeLookUpTables_[index].end() && Key == cit->first) 
	{
		return &rmes_[cit->second];
	}
	return NULL;
}

SU3xSU2::RME::DOUBLE* CrmeTable::GetRME_index(const UN::SU3xSU2& omegaf, const UN::SU3xSU2& omegai, size_t index)
{
	assert(index < rmeLookUpTables_.size());

	RMEKey Key(omegaf, omegai);
	RME_TABLE_ENTRY::const_iterator cit = std::lower_bound(rmeLookUpTables_[index].begin(), rmeLookUpTables_[index].end(), Key, RMEKEY_EQUAL());

	if (cit != rmeLookUpTables_[index].end() && Key == cit->first) 
	{
		return &rmes_[cit->second];
	}
	return NULL;
}



uint16_t CrmeTable::GetNumberOfRmes(uint32_t i, uint32_t j) const
{
	if (j < (rmeLookUpTables_[i].size() - 1))	// j is not the last one
	{
		return (rmeLookUpTables_[i][j + 1].second - rmeLookUpTables_[i][j].second);
	}
	else if (i < (rmeLookUpTables_.size() - 1))	// although j is the last one, there is another set of data in rmeLookUpTables_[i+1]
	{
		return (rmeLookUpTables_[i + 1][0].second - rmeLookUpTables_[i][j].second);
	}
	else // we are at the very end 
	{
		return rmes_.size() - rmeLookUpTables_[i][j].second;
	}
}

// Load rme table and return the number fermions in the last ket state (Ai) that was read from the input file
size_t CrmeTable::LoadRMETable(std::ifstream& RMETableFile, bool useBinaryIO)
{
	assert(structure_.empty());
	assert(tensorLabels_.empty());

	if (useBinaryIO)
	{
		boost::archive::binary_iarchive ia(RMETableFile);
		ia >> *this;
	}	
	else
	{
		boost::archive::text_iarchive ia(RMETableFile);
		ia >> *this;
	}

	size_t Ai_max = Aimin_ + rmeLookUpTables_.size() - 1;
	return Ai_max;
}

void CrmeTable::SaveRMETable(bool useBinaryIO)
{
	if (useBinaryIO)	// ==> save as a BINARY archive
	{
		std::ofstream RMETableFile(GetRMETableFileName_bin(structure_, tensorLabels_).c_str(), std::ios::binary |  std::ios::out | std::ios::trunc);  
		boost::archive::binary_oarchive oa(RMETableFile);
		oa << *this;
	}
	else	//	==> save as a TEXT archive
	{
		std::ofstream RMETableFile(GetRMETableFileName_text(structure_, tensorLabels_).c_str(), std::ios::out | std::ios::trunc);  
		boost::archive::text_oarchive oa(RMETableFile);
		oa << *this;
	}
}

void CrmeTable::SaveRMETableOldFormat()
{
	std::string file_name = GetRMETableFileNameRoot(structure_, tensorLabels_).c_str();
	std::ofstream RMETableFile(file_name.c_str());  
	RMETableFile.precision(10);
	UN::SU3xSU2 Ket;
	UN::SU3xSU2 Bra;
	for (size_t i = 0; i < rmeLookUpTables_.size(); ++i)
	{
		size_t Ai = Aimin_ + i;
		for (size_t j = 0; j < rmeLookUpTables_[i].size(); ++j)
		{
			(rmeLookUpTables_[i][j].first).GetLabels(Bra, Ket);
			uint16_t ipos = rmeLookUpTables_[i][j].second;
			uint16_t nrme = GetNumberOfRmes(i, j);

			RMETableFile << Ai << "\t";
			RMETableFile << (int)Bra.lm << " " << (int)Bra.mu << " " << (int)Bra.S2 << "\t";
			RMETableFile << (int)Ket.lm << " " << (int)Ket.mu << " " << (int)Ket.S2 << "\t";
			RMETableFile << nrme << " ";
			for (size_t k = 0; k < nrme; ++k)
			{
				RMETableFile << rmes_[ipos + k] << " ";
			}
			RMETableFile << std::endl;
		}
	}
}

void CrmeTable::CreateTensorFromStructure(std::stack<SU3xSU2::Tensor>& st)
{
	size_t index(0);
	unsigned char n = GetHOShellNumber();
	for (int i = 0; i < structure_.size(); ++i)
	{
		if (structure_[i] != 0)
		{	
			if (structure_[i] > 0)
			{
				st.push(SU3xSU2::Creation(n));
			}
			else
			{
				st.push(SU3xSU2::Annihilation(n));
			}
		}
		else // if structure[i] == 0
		{
			SU3xSU2::Tensor RightTensor(st.top()); st.pop();
			SU3xSU2::Tensor LeftTensor(st.top()); st.pop();

			if (RightTensor.IsPureType() && RightTensor.GetNBasisOperators() > 1) // PureType == true if the single-shell tensor is composed entirely either from a+ or ta operators.
			{
				RightTensor.EliminateRedundancy();
			}
			if (LeftTensor.IsPureType() && LeftTensor.GetNBasisOperators() > 1) 
			{
				LeftTensor.EliminateRedundancy();
			}
			SU3xSU2::Tensor CoupledTensor(LeftTensor, 0, RightTensor, 0, tensorLabels_[index++]);
			st.push(CoupledTensor);
		}
	}
}

//	Note: TensorLabels is never empty!!!
CrmeTable::CrmeTable(const std::vector<char>& structure, const std::vector<SU3xSU2::LABELS>& TensorLabels, const CBaseSU3Irreps& BaseSU3Irreps, bool generate_rme, bool log_is_on, std::ostream& log_file, bool binaryIO)
{
	assert(!TensorLabels.empty()); 	//	tensor labels must not be empty, since CrmeTable uses methods lm(), mu(), rho(), S2(), which
									//	returns value tensorLabels_[m_nLabels - 1].rho, lm, mu etc...
									//	==> we need to ensure that tensorLabels_ has at least one element!!!
	std::string file_name = (binaryIO) ? GetRMETableFileName_bin(structure, TensorLabels) : GetRMETableFileName_text(structure, TensorLabels);
	std::ifstream RMETableFile;
	if (!binaryIO)
	{
		RMETableFile.open(file_name.c_str()); // open text input file
	}
	else
	{
		RMETableFile.open(file_name.c_str(), std::ios::binary | std::ios::in); // open binary input file
	}

	if (!RMETableFile) // input file does not exist
	{
		if (generate_rme)
		{
			structure_    = structure;
			tensorLabels_ = TensorLabels;
			rmes_.reserve(1024);

			if (log_is_on)
			{
				log_file << "Not able to open file storing rme: " << file_name << " ==> calculating rme on-the-fly.\n";
			}
////////////////////////////////////////////////////////////////////		
//	Construct SU3xSU2::Tensor from structure and tensorLabels_
////////////////////////////////////////////////////////////////////		
			std::stack<SU3xSU2::Tensor> st; // we do not have troubles with slicing since SU3xSU2::Creation and SU3xSU2::Annihilation are in fact just a
											// spetial instances of SU3xSU2::Tensor
			CreateTensorFromStructure(st);
			SU3xSU2::Tensor ResultingTensor = st.top(); st.pop();
			assert(st.empty());
////////////////////////////////////////////////////////////////////		
////////////////////////////////////////////////////////////////////		
			unsigned char n = GetHOShellNumber();
			Aimin_ = GetNumberOfAnnihilators(); // a+, a++ have Aimin_ = 0 ===> we have <Af = dA IR0 ||| T^{IR0} ||| Ai = 0 (0 0) 0 > = 1.0
			int Amax = BaseSU3Irreps.GetAmax(n);
			int Ai_max = std::min(Amax, Amax - GetdA()); // Reason for std::max: if dA < 0 (e.g. tata when dA = -2) ===> Ai_max would be > Amax (e.g. Ai_max = Amax + 2 in case of tata) !!!
			int nRMEEntries = Ai_max - Aimin_ + 1; 

			rmeLookUpTables_.resize(nRMEEntries);  // rmeLookUpTables_[0] ... Ai=Aimin_, rmeLookUpTables_[1] ... Ai=(Aimin_+1), ... rmeLookUpTables_[nRMEEntries-1] ... Ai=Ai_max

			if (nSPS(n) <= 32)
			{
				CalculateSortRMETable<UN::BASIS_STATE_32BITS>(Aimin_, ResultingTensor, BaseSU3Irreps);
			}
			else if (nSPS(n) <= 64)
			{
				CalculateSortRMETable<UN::BASIS_STATE_64BITS>(Aimin_, ResultingTensor, BaseSU3Irreps);
			}
			else if (nSPS(n) <= 128)
			{
				CalculateSortRMETable<UN::BASIS_STATE_128BITS>(Aimin_, ResultingTensor, BaseSU3Irreps);
			}
			else if (nSPS(n) <= 256)
			{
				CalculateSortRMETable<UN::BASIS_STATE_256BITS>(Aimin_, ResultingTensor, BaseSU3Irreps);
			}
			else 
			{
				std::string error_message("Calculation of single-shell rme for harmonic oscillator shell n = ");
				error_message += n;
				error_message += " has not been implemented yet!";
				throw std::logic_error(error_message);
			}
			SaveRMETable(binaryIO);
		}
		else
		{
			std::string error_message("Not able to open file storing rme: ");
			error_message += file_name;
			error_message += "!\n ==> terminating calculation.";
			throw std::logic_error(error_message);
		}
	}
	else
	{
		if (log_is_on)
		{
			log_file << "Loading rme from " << file_name << " file" << "\t ... ";
		}
		size_t AiLastIncluded = LoadRMETable(RMETableFile, binaryIO);	// return the last value of Ai that was read from the input file
		int Amax = BaseSU3Irreps.GetAmax(GetHOShellNumber());
		int Ai_max = std::min(Amax, Amax - GetdA()); 
		int nRMEEntries = Ai_max - Aimin_ + 1; //	required number of elements for vector rmeLookUpTables_

		if (nRMEEntries < rmeLookUpTables_.size())	// the input file contains more data than needed ==> trim whatever we don't need
		{
			uint32_t ipos = rmeLookUpTables_[nRMEEntries][0].second; // position of the first rme which isn't needed
			rmes_.erase(rmes_.begin() + ipos, rmes_.end());

			// we need to keep rmeLookUpTables_[0] ... rmeLookUpTables_[nRMEEntries - 1]
			rmeLookUpTables_.erase(rmeLookUpTables_.begin() + nRMEEntries, rmeLookUpTables_.end());

			assert(nRMEEntries == rmeLookUpTables_.size());

			//	erase does not trim allocated memory ==> we have to do trimming explicitly
			std::vector<RME_TABLE_ENTRY>(rmeLookUpTables_).swap(rmeLookUpTables_);
			std::vector<SU3xSU2::RME::DOUBLE>(rmes_).swap(rmes_);
		}

		if (nRMEEntries > rmeLookUpTables_.size()) // ==> there are Ai's that are required by a given model space, but they are missing in the input file
		{
			if (generate_rme)
			{
				if (log_is_on)
				{
					log_file << "It seems that the rme table file " << file_name << " does not contain all Ai that are needed.\n";
					log_file  << "Calculating rme for ";
					for (size_t Ai = AiLastIncluded + 1; Ai <= Ai_max; ++Ai)
					{
						log_file << Ai << " ";
					}
					log_file << "\n";
				}
////////////////////////////////////////////////////////////////////		
//	in order to calculate rme on-the-fly, we need to construct 
//	SU3xSU2::Tensor from structure and tensorLabels_
////////////////////////////////////////////////////////////////////		
				std::stack<SU3xSU2::Tensor> st; // we do not have troubles with slicing since SU3xSU2::Creation and SU3xSU2::Annihilation are in fact just a
												// spetial instances of SU3xSU2::Tensor
				CreateTensorFromStructure(st);
				SU3xSU2::Tensor ResultingTensor = st.top(); st.pop();
				assert(st.empty());
///////////////////////////////////////////////////////////////		
				unsigned char n = GetHOShellNumber();
				// Create empty elements in rmeLookUpTables_ corresponding to those Ai that are missing, 
				// since method CalculateSortRMETable assumes: (1) they exist & (2) they are empty
				rmeLookUpTables_.insert(rmeLookUpTables_.end(), (nRMEEntries - rmeLookUpTables_.size()), RME_TABLE_ENTRY());
				rmes_.reserve(rmes_.size() + 1024); // reserve enough of memory to avoid reallocation of array .. it may not be enough
				if (nSPS(n) <= 32)
				{
					CalculateSortRMETable<UN::BASIS_STATE_32BITS>(AiLastIncluded + 1, ResultingTensor, BaseSU3Irreps);
				}
				else if (nSPS(n) <= 64)
				{
					CalculateSortRMETable<UN::BASIS_STATE_64BITS>(AiLastIncluded + 1, ResultingTensor, BaseSU3Irreps);
				}
				else if (nSPS(n) <= 128)
				{
					CalculateSortRMETable<UN::BASIS_STATE_128BITS>(AiLastIncluded + 1, ResultingTensor, BaseSU3Irreps);
				}
				else if (nSPS(n) <= 256)
				{
					CalculateSortRMETable<UN::BASIS_STATE_256BITS>(AiLastIncluded + 1, ResultingTensor, BaseSU3Irreps);
				}
				else
				{
					std::string error_message("Calculation of single-shell rme for harmonic oscillator shell n = ");
					error_message += n;
					error_message += " has not been implemented yet!";
					throw std::logic_error(error_message);
				}
				SaveRMETable(binaryIO); // save all rme 
			}
			else
			{
				std::string error_message("It seems that rme table file ");
				error_message += file_name;
				error_message += " does not contain all Ai that are needed!\n";
				throw std::logic_error(error_message);
			}
		}
		if (log_is_on)
		{
			log_file << "Done" << std::endl;
		}
		// We do not need to sort RMETable that is loaded from the file as it is automatically sorted in CalculateRMETable
	}		
}

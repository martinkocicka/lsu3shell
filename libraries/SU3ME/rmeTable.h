#ifndef RMETABLE_H
#define RMETABLE_H
#include <SU3ME/global_definitions.h>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <SU3ME/CHwsGen.h>
#include <SU3ME/Tensor.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <SU3ME/RME.h>

#include <stack>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/utility.hpp>

//	Note that RMEKey does not store information on the inner multiplicity, i.e.
//	occuring in UN>SU(3)xSU(2) reduction, It is because we store rme for all
//	multiplicities <Af multf (lmf muf)Sf ||| T^rho0(lm0 mu0)S0 ||| Ai multi
//	(lmi mui)Si>_rhot and it is up to external code to obtain that multiplicity
//	using, e.g. CBaseSU3Irreps 
struct RMEKey
{
	uint32_t m_Su3Labels;
	uint16_t m_Spin;

	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & m_Su3Labels;
        ar & m_Spin;
    }
	
	inline RMEKey(const UN::SU3xSU2& Bra, const UN::SU3xSU2& Ket):m_Su3Labels((Bra.lm << 24) | (Bra.mu << 16) | (Ket.lm << 8) | Ket.mu), m_Spin((Bra.S2 << 8) | Ket.S2) {}
	RMEKey() {};
	inline void SetKey(const UN::SU3xSU2& Bra, const UN::SU3xSU2& Ket) 
	{ 
		m_Su3Labels = (Bra.lm << 24) | (Bra.mu << 16) | (Ket.lm << 8) | Ket.mu; 
		m_Spin = (Bra.S2 << 8) | Ket.S2;
	}
	inline void GetLabels(UN::SU3xSU2& Bra, UN::SU3xSU2& Ket) const
	{
		Ket.mu = m_Su3Labels & 0xFF;
		Ket.lm = (m_Su3Labels >> 8) & 0xFF;
		Bra.mu = (m_Su3Labels >> 16) & 0xFF;
		Bra.lm = (m_Su3Labels >> 24) & 0xFF;
		Ket.S2 = m_Spin & 0xFF;
		Bra.S2 = (m_Spin >> 8) & 0xFF;
	}
	inline bool operator< (const RMEKey& rhs) const { return (m_Su3Labels < rhs.m_Su3Labels || (m_Su3Labels == rhs.m_Su3Labels && m_Spin < rhs.m_Spin)); }
	inline bool operator!=(const RMEKey& rhs) const { return (m_Su3Labels != rhs.m_Su3Labels || m_Spin != rhs.m_Spin); }
	inline bool operator==(const RMEKey& rhs) const { return (m_Su3Labels == rhs.m_Su3Labels && m_Spin == rhs.m_Spin); }
};


inline std::ostream& operator << (std::ostream& os, const RMEKey& rmekey)
{
	UN::SU3xSU2 Bra, Ket;
	rmekey.GetLabels(Bra, Ket);
	return (os << Bra << " " << Ket);
}

/////////////////////////////////////////
////////////    CrmeTable   //////////////
/////////////////////////////////////////
//
//	This class maintain table of rme for single-shell tensor whose tensorial
//	sctucture is completely described by 
//	(1) structure_ (e.g. 3 -3 -3 0 0 for tensor {a+ {tata}}
//	and
//	(2)	tensorLabels_ data (e.g. 1(0 4)0; 1(0 2)1/2)  
//
//	Aimin ... equals to number of annihilation operators
//	rmeLookUpTables_.size() ... equals to Ai_max - Aimin_ + 1
//
//	Crme table stores rme for all multiplicities <Af ... (lmf muf)Sf ||| T^...(lm0 mu0)S0 ||| Ai ... (lmi mui)Si>_{...}
//	... stands for all possible multiplicities multi, multf, rho0, and rhot. 
//
//	Ai = Ai_min ==> rmeLookUpTables_[0] = {
//										<mRMEKey[(lmf muf)Sf, (lmi mui)Si], 		-->{<Ai+dA ... (lmf muf)Sf ||| T^...(lm0 mu0)S0 ||| Ai=Ai_min ... (lmi mui)Si>_{...}}>, 
//										<mRMEKey[(lmf' muf')Sf', (lmi' mui')Si'], 	-->{<Ai+dA ... (lmf' muf')Sf' ||| T^...(lm0 mu0)S0 ||| Ai=Ai_min ... (lmi' mui')Si'>_{...}}>, 
//										.
//										.
//										.
//									}
//						.
//						.
//						.
//	Ai = Amax ==> rmeLookUpTables_[rmeLookUpTables_.size()-1]= {
//										<mRMEKey[(lmf muf)Sf, (lmi mui)Si], 		-->{<Ai+dA ... (lmf muf)Sf ||| T^...(lm0 mu0)S0 ||| Ai=Amax ... (lmi mui)Si>_{...}}>, 
//										<mRMEKey[(lmf' muf')Sf', (lmi' mui')Si'], 	-->{<Ai+dA ... (lmf' muf')Sf' ||| T^...(lm0 mu0)S0 ||| Ai=Amax ... (lmi' mui')Si'>_{...}}>, 
//										.
//										.
//										.
//									}
class CrmeTable
{
	//	<vacuum ||| ta ||| (n 0) 1/2>
	inline SU3xSU2::RME::DOUBLE vacuum_ta_n0() 
	{ 
		char n = tensorLabels_.back().mu;
		return sqrt((SU3xSU2::RME::DOUBLE)(n + 1)*(n + 2)); 
	}

	//	<w0 ||| {a+ x a+}^{w0 S0} ||| vacuum> 
	inline SU3xSU2::RME::DOUBLE w0_adad_vacuum() 
	{
#ifdef FRONT_CREATE		
		return SQRT(2);
#elif defined(REAR_CREATE)
		return -SQRT(2);
#endif		
	}

	//	<vacuum ||| {ta x ta}^{w0 S0} || tw0 S0>
	inline SU3xSU2::RME::DOUBLE vacuum_tata_w0() 
	{
		SU3xSU2::LABELS tw0 = SU3::Conjugate(tensorLabels_.back()); // I could have used this irrep without conjugation, but I want to be as clear as possible 
#ifdef FRONT_CREATE		
		return  MINUSto(1 - tw0.S2/2 - tw0.lm - tw0.mu)*SQRT(2*(tw0.S2 + 1))*SQRT(SU3::dim(tw0)); 
#elif defined(REAR_CREATE)
		return -MINUSto(1 - tw0.S2/2 - tw0.lm - tw0.mu)*SQRT(2*(tw0.S2 + 1))*SQRT(SU3::dim(tw0)); 
#endif		
	}

	template<typename UN_BASIS_STATE> void CalculateSortRMETable(const size_t StartingAi, const SU3xSU2::Tensor& Tensor, const CBaseSU3Irreps& BaseSU3Irreps);

	size_t LoadRMETable(std::ifstream& RMETableFile, bool useBinaryIO);
	void SaveRMETable(bool useBinaryIO);
	void SaveRMETableOldFormat();
	void CreateTensorFromStructure(std::stack<SU3xSU2::Tensor>& st);
	uint16_t GetNumberOfRmes(uint32_t i, uint32_t j) const;
	inline char rho() const {return tensorLabels_.back().rho;}
	inline char S2() const {return tensorLabels_.back().S2;}
	inline char lm() const {return tensorLabels_.back().lm;}
	inline char mu() const {return tensorLabels_.back().mu;}
	inline unsigned char GetHOShellNumber() const {return abs(structure_[0])-1;} // Get number of shell in which single-shell tensor represented by CrmeTable acts
	inline unsigned char GetNumberOfAnnihilators() const { return std::count_if(structure_.begin(), structure_.end(), std::bind2nd(std::less<char>(), 0));}
	inline unsigned char GetNumberOfCreators() const { return std::count_if(structure_.begin(), structure_.end(), std::bind2nd(std::greater<char>(), 0));}
	inline char GetdA() const { return GetNumberOfCreators() - GetNumberOfAnnihilators();}
	inline bool IsPureType() const	// true if structure contains besides zeros, only one kind of elements;
	{
		return (structure_.size() - std::count(structure_.begin(), structure_.end(), 0) - std::count(structure_.begin(), structure_.end(), structure_[0])) == 0;
	}
	inline char GetAimin() const {return Aimin_;}
	inline char GetAimax() const {return rmeLookUpTables_.size() + Aimin_ -1;} // since rmeLookUpTables_.size() = Aimax - Aimin_ + 1 ... see CrmeTable::CrmeTable(...)
	inline const std::vector<char>& GetStructure() const {return structure_;}
	inline operator SU3::LABELS() {return tensorLabels_.back();}
	public:
	CrmeTable() {}
	CrmeTable(	const std::vector<char>& structure, const std::vector<SU3xSU2::LABELS>& TensorLabels, const CBaseSU3Irreps& BaseSU3Irreps, 
				bool generate_rme = true, bool log_is_on = true, std::ostream& log_file = std::cout, bool binaryIO = true);


	SU3xSU2::RME::DOUBLE* GetRME_Ai(const UN::SU3xSU2& omegaf, const UN::SU3xSU2& omegai, unsigned char Ai);
	SU3xSU2::RME::DOUBLE* GetRME_index(const UN::SU3xSU2& omegaf, const UN::SU3xSU2& omegai, size_t index);
	bool IsTensorEqual (const std::vector<char>& structure, const SU3xSU2_VEC& TensorLabels) const;
	inline operator SU3xSU2::LABELS() {return tensorLabels_.back();}

	void Show(const CBaseSU3Irreps& BaseSU3Irreps);
	size_t CalculateMemorySize();
	void ShowTensorLabels();
	public:
	typedef std::pair<RMEKey, uint16_t> KEY_FIRST_RME;	// 65535 rmes is most likely enough for the most cases
	private:

    friend class boost::serialization::access;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
		ar & Aimin_;
		ar & tensorLabels_;
		ar & structure_;
        ar & rmeLookUpTables_;
        ar & rmes_;
    }

	typedef std::vector<KEY_FIRST_RME> RME_TABLE_ENTRY;
//	IMPORTANT: Note that RMEKey does not depend on multf and multfi. It is
//	assumed that this information is known to the outer code, using e.g.
//	that maximal mult is readily available in class CBasuSU3Irreps.
	std::vector<RME_TABLE_ENTRY> rmeLookUpTables_;
	std::vector<SU3xSU2::RME::DOUBLE> rmes_;

	std::vector<char> structure_;		// 	structure of the single-shell tensors in terms of creation/annihilation operators
										//	and their coupling creation of the single shell operator ... reverse polish notation

	SU3xSU2_VEC tensorLabels_;	//	SU3xSU2 labels descriging to zeros in structure_, i.e. labels resulting from coupling subgroups of tensors

	unsigned char Aimin_;				//	minimal number of Ai that is allowed for this tensor, e.g. Aimin_ = 2 for tata tensor, since < ||| tata |||A=1 ...> = < ||| tata |||vacuum ...> = vacuum
										//	it is thus equal to number of annihilation operators in single-shell tensor
};

//	This method is launched by CrmeTable constructor, when it is unable to open
//	file containing rme of a given single-shell tensor. If such a file does not
//	exist, it tries to calculate rme on the-fly using this method.
template<typename UN_BASIS_STATE>
void CrmeTable::CalculateSortRMETable(const size_t StartingAi, const SU3xSU2::Tensor& Tensor, const CBaseSU3Irreps& BaseSU3Irreps)
{
	std::vector<SU3xSU2::RME::DOUBLE> rmes_temp; 
	rmes_temp.reserve(1024);

	assert(abs(Tensor.rho) == rho());

	int n = GetHOShellNumber();

	size_t nsps = nSPS(n);
	int mult, lm, mu; 
	size_t Ai, Af;
	int dA = GetdA();

	RMEKey Key;
	UN::SU3xSU2 Bra, Ket;

	// if we are unable to open file with HWS, we generate this file on-the-fly
	// these are the two parameters of the HWS generator: CNullSpaceSolver::SolverType and HWSOutputType = HWSOutputText/HWSOutputBin
	// we choose: QR with column pivoting and text file output
	CNullSpaceSolver::SolverType NullSpaceMethodType = CNullSpaceSolver::ColPivotQR_Eigen;
	HWSOutputType OutputType = HWSOutputText;

	for (size_t i = (StartingAi - Aimin_); i < rmeLookUpTables_.size(); ++i) // iterate over the number of fermions in initial (a.k.a. ket) state
	{
		assert(rmeLookUpTables_[i].empty());
		 rmeLookUpTables_[i].reserve(1024);

		Ai = Aimin_ + i;
		Af = Ai + dA;

		assert(Af <= BaseSU3Irreps.GetAmax(n));

///////////////////////////////////////////////////////////////////////////////////////
//	The following piece of code works only when multf^{max}=1, which is always
//	true for T^{omega0 S0} = a+, or a+a+ which is the case of 2B interaction as
//	SU(3)xSU(2) irreps for two-fermions on HO shell n are always outer
//	multiplicity free.
//
//	HOWEVER: if one were to include more general tensors, such as
//	{{a+(4 0) x a+(4 0)}^{tensorLabels_[0]} x a+(4 0)}^tensorLabels_[1] 
//	has two possible tensors
//
//	{{a+(4 0) x a+(4 0)}(0 4)S=0 x a+(4 0)}^(4 4)_{1/2}
//	and
//	{{a+(4 0) x a+(4 0)}(4 2)S=0 x a+(4 0)}^(4 4)_{1/2}
//
//	one should calculate those rme numerically, since it is not clear to me just by 
//	inspection identify, which multf in bra state will be result of action
//	|A = 3; multf = ? (4 4)1/2> = {{a+^(4 0) x a+^(4 0)}^(0 4)S=0 x a^(4 0)}^(4 4)_{1/2} | vacuum>
//	|A = 3; multf = ? (4 4)1/2> = {{a+^(4 0) x a+^(4 0)}^(4 2)S=0 x a^(4 0)}^(4 4)_{1/2} | vacuum>
//
//	in fact, it could be also a superposition of what my code generates as HWS.
//	At least by inspection generation of HWS is not based on inner coupling
//	single-fermion SU(3)xSU(2) operators.
//	TODO: ==> this is ok, for the time being (i.e. 2B interaction), but I have to implement 
//	possibility to evaluate rme to evaluate Tensor.CalculateRME(bra, bra_state, VACUUM, vacuum_state, drme);
///////////////////////////////////////////////////////////////////////////////////////

		if (Ai == 0) // ==> we have < omega0 S0 ||| T^{omega0 S0} ||| vacuum> = 1 (see CodeDocs relation (3.6))
		{
			assert(IsPureType());
			// Notes:
			// (1) only tensor made of creation operators can have |ket> == |vacuum> ==> GetNumberOfAnnihilators() must return 0 !
			// (2) if GetNumberOfCreators() > 2 ==> one needs to find an analytical expression for <w0 S0||| {a+ x a+ x ... a+}^{w0 S0} ||| vacuum>
			assert(GetNumberOfCreators() <= 2 && GetNumberOfAnnihilators() == 0); 
								
			size_t nCreators = GetNumberOfCreators();
			SU3xSU2::RME::DOUBLE rme_value;
			if (nCreators == 1)
			{
				rme_value = 1;
			} 
			else if (nCreators == 2)
			{
				rme_value = w0_adad_vacuum();
			}
			rmeLookUpTables_[i].push_back(std::make_pair(RMEKey(UN::SU3xSU2(1, this->lm(), this->mu(), this->S2()), UN::SU3xSU2(+1, 0, 0, 0)), rmes_.size()));
			rmes_.insert(rmes_.end(), rme_value);

			continue;
		}

		if (Af == 0) // ==> we have either <vacuum ||| ta ||| (n 0)1/2> or <vacuum ||| tata^{w0 S0} ||| tw0 S0>
		{
			assert(IsPureType());
			// Notes:
			// (1) only annihilators can have <bra| == <vacuum| ==> GetNumberOfCreators() must return 0 !
			// (2) if GetNumberOfAnnihilators() > 2 ==> one needs to find an analytical expression for <vacuum||| {ta x ta x ... ta}^{w0 S0} ||| tw0 S0>
			assert(GetNumberOfAnnihilators() <=2 && GetNumberOfCreators() == 0); 

			size_t nAnnihilators = GetNumberOfAnnihilators();
			SU3xSU2::RME::DOUBLE rme_value;
			if (nAnnihilators == 1)
			{
				rme_value = vacuum_ta_n0();
			}
			else if (nAnnihilators == 2)
			{
				rme_value = vacuum_tata_w0();
			}
			rmeLookUpTables_[i].push_back(std::make_pair(RMEKey(UN::SU3xSU2(+1, 0, 0, 0), UN::SU3xSU2(1, this->mu(), this->lm(), this->S2())), rmes_.size()));
			rmes_.insert(rmes_.end(), rme_value);

			continue;
		}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
		std::map<RMEKey, std::vector<SU3xSU2::RME::DOUBLE> > temp_storage;

		std::vector<SU2::LABEL> SpinsKet;
		std::vector<SU2::LABEL> SpinsBra;
		BaseSU3Irreps.GetSpins(n, Ai, SpinsKet);
		if (Ai != Af) 
		{
			BaseSU3Irreps.GetSpins(n, Af, SpinsBra);
		}
		else	// Ai == Af ==> SpinsBra = SpinsKet
		{
			SpinsBra = SpinsKet;
		}

		for (size_t iSi = 0; iSi < SpinsKet.size(); ++iSi)	// iterate over all possible spins in initial/ket states describing Ai fermions in n-the HO shell
		{
			Ket.S2 = SpinsKet[iSi];
			std::string sFileNameKetHWS(CHwsGen<UN_BASIS_STATE>::GetHWSFileName(n, Ai, Ket.S2, OutputType));	//	file containing all HWS for HO shell n with A = Ai fermions and spin Ket.S2
			std::ifstream hws_file_ket(sFileNameKetHWS.c_str());
			if (!hws_file_ket) // unable to read HWS file ==> I am going to calculate and produce the HWS file on-the-fly
			{
				std::cout << "Not able to open file " << sFileNameKetHWS << "!" << std::endl;
				std::cout << "Trying to generate HWS states for n = " << n << " A = " << Ai << " S2 = " << (int)Ket.S2 << " ..." << std::endl;

				if (nsps > nspsmax) {
					std::cerr << "HWS generator for harmonic ocillator shell n=" << n << " has not been implemented yet." << std::endl;
					exit(EXIT_FAILURE);
				}
				CHwsGen<UN_BASIS_STATE> HWSGEN(nmax, NullSpaceMethodType, OutputType); 
				HWSGEN.GenerateHWS(n, Ai, Ket.S2);	// generate and save all HWS on HO shell n, with Ai fermions and spin Ket.S2	
				hws_file_ket.open(sFileNameKetHWS.c_str());	// try to open file with HWS
				if (!hws_file_ket)
				{
					std::cerr << "Ooops, it just happened again: not able to open file " << sFileNameKetHWS << "!" << std::endl;
					exit(EXIT_FAILURE);
				}
			}
			//	at this point we have opened file storing HWS for n HO shell, Ai fermions and Ket.S2 spin
			while (true) // read initial (a.k.a. Ket) HWS states until you hit the end of file
			{
				hws_file_ket >> mult;	//	read header: mult (lm mu), we alos know that Si = Ket.S2
				if (hws_file_ket.eof()) { // we hit the end of file 
					break;
				}
				hws_file_ket >> lm >> mu;
				Ket.mult = mult;
				Ket.lm = lm;
				Ket.mu = mu;

				assert(OutputType == HWSOutputText);	//	CState does not implement reading from a binary HWS file
				CState<UN_BASIS_STATE> State_ket(hws_file_ket, Ket.mult, n, Ai); // CState loads HWS ket states for all multiplicities alpha from the file "hws_file_ket".
				
				for (size_t iSf = 0; iSf < SpinsBra.size(); ++iSf) // iterate over all possible spins in final/bra states describing Af fermions in n-the HO shell
				{
					Bra.S2 = SpinsBra[iSf];
					std::string sFileNameBraHWS(CHwsGen<UN::BASIS_STATE_32BITS>::GetHWSFileName(n, Af, Bra.S2, OutputType));
					std::ifstream hws_file_bra(sFileNameBraHWS.c_str());
					if (!hws_file_bra) 
					{
						std::cout << "Not able to open file " << sFileNameBraHWS << "!" << std::endl;
						std::cout << "Trying to generate HWS states for n = " << n << " A = " << Af << " S2 = " << (int)Bra.S2 << std::endl;
			
						if (nsps > nspsmax) {
							std::cerr << "HWS generator for harmonic ocillator shell n=" << n << " has not been implemented yet." << std::endl;
							exit(EXIT_FAILURE);
						}
						CHwsGen<UN_BASIS_STATE> HWSGEN(nmax, NullSpaceMethodType, OutputType);	//	calculate HWS on-the-fly
						HWSGEN.GenerateHWS(n, Af, Bra.S2);										//	save it into CHwsGen::GetHWSFileName(n, Af, Bra.S2, HWSOutputText)
						hws_file_bra.open(sFileNameBraHWS.c_str());
						if (!hws_file_bra)
						{
							std::cerr << "Ooops, it just happened again: not able to open file " << sFileNameBraHWS << "!" << std::endl;
							exit(EXIT_FAILURE);
						}
					}
					while (true) // read final (a.k.a. Bra) HWS states until you hit the end of file
					{
						hws_file_bra >> mult;
						if (hws_file_bra.eof()) {
							break;
						}
						hws_file_bra >> lm >> mu;
						Bra.mult = mult;
						Bra.lm = lm;
						Bra.mu = mu;

						assert(OutputType == HWSOutputText);	//	CState does not implement reading from a binary HWS file
						CState<UN_BASIS_STATE> State_bra(hws_file_bra, Bra.mult, n, Af);// CState loads HWS ket states for all multiplicities alpha from the file "hws_file_ket".

						// Now we have all that we need to calculate reduced matrix elements: Ket HWS, Bra HWS and Tensor operator
						// First we need to check whether Si coupled with S0 can yield Sf
						if (!SU2::mult(Ket.S2, this->S2(), Bra.S2)) // Does Si x S0 ---> Sf ???
						{
							continue;
						}
						// Second test whether (lmi mui) coupled with (lm0 mu0) can yield (lmf muf)
						if (!SU3::mult(Ket, SU3::LABELS(this->lm(), this->mu()), Bra)) // Does (lmi mui) x (lm0 mu0) ----> (lmf muf) ???
						{
							continue;
						}
						std::vector<double> rme; 
						Tensor.CalculateRME(Bra, State_bra, Ket, State_ket, rme);
						if (std::count_if(rme.begin(), rme.end(), Negligible) < rme.size()) // if at least one rme is not vanishing
						{
							rmes_temp.resize(0);
							rmes_temp.insert(rmes_temp.end(), rme.begin(), rme.end());
							temp_storage.insert(std::make_pair(RMEKey(Bra, Ket), rmes_temp));
						}
					}
				}
			}
		}

		// copy content of temp_storage into rmeLookUpTables_[i] ==> elements of rmeLookUpTables_[i] will be sorted by RMEKey
		std::map<RMEKey, std::vector<SU3xSU2::RME::DOUBLE> >::const_iterator it = temp_storage.begin();
		for (; it != temp_storage.end(); ++it)
		{
			rmeLookUpTables_[i].push_back(std::make_pair(it->first, rmes_.size()));
			rmes_.insert(rmes_.end(), it->second.begin(), it->second.end());
		}

		if (rmeLookUpTables_[i].empty())	// ==> we need to ensure that method GetNumberOfRmes() never reads data from an empty rmeLookUpTables_[i]
		{
			rmeLookUpTables_[i].push_back(std::make_pair(RMEKey(UN::SU3xSU2(+1, 0xFF, 0xFF, 0xFF), UN::SU3xSU2(1, 0xFF, 0xFF, 0xFF)), rmes_.size()));
		}
//	Trim excess memory in rmeLookUpTables_[i]
		RME_TABLE_ENTRY(rmeLookUpTables_[i]).swap(rmeLookUpTables_[i]);
	}
//	Trim excess memory in rmes_
	std::vector<SU3xSU2::RME::DOUBLE>(rmes_).swap(rmes_);
}

struct RMEKEY_EQUAL
{
	bool operator()( const CrmeTable::KEY_FIRST_RME& lhs, const RMEKey& rhs ) const { return lhs.first < rhs; };
 	bool operator()( const RMEKey& lhs, const CrmeTable::KEY_FIRST_RME& rhs ) const { return lhs < rhs.first; };
};
#endif

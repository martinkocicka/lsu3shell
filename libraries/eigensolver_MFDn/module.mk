$(eval $(begin-module))

################################################################
# unit definitions
################################################################

# module_units_h := 
# module_units_cpp-h := 
module_units_f := subrts_general subrts_MPI_IO subrts_diagonalize_Lanczos subrts_matvec subrts_diagonalize_blocksparse_Lanczos subrts_matvec_blocksparse
# module_programs_cpp :=
#module_programs_f := 
#module_generated :=

################################################################
# library creation flag
################################################################
$(eval $(library))

$(eval $(end-module))

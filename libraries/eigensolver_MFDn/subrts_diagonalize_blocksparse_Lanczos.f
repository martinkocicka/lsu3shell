C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     iterative procedure w. fixed number of Lanczos iterations
C     for finding lowest eigenvalues of real sparse symmetric matrix
C     stored in compressed column format
C     2D distribution of N processors
C
C     returns lowest eigenvalues
C     writes corresponding eigenvectors to disk
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      subroutine matrix_diagonalize_vbc(ncols, nrows, nnz, xme,
     $     nblocks, colind, rowind, colnnz, rownnz,
     $     startitr, maxitr, tol,
     $     neigen, eigenvals, vectors, nstates_sofar)
C     
      use nodeinfo
      implicit none
C
      include "mpif.h"
      integer :: status(MPI_STATUS_SIZE), ierr
      integer(kind=MPI_OFFSET_KIND) :: disp, offset
C
C     input
      logical, intent(in) :: vectors
      integer*8, intent(in) :: nstates_sofar
      integer, intent(in) :: ncols, nrows, nnz, nblocks
      integer, intent(in) :: maxitr, startitr, neigen
      integer, dimension(nblocks), intent(in) :: rowind, colind
      integer*4, dimension(nblocks), intent(in) :: rownnz, colnnz
      real*4, dimension(nnz), intent(in) :: xme
      real*4, intent(in) :: tol
C
C     output (also: eigenvectors written to disk)
      real*4, dimension(neigen), intent(out) :: eigenvals
C
C     local variables
      real*8, dimension(:), allocatable :: alpha, beta
      real*8, dimension(:,:), allocatable :: trivec
      real*8, dimension(:), allocatable :: work, eivals
      real*4, dimension(:,:), allocatable :: savlvec
      real*4, dimension(:), allocatable :: vamp, vampnew, tamp, tampnew
      real*4, dimension(:), allocatable :: vector, errors, toterr
      real*4, dimension(:), allocatable :: maxerr,totmax,maxrel,relmax
      real*4, dimension(:), allocatable :: eigen, subdiag
      real*4, dimension(:,:), allocatable :: eifuns
      real*4, dimension(:), allocatable :: works
      integer, dimension(:), allocatable :: iwork, ifail
      real*8 :: alph, alpht, betapre, betsq, ovlp, ovlpt, tmp
      real*8 :: tstart, tbefore, tafter, tprevious, tavg
      real*8 :: dummy_lo, dummy_up
      real*4 :: sdot, temp
C
      integer :: nanalyse, nlocmax, lvecdim, lvecnum, istore, icylvec
      integer :: i, j, nit, nitprevious, ifile, info, neivals
      integer :: ii, jj, kk, iiglobal
C
      character(16) :: lvecfile
      character(17) :: filename
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      if (mypid.eq.0) then
         print*
         print*, ' Starting Lanczos iterations  '
         print*, ' Requested number of eigenvalues', neigen
         print*, ' Max. number of iterations      ', maxitr
         if (startitr.le.0) then
            print*, ' Starting with first basis state as pivot'
         else
            print*, ' Starting from iteration no     ',startitr
         endif
         print*
      endif
C
      nanalyse = 10
C
C     allocate arrays for vectors
      nlocmax = max(ncols, nrows)
      allocate(vamp(nlocmax))
      allocate(tamp(nlocmax))
      allocate(vampnew(nlocmax))
      allocate(tampnew(nlocmax))
C
      vamp = 0.0
      tamp = 0.0
      vampnew = 0.0
      tampnew = 0.0
C
C     allocate arrays for storage of Lanczos vectors
      if (ilvec.eq.1) then
         lvecdim = ncols
      elseif (ilvec.eq.3) then
         lvecdim = nrows
      else
         print*, mypid, ' error in storage of LWF'
         call cancelall(601)
      endif 
C
      lvecnum = maxitr/lvecgroups + 1
C
      allocate(savlvec(lvecdim, lvecnum))
      savlvec = 0.0
C
C     allocate Lanczos arrays
      allocate(alpha(maxitr),beta(maxitr))
      if ((mypid.eq.0).or.(mypid.eq.(ndiag-1))) then         
         allocate(eigen(maxitr), subdiag(maxitr))
      endif
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      istore = 0
      icylvec = 0
C
      if (startitr.eq.0) then
C
C     set initial pivot vector
         if (mypid.eq.0) then
            vamp(1) = 1.0
            tamp(1) = 1.0
         endif
C
C     store initial pivot vector on diagonals
C     open file for lanczos vectors and write the initial pivot vector
         if (mypid.lt.ndiag) then
            icylvec = icylvec + 1
            savlvec(1:lvecdim,icylvec) = vamp(1:lvecdim)
C     
            ifile = mypid + 1
            lvecfile = 'mfdn_lvectors'//achar(ifile/100+48)
     $           //achar(mod(ifile/10,10)+48)
     $           //achar(mod(ifile,10)+48)
            open(unit=101, file=lvecfile, status='unknown',
     $           action='write', form='unformatted')
            write(101) vamp(1:lvecdim)
            call flush(101)
         endif
C
C     open file for alpha and beta arrays on root
         if (mypid.eq.(ndiag-1)) then
            open(unit=29, file='mfdn_alphabeta.dat',
     $           status='unknown', action='write')
         endif
C     
      else
C
C     read previous Lanczos vectors and alpha, beta
         if (mypid.eq.(ndiag-1)) then
            open(unit=29, file='mfdn_alphabeta.dat',
     $           status='old', action='readwrite')
            do i = 1, startitr
               read(29, *) nit, alpha(i), beta(i)
            enddo
         endif
C
         if (mypid.lt.ndiag) then
C     Bcast alpha, beta to diagonals
            call MPI_Bcast(alpha,startitr, MPI_REAL8,
     $           ndiag-1, diag_comm, ierr)
            call MPI_Bcast(beta, startitr, MPI_REAL8,
     $           ndiag-1, diag_comm, ierr)
C     
C     open Lanczos vectors file, read and store initial vector     
            ifile = mypid + 1
            lvecfile = 'mfdn_lvectors'//achar(ifile/100+48)
     $           //achar(mod(ifile/10,10)+48)
     $           //achar(mod(ifile,10)+48)
            open(unit=101, file=lvecfile, status='old',
     $           action='readwrite', form='unformatted')
C     
            read(101) vamp(1:lvecdim)
            icylvec = icylvec + 1
            savlvec(1:lvecdim,icylvec) = vamp(1:lvecdim)
         endif
C
C     read and store subsequent vectors
         do i = 1, startitr-1
            istore = mod(i,lvecgroups)
            if (mypid.lt.ndiag) then
               read(101) vamp(1:lvecdim)
               if (istore.eq.0) then
                  icylvec = icylvec + 1
                  savlvec(1:lvecdim,icylvec) = vamp(1:lvecdim)
               else
                  call MPI_Send(vamp(1:lvecdim), lvecdim,
     $                 MPI_REAL4, istore, i, lvec_sto_comm, ierr)
               endif
            elseif (istore.eq.my_sto_id) then
               icylvec = icylvec + 1
               call MPI_Recv(savlvec(1:lvecdim,icylvec), lvecdim,
     $              MPI_REAL4, 0, i, lvec_sto_comm, status, ierr)
            endif
         enddo
C
C     prepare for new matvec
         i = startitr
         istore = mod(i,lvecgroups)
         if (mypid.lt.ndiag) then
C     create subtraction component on diagonal sectors
            vampnew(1:ncols) = -beta(startitr) * vamp(1:ncols)
            read(101) vamp(1:lvecdim)
            tamp(1:lvecdim) = vamp(1:lvecdim)
            if (istore.eq.0) then
               icylvec = icylvec + 1
               savlvec(1:lvecdim,icylvec) = vamp(1:lvecdim)
            else
               call MPI_Send(vamp(1:lvecdim), lvecdim,
     $              MPI_REAL4, istore, i, lvec_sto_comm, ierr)
            endif
         elseif (istore.eq.my_sto_id) then
            icylvec = icylvec + 1
            call MPI_Recv(savlvec(1:lvecdim,icylvec), lvecdim,
     $           MPI_REAL4, 0, i, lvec_sto_comm, status, ierr)
         endif
      endif
C     
      call MPI_Bcast(vamp, ncols, MPI_REAL4, 0, col_comm, ierr)
      call MPI_Bcast(tamp, nrows, MPI_REAL4, 0, row_comm, ierr)
C     
      if (mypid.eq.(ndiag-1)) then
         open(file='mfdn_eigenvalues.dat', unit=28, 
     $        status='unknown', action='write')
      endif
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      tstart = MPI_WTIME()
C
      do nit = startitr+1, maxitr
C
C     actual matvec (double precision vectors, single precision m.e.)
C     distributed over all ndiag * (ndiag+1) / 2 processors
C
         if (mypid.lt.ndiag) then
            call matvecblockdiag(nblocks,
     $           rowind, colind, rownnz, colnnz, nnz, xme,
     $           ncols, vamp, vampnew)
         else
            call matvecblockoffdiag(nblocks,
     $           rowind, colind, rownnz, colnnz, nnz, xme,
     $           nrows, ncols, vamp, tamp, tampnew, vampnew)
         endif
C
C     first accumulate vampnew along column communicators
C     into 'auxiliary' array tampnew (not used yet on diags!!!)
C     (dangerous: tampnew on the off-diagonals is in use 
C     and should not change...)
         call MPI_Reduce(vampnew, tampnew, ncols, MPI_REAL4,
     &        MPI_SUM, 0, col_comm, ierr)
C     now we can zero input vectors vampnew(:)
         vampnew = 0.0
C
C     next, accumulate tampnew along row communicators 
C     into vampnew (on diags)
         call MPI_Reduce(tampnew, vampnew, nrows, MPI_REAL4,
     &        MPI_SUM, 0, row_comm, ierr)
C     
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     Re-orthogonalize
C
C     First, orthogonalize and normalize vampnew (on diagonals only),
C     and collect alpha(n) and beta(n) for tri-diagonal matrix
C
         if (mypid.lt.ndiag) then
C     calculate alpha
            alpht = 0.0d0
!$OMP PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(i)
!$OMP& REDUCTION(+:alpht)
            do i = 1, ncols
               alpht = alpht + vamp(i) * vampnew(i)
            enddo
!$OMP END PARALLEL DO     
            call MPI_Allreduce(alpht, alph, 1,
     $           MPI_REAL8, MPI_SUM, diag_comm, ierr)
C
C     calculate beta
            alpht = 0.0d0
!$OMP PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(i)
!$OMP& REDUCTION(+:alpht)
            do i = 1, ncols
               vampnew(i) = vampnew(i) - alph * vamp(i)
               alpht = alpht + vampnew(i) * vampnew(i)
            enddo
!$OMP END PARALLEL DO
            call MPI_Allreduce(alpht, betsq, 1,
     $           MPI_REAL8, MPI_SUM, diag_comm, ierr)
C
            betapre = dsqrt(betsq)
            tmp = 1.d0 / betapre
!$OMP PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(i)
            do i = 1, ncols
               vampnew(i) =  vampnew(i) * tmp
            enddo
!$OMP END PARALLEL DO
C     
            alpha(nit) = alph
            beta(nit) = betapre
C
            if (mypid.eq.(ndiag-1)) then
               write(29, *) nit, alpha(nit), beta(nit)
               flush(29)
            endif
C     
         endif
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     Next: Orthogonalization of vampnew wrt previous vectors:
C       vampnew(1:lvecdim) -- new vector
C       savlvec(1:lvecdim,1:icylvec) -- previous vectors
C       tamp(1:lvecdim) -- local sum of (overlap_i * vector_i) 
C       vampnew(1:ncols) -- accumulated overlaps on diagonals
C
C     distribute vampnew to all processors
C
         call MPI_Bcast(vampnew, lvecdim,
     $        MPI_REAL4, 0, lvec_sto_comm, ierr)
C
C     orthogonalize
         ovlp = 0.0d0
         tamp = 0.0
C
         do j = 1, icylvec
C     calculate overlap (dot-product)
            ovlpt = 0.0d0
!$OMP PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(i)
!$OMP& REDUCTION(+:ovlpt)
            do i = 1, lvecdim
               ovlpt = ovlpt + savlvec(i, j) * vampnew(i)
            enddo
!$OMP END PARALLEL DO
            call MPI_Allreduce(ovlpt, ovlp, 1,
     $           MPI_REAL8, MPI_SUM, lvec_ovl_comm, ierr)
C     
C     building up the vector tamp, to be subtracted from vampnew
!$OMP PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(i)
            do i = 1, lvecdim
               tamp(i) = tamp(i) + ovlp * savlvec(i,j)
            enddo
!$OMP END PARALLEL DO
         enddo
C
C     collect all the overlap contributions that need to be subtracted
C     from vampnew (that is, tamp) into tampnew on the diagonals
         call MPI_Reduce(tamp, tampnew, lvecdim,
     $        MPI_REAL4, MPI_SUM, 0, lvec_sto_comm, ierr)
C
C     subtract all the overlaps (tampnew) on the diagonals only
C     obtain norm of newest vector, vampnew, and normalize it
         if (mypid.lt.ndiag) then
            alpht = 0.0d0
!$OMP PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(i)
!$OMP& REDUCTION(+:alpht)
            do i = 1, ncols
               vampnew(i) = vampnew(i) - tampnew(i)
               alpht = alpht + vampnew(i) * vampnew(i)
            enddo
!$OMP END PARALLEL DO
            call MPI_Allreduce(alpht, alph, 1,
     $           MPI_REAL8, MPI_SUM, diag_comm, ierr)
C
            alph = 1.d0 / dsqrt(alph)
!$OMP PARALLEL DO
!$OMP& DEFAULT(SHARED) PRIVATE(i)
            do i = 1, ncols
C     collect final, normalized, vector into tamp 
               tamp(i) = alph * vampnew(i)
C     create subtraction component on diagonal sectors
               vampnew(i) = -betapre * vamp(i)
C     copy tamp into vamp as preparation for next iteraction
               vamp(i) = tamp(i)
            enddo
!$OMP END PARALLEL DO
         else
C     reset output vectors to zero on off-diagonas
            vampnew = 0.0
            tampnew = 0.0
         endif
C
C     as preparation for next iteraction 
C     distribute vamp along col_comm, and tamp along row_comm
         call MPI_Bcast(vamp, ncols, MPI_REAL4, 0, col_comm, ierr)
         call MPI_Bcast(tamp, nrows, MPI_REAL4, 0, row_comm, ierr)
C     
C     write vamp from diagonals
         if (mypid.lt.ndiag) then
            write(101) vamp(1:lvecdim)
            call flush(101)
         endif
C
C     finally copy orthonormalized lvec from vamp or tamp to savlvec() 
C     in sections of length lvecdim on appropriate processors
         istore = mod(nit,lvecgroups)
         if (istore.eq.my_sto_id) then
            icylvec = icylvec + 1
            if (ilvec.eq.1) then
               savlvec(1:lvecdim,icylvec) = vamp(1:lvecdim)
            else
               savlvec(1:lvecdim,icylvec) = tamp(1:lvecdim)
            endif 
         endif
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     Diagonalize tri-diagonal matrix on root
C     for the first nanalyse iterations,
C     and then only mod(nit,nanalyse) = 0  
C     and for the last 3 iterations
C     
         if ((mypid.eq.0).or.(mypid.eq.(ndiag-1))) then
            if ((nit.le.10).or.(nit.ge.maxitr-2).or.
     $           (mod(nit,nanalyse).eq.0)) then
C     
               if (nit.ge.10*nanalyse) nanalyse = 10*nanalyse
C     
               eigen(1:nit) = alpha(1:nit)
               subdiag(1:nit) = beta(1:nit)
C     
               call sstev('N', nit, eigen, subdiag,
     $              eifuns, nit, works, info)
C     
               if (mypid.eq.0) then
                  tafter = MPI_WTIME()
                  if (nit.eq.1) then 
                     tavg = tafter - tstart
                     print*,
     $                    ' Time first Lanczos iteration', tavg
                     print*,
     $                    ' Eigenvalue after first Lanczos it',
     $                    eigen(1)
                     print*,
     $                    ' It #, Average time, Ei for i = 1 through 4'
                     tprevious = tafter
                     nitprevious = nit
                  else
                     tavg = (tafter - tprevious)/(nit-nitprevious)
                     write(6,599) nit, tavg, eigen(1:min(nit,4))
                     tprevious = tafter
                     nitprevious = nit
                  endif
               endif
 599           format(i5, '  Avg', e11.3, '   Ei', 4f12.4)
C
               if (mypid.eq.(ndiag-1)) then
                  write(28,*) 'spectrum after',nit,' iterations'
                  do i = 1, min(nit, 50)
                     write(28,*) i,  eigen(i)
                  enddo
               endif
            endif
         endif
C     
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      enddo
C
      close(unit=101, status='keep')
C         
      tafter = MPI_WTIME()
      if (mypid.eq.0) then
         print*
         print*, 'Total Time Lanczos iterations',
     $      tafter - tstart,'for',maxitr,'its'
         print*
      elseif (mypid.eq.(ndiag-1)) then
         close(unit=28, status='keep')
         close(unit=29, status='keep')
      endif
C
      if ((mypid.eq.0).or.(mypid.eq.(ndiag-1))) then         
         deallocate(eigen, subdiag)
      endif
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      call MPI_Bcast(alpha, maxitr, MPI_REAL8, 0, icomm, ierr)
      call MPI_Bcast(beta, maxitr, MPI_REAL8, 0, icomm, ierr)
C
      if (vectors) then
C     calculate eigenvalues and eigenvectors on all procs
         allocate(trivec(maxitr, neigen))
         allocate(eivals(maxitr))
         allocate(work(5*maxitr))
         allocate(iwork(5*maxitr))
         allocate(ifail(maxitr))
C     
C         call dstev('V',maxitr,alpha,beta,trivec,maxitr,workd,info)
         call dstevx('V','I',maxitr,alpha,beta, dummy_lo, dummy_up,
     $        1, neigen, 1d-8, neivals, eivals, trivec, maxitr,
     $        work, iwork, ifail, info)

         eigenvals(1:neigen) = eivals(1:neigen)
C
         deallocate(eivals, work)
         deallocate(iwork, ifail)
C
C     construct eigenvectors from Lanczos vectors
         disp = 0
         offset = nstates_sofar
         if (mypid.lt.ndiag) then
            allocate(vector(ncols))
            allocate(errors(neigen))
            allocate(toterr(neigen))
            allocate(maxrel(neigen))
            allocate(relmax(neigen))
            allocate(maxerr(neigen))
            allocate(totmax(neigen))
         endif
         do kk = 1, neigen
            vamp = 0.0
            tamp = 0.0
            do ii = 1, icylvec
               iiglobal = 1 + my_sto_id + (ii-1)*lvecgroups 
               if (iiglobal.gt.maxitr) exit
C     
               do jj = 1, lvecdim
                  vamp(jj) = vamp(jj) +
     $                 savlvec(jj,ii) * trivec(iiglobal,kk)
               enddo
            enddo
C     
C     now sum the contributions to the diagonal processes
            call MPI_Reduce(vamp, tamp, lvecdim,
     $           MPI_REAL4, MPI_SUM, 0, lvec_sto_comm, ierr)
C
C     and write from diagonals
            if (mypid.lt.ndiag) then
               vamp(1:ncols)  = tamp(1:nrows)
               vector(1:ncols)  = tamp(1:nrows)
C
               filename = 'eigenvector'//achar(kk/10+48)
     $              //achar(mod(kk,10)+48)//'.dat'
               call vector2file(diag_comm,
     $              disp, offset, lvecdim, vector, filename)
            endif
C     error estimate A: compute residual norm(H*v - lambda*v)
C     error estimate B: compute max(eps_i) = 
C                       max[(H_{ij}*v_j - lambda*v_i)/(lambda*v_i)]
C                       for |v_i| > 1e-6
C     and  max(eps_i) = max[(H_{ij}*v_j - lambda*v_i)]/lambda
C                       for |v_i| < 1e-6
C     error estimate C: compute max(eps_i) = 
C                       max[(H_{ij}*v_j - lambda*v_i)]/lambda
C     for each desired eigenpair (not necessarily converged!)
C
            call MPI_Bcast(vamp,ncols,MPI_REAL4,0,col_comm,ierr)
            call MPI_Bcast(tamp,nrows,MPI_REAL4,0,row_comm,ierr)
            vampnew = 0.0
            tampnew = 0.0
            if (mypid.lt.ndiag) then
               call matvecblockdiag(nblocks,
     $              rowind, colind, rownnz, colnnz, nnz, xme,
     $              ncols, vamp, vampnew)
            else
               call matvecblockoffdiag(nblocks,
     $              rowind, colind, rownnz, colnnz, nnz, xme,
     $              nrows, ncols, vamp, tamp, tampnew, vampnew)
            endif
            vamp = 0.0
            tamp = 0.0
            call MPI_Reduce(vampnew, tampnew, ncols, MPI_REAL4,
     &           MPI_SUM, 0, col_comm, ierr)
            call MPI_Reduce(tampnew, vamp, nrows, MPI_REAL4,
     &           MPI_SUM, 0, row_comm,ierr)
C     
            if (mypid.lt.ndiag) then
               call saxpy(ncols, -eigenvals(kk), vector, 1, vamp, 1)
               errors(kk) = sdot(ncols, vamp, 1, vamp, 1)
               maxrel(kk) = 0.0
               maxerr(kk) = 0.0
               do i = 1, ncols
                  if (abs(vector(i)).gt.1e-6) then
                     temp = abs(vamp(i)/vector(i))
                  endif
                  maxrel(kk) = max(temp, maxrel(kk))
                  maxerr(kk) = max(abs(vamp(i)), maxerr(kk))
               enddo
            endif
C
         enddo
         deallocate(trivec)
C
         if (mypid.lt.ndiag) then
            call MPI_Reduce(errors, toterr, neigen, MPI_REAL4,
     $           MPI_SUM, 0, diag_comm, ierr)
            call MPI_Reduce(maxrel, relmax, neigen, MPI_REAL4,
     $           MPI_MAX, 0, diag_comm, ierr)
            call MPI_Reduce(maxerr, totmax, neigen, MPI_REAL4,
     $           MPI_MAX, 0, diag_comm, ierr)
            if (mypid.eq.0) then
               print*
               print*, ' Number of MATVECs performed', maxitr+neigen
               print*
               print*, 'requested eigenvalues lambda '
               print*, 'and errors A norm(H*v - lambda*v)/lambda'
               print*,
     $'and B max(abs((H*v - lambda*v)_i/(lambda*v_i))) for |v_i| > 1e-6'
               print*, 'and C max(abs((H*v - lambda*v)_i/lambda))'
               do kk = 1, neigen
                  toterr(kk) = sqrt(toterr(kk))
                  if (abs(eigenvals(kk)) .gt. 1e-6) then
                     toterr(kk) = toterr(kk)/abs(eigenvals(kk))
                     relmax(kk) = relmax(kk)/abs(eigenvals(kk))
                     totmax(kk) = totmax(kk)/abs(eigenvals(kk))
                  endif
                  print*, kk, eigenvals(kk), toterr(kk),
     $                 relmax(kk), totmax(kk)
               enddo
            endif
            deallocate(vector)
            deallocate(errors)
            deallocate(toterr)
            deallocate(maxrel)
            deallocate(relmax)
            deallocate(maxerr)
            deallocate(totmax)
         endif

      else
C     calculate eigenvalues on all procs (no error estimate)
         call dstev('N', maxitr, alpha, beta,
     $        trivec, maxitr, work, info)
         eigenvals(1:neigen) = alpha(1:neigen)
C
         if (mypid.eq.0) then
            print*
            print*, ' Number of MATVECs performed', maxitr
            print*
            print*, 'requested eigenvalues'
            do kk = 1, neigen
               print*, kk, eigenvals(kk)
            enddo
         endif
C
      endif
C     
      deallocate(vampnew, tampnew)
      deallocate(vamp, tamp)
      deallocate(alpha, beta)
      deallocate(savlvec)
C
      return
C
      end subroutine matrix_diagonalize_vbc
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

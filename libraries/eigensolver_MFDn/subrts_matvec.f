C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      subroutine matvecsparsediag(nloc, colptrs, nxrem, irem, xrem,
     $           x, y)
      implicit none

      integer, intent(in) :: nloc, nxrem
      integer*4, dimension(nloc+1), intent(in) :: colptrs
      integer*4, dimension(nxrem), intent(in) :: irem
      real*4, dimension(nxrem), intent(in) :: xrem
      real*4, dimension(nloc), intent(in) :: x
      real*4, dimension(nloc), intent(inout) :: y
C
C     local variables
      real*4, dimension(nloc) :: private_y
      integer :: i, j, k, k1, k2
      real*4 :: xcoef
C
!$OMP PARALLEL DEFAULT(SHARED)
!$OMP&         PRIVATE(private_y, i, j, k, k1, k2, xcoef)
      private_y = 0.d0
!$OMP DO SCHEDULE(RUNTIME)
      do j = 1, nloc
         k1 = colptrs(j)
         k2 = colptrs(j+1) - 1
         do k = k1, k2
            i = irem(k)
            xcoef = xrem(k)
C
C     compute y(i) = y(i) + a(i,j) * x(j)
C
            private_y(i) = private_y(i) + xcoef * x(j)
C     for transpose part of symmetric matrix
	    if (i.ne.j) then
               y(j) = y(j) + xcoef * x(i)		
            endif 
C
         enddo
      enddo
!$OMP END DO
!$OMP CRITICAL
      y = y + private_y
!$OMP END CRITICAL
!$OMP END PARALLEL

      return
C
      end subroutine matvecsparsediag
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      subroutine matvecsparse(nloc, nloct,
     $   colptrs, nxrem, irem, xrem,x, y, xt, yt)
      implicit none

      integer, intent(in) :: nloc, nloct, nxrem
      integer*4, dimension(nloc+1), intent(in) :: colptrs
      integer*4, dimension(nxrem), intent(in) :: irem
      real*4, dimension(nxrem), intent(in) :: xrem
      real*4, dimension(nloc), intent(in) :: x
      real*4, dimension(nloct), intent(inout) :: y
      real*4, dimension(nloct), intent(in) :: xt
      real*4, dimension(nloc), intent(inout) :: yt
C
C     local variables
      real*4, dimension(nloct) :: private_y
      integer :: i, j, k, k1, k2
      real*4 :: xcoef
C
!$OMP PARALLEL DEFAULT(SHARED)
!$OMP&         PRIVATE(private_y, i, j, k, k1, k2, xcoef)
      private_y = 0.d0
!$OMP DO SCHEDULE(RUNTIME)
      do j = 1, nloc
         k1 = colptrs(j)
         k2 = colptrs(j+1) - 1
         do k = k1, k2
C     i should run from 1 to nloct
            i = irem(k)
            xcoef = xrem(k)
C
C     compute y(i) = y(i) + a(i,j) * x(j)
C
            private_y(i) = private_y(i) + xcoef * x(j)
C     for transpose part of symmetric matrix
            yt(j) = yt(j) + xcoef * xt(i)		
C
         enddo
      enddo
!$OMP END DO
!$OMP CRITICAL
      y = y + private_y
!$OMP END CRITICAL
!$OMP END PARALLEL

      return
C     
      end subroutine matvecsparse
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

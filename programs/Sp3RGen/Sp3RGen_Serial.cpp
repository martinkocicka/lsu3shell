#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

#include <Eigen/Core>
#include <Eigen/SVD>
#include <Eigen/QR>

#include <algorithm>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <stack>

#include <boost/mpi.hpp> // cause loading operators uses MPI calls no matter if the code is strictly serial ...
#include <boost/chrono.hpp> 

using namespace std;

// Calculate total harmonic oscillator energy of nuclei at Nhw = n0hw space.
double getN0(int nprotons, int nneutrons, int n0hw)
{
	// N0 ... harmonic oscillator energy of a valence space configurations
	double N0 = 0;
	std::vector<uint8_t> proton_valence_distr;
	std::vector<uint8_t> neutron_valence_distr;

	for (int n = 0; true; ++n)
	{
		int8_t nferms = (n+1)*(n+2);
		if (nprotons > nferms)
		{
			proton_valence_distr.push_back(nferms);
			nprotons -= nferms;
		}
		else
		{
			proton_valence_distr.push_back(nprotons);
			break;
		}
	}

	for (int n = 0; true; ++n)
	{
		int8_t nferms = (n+1)*(n+2);
		if (nneutrons > nferms)
		{
			neutron_valence_distr.push_back(nferms);
			nneutrons -= nferms;
		}
		else
		{
			neutron_valence_distr.push_back(nneutrons);
			break;
		}
	}

	for (int n = 0; n < proton_valence_distr.size(); ++n)
	{
		N0 += proton_valence_distr[n]*(n + 1.5);
	}
	for (int n = 0; n < neutron_valence_distr.size(); ++n)
	{
		N0 += neutron_valence_distr[n]*(n + 1.5);
	}
//	now we have to add additional energy [n0hw] energy and subtract center-of-mass HO energy
	return (N0 + n0hw) - 1.5;
}

inline double C2SU3(int lm, int mu)
{
	return (2.0/3.0)*(lm*lm + mu*mu + lm*mu + 3*lm + 3*mu);
}

inline double C2Sp3R(int lm, int mu, double N)
{
	return C2SU3(lm, mu)+(1.0/3.0)*N*N - 4.0*N;
}

uintmax_t CalculateME(	
					const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
				    vector<float>& nonzero_me,
    				vector<int>&   columns,
    				vector<int>&   rowPtrs)
{
    uintmax_t local_nnz = 0;

	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_J_distr_p;
	unsigned char num_vacuums_J_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

	InitializeIdenticalOperatorRME(identityOperatorRMEPP);
	InitializeIdenticalOperatorRME(identityOperatorRMENN);

	SingleDistribution distr_ip, distr_in, distr_jp, distr_jn;
	UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
	SU3xSU2_VEC vW_ip, vW_in, vW_jp, vW_jn;

	unsigned char deltaP, deltaN;

	const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

	uint32_t blockFirstRow(0);
	
	int32_t icurrentDistr_p, icurrentDistr_n; 
	int32_t icurrentGamma_p, icurrentGamma_n; 
	
	vector<vector<float> > vals_local(bra.MaxNumberOfStatesInBlock());
	vector<vector<size_t> > col_ind_local(bra.MaxNumberOfStatesInBlock());
	for (int i = 0; i < bra.MaxNumberOfStatesInBlock(); ++i)
	{
		vals_local[i].reserve(ket.MaxNumberOfStatesInBlock());
		col_ind_local[i].reserve(ket.MaxNumberOfStatesInBlock());
	}

// loop over block rows
	for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		if (bra.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
		uint32_t ip = bra.getProtonIrrepId(ipin_block);
		uint32_t in = bra.getNeutronIrrepId(ipin_block);

		SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

		uint16_t aip_max = bra.getMult_p(ip);
		uint16_t ain_max = bra.getMult_n(in);

		unsigned long blockFirstColumn(blockFirstRow);

		uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max()); 
		uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max()); 
		
		uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max()); 
		uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max()); 

		uint32_t last_jp(std::numeric_limits<uint32_t>::max());
		uint32_t last_jn(std::numeric_limits<uint32_t>::max());

//	loop over (jp, jn) pairs
// loop over block columns
		for (unsigned int jpjn_block = ipin_block; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			if (ket.NumberOfStatesInBlock(jpjn_block) == 0)
			{
				continue;
			}
			bool isDiagonalBlock = (ipin_block == jpjn_block); 

			uint32_t jp = ket.getProtonIrrepId(jpjn_block);
			uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

			SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
			SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			uint16_t ajp_max = ket.getMult_p(jp);
			uint16_t ajn_max = ket.getMult_n(jn);

			if (jp != last_jp)
			{
				icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
				icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

				if (ilastDistr_p != icurrentDistr_p)
				{
					distr_ip.resize(0); bra.getDistr_p(ip, distr_ip);
					gamma_ip.resize(0); bra.getGamma_p(ip, gamma_ip);
					vW_ip.resize(0); bra.getOmega_p(ip, vW_ip);
				
					distr_jp.resize(0); ket.getDistr_p(jp, distr_jp);
					hoShells_p.resize(0);
					deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p, num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
				}

				if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p)
				{
					if (deltaP <= 4)
					{
						if (!selected_tensorsPP.empty())
						{
							std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsPP.resize(0);

						if (!selected_tensors_p_pn.empty())
						{
							std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_p_pn.resize(0);

						gamma_jp.resize(0); ket.getGamma_p(jp, gamma_jp);
						TransformGammaKet_SelectByGammas(hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensorsPP, selected_tensors_p_pn);
					}
				}
	
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					vW_jp.resize(0); ket.getOmega_p(jp, vW_jp);
					TransformOmegaKet_CalculateRME(distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP, selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
				}
				ilastDistr_p = icurrentDistr_p;
				ilastGamma_p = icurrentGamma_p;
			}

			if (jn != last_jn)
			{
				icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
				icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

				if (ilastDistr_n != icurrentDistr_n)
				{
					distr_in.resize(0); bra.getDistr_n(in, distr_in);
					gamma_in.resize(0); bra.getGamma_n(in, gamma_in);
					vW_in.resize(0); bra.getOmega_n(in, vW_in); 

					distr_jn.resize(0); ket.getDistr_n(jn, distr_jn);
					hoShells_n.resize(0);
					deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n, num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
				}

				if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n)
				{
					if (deltaN <= 4)
					{
						if (!selected_tensorsNN.empty())
						{
							std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsNN.resize(0);

						if (!selected_tensors_n_pn.empty())
						{
							std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_n_pn.resize(0);

						gamma_jn.resize(0); ket.getGamma_n(jn, gamma_jn);
						TransformGammaKet_SelectByGammas(hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn, selected_tensorsNN, selected_tensors_n_pn);
					}
				}

				if (deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					vW_jn.resize(0); ket.getOmega_n(jn, vW_jn);
					TransformOmegaKet_CalculateRME(distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN, selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
				}	

				ilastDistr_n = icurrentDistr_n;
				ilastGamma_n = icurrentGamma_n;
			}


			bool vanishingBlock = true;
			//	loop over wpn that result from coupling ip x in	
			uint32_t ibegin = bra.blockBegin(ipin_block);
			uint32_t iend = bra.blockEnd(ipin_block);
			uint32_t currentRow = blockFirstRow;
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
				size_t afmax = aip_max*ain_max*omega_pn_I.rho;
				IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));
				
				uint32_t currentColumn = (isDiagonalBlock) ? currentRow : blockFirstColumn;
				uint32_t jbegin = (isDiagonalBlock) ? iwpn : ket.blockBegin(jpjn_block);
				uint32_t jend = ket.blockEnd(jpjn_block);
				for (int jwpn = jbegin; jwpn < jend; ++jwpn)
				{
					SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
					size_t aimax = ajp_max*ajn_max*omega_pn_J.rho;
					IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));
	
					if (deltaP + deltaN <= 4)
					{
						Reset_rmeCoeffs(rmeCoeffsPNPN);
						if (in == jn && !rmeCoeffsPP.empty())
						{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
							CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
						}

						if (ip == jp  && !rmeCoeffsNN.empty())
						{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
							CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
						}

						if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
						{
							CalculatePNInteractionMeData(interactionPN, omega_pn_I, omega_pn_J, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
						}

						if (!rmeCoeffsPNPN.empty())
						{
							vanishingBlock = false;
							if (isDiagonalBlock && iwpn == jwpn)
							{
								CalculateME_Diagonal_UpperTriang_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
							else
							{
								CalculateME_nonDiagonal_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
						}
					}
					currentColumn += aimax*ket.omega_pn_dim(jwpn);
				}
				currentRow += afmax*bra.omega_pn_dim(iwpn);
			}
			blockFirstColumn += ket.NumberOfStatesInBlock(jpjn_block);
			last_jp = jp;
			last_jn = jn;
		}
// CSR stuff
		for (size_t i = 0; i < vals_local.size(); ++i)
		{
			rowPtrs.push_back(nonzero_me.size());
			for (size_t j = 0; j < vals_local[i].size(); ++j)
			{
				nonzero_me.push_back(vals_local[i][j]);
				columns.push_back(col_ind_local[i][j]);
//				cout << blockFirstRow + i + 1 << " " << col_ind_local[i][j] + 1 << " " << vals_local[i][j] << endl;
			}
			vals_local[i].resize(0);
			col_ind_local[i].resize(0);
		}
		blockFirstRow += bra.NumberOfStatesInBlock(ipin_block);
	}
	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;

    return local_nnz;
}

void GenerateSp3R(double AB00eigenvalue, vector<float>& nonzero_me, vector<int>& columns, vector<int>& rowPtrs)
{
// NOTE: SVD interface may change in future as it belongs to the experimental part of eigen
// Eigen does not implement SVD for nrows < ncols. 
	size_t icol, irow, iMult;
	double  dmax;
	size_t nElems = nonzero_me.size();
	size_t nrows = rowPtrs.size();
	size_t ncols = rowPtrs.size();
	size_t nelems;

//	Create matrix A and fill it with zeros
	Eigen::MatrixXd A((int)nrows, (int)ncols);
	A.setZero();

// 	A <--- CSR
	cout << endl;
	for (int irow = 0; irow < nrows; ++irow)
	{
		if (irow < nrows - 1)
		{
			nelems = rowPtrs[irow+1] - rowPtrs[irow];
		}
		else
		{
			nelems = nonzero_me.size() - rowPtrs[irow];
		}
		size_t ifirst = rowPtrs[irow];
		for (int j = 0; j < nelems; ++j)
		{
			icol = columns[ifirst + j];

			A(irow, icol) = nonzero_me[ifirst+j];
			A(icol, irow) = nonzero_me[ifirst+j];
//			cout << irow+1 << " " << icol+1 << " " << nonzero_me[ifirst + j] << endl;
//			cout << A << endl << endl;
		}
	}

//	for (int i = 0; i < nrows; ++i)
//	{
//		A(i, i) -= AB00eigenvalue;
//	}

//	perform SVD 
	Eigen::JacobiSVD<Eigen::MatrixXd> svdA(A, Eigen::ComputeFullV);
	cout << svdA.singularValues() << endl;

// last Mult columns of V contains SU(3) HWS	
	const Eigen::MatrixXd& V = svdA.matrixV();
//	cout << V << endl;

	Eigen::VectorXd null_vector((int)nrows); 
	for (int j = 0, iMult = 0; j < ncols; ++j, ++iMult)
	{
		Eigen::VectorXd HWS((int)ncols); 
		for (int i = 0; i < ncols; ++i) 
		{
			HWS(i) = V(i, j);
//			m_HWSVectors[iMult][i] = HWS(i);
		}
		null_vector = A*HWS;
		dmax = null_vector.maxCoeff();
		std::cout << "max(A * HWS) = " << dmax << std::endl;
//		if (fabs(dmax) > PRECISION_LIMIT) 
//		{
//			std::cerr << "SU(3)xSU(2) HWS are not annihilated by SU(3) and SU(2) raising operators!!!" << std::endl;
//		} 
	}
}

void GenerateSp3R_SVD(vector<float>& nonzero_me, vector<int>& columns, vector<int>& rowPtrs)
{
// NOTE: SVD interface may change in future as it belongs to the experimental part of eigen
// Eigen does not implement SVD for nrows < ncols. 
	size_t icol, irow, iMult;
	double  dmax;
	size_t nElems = nonzero_me.size();
	size_t nrows = rowPtrs.size();
	size_t ncols = rowPtrs.size();
	size_t nelems;

//	Create matrix A and fill it with zeros
	Eigen::MatrixXd A((int)nrows, (int)ncols);
	A.setZero();

// 	A <--- CSR
	cout << endl;
	for (int irow = 0; irow < nrows; ++irow)
	{
		if (irow < nrows - 1)
		{
			nelems = rowPtrs[irow+1] - rowPtrs[irow];
		}
		else
		{
			nelems = nonzero_me.size() - rowPtrs[irow];
		}
		size_t ifirst = rowPtrs[irow];
		for (int j = 0; j < nelems; ++j)
		{
			icol = columns[ifirst + j];

			A(irow, icol) = nonzero_me[ifirst+j];
			A(icol, irow) = nonzero_me[ifirst+j];
		}
	}

//	perform SVD 
	Eigen::JacobiSVD<Eigen::MatrixXd> svdA(A, Eigen::ComputeFullV);
//TODO: iterate over singular values and compare them with eigenvalues whose eigenvectors we want 
//      store position of those that match in some auxiliary data structure together with 
//      quantum labels of a associated Sp(3,R) state.
//
//	Eigen::VectorXd null_vector((int)nrows); 
//	for (loop over j that we are interested)
//	{
//		Eigen::VectorXd sp3RState((int)ncols); 
//		for (int i = 0; i < ncols; ++i) 
//		{
//			sp3RState(i) = V(i, j);
//			m_HWSVectors[iMult][i] = HWS(i);
//		}
//	}


// THIS CODE IS JUST FOR TEXTING PURPOSES:
	cout << "Operator has the following eigenvalues:" << endl;
	cout << svdA.singularValues() << endl;


//	const Eigen::MatrixXd& V = svdA.matrixV();
//	cout << V << endl;

/*
	Eigen::VectorXd null_vector((int)nrows); 
	for (int j = 0, iMult = 0; j < ncols; ++j, ++iMult)
	{
		Eigen::VectorXd HWS((int)ncols); 
		for (int i = 0; i < ncols; ++i) 
		{
			HWS(i) = V(i, j);
//			m_HWSVectors[iMult][i] = HWS(i);
		}
		null_vector = A*HWS;
//		dmax = null_vector.maxCoeff();
//		std::cout << "max(A * HWS) = " << dmax << std::endl;
//		if (fabs(dmax) > PRECISION_LIMIT) 
//		{
//			std::cerr << "SU(3)xSU(2) HWS are not annihilated by SU(3) and SU(2) raising operators!!!" << std::endl;
//		} 
	}
*/	
}


int main(int argc,char **argv)
{
 	boost::mpi::environment env(argc, argv);
 	boost::mpi::communicator mpi_comm_world;
	if (mpi_comm_world.size() > 1)
	{
		if (mpi_comm_world.rank() == 0)
		{
			std::cerr << "This is a serial code and hence number of precessors must be equal to one." << std::endl;
		}
		mpi_comm_world.abort(EXIT_FAILURE);
	}

	boost::chrono::system_clock::time_point start;
	boost::chrono::duration<double> duration;

	if (su3shell_data_directory == NULL)
	{
		cerr << "System variable 'SU3SHELL_DATA' was not defined!" << endl;
		mpi_comm_world.abort(EXIT_FAILURE);
	}

	if (argc != 2)
	{
	  	cerr << "Usage: "<< argv[0] <<" <file name with run parameters>" << endl;
		mpi_comm_world.abort(EXIT_FAILURE);
	}

	int  nprotons, nneutrons, nmax;
	int  ssp, ssn, ss;

	std::ifstream input_file(argv[1]);
	if (!input_file)
	{
		std::cerr << "Could not open '" << argv[1] << "' input file." << std::endl;
		return EXIT_FAILURE;
	}

	InitSqrtLogFactTables();

	input_file >> nprotons >> nneutrons >> nmax;
	std::cout << "Z:" << nprotons  << " N:" << nneutrons << " Nmax:" << nmax << std::endl;
	input_file >> ssp >> ssn >> ss;
	std::cout << "SSp:" << ssp  << " SSn:" << ssn << " SS:" << ss << std::endl;


	ofstream interaction_log_file("/dev/null");
	CBaseSU3Irreps baseSU3Irreps(nprotons, nneutrons, nmax); 
	bool log_is_on = false;
	bool generate_missing_rme = false;	

    try {
		CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);
		CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on, interaction_log_file);
		COperatorLoader operatorLoader;
		
		operatorLoader.AddAB00(1.0);
		operatorLoader.AddNcm(nprotons + nneutrons, 5000);
		operatorLoader.Load(0, interactionPPNN, interactionPN);

		while (true)
		{
			int nhw, lm, mu;

			input_file >> nhw >> lm >> mu;
			if (!input_file)

			{
				break;
			}
//	 		model space made of Nhw (lm mu)Sp Sn S 
			proton_neutron::ModelSpace ncsmModelSpace(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu);
			lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);	
			std::cout << "Calculating <" << nhw << "hw(" << lm << " " << mu << ")Sp:" << ssp << " Sn:" << ssn << " S:" << ss << "|| -1[AxB] + 5000Ncm ||";
			std::cout << nhw << "hw(" << lm << " " << mu << ")Sp:" << ssp << " Sn:" << ssn << " S:" << ss << ">" << std::endl;
			std::cout << "size: " << basis.dim() << " x " << basis.dim() << std::endl;
			std::cout.flush();

		    vector<float> nonzero_me;
   			vector<int>   columns;
   			vector<int>   rowPtrs;
			
			boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
			CalculateME(interactionPPNN, interactionPN, basis, basis, nonzero_me, columns, rowPtrs);
			boost::chrono::duration<double> duration = boost::chrono::system_clock::now() - start;
			cout << "Resulting time: " << duration << endl;

//	SVD decomposition finds all eigenvalues of operator
//	TODO: 
//	(1) read input file and create a list of all eigenvalues we are interested in
//	(2) iterate over eigenvalues and if they are in the list ==> store corresponding eigenstate
			GenerateSp3R_SVD(nonzero_me, columns, rowPtrs);
			return EXIT_SUCCESS;
//	This part of code is not ready. It should use QR decomposition to get 
//	a null space of operator [ [AxB]^(00) + 5000Ncm - ABeig(N0, lm0, mu0)I ]
/*			
			int nbandheads, n0hw, lm0, mu0;
			input_file >> nbandheads;

			for (int i = 0; i < nbandheads; ++i)
			{
				input_file >> n0hw >> lm0 >> mu0;

				double n0 = getN0(nprotons, nneutrons, n0hw);
				int r = (nhw - n0hw)/2;
				double AB00eigenvalue = (1.0/(2.0*sqrt(6.0)))*(C2Sp3R(lm, mu, (n0 + 2*r)) - C2Sp3R(lm0, mu0, n0));

				cout << "\tGenerating Sp:" << ssp << " Sn:" << ssn << " S:" << ss << " " << n0hw << "hw(" << lm0 << " " << mu0 << ") " << nhw << "hw(" << lm << " " << mu << ") ...";
				std::cout.flush();
				boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
				GenerateSp3R(AB00eigenvalue, nonzero_me, columns, rowPtrs);
				cout << "Resulting time: " << duration << endl;
				return 0;
			}
*/			
		}
    }
    catch (const std::exception& e) 
	{
        cerr << e.what() << std::endl;
		boost::mpi::environment::abort(EXIT_FAILURE);
    }
}

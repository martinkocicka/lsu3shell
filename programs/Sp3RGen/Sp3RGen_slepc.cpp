#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <iomanip>  // std::setprecision

#include <boost/chrono.hpp> 

#include "slepceps.h"

using namespace std;

static char help[] = "###### Serial generator of Sp(3,R) states ######\n\n" "Command line option that must be provided:\n"
" -run_params <filename> \n <filename>: the name of an input file with the following structures: \n\n"
" \t#protons #neutrons \n \tNhw SSp SSn SS lm mu \n"
" optional parameter: -lambda <int> \n" 
" <int>: coefficient in front of Lawson term Ncm\n"
" optional parameter: -output_text to store resulting eigenstates also as text files\n\n";

void SaveAsBin(const char* file_name, const PetscScalar* wave_function, size_t nrows)
{
	ofstream f(file_name, std::ios::binary | std::ios::trunc);
	float value;
	for (int  i = 0; i < nrows; ++i)
	{
		value = wave_function[i];
		f.write((const char*)&value, sizeof(float));
	}
}

void SaveAsText(const char* file_name, const PetscScalar* wave_function, size_t nrows)
{
	ofstream f(file_name, std::ios::trunc);
	for (int  i = 0; i < nrows; ++i)
	{
		f << wave_function[i] << endl;
	}
}

uintmax_t CalculateME(	
					const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
				    vector<double>& nonzero_me,
    				vector<int>&   columns,
    				vector<int>&   rowPtrs)
{
    uintmax_t local_nnz = 0;

	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_J_distr_p;
	unsigned char num_vacuums_J_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

	InitializeIdenticalOperatorRME(identityOperatorRMEPP);
	InitializeIdenticalOperatorRME(identityOperatorRMENN);

	SingleDistribution distr_ip, distr_in, distr_jp, distr_jn;
	UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
	SU3xSU2_VEC vW_ip, vW_in, vW_jp, vW_jn;

	unsigned char deltaP, deltaN;

	const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

	uint32_t blockFirstRow(0);
	
	int32_t icurrentDistr_p, icurrentDistr_n; 
	int32_t icurrentGamma_p, icurrentGamma_n; 
	
	vector<vector<float> > vals_local(bra.MaxNumberOfStatesInBlock());
	vector<vector<size_t> > col_ind_local(bra.MaxNumberOfStatesInBlock());

	for (int i = 0; i < bra.MaxNumberOfStatesInBlock(); ++i)
	{
		vals_local[i].reserve(ket.MaxNumberOfStatesInBlock());
		col_ind_local[i].reserve(ket.MaxNumberOfStatesInBlock());
	}

// loop over block rows
	for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		unsigned int nrows_in_block = bra.NumberOfStatesInBlock(ipin_block);
		if (nrows_in_block == 0)
		{
			continue;
		}

		vals_local.resize(nrows_in_block);
		col_ind_local.resize(nrows_in_block);

		uint32_t ip = bra.getProtonIrrepId(ipin_block);
		uint32_t in = bra.getNeutronIrrepId(ipin_block);

		SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

		uint16_t aip_max = bra.getMult_p(ip);
		uint16_t ain_max = bra.getMult_n(in);

		unsigned long blockFirstColumn(blockFirstRow);

		uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max()); 
		uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max()); 
		
		uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max()); 
		uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max()); 

		uint32_t last_jp(std::numeric_limits<uint32_t>::max());
		uint32_t last_jn(std::numeric_limits<uint32_t>::max());

//	loop over (jp, jn) pairs
// loop over block columns
		for (unsigned int jpjn_block = ipin_block; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			if (ket.NumberOfStatesInBlock(jpjn_block) == 0)
			{
				continue;
			}
			bool isDiagonalBlock = (ipin_block == jpjn_block); 

			uint32_t jp = ket.getProtonIrrepId(jpjn_block);
			uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

			SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
			SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			uint16_t ajp_max = ket.getMult_p(jp);
			uint16_t ajn_max = ket.getMult_n(jn);

			if (jp != last_jp)
			{
				icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
				icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

				if (ilastDistr_p != icurrentDistr_p)
				{
					distr_ip.resize(0); bra.getDistr_p(ip, distr_ip);
					gamma_ip.resize(0); bra.getGamma_p(ip, gamma_ip);
					vW_ip.resize(0); bra.getOmega_p(ip, vW_ip);
				
					distr_jp.resize(0); ket.getDistr_p(jp, distr_jp);
					hoShells_p.resize(0);
					deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p, num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
				}

				if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p)
				{
					if (deltaP <= 4)
					{
						if (!selected_tensorsPP.empty())
						{
							std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsPP.resize(0);

						if (!selected_tensors_p_pn.empty())
						{
							std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_p_pn.resize(0);

						gamma_jp.resize(0); ket.getGamma_p(jp, gamma_jp);
						TransformGammaKet_SelectByGammas(hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensorsPP, selected_tensors_p_pn);
					}
				}
	
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					vW_jp.resize(0); ket.getOmega_p(jp, vW_jp);
					TransformOmegaKet_CalculateRME(distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP, selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
				}
				ilastDistr_p = icurrentDistr_p;
				ilastGamma_p = icurrentGamma_p;
			}

			if (jn != last_jn)
			{
				icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
				icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

				if (ilastDistr_n != icurrentDistr_n)
				{
					distr_in.resize(0); bra.getDistr_n(in, distr_in);
					gamma_in.resize(0); bra.getGamma_n(in, gamma_in);
					vW_in.resize(0); bra.getOmega_n(in, vW_in); 

					distr_jn.resize(0); ket.getDistr_n(jn, distr_jn);
					hoShells_n.resize(0);
					deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n, num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
				}

				if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n)
				{
					if (deltaN <= 4)
					{
						if (!selected_tensorsNN.empty())
						{
							std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsNN.resize(0);

						if (!selected_tensors_n_pn.empty())
						{
							std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_n_pn.resize(0);

						gamma_jn.resize(0); ket.getGamma_n(jn, gamma_jn);
						TransformGammaKet_SelectByGammas(hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn, selected_tensorsNN, selected_tensors_n_pn);
					}
				}

				if (deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					vW_jn.resize(0); ket.getOmega_n(jn, vW_jn);
					TransformOmegaKet_CalculateRME(distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN, selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
				}	

				ilastDistr_n = icurrentDistr_n;
				ilastGamma_n = icurrentGamma_n;
			}


			bool vanishingBlock = true;
			//	loop over wpn that result from coupling ip x in	
			uint32_t ibegin = bra.blockBegin(ipin_block);
			uint32_t iend = bra.blockEnd(ipin_block);
			uint32_t currentRow = blockFirstRow;
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
				size_t afmax = aip_max*ain_max*omega_pn_I.rho;
				IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));
				
				uint32_t currentColumn = (isDiagonalBlock) ? currentRow : blockFirstColumn;
				uint32_t jbegin = (isDiagonalBlock) ? iwpn : ket.blockBegin(jpjn_block);
				uint32_t jend = ket.blockEnd(jpjn_block);
				for (int jwpn = jbegin; jwpn < jend; ++jwpn)
				{
					SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
					size_t aimax = ajp_max*ajn_max*omega_pn_J.rho;
					IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));
	
					if (deltaP + deltaN <= 4)
					{
						Reset_rmeCoeffs(rmeCoeffsPNPN);
						if (in == jn && !rmeCoeffsPP.empty())
						{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
							CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
						}

						if (ip == jp  && !rmeCoeffsNN.empty())
						{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
							CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
						}

						if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
						{
							CalculatePNInteractionMeData(interactionPN, omega_pn_I, omega_pn_J, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
						}

						if (!rmeCoeffsPNPN.empty())
						{
							vanishingBlock = false;
							if (isDiagonalBlock && iwpn == jwpn)
							{
								CalculateME_Diagonal_UpperTriang_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
							else
							{
								CalculateME_nonDiagonal_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
						}
					}
					currentColumn += aimax*ket.omega_pn_dim(jwpn);
				}
				currentRow += afmax*bra.omega_pn_dim(iwpn);
			}
			blockFirstColumn += ket.NumberOfStatesInBlock(jpjn_block);
			last_jp = jp;
			last_jn = jn;
		}
// CSR stuff
		for (size_t i = 0; i < vals_local.size(); ++i)
		{
			rowPtrs.push_back(nonzero_me.size());
			for (size_t j = 0; j < vals_local[i].size(); ++j)
			{
				nonzero_me.push_back(vals_local[i][j]);
				columns.push_back(col_ind_local[i][j]);
//				cout << blockFirstRow + i + 1 << " " << col_ind_local[i][j] + 1 << " " << vals_local[i][j] << endl;
			}
			vals_local[i].resize(0);
			col_ind_local[i].resize(0);
		}
		blockFirstRow += nrows_in_block;
	}

	rowPtrs.push_back(nonzero_me.size());

	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;

    return local_nnz;
}


int main(int argc, char* argv[]) {
    PetscErrorCode ierr;

	SlepcInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

//	su3shell_data_directory ... global variable that defines directory with operators/interactions in SU(3) tensor form
	if (su3shell_data_directory == NULL)
	{
		cerr << "System variable 'SU3SHELL_DATA' was not defined!" << endl;
		ierr = SlepcFinalize(); CHKERRQ(ierr);
		return EXIT_FAILURE;
	}

//	open file with definition of model space in which we are looking for Sp(3, R) states
    char filename[2000];
    PetscBool found;
    ierr = PetscOptionsGetString(PETSC_NULL, "-run_params", filename, 2000, &found); CHKERRQ(ierr);
    if (!found) 
	{
		std::cerr << help << endl;
		ierr = SlepcFinalize(); CHKERRQ(ierr);
		return EXIT_FAILURE;
    }
//	Try to obtain coefficient in front of Lawson term Ncm.
	PetscInt lambda;
    ierr = PetscOptionsGetInt(PETSC_NULL, "-lambda", &lambda, &found); CHKERRQ(ierr);
	if (!found)
	{
		lambda = 5000;
	}
//	Open input file and read parameters: Z N Nhw SSp SSn SS lm mu
	std::ifstream input_file(filename);
	if (!input_file)
	{
		std::cerr << "Could not open '" << filename << "' input file." << std::endl;
		ierr = SlepcFinalize(); CHKERRQ(ierr);
		return EXIT_FAILURE;
	}

    PetscBool text_output;
    ierr = PetscOptionsHasName(PETSC_NULL, "-output_text", &text_output); CHKERRQ(ierr);

	int nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu;
	input_file >> nprotons >> nneutrons;
	std::cout << "Z:" << nprotons  << " N:" << nneutrons << std::endl;
	input_file >> nhw >> ssp >> ssn >> ss >> lm >> mu;
	std::cout << "Nhw:" << nhw << " SSp:" << ssp  << " SSn:" << ssn << " SS:" << ss << std::endl;

	stringstream ss_base_file_name;
	ss_base_file_name << "Z" << nprotons << "_N" << nneutrons << "_" << nhw << "hw_SSp" << ssp << "_SSn" << ssn << "_SS" << ss << "_lm" << lm << "_mu" << mu << "_";

//	Load base operators to assemble (-1).[AxB]^(0 0) + (lambda).Ncm
	InitSqrtLogFactTables();

	ofstream interaction_log_file("/dev/null");
	CBaseSU3Irreps baseSU3Irreps(nprotons, nneutrons, nhw); 
	bool log_is_on = false;
	bool generate_missing_rme = false;	

	CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);
	CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on, interaction_log_file);
	COperatorLoader operatorLoader;

    try 
	{
		operatorLoader.AddAB00(-1.0);
		operatorLoader.AddNcm(nprotons + nneutrons, lambda);
		operatorLoader.Load(0, interactionPPNN, interactionPN);
	}
    catch (const std::exception& e) 
	{
        cerr << e.what() << std::endl;
		ierr = SlepcFinalize(); CHKERRQ(ierr);
		return EXIT_FAILURE;
    }

//	model space made of Nhw (lm mu)Sp Sn S 
	proton_neutron::ModelSpace ncsmModelSpace(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu);
	lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);	
	std::cout << "Calculating <" << nhw << "hw(" << lm << " " << mu << ")Sp:" << ssp << " Sn:" << ssn << " S:" << ss << "|| (-1).[AxB]^(00) + (" << lambda << ").Ncm ||";
	std::cout << nhw << "hw(" << lm << " " << mu << ")Sp:" << ssp << " Sn:" << ssn << " S:" << ss << ">" << std::endl;
	std::cout << "size: " << basis.dim() << " x " << basis.dim() << std::endl;
	std::cout.flush();

//	Calculate matrix elements
    vector<double> nonzero_me;
	vector<int>   columns;
   	vector<int>   rowPtrs;
			
	boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
	CalculateME(interactionPPNN, interactionPN, basis, basis, nonzero_me, columns, rowPtrs);
	boost::chrono::duration<double> duration = boost::chrono::system_clock::now() - start;
	cout << "Resulting time: " << duration << endl;

//	Prepare SLEPc compatible matrix
	PetscInt nrows, ncols, nnz;
    nrows = basis.dim();
    ncols = basis.dim();
	nnz = nonzero_me.size();

    std::cout << "Matrix dimensions: " << nrows << ", " << ncols << ", " << nnz << std::endl;

start = boost::chrono::system_clock::now();

    // preallocation
    std::vector<PetscInt> nnzs(nrows);
	for (PetscInt irow = 0; irow < nrows; ++irow)
        nnzs[irow] = rowPtrs[irow+1] - rowPtrs[irow];
    
	Mat A;
/*
    ierr = MatCreate(PETSC_COMM_SELF, &A); CHKERRQ(ierr);
    ierr = MatSetType(A, MATSEQSBAIJ); CHKERRQ(ierr);
    ierr = MatSeqSBAIJSetPreallocation(A, 1, PETSC_DEFAULT, &(nnzs[0])); CHKERRQ(ierr);
*/
    ierr = MatCreateSeqSBAIJ(PETSC_COMM_SELF, 1, nrows, ncols, PETSC_DEFAULT, &(nnzs[0]), &A); CHKERRQ(ierr);
 // ierr = MatCreateSeqSBAIJ(PETSC_COMM_SELF, 1, nrows, ncols, PETSC_DEFAULT, PETSC_NULL, &A); CHKERRQ(ierr);
	
	PetscInt icol, ifirst, nelems;
   	PetscScalar me_ij;
	for (PetscInt irow = 0; irow < nrows; ++irow)
	{
		nelems = rowPtrs[irow+1] - rowPtrs[irow];
		ifirst = rowPtrs[irow];
		for (PetscInt j = 0; j < nelems; ++j)
		{
			icol = columns[ifirst + j];
        	me_ij = nonzero_me[ifirst+j];
			ierr = MatSetValue(A, irow, icol, me_ij, INSERT_VALUES); CHKERRQ(ierr);
		}
	}

duration = boost::chrono::system_clock::now() - start;
cout << "SLEPc matrix setting values time: " << duration << endl;

start = boost::chrono::system_clock::now();
	
	PetscPrintf(PETSC_COMM_SELF, "Starting assembly... ");
	ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_SELF, "[DONE]\n");

duration = boost::chrono::system_clock::now() - start;
cout << "SLEPc matrix assembly time: " << duration << endl;

//	Prepare SLEPc eigensolver
	EPS eps;
	ierr = EPSCreate(PETSC_COMM_SELF, &eps); CHKERRQ(ierr);

	ierr = EPSSetOperators(eps, A, PETSC_NULL); CHKERRQ(ierr);
	// EPS_HEP: Hermitian eigenvalue problem
	ierr = EPSSetProblemType(eps, EPS_HEP); CHKERRQ(ierr);
	// # eigenvalues to compute can be set by "-eps_nev <nev>"
	ierr = EPSSetDimensions(eps, 10, PETSC_DECIDE, PETSC_DECIDE); CHKERRQ(ierr);
	ierr = EPSSetTolerances(eps, PETSC_IGNORE, PETSC_DECIDE); CHKERRQ(ierr);

	// we are interested in the lowest lying eigenvalues ==> EPS_SMALLEST_REAL
	ierr = EPSSetWhichEigenpairs(eps, EPS_SMALLEST_REAL); CHKERRQ(ierr);

	//	Implicitly use Krylov-Schur method
	ierr = EPSSetType(eps, EPSKRYLOVSCHUR); CHKERRQ(ierr);
	
	ierr = EPSSetFromOptions(eps); CHKERRQ(ierr);

start = boost::chrono::system_clock::now();
//	Find eigenvalues and eigenvectors
	ierr = EPSSolve(eps); CHKERRQ(ierr);
duration = boost::chrono::system_clock::now() - start;
cout << "SLEPc solve time: " << duration << endl;

	PetscInt maxit, its, nconv;
	PetscReal tol;
	ierr = EPSGetIterationNumber(eps, &its); CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_SELF, "Number of iterations of the method: %lu\n", its);
	ierr = EPSGetTolerances(eps, &tol, &maxit); CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_SELF, "Stopping condition: tol = %.4g, maxit = %lu\n", tol, maxit);
	ierr = EPSGetConverged(eps, &nconv); CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_SELF, "Number of converged eigenpairs: %lu\n", nconv);

    PetscScalar eig_re, eig_im;
	Vec xr, xi;

	VecCreateSeq(PETSC_COMM_SELF, nrows, &xr); 
	VecCreateSeq(PETSC_COMM_SELF, nrows, &xi); 

	for (int k = 0; k < nconv; k++) 
	{
        ierr = EPSGetEigenpair(eps, k, &eig_re, &eig_im, xr, xi); CHKERRQ(ierr);

        PetscReal error;
        ierr = EPSComputeRelativeError(eps, k, &error); CHKERRQ(ierr);

		if (Negligible(eig_im))
		{
			PetscPrintf(PETSC_COMM_SELF, "Eigenvalue number %3lu: %9f, relative error: %12g\n", k + 1, eig_re, error);

			stringstream ss_file_name;
			ss_file_name << ss_base_file_name.str() << "_" << std::fixed << std::setprecision(5) << eig_re << "_eigen" << k << ".dat";

			PetscScalar* wave_function;
			VecGetArray(xr, &wave_function);
			SaveAsBin(ss_file_name.str().c_str(), wave_function, nrows);
			if (text_output == 1)
			{
				stringstream ss_text_file_name;
				ss_text_file_name << ss_base_file_name.str() << "_" << std::fixed << std::setprecision(5) << eig_re << "_eigen" << k << ".text";
				SaveAsText(ss_text_file_name.str().c_str(), wave_function, nrows);
			}

			VecRestoreArray(xr, &wave_function);

		}
		else
		{
			PetscPrintf(PETSC_COMM_SELF, "Eigenvalue number %3lu: %9f+%9fi, relative error: %12g)\n", k + 1, eig_re, eig_im, error);
		}
	}
//	Clear memory
	ierr = VecDestroy(&xr); CHKERRQ(ierr);
	ierr = VecDestroy(&xi); CHKERRQ(ierr);
	ierr = EPSDestroy(&eps); CHKERRQ(ierr);
	ierr = MatDestroy(&A); CHKERRQ(ierr);

	ierr = SlepcFinalize(); CHKERRQ(ierr);

	return EXIT_SUCCESS;
}

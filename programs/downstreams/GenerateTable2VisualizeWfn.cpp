#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
#include <sstream>

using namespace std;

enum eRecord {kN = 0, kSSp = 1, kSSn = 2, kSS = 3, kLM = 4, kMU = 5};

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << endl;
	  	cout << "Usage: "; 
		cout << argv[0] << "<model space> <ndiag> <wave function> <output table filename>" << endl;
		return EXIT_FAILURE;
	}

	unsigned int ndiag;
	const string model_space_definition_file_name(argv[1]);
	// get number of diagonal processes
	std::istringstream(argv[2]) >> ndiag;
	// get filename of the input wave function
	const string wfn_file_name(argv[3]);

	
	proton_neutron::ModelSpace ncsmModelSpace(model_space_definition_file_name);
	CncsmSU3Basis  basis(ncsmModelSpace);
	SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container(basis.GenerateFinal_SU3xSU2Irreps(), ncsmModelSpace.JJ());

//	irreps_dim_projections: contains a map of {N, Sp, Sn, S, (lm mu)} irreps which
//	are associated with the dimension & probability amplitude. Irreps span ncsmModelSpace[i].
	map<vector<int>, pair<size_t, float> > irreps_dims_projections;

//	max_prob[N] = maximal probability found in Nhw space
	vector<float> max_prob(basis.Nmax() + 1, 0.0);	// Nmax == 0 ==> I still need to reserve one element 

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary);

	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return EXIT_FAILURE;
	}
	cout << "Reading " << wfn_file_name << " that was saved with " << ndiag << " processors." << endl;
	cout << "Many-body basis is divided into " << ndiag << " sections." << endl;

	float dcoeff;
	SU3xSU2::LABELS w_pn;
	unsigned int rhomax_x_dim_wpn;
	unsigned int ap, an, amax;
	size_t nstates, number_coeffs_read(0);

//	iterate over the segments of the basis [which is split between ndiag diagonal processes].
	cout << "Calculating dimensions and probability amplitudes of N Sp Sn S (l m) subspaces ... ";
	cout.flush();
	for (size_t idiag = 0; idiag < ndiag; ++idiag)
	{
		PNConfIterator iter = basis.firstPNConf(idiag, ndiag);
		for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
		{
			w_pn             = iter.getCurrentSU3xSU2();
			rhomax_x_dim_wpn = wpn_irreps_container.rhomax_x_dim(w_pn);	// rhomax * dim[(lm mu)S]
		 	ap = iter.getMult_p();
			an = iter.getMult_n();
			amax             = ap*an*w_pn.rho;
			nstates          = ap*an*rhomax_x_dim_wpn;

			if (nstates == 0)
			{
				continue;
			}

			IRREPBASIS irrep_basis(wpn_irreps_container.getSU3xSU2PhysicalBasis(w_pn));

			float wpn_projection = 0.0;
			for (int istate = 0; istate < nstates; ++istate)
			{
				wfn_file.read((char*)&dcoeff, sizeof(float));

				if (!wfn_file) //	check that we have not reached the end of wave function file yet
				{
					cout << "End! Last coeff read: " << dcoeff << endl;
					return EXIT_FAILURE;
				}
							
				if (!Negligible(dcoeff))
				{
					wpn_projection += dcoeff*dcoeff;
				}
			}

			//	key: {N, SSp, SSn, SS, lm, mu}
			vector<int> NSpSnlmmu(6);

			NSpSnlmmu[kN]   = iter.nhw(); 
			NSpSnlmmu[kSSp] = iter.getProtonSU3xSU2().S2; 
			NSpSnlmmu[kSSn] = iter.getNeutronSU3xSU2().S2; 
			NSpSnlmmu[kSS]  = w_pn.S2; 
			NSpSnlmmu[kLM]  = w_pn.lm; 
			NSpSnlmmu[kMU]  = w_pn.mu;
			//  find key
			map<vector<int>, pair<size_t, float> >::iterator it = irreps_dims_projections.find(NSpSnlmmu);
			if (it == irreps_dims_projections.end())
			{
				// key does not exist => initialize it
				irreps_dims_projections[NSpSnlmmu] = make_pair(nstates, wpn_projection);
			}
			else
			{
				// a given N Sp S n S (l m) subspace has been  
				it->second.first += nstates;
				it->second.second += wpn_projection;
			}
		}
	}
	cout << "Done" << endl;

	map<int, float> N_max_prob;
	for (map<vector<int>, pair<size_t, float> >::iterator it = irreps_dims_projections.begin(); it != irreps_dims_projections.end(); ++it)
	{
		int N = it->first[kN]; 
		float current_prob = it->second.second;

		if (max_prob[N] < current_prob)
		{
			max_prob[N] = current_prob;
		}
	}

	ofstream output_table_file(argv[4]);
	for (map<vector<int>, pair<size_t, float> >::iterator it = irreps_dims_projections.begin(); it != irreps_dims_projections.end(); ++it)
	{
		const vector<int>& NSpSnlmmu = it->first; 
		const pair<size_t, float>& dim_prob = it->second;
		float dmax = max_prob[NSpSnlmmu[kN]];
		output_table_file << NSpSnlmmu[kN] << " ";
		output_table_file << NSpSnlmmu[kSSp] << " ";
		output_table_file << NSpSnlmmu[kSSn] << " ";
		output_table_file << NSpSnlmmu[kSS] << " "; 
		output_table_file << NSpSnlmmu[kLM] << " "; 
		output_table_file << NSpSnlmmu[kMU] << " ";
		output_table_file << dim_prob.first << " ";
		output_table_file << dim_prob.second/dmax << endl;
	}
	return EXIT_SUCCESS;
}

$(eval $(begin-module))

################################################################
# unit definitions
################################################################

# module_units_h := 
# module_units_cpp-h := 
# module_units_f := 
module_programs_cpp := observablesSU3_MPI ncsmSU3xSU2wfnDecomposition \
transitions_BE2_BM1_MPI GenerateTable2VisualizeWfn  \
ObdSU3Calculator_MPI transitions_BE2_BM1_obds \
rmsE2M1observables_MPI
# module_programs_f :=
# module_generated :=

################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))

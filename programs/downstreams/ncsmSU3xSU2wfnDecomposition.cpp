#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
#include <sstream>

using namespace std;

struct LessC2
{
	bool operator()(const SU3::LABELS& lhs, const SU3::LABELS& rhs) const
	{
		return (lhs.C2() < rhs.C2()) || (lhs.C2() == rhs.C2() && lhs < rhs);
	}
};


void Print(const CTuple<int, 4>& lmS)
{
	cout << lmS[3] << "(" << lmS[0] << " " << lmS[1] << ")" << lmS[2];
}

void PrintState(const vector<CTuple<int, 4> >& Labels_p, const vector<CTuple<int, 4> >& Labels_n,  const SU3xSU2::LABELS& w_pn)
{
	cout << "[";
	size_t index;
	size_t nOmegas_p = (Labels_p.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_p; ++i)
	{
		cout << "{";
	}
	Print(Labels_p[0]);
	if (nOmegas_p > 0)
	{
		cout << " x ";
		Print(Labels_p[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_p - 1; ++i)
		{
			Print(Labels_p[index]); cout << " x ";
			Print(Labels_p[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_p[index]);
	}

	cout << "] x ["; 
	size_t nOmegas_n = (Labels_n.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_n; ++i)
	{
		cout << "{";
	}
	Print(Labels_n[0]);
	if (nOmegas_n > 0)
	{
		cout << " x ";
		Print(Labels_n[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_n - 1; ++i)
		{
			Print(Labels_n[index]); cout << " x ";
			Print(Labels_n[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_n[index]);
	}
	cout << "] " << (int)w_pn.rho << "(" << (int)w_pn.lm << " " << (int)w_pn.mu << ")" << (int)w_pn.S2; 
}

////////////////////////////////////////////////////////////////////////
//	The following set of functions is merely for the output purposes.  I needed
//	to do this quickly, so it is messy ... but seems to work at least good
//	enough for the testing/debbuging rme calculation purposes ...
//
void Set(CTuple<int, 4>& lmS, const UN::SU3xSU2& gamma)
{
	lmS[0] = gamma.lm; lmS[1] = gamma.mu; lmS[2] = gamma.S2; lmS[3] = gamma.mult;
}
void Set(CTuple<int, 4>& lmS, const SU3xSU2::LABELS& omega)
{
	lmS[0] = omega.lm; lmS[1] = omega.mu; lmS[2] = omega.S2; lmS[3] = omega.rho;
}
//	Take gamma and omega and creates a single array of four integers with structure
//
//	Labels = { 	{gamma[0].lm, gamma[0].mu, gamma[0].S2, gamma[0].mult}, 
//				{gamma[1].lm, gamma[1].mu, gamma[1].S2, gamma[1].mult}, 
//				{omega[0].lm, omega[0].mu, omega[0].S2, omega[0].rho}, 
//				{gamma[2].lm, gamma[2].mu, gamma[2].S2, gamma[2].mult}, 
//				{omega[1].lm, omega[1].mu, omega[1].S2, omega[1].rho}, 
//				.
//				.
//				.
//				{omega[#shells - 1].lm, omega[#shells - 1].mu, omega[#shells - 1].S2, omega[#shells - 1].rho}, 
template<class T>
void Output(const T& gamma, const SU3xSU2_VEC& omega, vector<CTuple<int, 4> >& Labels)
{
	CTuple<int, 4> lmS;

	Set(lmS, gamma[0]); Labels.push_back(lmS); 

	if (omega.empty())
	{
		return;
	}

	Set(lmS, gamma[1]); Labels.push_back(lmS);

	for (size_t iomega = 0; iomega < omega.size() - 1; ++iomega)
	{
		Set(lmS, omega[iomega]); Labels.push_back(lmS);
		Set(lmS, gamma[iomega+2]); Labels.push_back(lmS);
	}
	Set(lmS, (omega.back()));
	Labels.push_back(lmS); 
}

void Print( const SingleDistribution& distr_p, const SingleDistribution& distr_n)
{
	cout << "[";
	for (size_t i = 0; i < distr_p.size() - 1; ++i)
	{
		cout << (int)distr_p[i] << " ";
	}
	cout << (int)distr_p.back() << "] x [";

	for (size_t i = 0; i < distr_n.size() - 1; ++i)
	{
		cout << (int)distr_n[i] << " ";
	}
	cout << (int)distr_n.back() << "]" << endl;
}

void Print(const UN::SU3xSU2_VEC& gamma_p, const SU3xSU2_VEC& omega_p, const UN::SU3xSU2_VEC& gamma_n, const SU3xSU2_VEC& omega_n, const SU3xSU2::LABELS& w_pn)
{
	vector<CTuple<int, 4> > Labels_p;
	vector<CTuple<int, 4> > Labels_n;

	Output(gamma_p, omega_p, Labels_p);
	Output(gamma_n, omega_n, Labels_n);

	PrintState(Labels_p, Labels_n, w_pn);
}

struct Less
{
	bool operator() (const pair<float, vector<int> >& l,  const pair<float, vector<int> >& r) {return l.first > r.first;}
};

//	Input:
//	irreps_projections[i]: contains a map of {Sp, Sn, S, (lm mu)} irreps which
//	are associated with the probability amplitude. Irreps span ncsmModelSpace[i].
//
//	irreps_nstates[i]: contains a map of {Sp, Sn, S, (lm mu)} irreps associated
//	with the number of states belonging to the latter irrep that have non-vanishing 
//	projection with	the input wave function.
void AnalyzeData(const vector<map<vector<int>, float> >& irreps_projections, const vector<map<vector<int>, size_t> >& irreps_nstates, const proton_neutron::ModelSpace& ncsmModelSpace)
{
	vector<float> projections_nhw;
	proton_neutron::ModelSpace::const_iterator nhwspace = ncsmModelSpace.begin();
	for (size_t i = 0; nhwspace < ncsmModelSpace.end(); ++nhwspace, ++i)
	{
		float projection = 0.0;
		cout << "Nmax = " << (int)nhwspace->N() << endl;

//	vector of probability amplitudes and quantum labels Sp, Sn, S, (lm mu).
		vector<pair<float, vector<int> > > irreps_ordered_by_amplitudes;
		for (map<vector<int>, float>::const_iterator ir = irreps_projections[i].begin(); ir != irreps_projections[i].end(); ++ir)
		{
			irreps_ordered_by_amplitudes.push_back(make_pair(ir->second, ir->first));
		}
//	order elements of the vector according the the amplitudes
		sort(irreps_ordered_by_amplitudes.begin(), irreps_ordered_by_amplitudes.end(), Less());

//	for each model nhw subspace, print a table of Sp, Sn, S, (lm mu) and their
//	probability amplitudes and number of states that have non-negligible
//	contribution to the input wave function

//	create a vector of unique SU3 labels
		set<SU3::LABELS, LessC2> su3_labels_set;
//	create a vector of {(Sp Sn S), projection} pairs 
		map<vector<int>, float> spsnS_projections;

		vector<int> spsnS(3);
		for (vector<pair<float, vector<int> > >::const_iterator it = irreps_ordered_by_amplitudes.begin(); it != irreps_ordered_by_amplitudes.end(); ++it)
		{
			map<vector<int>, size_t>::const_iterator irdim = irreps_nstates[i].find(it->second);
			cout << "Sp=" << it->second[0] << " Sn=" << it->second[1] << " S=" << it->second[2] << "(" << it->second[3] << " " << it->second[4] << ")";
			cout << "\t" << irdim->second << "\t\t" << it->first << endl;
			
			projection += it->first;

			su3_labels_set.insert(SU3::LABELS(1, it->second[3], it->second[4]));

			spsnS[0] = it->second[0]; spsnS[1] = it->second[1]; spsnS[2] = it->second[2];
			spsnS_projections[spsnS]+= it->first;
		}

		vector<pair<float, vector<int> > > spsnS_ordered_by_amplitudes;
		for (map<vector<int>, float>::const_iterator ir = spsnS_projections.begin(); ir != spsnS_projections.end(); ++ir)
		{
			spsnS_ordered_by_amplitudes.push_back(make_pair(ir->second, ir->first));
		}
//	order elements of the vector according the the amplitudes
		sort(spsnS_ordered_by_amplitudes.begin(), spsnS_ordered_by_amplitudes.end(), Less());

//	save into Nmax*.table file resulting projections of SpSnS(l m) subspaces so
//	that it can be used by Open Office to create decomposition plots
		string filename("Nmax");
		stringstream ss;//create a stringstream
		ss << (int)(nhwspace->N());
   		filename += ss.str();
		filename += ".table";
		cout << "Saving into " << filename << endl;

		ofstream table_file(filename.c_str());
		if (!table_file)
		{
			cout << "Error!" << endl;
			exit(EXIT_FAILURE);
		}

		table_file << ";";
		
		set<SU3::LABELS>::const_iterator sit = su3_labels_set.begin();
		for (size_t i = 0; sit != su3_labels_set.end(); ++sit, ++i)
		{
			if (i == su3_labels_set.size() - 1)
			{
				table_file << "(" << (int)sit->lm << " " << (int)sit->mu << ")";
			}
			else
			{
				table_file << "(" << (int)sit->lm << " " << (int)sit->mu << ");";
			}
		}
		table_file << endl;

		for (vector<pair<float, vector<int> > >::const_iterator it = spsnS_ordered_by_amplitudes.begin(); it < spsnS_ordered_by_amplitudes.end(); ++it)
		{

			table_file << "Sp=";
			if ((it->second[0] % 2) == 0) {
				table_file << it->second[0]/2 << " ";
			}
			else {
				table_file <<  it->second[0] << "/2 ";
			}
			table_file << "Sn=";
			if ((it->second[1] % 2) == 0) {
				table_file << it->second[1]/2 << " ";
			}
			else {
				table_file <<  it->second[1] << "/2 ";
			}
			table_file << "S=";
			if ((it->second[2] % 2) == 0) {
				table_file << it->second[2]/2 << ";";
			}
			else {
				table_file <<  it->second[2] << "/2;";
			}

			vector<float> su3_amplitudes(su3_labels_set.size());
			for (vector<pair<float, vector<int> > >::const_iterator all_it = irreps_ordered_by_amplitudes.begin(); all_it != irreps_ordered_by_amplitudes.end(); ++all_it)
			{
				if (it->second[0] == all_it->second[0] && it->second[1] == all_it->second[1] && it->second[2] == all_it->second[2])
				{
					SU3::LABELS ir2find(1, all_it->second[3], all_it->second[4]);
					size_t index = std::distance(su3_labels_set.begin(), su3_labels_set.find(ir2find));
					su3_amplitudes[index] = all_it->first;
				}
			}

			for (size_t i = 0; i < su3_amplitudes.size(); ++i)
			{
				if (i == su3_amplitudes.size() - 1)
				{
					table_file << su3_amplitudes[i];
				}
				else
				{
					table_file << su3_amplitudes[i] << ";";
				}
			}
			table_file << endl;
		}
	 	projections_nhw.push_back(projection);
	}

	for (size_t i = 0; i < projections_nhw.size(); ++i)
	{
		cout << projections_nhw[i] << endl;
	}
	cout << "total: " << std::accumulate(projections_nhw.begin(), projections_nhw.end(), 0.0) << endl;
}


int main(int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "Usage: " << argv[0] << " <model space> <ndiag> <eigenstate>" << endl;
		return 0;
	}

	unsigned int ndiag;
	const string model_space_definition_file_name(argv[1]), wfn_file_name(argv[3]);
	std::istringstream(argv[2]) >> ndiag;

	cout << "Reading " << wfn_file_name << " that was saved with " << ndiag << " processors." << endl;
	
	proton_neutron::ModelSpace ncsmModelSpace(model_space_definition_file_name);
	
//	vector of nhw model spaces
	vector<int> nhw_subspaces(ncsmModelSpace.size());
	
	proton_neutron::ModelSpace::const_iterator nhwspace = ncsmModelSpace.begin();
	for (size_t i = 0; nhwspace < ncsmModelSpace.end(); ++nhwspace, ++i)
	{
		nhw_subspaces[i] = nhwspace->N();
	}

	float dcoeff;
//	irreps_projections[i]: contains a map of {Sp, Sn, S, (lm mu)} irreps which
//	are associated with the probability amplitude. Irreps span ncsmModelSpace[i].
	vector<map<vector<int>, float> > irreps_projections(ncsmModelSpace.size());
//	irreps_nstates[i]: contains a map of {Sp, Sn, S, (lm mu)} irreps associated
//	with the number of states belonging to the latter irrep that have non-vanishing 
//	projection with	the input wave function.
	vector<map<vector<int>, size_t> > irreps_nstates(ncsmModelSpace.size());

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary);

	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return 1;
	}

	lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, ndiag);

//	iterate over the segments of the basis [which is split between ndiagonal processes].
	for (size_t idiag = 0; idiag < ndiag; ++idiag, basis.Reshuffle(ncsmModelSpace, idiag, ndiag))
	{
		for (unsigned int ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++)
		{
			if (basis.NumberOfStatesInBlock(ipin_block) == 0)
			{
				continue;
			}
			
			uint32_t ip = basis.getProtonIrrepId(ipin_block);
			uint32_t in = basis.getNeutronIrrepId(ipin_block);
			uint32_t Nhw =  basis.nhw_p(ip) + basis.nhw_n(in);

			SU3xSU2::LABELS w_ip(basis.getProtonSU3xSU2(ip));
			SU3xSU2::LABELS w_in(basis.getNeutronSU3xSU2(in));

			uint16_t aip_max = basis.getMult_p(ip);
			uint16_t ain_max = basis.getMult_n(in);

			uint32_t ibegin = basis.blockBegin(ipin_block);
			uint32_t iend = basis.blockEnd(ipin_block);
//	loop over wpn that result from coupling ip x in	
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				IRREPBASIS irrep_basis(basis.Get_Omega_pn_Basis(iwpn));
				uint16_t amax = aip_max*ain_max*SU3::mult(w_ip, w_in, irrep_basis);

				float wpn_projection = 0.0; // the total projection of | (ap=* wp) x (an=* wn) -> rho=* wpn k=* L=* J=*>  states
//	vector of { <LJka, |a|^2>, <L'K'k'a', |b|^2>, .... , <L''J''k''a'', |c|^2> }			
				vector<pair<CTuple<int, 4>, float> > states_LJka;
//	The order of loops is extremely important and must be same as the one being
//	used to calculate the matrix elements.
				for (irrep_basis.rewind(); !irrep_basis.IsDone(); irrep_basis.nextL())
				{
					for (int J = irrep_basis.Jmin(); J <= irrep_basis.Jmax(); J += 2)
					{
						for (int k = 0; k < irrep_basis.kmax(); ++k)
						{
							size_t kfLfJId = irrep_basis.getId(k, J);
							for (int a = 0; a < amax; ++a)
							{
								wfn_file.read((char*)&dcoeff, sizeof(float));
//	we should never reach the end of the file before iterating through all
//	possible comfigurations
								if (!wfn_file)
								{
									cout << "End! Last coeff read: " << dcoeff << endl;
								}
//					assert(wfn_file);
//	the (id+1) of a given state is equal to the number of states that have been
//	so far read from the input file. 
//						assert((basis_state_id + 1) == number_coeffs_read);
								if (!Negligible(dcoeff))
								{
									wpn_projection += dcoeff*dcoeff;
									CTuple<int, 4> JLka;
									JLka[0] = irrep_basis.L(); 
									JLka[1] = J; 
									JLka[2] = k; 
									JLka[3] = a; 
									states_LJka.push_back(make_pair(JLka, dcoeff));
								}
							}
						}
					}
				}

				if (states_LJka.empty())// ==> there is no state in w_pn irrep that would project on the input wave function			{
				{
					continue;
				}

				vector<int> spsnslmu(5);
				spsnslmu[0] = w_ip.S2; 
				spsnslmu[1] = w_in.S2; 
				spsnslmu[2] = irrep_basis.S2; 
				spsnslmu[3] = irrep_basis.lm; 
				spsnslmu[4] = irrep_basis.mu;
				int index = (std::find(nhw_subspaces.begin(), nhw_subspaces.end(), (int)Nhw) -  nhw_subspaces.begin());
				assert(index < nhw_subspaces.size());
				(irreps_projections[index])[spsnslmu] += wpn_projection;
//	add number of states that in SpSnS(lm mu) subspace that project on the given wave function
				(irreps_nstates[index])[spsnslmu] += states_LJka.size();
			}
		}
	}
	AnalyzeData(irreps_projections, irreps_nstates, ncsmModelSpace);
}

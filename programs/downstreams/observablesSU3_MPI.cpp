#include <mpi.h>
#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>

#include <stdexcept>
#include <sstream>
#include <cmath>
#include <vector>
#include <stack>
#include <ctime>

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::stringstream;
using std::cin;

//#define SAVE_MATRIX_AS_TEXT

/**	data for eigenstates */
typedef vector<float> WFN;

/**
 *
 * Stores position of the first state of each proton-neutron irrep 
 * that belongs to a particular section of basis
 */
class Section_Wfn_Structure
{
	/** this vector stores for each w_pn irrep a position of the first state with respect to the first state in a this section of basis  */
	vector<size_t> wpn_starting_position_;
	/**  dimension of the section  */
	size_t dim_; 		
	/* position of the first state of this section in the global basis */
	size_t firstStateId_global_; 
	public:
	/**
	 * \param section_dims {contains number of states spanning each section of the basis}
	 * \param isection {section number}
	 */
	Section_Wfn_Structure(const CncsmSU3Basis& basis, const vector<size_t>& section_dims, int isection, const SU3xSU2::IrrepsContainer<IRREPBASIS>& irreps_basis); 
	/** returns first state position of the ith w_pn irrep in this section */
	inline size_t global_position(size_t iw_pn) const {return (firstStateId_global_ + wpn_starting_position_[iw_pn]);}
	inline size_t local_position(size_t iw_pn) const {return wpn_starting_position_[iw_pn];}
	unsigned long dim() const {return dim_;}
	void Show() {std::copy(wpn_starting_position_.begin(), wpn_starting_position_.end(), std::ostream_iterator<size_t>(cout, "\n"));}
};

Section_Wfn_Structure::Section_Wfn_Structure(const CncsmSU3Basis& basis, const vector<size_t>& section_dims, int isection, const SU3xSU2::IrrepsContainer<IRREPBASIS>& irreps_basis): dim_(section_dims[isection]), firstStateId_global_(std::accumulate(section_dims.begin(), section_dims.begin() + isection, 0))
{
//	basis is split into ndiag sections	
	size_t ndiag = section_dims.size();
	PNConfIterator iter = basis.firstPNConf(isection, ndiag);
	size_t nstates_in_block, stateId(0);
	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf(), stateId += nstates_in_block)
	{
		wpn_starting_position_.push_back(stateId);
		//	number of states spanning the current proton-neutron SU(3) irrep
		nstates_in_block = iter.getMult_p()*iter.getMult_n()*irreps_basis.rhomax_x_dim(iter.getCurrentSU3xSU2());
	}
}

/** assign row and column of the block matrix for which process with my_rank will be responsible */
void MapRankToColRow(const int my_rank, const int num_columns, int& row, int& col)
{
	col = my_rank % num_columns;
	row = (my_rank - col)/num_columns;
}


void CalculateME(	const CInteractionPPNN& interactionPPNN,
					const CInteractionPN& interactionPN,
					const CncsmSU3Basis& bra_basis, 
					const unsigned int nbra_sections,
					const unsigned int iblock, 
					const unsigned int firstStateId_bra,
 					const SU3xSU2::IrrepsContainer<IRREPBASIS>& bra_irreps_basis,
					const CncsmSU3Basis& ket_basis, 
					const unsigned int nket_sections,
					const unsigned int jblock,
					const unsigned int firstStateId_ket,
 					const SU3xSU2::IrrepsContainer<IRREPBASIS>& ket_irreps_basis,
#ifdef SAVE_MATRIX_AS_TEXT							
					ofstream& fresults,
#endif							
					std::vector<float>& vals, std::vector<size_t>& column_indices, std::vector<size_t>& row_ptr)
{
	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_ket_distr_p;
	unsigned char num_vacuums_ket_distr_n;
	SingleDistribution distr_p_ket, distr_n_ket;
	SingleDistribution distr_p_bra, distr_n_bra;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;
	unsigned int ap_bra, an_bra;
	unsigned int ap_ket, an_ket;
	SU3xSU2::LABELS omega_pn_bra, omega_pn_ket;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

	InitializeIdenticalOperatorRME(identityOperatorRMEPP);
	InitializeIdenticalOperatorRME(identityOperatorRMENN);
	
	unsigned long stateId_bra(firstStateId_bra);
	unsigned int dim_bra_irrep = 0;

	unsigned char status_bra_basis;
	UN::SU3xSU2_VEC gamma_p_bra, gamma_n_bra;

	UN::SU3xSU2_VEC	gamma_p_bra_vacuum; 
	SU3xSU2_VEC omega_p_bra_vacuum;
	UN::SU3xSU2_VEC	gamma_n_bra_vacuum; 
	SU3xSU2_VEC omega_n_bra_vacuum;

	SU3xSU2_VEC omega_p_bra, omega_n_bra;

	unsigned int dim_ket_irrep = 0;
	unsigned char status_ket_basis;
	UN::SU3xSU2_VEC gamma_p_ket, gamma_n_ket;
	SU3xSU2_VEC omega_p_ket, omega_n_ket;

	unsigned char deltaP, deltaN;

	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > TensorsPP;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > TensorsNN;

	PNConfIterator bra = bra_basis.firstPNConf(iblock, nbra_sections);
	PNConfIterator ket = ket_basis.firstPNConf(jblock, nket_sections);

	for (bra.rewind(); bra.hasPNConf(); bra.nextPNConf())
	{
		dim_bra_irrep = bra_irreps_basis.rhomax_x_dim(bra.getCurrentSU3xSU2());

		status_bra_basis = bra.status();
		switch (status_bra_basis)
		{
			case PNConfIterator::kNewDistr_p: 
			case PNConfIterator::kNewDistr_n: 
			case PNConfIterator::kNewGamma_p: 
			case PNConfIterator::kNewGamma_n: 
			case PNConfIterator::kNewOmega_p: 
				ap_bra = bra.getMult_p();
			case PNConfIterator::kNewOmega_n: 
				an_bra = bra.getMult_n();
			case PNConfIterator::kNewPNOmega:
				omega_pn_bra = bra.getCurrentSU3xSU2();	
		}

		if (dim_bra_irrep == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
		{
			continue;	//	move to the next configuration 
		}

		unsigned long stateId_ket(firstStateId_ket); // operator is
		size_t afmax = ap_bra*an_bra*omega_pn_bra.rho;
		IRREPBASIS  braSU3xSU2basis(bra_irreps_basis.getSU3xSU2PhysicalBasis(omega_pn_bra));
		size_t num_rows_in_block = afmax*braSU3xSU2basis.dim();
		vector<vector<float> > vals_local(num_rows_in_block);	// how to resize elements ?
		vector<vector<size_t> > col_ind_local(num_rows_in_block);

		for (ket.rewind(); ket.hasPNConf(); ket.nextPNConf())
		{
			dim_ket_irrep = ket_irreps_basis.rhomax_x_dim(ket.getCurrentSU3xSU2());
			status_ket_basis = ket.status();

			if (status_ket_basis == PNConfIterator::kNewDistr_p)
			{
				distr_p_bra.resize(0); bra.getDistr_p(distr_p_bra);
				gamma_p_bra.resize(0); bra.getGamma_p(gamma_p_bra);
				omega_p_bra.resize(0); bra.getOmega_p(omega_p_bra);
				
				distr_p_ket.resize(0); ket.getDistr_p(distr_p_ket);
				hoShells_p.resize(0);
				deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_p_bra, gamma_p_bra, omega_p_bra, distr_p_ket, hoShells_p, num_vacuums_ket_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
			}

			if (status_ket_basis >= PNConfIterator::kNewDistr_n)
			{
				distr_n_bra.resize(0); bra.getDistr_n(distr_n_bra);
				gamma_n_bra.resize(0); bra.getGamma_n(gamma_n_bra);
				omega_n_bra.resize(0); bra.getOmega_n(omega_n_bra); 

				distr_n_ket.resize(0); ket.getDistr_n(distr_n_ket);
				hoShells_n.resize(0);
				deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_n_bra, gamma_n_bra, omega_n_bra, distr_n_ket, hoShells_n, num_vacuums_ket_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
			}

			if (status_ket_basis >= PNConfIterator::kNewGamma_p)
			{
				if (deltaP <= 4)
				{
					if (!selected_tensorsPP.empty())
					{
						std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
					}
					selected_tensorsPP.resize(0);

					if (!selected_tensors_p_pn.empty())
					{
						std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
					}
					selected_tensors_p_pn.resize(0);

					gamma_p_ket.resize(0);
					ket.getGamma_p(gamma_p_ket);
					TransformGammaKet_SelectByGammas(hoShells_p, distr_p_ket, num_vacuums_ket_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_p_bra, gamma_p_ket, selected_tensorsPP, selected_tensors_p_pn);
				}
			}

			if (status_ket_basis >= PNConfIterator::kNewGamma_n)
			{
				if (deltaP + deltaN <= 4)
				{
					if (!selected_tensorsNN.empty())
					{
						std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
					}
					selected_tensorsNN.resize(0);

					if (!selected_tensors_n_pn.empty())
					{
						std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
					}
					selected_tensors_n_pn.resize(0);

					gamma_n_ket.resize(0);
					ket.getGamma_n(gamma_n_ket);
					TransformGammaKet_SelectByGammas(	hoShells_n, distr_n_ket, num_vacuums_ket_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_n_bra, gamma_n_ket, selected_tensorsNN, selected_tensors_n_pn);
				}
			}

			if (status_ket_basis >= PNConfIterator::kNewOmega_p)
			{
				ap_ket = ket.getMult_p();
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					omega_p_ket.resize(0); ket.getOmega_p(omega_p_ket);
					TransformOmegaKet_CalculateRME(distr_p_ket, gamma_p_bra, omega_p_bra, gamma_p_ket, num_vacuums_ket_distr_p, selected_tensorsPP, selected_tensors_p_pn, omega_p_ket, rmeCoeffsPP, rme_index_p_pn);
				}
			}

			if (status_ket_basis >= PNConfIterator::kNewOmega_n)
			{
				an_ket = ket.getMult_n();
				if (deltaP + deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					omega_n_ket.resize(0); ket.getOmega_n(omega_n_ket);
					TransformOmegaKet_CalculateRME(distr_n_ket, gamma_n_bra, omega_n_bra, gamma_n_ket, num_vacuums_ket_distr_n, selected_tensorsNN, selected_tensors_n_pn, omega_n_ket, rmeCoeffsNN, rme_index_n_pn);
				}	
			}

			if (dim_ket_irrep == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
			{
				continue;	//	move to the next configuration
			}

			if (deltaP + deltaN <= 4)
			{
				omega_pn_ket = ket.getCurrentSU3xSU2();	
				Reset_rmeCoeffs(rmeCoeffsPNPN);

				if (bra.neutronConf() == ket.neutronConf() && !rmeCoeffsPP.empty())
				{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
					CreateIdentityOperatorRME(bra.getNeutronSU3xSU2(), ket.getNeutronSU3xSU2(), an_ket, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
					Calculate_Proton_x_Identity_MeData(omega_pn_bra, omega_pn_ket, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
				}

				if (bra.protonConf() == ket.protonConf() && !rmeCoeffsNN.empty())
				{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
					CreateIdentityOperatorRME(bra.getProtonSU3xSU2(), ket.getProtonSU3xSU2(), ap_ket, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
					Calculate_Identity_x_Neutron_MeData(omega_pn_bra, omega_pn_ket, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
				}

				if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
				{
					CalculatePNInteractionMeData(interactionPN, omega_pn_bra, omega_pn_ket, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
				}
				size_t aimax = ap_ket*an_ket*omega_pn_ket.rho;
				IRREPBASIS ketSU3xSU2basis(ket_irreps_basis.getSU3xSU2PhysicalBasis(omega_pn_ket));
				if (!rmeCoeffsPNPN.empty())
				{
					CalculateME_nonDiagonal_nonScalar(afmax, braSU3xSU2basis, stateId_bra, aimax, ketSU3xSU2basis, stateId_ket, rmeCoeffsPNPN, vals_local, col_ind_local);
				}
			}
			stateId_ket += ap_ket*an_ket*dim_ket_irrep;
		}
#ifdef SAVE_MATRIX_AS_TEXT		
		for (size_t i = 0; i < vals_local.size(); ++i)
		{
			size_t irow = stateId_bra + i;
			for (size_t j = 0; j < vals_local[i].size(); ++j)
			{
				fresults << (irow + 1) << " " << (col_ind_local[i][j] + 1) << " " << vals_local[i][j] << "\n";
			}
		}
#endif		
		for (size_t irow = 0; irow < num_rows_in_block; ++irow)
		{
			vals.insert(vals.end(), vals_local[irow].begin(), vals_local[irow].end());
			column_indices.insert(column_indices.end(), col_ind_local[irow].begin(), col_ind_local[irow].end()); 
			row_ptr.push_back(row_ptr.back() + vals_local[irow].size());
		}
		stateId_bra += ap_bra*an_bra*dim_bra_irrep; 
	}

	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;
}

float bra_x_Observable_x_ket(const WFN& bra_wfn, const vector<float>& vals, const vector<size_t>& column_indices, const vector<size_t>& row_ptrs, const WFN& ket_wfn)
{
	vector<float> observable_x_ket(bra_wfn.size(), 0); // size equal to bra vector 

	assert(vals.size() == row_ptrs.back());
	assert(observable_x_ket.size() == row_ptrs.size() - 1);

	for (size_t irow = 0; irow < row_ptrs.size() - 1; ++irow)
	{
		for (size_t ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival)
		{
			observable_x_ket[irow] += vals[ival]*ket_wfn[column_indices[ival]];
		}
	}

	float dresult = std::inner_product(bra_wfn.begin(), bra_wfn.end(), observable_x_ket.begin(), 0.0);
	return dresult;
}


/**
 * Read eigenstate created with ndiag_wfn diagonal processes and create for each section of the basis corresponding section of the eigenstate
 */
void ReadWfn_SplitToSections(const CncsmSU3Basis& basis, const int ndiag_wfn, const string& wfn_file_name, const vector<Section_Wfn_Structure>& sections, vector<vector<float> >& wfn_sections, const SU3xSU2::IrrepsContainer<IRREPBASIS>& irreps_basis)
{
	float dcoeff;
	int num_sections = sections.size();
	int nstates;
	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary);

	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return;
	}

	wfn_sections.resize(num_sections);
	for (size_t isection = 0; isection < num_sections; ++isection)
	{
		wfn_sections[isection].resize(sections[isection].dim());
	}

	for (int idiag = 0; idiag < ndiag_wfn; ++idiag)
	{
		unsigned int wpn_id = idiag;	//	wpn_id ... position of wpn in a basis with ndiag_wfn = 1

		PNConfIterator iter = basis.firstPNConf(idiag, ndiag_wfn);
		for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf(), wpn_id += ndiag_wfn)
		{
			nstates = iter.getMult_p()*iter.getMult_n()*irreps_basis.rhomax_x_dim(iter.getCurrentSU3xSU2());
			int isection = wpn_id % num_sections;
			int ipos = (wpn_id - isection)/num_sections;
			int firstStateId = sections[isection].local_position(ipos);

			for (int i = 0; i < nstates; ++i)
			{
				wfn_file.read((char*)&dcoeff, sizeof(float));
				wfn_sections[isection][firstStateId + i] = dcoeff;
			}
		}
	}
}

/**
 * Read eigenstate created with ndiag_wfn diagonal processes and create a vector that contains a my_section(th) section of the eigenstate.
 */
void ReadWfn_ToSection(const CncsmSU3Basis& basis, const int ndiag_wfn, const string& wfn_file_name, const int num_sections, const int my_section, const Section_Wfn_Structure& section, vector<float>& wfn_section, const SU3xSU2::IrrepsContainer<IRREPBASIS>& irreps_basis)
{
	float dcoeff;
	int nstates, ipos, firstStateId;
	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary);

	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return;
	}

	wfn_section.resize(section.dim());	// allocate wfn so that it is equal to the dimension of the section
	for (int idiag = 0; idiag < ndiag_wfn; ++idiag)
	{
		unsigned int wpn_id = idiag;	//	wpn_id ... position of wpn in a basis with ndiag_wfn = 1

		PNConfIterator iter = basis.firstPNConf(idiag, ndiag_wfn);
		for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf(), wpn_id += ndiag_wfn)
		{
			nstates = iter.getMult_p()*iter.getMult_n()*irreps_basis.rhomax_x_dim(iter.getCurrentSU3xSU2());
			int isection = wpn_id % num_sections;
			if (isection == my_section)
			{
				ipos = (wpn_id - my_section)/num_sections;
				firstStateId = section.local_position(ipos);
			}

			for (int i = 0; i < nstates; ++i)
			{
				wfn_file.read((char*)&dcoeff, sizeof(float));
				if (isection == my_section)
				{
					wfn_section[firstStateId + i] = dcoeff;
				}
			}
		}
	}
}


int main(int argc,char **argv)
{
   	MPI_Init(&argc, &argv);

	int my_rank, nprocs, my_row, my_col;
	MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);

	cout.precision(10);

	if (argc != 2)
	{
		if (my_rank == 0)
		{
			cout << "Usage: "<< argv[0] <<" <input file name>" << endl;
			cout << "Structure of input file:" << endl;
			cout << "<operator type> one of the following BE2 BM1" << endl;
			cout << "<number of sections utilized to produce bra wfns>			<number of sections utilized to produce ket wfns>" << endl;
			cout << "<number of sections in bra space>               			<number of sections in ket spaces>" << endl;
			cout << "<number of bra pairs>" << endl;
			cout << "<bra file name> 											<ket file name>" << endl;
			cout << ". 															." << endl;
			cout << ". 															." << endl;
			cout << ". 															." << endl;
			cout << "<hw value> in the case of E2 transitions" << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	ifstream input_file(argv[1]);
	if (!input_file)
	{
		if (my_rank == 0)
		{
			cerr << "Could not open '" << argv[1] << "' input file!" << endl;
		}
	}	
//	Read an input file:
	int nbra_sections, nket_sections, ndiag_bra, ndiag_ket;
	string bra_model_space_filename, ket_model_space_filename;
	int num_bra_ket_wfns;

	input_file >> nbra_sections >> nket_sections >> ndiag_bra >> ndiag_ket;
	if (nprocs != nbra_sections*nket_sections)
	{
		if (my_rank == 0)
		{
			cerr << "Total number of processes must be equal to the product of (#sections in bra space) and (#sections in ket space)!" << endl;
			cerr << "(#sections in bra space) = " << nbra_sections << endl;
			cerr << "(#sections in ket space) = " << nket_sections << endl;
			cerr << "#processes = " << nprocs << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}
	input_file >> bra_model_space_filename >> ket_model_space_filename;
	input_file >> num_bra_ket_wfns;

	vector<pair<string, string> > bra_ket_wfns(num_bra_ket_wfns);
	for (int i = 0; i < num_bra_ket_wfns; ++i)
	{
		input_file >> bra_ket_wfns[i].first >> bra_ket_wfns[i].second;
	}
//	assign a row and a column of a submatrix that the process with my_rank will be evaluating
	MapRankToColRow(my_rank, nket_sections, my_row, my_col);

#ifdef SAVE_MATRIX_AS_TEXT
	stringstream block_matrix_output_file_name;
	block_matrix_output_file_name << "matrix_row";
// Since I am saving it to be compatible with MFDn output I need to interchange
// row <----> columns in block coordinates			
	if ((my_row+1) < 10)	// row
	{
		block_matrix_output_file_name << "0";
	}
	block_matrix_output_file_name << (my_row + 1) << "_col"; // row is saved as column
	if ((my_col+1) < 10)
	{
		block_matrix_output_file_name << "0";
	}
	block_matrix_output_file_name << (my_col + 1);
#endif	
	InitSqrtLogFactTables();
	proton_neutron::ModelSpace bra_ncsmModelSpace(bra_model_space_filename);
	proton_neutron::ModelSpace ket_ncsmModelSpace(ket_model_space_filename);

	CncsmSU3Basis bra_basis(bra_ncsmModelSpace);
	CncsmSU3Basis ket_basis(ket_ncsmModelSpace);
	SU3xSU2::IrrepsContainer<IRREPBASIS> bra_irreps_basis(bra_basis.GenerateFinal_SU3xSU2Irreps(), bra_ncsmModelSpace.JJ());
	SU3xSU2::IrrepsContainer<IRREPBASIS> ket_irreps_basis(ket_basis.GenerateFinal_SU3xSU2Irreps(), ket_ncsmModelSpace.JJ());

	bool allow_generate_missing_rme_files = (nprocs == 1);

	std::ofstream output_file("/dev/null");
			
//	if we are loading one-body operator only, then root process read this file
//	and broadcast its structure to all other processes. When done, all processes
//	construct data structures, including rme look up tables. Since the table of rme 
//	look up tables will be empty, their content is read from file by each process.
//	If file does not exist, exception is thrown.
	CInteractionPPNN interactionPPNN(bra_basis, true, output_file);
	CInteractionPN interactionPN(bra_basis, allow_generate_missing_rme_files, true, output_file);

	int Z = bra_ncsmModelSpace.number_of_protons();
	int N = bra_ncsmModelSpace.number_of_neutrons();
	int A = Z + N;
	try
	{
		COperatorLoader operatorLoader;

//		Load mass r^2 intrinsic operator
//		operatorLoader.Add_r2_mass_intrinsic(A);
//		operatorLoader.AddOneBodyOperator("operatorsSU3/M1/M1_1b_nmax10", 1.0);
		operatorLoader.AddOneBodyOperator("SU3_Interactions_Operators/Q2/Q2_1b_nmax10", 1.0);

		operatorLoader.Load(my_rank, interactionPPNN, interactionPN);
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
       MPI_Abort(MPI_COMM_WORLD, -1);
    }

//	The order of coefficients is given as follows:
// 	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//	TransformTensorStrengthsIntoPP_NN_structure turns that into:
// 	index = type*k0max*rho0max + k0*rho0max + rho0
	interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

	time_t start,end;
	time(&start);
#ifdef SAVE_MATRIX_AS_TEXT
	ofstream fresults(block_matrix_output_file_name.str().c_str());
	fresults.precision(10);
#endif
	vector<float> vals;	
	vector<size_t> column_indices;
	vector<size_t> row_ptrs;
	row_ptrs.push_back(0);

	cout << "Process " << my_rank << " starts calculation of me" << endl;
	CalculateME(interactionPPNN, interactionPN, bra_basis, nbra_sections, my_row, 0, bra_irreps_basis, ket_basis, nket_sections, my_col, 0, ket_irreps_basis
#ifdef SAVE_MATRIX_AS_TEXT	
	,fresults
#endif	
	, vals, column_indices, row_ptrs);

/**	
	Construct data structures allowing me to transform a wave function with the
	basis states order given by the ndiag-cyclic distribution into ndiag'-cyclic 
	distribution.
*/	
	std::vector<unsigned long> bra_dims(nbra_sections, 0), ket_dims(nket_sections, 0);
	CalculateDimAllSections(bra_basis, bra_dims, nbra_sections, bra_irreps_basis);
	CalculateDimAllSections(ket_basis, ket_dims, nket_sections, ket_irreps_basis);

	vector<Section_Wfn_Structure> bra_sections, ket_sections;
	for (int isection = 0; isection < nbra_sections; ++isection)
	{
		bra_sections.push_back(Section_Wfn_Structure(bra_basis, bra_dims, isection, bra_irreps_basis));	// calculate starting positions of amplitudes
	}
	for (int isection = 0; isection < nket_sections; ++isection)
	{
		ket_sections.push_back(Section_Wfn_Structure(ket_basis, ket_dims, isection, ket_irreps_basis));	// calculate starting positions of amplitudes
	}

	for (size_t i = 0; i < bra_ket_wfns.size(); ++i)
	{

/** use bra_sections and ket_sections data structures to read wave functions properly */ 
		vector<float> bra_wfn, ket_wfn;
		ReadWfn_ToSection(bra_basis, ndiag_bra, bra_ket_wfns[i].first, bra_sections.size(), my_row, bra_sections[my_row], bra_wfn, bra_irreps_basis);
		ReadWfn_ToSection(ket_basis, ndiag_ket, bra_ket_wfns[i].second, ket_sections.size(), my_col, ket_sections[my_col], ket_wfn, ket_irreps_basis);
		float result_local = bra_x_Observable_x_ket(bra_wfn, vals, column_indices, row_ptrs, ket_wfn);
		float total;
		MPI_Reduce(&result_local, &total, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

		if (my_rank == 0)
		{
			cout << "Result: " << total << endl;
		}
	}
	time(&end);
	double dif = difftime (end,start);
	cout << "Resulting time: " << dif << " seconds." << endl;
	MPI_Finalize();
}

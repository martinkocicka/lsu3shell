#include <mpi.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/BroadcastDataContainer.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>

#include <stdexcept>
#include <sstream>
#include <cmath>
#include <vector>
#include <stack>

//#define SAVE_MATRIX_AS_TEXT

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::pair;
using std::make_pair;
using std::stringstream;
using std::cin;


/* 
	\f$b_{0}=\sqrt{\frac{197.327}{938.92\hbar\Omega}}\f$
*/	
inline double HoLength_fm(double hw)
{
	return sqrt((197.327*197.327)/(938.92*hw)); // hw must be given in MeV; result units: fm
}
/**
    This function calculates
	\f$  C_{JJ\,20}^{JJ}b_{0}^{2}\left[\frac{1}{b_{0}^{2}}\frac{\lange J||Q_{2}||J\rangle}{\sqrt{2J + 1}}\right]\f$
*/
double CalculateE2(double total, SU2::LABEL JJ, double b0)
{
	return clebschGordan(JJ, JJ, 4, 0, JJ, JJ)*b0*b0*total;
}
/**
    This function calculates
	\f$ \sqrt{\frac{4\pi}{3}}C_{JJ\,10}^{JJ}\left[\frac{\langle J||M_{1}||J\rangle}{\sqrt{2J + 1}}\right]\f$
	in units of \f$\left[\mu_{N}\right]\f$.
*/
double CalculateM1(double total, SU2::LABEL JJ)
{
	return sqrt((4*M_PI)/3.0)*clebschGordan(JJ, JJ, 2, 0, JJ, JJ)*total;
}

//#define SAVE_MATRIX_AS_TEXT

void MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col)
{
/*
	if (ndiag % 2 == 0)
	{
		if (my_rank == 0)
		{
			cout << "number of diagonal processes = " << ndiag <<
" must be an odd number." << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}
*/
	int executing_process_id(0);
	for (size_t i = 0; i < ndiag; ++i)
	{
		row = 0;
		for (col = i; col < ndiag; ++col, ++row, ++executing_process_id)
		{
			if (my_rank == executing_process_id)
			{
				return;
			}
		}
	}
}

unsigned long CalculateME(	
					const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					const unsigned long firstStateId_I, const unsigned int idiag, 
					const unsigned long firstStateId_J, const unsigned int jdiag,
					std::vector<float>& vals, std::vector<size_t>& column_indices, std::vector<size_t>& row_ptr)
{
#ifdef SAVE_MATRIX_AS_TEXT
	stringstream block_matrix_output_file_name;
	block_matrix_output_file_name << "matrix_row";
// Since I am saving it to be compatible with MFDn output I need to interchange
// row <----> columns in block coordinates			
	if ((my_row+1) < 10)	// row
	{
		block_matrix_output_file_name << "0";
	}
	block_matrix_output_file_name << (my_row + 1) << "_col"; // row is saved as column
	if ((my_col+1) < 10)
	{
		block_matrix_output_file_name << "0";
	}
	block_matrix_output_file_name << (my_col + 1);

	ofstream fresults(block_matrix_output_file_name.str().c_str());
	fresults.precision(10);
#endif

	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_J_distr_p;
	unsigned char num_vacuums_J_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

	InitializeIdenticalOperatorRME(identityOperatorRMEPP);
	InitializeIdenticalOperatorRME(identityOperatorRMENN);

	SingleDistribution distr_ip, distr_in, distr_jp, distr_jn;
	UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
	SU3xSU2_VEC vW_ip, vW_in, vW_jp, vW_jn;

	unsigned char deltaP, deltaN;

	unsigned long number_nonzero_me(0);
	
	const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

	uint32_t blockFirstRow(firstStateId_I);
	
	int32_t icurrentDistr_p, icurrentDistr_n; 
	int32_t icurrentGamma_p, icurrentGamma_n;

	for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		if (bra.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
		uint32_t ip = bra.getProtonIrrepId(ipin_block);
		uint32_t in = bra.getNeutronIrrepId(ipin_block);

		SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

		uint16_t aip_max = bra.getMult_p(ip);
		uint16_t ain_max = bra.getMult_n(in);

		unsigned long blockFirstColumn((idiag == jdiag) ? blockFirstRow : firstStateId_J);

		vector<vector<float> > vals_local(bra.NumberOfStatesInBlock(ipin_block));
		vector<vector<size_t> > col_ind_local(bra.NumberOfStatesInBlock(ipin_block));

		uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max()); 
		uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max()); 
		
		uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max()); 
		uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max()); 

		uint32_t last_jp(std::numeric_limits<uint32_t>::max());
		uint32_t last_jn(std::numeric_limits<uint32_t>::max());
//	loop over (jp, jn) pairs
		for (unsigned int jpjn_block = (idiag == jdiag) ? ipin_block : 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			if (ket.NumberOfStatesInBlock(jpjn_block) == 0)
			{
				continue;
			}
			uint32_t jp = ket.getProtonIrrepId(jpjn_block);
			uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

			SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
			SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			uint16_t ajp_max = ket.getMult_p(jp);
			uint16_t ajn_max = ket.getMult_n(jn);

			if (jp != last_jp)
			{
				icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
				icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

				if (ilastDistr_p != icurrentDistr_p)
				{
					distr_ip.resize(0); bra.getDistr_p(ip, distr_ip);
					gamma_ip.resize(0); bra.getGamma_p(ip, gamma_ip);
					vW_ip.resize(0); bra.getOmega_p(ip, vW_ip);
				
					distr_jp.resize(0); ket.getDistr_p(jp, distr_jp);
					hoShells_p.resize(0);
					deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p, num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
				}

				if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p)
				{
					if (deltaP <= 4)
					{
						if (!selected_tensorsPP.empty())
						{
							std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsPP.resize(0);

						if (!selected_tensors_p_pn.empty())
						{
							std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_p_pn.resize(0);

						gamma_jp.resize(0); ket.getGamma_p(jp, gamma_jp);
						TransformGammaKet_SelectByGammas(hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensorsPP, selected_tensors_p_pn);
					}
				}
	
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					vW_jp.resize(0); ket.getOmega_p(jp, vW_jp);
					TransformOmegaKet_CalculateRME(distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP, selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
				}
				ilastDistr_p = icurrentDistr_p;
				ilastGamma_p = icurrentGamma_p;
			}

			if (jn != last_jn)
			{
				icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
				icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

				if (ilastDistr_n != icurrentDistr_n)
				{
					distr_in.resize(0); bra.getDistr_n(in, distr_in);
					gamma_in.resize(0); bra.getGamma_n(in, gamma_in);
					vW_in.resize(0); bra.getOmega_n(in, vW_in); 

					distr_jn.resize(0); ket.getDistr_n(jn, distr_jn);
					hoShells_n.resize(0);
					deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n, num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
				}

				if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n)
				{
					if (deltaN <= 4)
					{
						if (!selected_tensorsNN.empty())
						{
							std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsNN.resize(0);

						if (!selected_tensors_n_pn.empty())
						{
							std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_n_pn.resize(0);

						gamma_jn.resize(0); ket.getGamma_n(jn, gamma_jn);
						TransformGammaKet_SelectByGammas(hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn, selected_tensorsNN, selected_tensors_n_pn);
					}
				}

				if (deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					vW_jn.resize(0); ket.getOmega_n(jn, vW_jn);
					TransformOmegaKet_CalculateRME(distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN, selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
				}	

				ilastDistr_n = icurrentDistr_n;
				ilastGamma_n = icurrentGamma_n;
			}

			//	loop over wpn that result from coupling ip x in	
			uint32_t ibegin = bra.blockBegin(ipin_block);
			uint32_t iend = bra.blockEnd(ipin_block);
			uint32_t currentRow = blockFirstRow;
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
				size_t afmax = aip_max*ain_max*omega_pn_I.rho;
				IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));
				
				bool isDiagonalBlock = (idiag == jdiag && ipin_block == jpjn_block); 
				uint32_t currentColumn = (isDiagonalBlock) ? currentRow : blockFirstColumn;
				uint32_t jbegin = (isDiagonalBlock) ? iwpn : ket.blockBegin(jpjn_block);
				uint32_t jend = ket.blockEnd(jpjn_block);
				for (int jwpn = jbegin; jwpn < jend; ++jwpn)
				{
					SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
					size_t aimax = ajp_max*ajn_max*omega_pn_J.rho;
					IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));
	
					if (deltaP + deltaN <= 4)
					{
						Reset_rmeCoeffs(rmeCoeffsPNPN);
						if (in == jn && !rmeCoeffsPP.empty())
						{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
							CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
						}

						if (ip == jp  && !rmeCoeffsNN.empty())
						{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
							CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
						}

						if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
						{
							CalculatePNInteractionMeData(interactionPN, omega_pn_I, omega_pn_J, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
						}

						if (!rmeCoeffsPNPN.empty())
						{
							if (isDiagonalBlock && iwpn == jwpn)
							{
								assert(idiag == jdiag);
								CalculateME_Diagonal_UpperTriang_nonScalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
							else
							{
								CalculateME_nonDiagonal_nonScalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
						}
					}
					currentColumn += aimax*ket.omega_pn_dim(jwpn);
				}
				currentRow += afmax*bra.omega_pn_dim(iwpn);
			}
			blockFirstColumn += ket.NumberOfStatesInBlock(jpjn_block);
			last_jp = jp;
			last_jn = jn;
		}
#ifdef SAVE_MATRIX_AS_TEXT		
		for (size_t i = 0; i < vals_local.size(); ++i)
		{
			size_t irow = blockFirstRow + i;
			for (size_t j = 0; j < vals_local[i].size(); ++j)
			{
				fresults << (irow + 1) << " " << (col_ind_local[i][j] + 1) << " " << vals_local[i][j] << "\n";
			}
		}
#endif		
		for (size_t irow = 0; irow < bra.NumberOfStatesInBlock(ipin_block); ++irow)
		{
			vals.insert(vals.end(), vals_local[irow].begin(), vals_local[irow].end());
			column_indices.insert(column_indices.end(), col_ind_local[irow].begin(), col_ind_local[irow].end()); 
			row_ptr.push_back(row_ptr.back() + vals_local[irow].size());
		}
		blockFirstRow += bra.NumberOfStatesInBlock(ipin_block);
	}
	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;

	return number_nonzero_me;
}


bool ReadWfnSection(const string& wfn_file_name, lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, const int ndiag, vector<float>& wfn)
{
//	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
	assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return false;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
	{
		cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
		cout << "The actual size of the file: " << size << " bytes!";
		return false;
	}

	wfn.reserve(nelems); 
	float* wfn_full;
	wfn_full = new float[nelems];
	wfn_file.seekg (0, std::ios::beg);

//	Load wave function generated for ndiag_wfn into a file that contains order
//	of basis states for ndiag=1	
	uint32_t ipos;
	uint16_t number_of_states_in_block;
	uint32_t number_ipin_blocks = basis.NumberOfBlocks();
	for (uint16_t i = 0; i < ndiag_wfn; ++i)
	{
		
//		Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
//		
//	section --> {iblock0, iblock1, ...... }
//		----------------------------------
//		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
//		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
//		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
//		.
//		.
//		.
		for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
		{
//	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			
			wfn_file.read((char*)&wfn_full[ipos], number_of_states_in_block*sizeof(float));
		}
	}

//	This loop iterates over blocks that belong to isection of basis split into ndiag segments
//	I am using the fact that for isection of the basis split into ndiag segment has the
//	following (ip in) blocks: {isection, isection + ndiag, isection + 2ndiag ....}
	for (uint32_t isection = 0; isection < ndiag; ++isection)
	{
		for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks; ipin_block += ndiag)
		{
//	obtain position of the block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			wfn.insert(wfn.end(), &wfn_full[ipos], &wfn_full[ipos] + number_of_states_in_block);
		}
	}
//	trim excess memory
	vector<float>(wfn).swap(wfn);
	delete []wfn_full;
	return true;
}

class CObservablesRunParameters
{
	public:
	enum OperatorType {kE2, kM1, kRMS};

	CObservablesRunParameters(): ncsmModelSpace_(), wfns_() {}

	void Load(const std::string& file_name);
	
	inline OperatorType GetOperatorType() const {return operator_type_;}
	inline int GetNdiagWfn() const {return ndiag_input_wfn_;}
	inline int GetNdiagBasis() const {return ndiag_basis_;}
	inline float hw() const {return hw_;} 
	inline const proton_neutron::ModelSpace& GetModelSpace() {return ncsmModelSpace_;}
	inline std::string GetOperatorTypeName() const 
	{
		switch (operator_type_)
		{
			case kE2: return std::string("E2");
			case kM1: return std::string("M1");
			case kRMS: return std::string("RMS");
		}
	}
	inline const std::vector<std::string>& GetWnfs() const {return wfns_;}
	private:
	OperatorType operator_type_;
	int ndiag_input_wfn_;
	int ndiag_basis_;
	proton_neutron::ModelSpace ncsmModelSpace_;
	float hw_;
	std::vector<string> wfns_;

	friend class boost::serialization::access;
	template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
   	{
		ar & operator_type_;
		ar & ndiag_input_wfn_;
		ar & ndiag_basis_;
		ar & ncsmModelSpace_;
		ar & hw_;
		ar & wfns_;
   	}
};

void CObservablesRunParameters::Load(const std::string& file_name)
{
	ifstream input_file(file_name.c_str());
	if (!input_file)
	{
		std::stringstream error_message;
		error_message << "Could not open '" << file_name << "' input file! File:" << __FILE__ << "\tLine:" << __LINE__ << ".\n";
		throw std::logic_error(error_message.str());
	}
//	Read an input file:
	std::string operator_type_name;
	input_file >> operator_type_name;

	if (operator_type_name == "E2")
	{
		operator_type_ = kE2;
	}
	else if (operator_type_name == "M1")
	{
		operator_type_ = kM1;
	}
	else if (operator_type_name == "RMS")
	{
		operator_type_ = kRMS;
	}

	input_file >> ndiag_input_wfn_ >> ndiag_basis_;

	int nprocs;

	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	if (nprocs != ndiag_basis_*(ndiag_basis_ + 1)/2)
	{
		std::stringstream error_message;
		error_message << "Total number of processes must be equal to (#sections)*(#sections + 1)/2 !" << endl;
		error_message << "#processes required = " << ndiag_basis_*(ndiag_basis_ + 1)/2 << endl;
		error_message << "#processes = " << nprocs << endl;
		throw std::logic_error(error_message.str());
	}

	std::string model_space_filename;
	input_file >> model_space_filename;
	ncsmModelSpace_.Load(model_space_filename);

	int num_wfns;
	input_file >> num_wfns;

	wfns_.resize(num_wfns);
	for (int i = 0; i < num_wfns; ++i)
	{
		input_file >> wfns_[i];
	}

	if (operator_type_ == kE2 || operator_type_ == kRMS)
	{ 
		input_file >> hw_;	// calculating E2 & RMS require the value of HO strength
	}
}

float bra_x_Observable_x_ket_symmetric(	const vector<float>& bra_wfn, 
										const vector<float>& vals, const vector<size_t>& column_indices, const vector<size_t>& row_ptrs, 
										const vector<float>& ket_wfn, 
										int rowOffset, int columnOffset)
{
	vector<float> observable_x_ket(bra_wfn.size(), 0); // size equal to bra vector 

	assert(vals.size() == row_ptrs.back());

	for (size_t irow = 0; irow < row_ptrs.size() - 1; ++irow)
	{
		size_t iglobal_row = irow + rowOffset;
		for (size_t ival = row_ptrs[irow]; ival < row_ptrs[irow + 1]; ++ival)
		{
            size_t iglobal_col = column_indices[ival] + columnOffset;
            float  val  = vals[ival];

        //  observable_x_ket[irow] += vals[ival]*ket_wfn[column_indices[ival]];
            observable_x_ket[iglobal_row] += val * ket_wfn[iglobal_col];
            if (iglobal_row != iglobal_col)
                observable_x_ket[iglobal_col] += val * ket_wfn[iglobal_row];
		}
	}

	float dresult = std::inner_product(bra_wfn.begin(), bra_wfn.end(), observable_x_ket.begin(), 0.0);
	return dresult;
}

int main(int argc,char **argv)
{
	boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

   	MPI_Init(&argc, &argv);

	int my_rank, my_row, my_col;
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);

	cout.precision(10);

	if (argc != 2)
	{
		if (my_rank == 0)
		{
			cout << "Usage: "<< argv[0] <<" <input file name>" << endl;
			cout << "Structure of input file:" << endl;
			cout << "<operator type> one of the following E2 M1 RMS" << endl;
			cout << "<number of sections utilized to produce input wfns>" << endl;
			cout << "<number of diagonal processes [i.e. #section for basis spliting]>" << endl;
			cout << "<model space file name>" << endl;
			cout << "<number of input wave functions>" << endl;
			cout << "<file name 0>" << endl;
			cout << "<file name 1>" << endl;
			cout << " 	. " << endl;
			cout << " 	. " << endl;
			cout << " 	. " << endl;
			cout << "<hw value> in the case of E2 and RMS" << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}
	
	CObservablesRunParameters runParams;
//	root process reads the input file	
	if (my_rank == 0)
	{
		try
		{
			runParams.Load(argv[1]);
		}
		catch (const std::logic_error& e) 
		{ 
		   std::cerr << e.what() << std::endl;
    	   MPI_Abort(MPI_COMM_WORLD, -1);
    	}
	}
//	broadcast data from input file among collaborating processes
	BroadcastDataContainer(runParams, "CObservablesRunParameters");

//	assign a row and a column of a submatrix that the process with my_rank will be evaluating
	MapRankToColRow_MFDn_Compatible(runParams.GetNdiagBasis(), my_rank, my_row, my_col);

	InitSqrtLogFactTables();

	lsu3::CncsmSU3xSU2Basis bra_basis(runParams.GetModelSpace(), my_row, runParams.GetNdiagBasis());	
	lsu3::CncsmSU3xSU2Basis ket_basis(runParams.GetModelSpace(), bra_basis, my_col, runParams.GetNdiagBasis());	

	int Z = runParams.GetModelSpace().number_of_protons();
	int N = runParams.GetModelSpace().number_of_neutrons();
	int Nmax = runParams.GetModelSpace().back().N();
		
	std::ofstream output_file("/dev/null");
	CBaseSU3Irreps baseSU3Irreps(Z, N, Nmax); 
	CInteractionPPNN interactionPPNN(baseSU3Irreps, true, output_file);
	CInteractionPN interactionPN(baseSU3Irreps, true, false, output_file);

	SU2::LABEL JJ = runParams.GetModelSpace().JJ();
	int A = Z + N;
	try
	{
		string sOperatorFile(su3shell_data_directory);
		COperatorLoader operatorLoader;

		if (runParams.GetOperatorType() == CObservablesRunParameters::kE2)
		{ 
			sOperatorFile += "/SU3_Interactions_Operators/Q2/Q2_1b_nmax14";
			operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
		else if (runParams.GetOperatorType() == CObservablesRunParameters::kM1)
		{
			sOperatorFile += "/SU3_Interactions_Operators/M1/M1_1b_nmax14";
			operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
		else if (runParams.GetOperatorType() == CObservablesRunParameters::kRMS) 
		{
			operatorLoader.Add_r2_mass_intrinsic(A);
		}
		else // ==> add an arbitrary operator such as e.g. Hamiltonian ...
		{
			std::stringstream error_message;
			error_message << "Unknown type of operator!" << endl;
			error_message << "Allowed values: any of the following E2 M1 RMS" << endl;
			error_message << "File:" << __FILE__ << "\tLine:" << __LINE__ << ".\n";
			throw std::logic_error(error_message.str());

//            sOperatorFile += "/SU3_Interactions_Operators/LS/LS_1b_nmax8";
//            operatorLoader.AddOneBodyOperator(sOperatorFile, 1.0);
		}
/** J0 and M0 labels of the operator are stored as: MECalculatorData::JJ0_ and  MECalculatorData::MM0_ 
 	and are being read from one-body interaction files ...
 */
		operatorLoader.Load(my_rank, interactionPPNN, interactionPN); // interaction PN is empty ...
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
       MPI_Abort(MPI_COMM_WORLD, -1);
    }

	if (!SU2::mult(JJ, MECalculatorData::JJ0(), JJ))
	{
		if (my_rank == 0)
		{
			cout << "Operator " << runParams.GetOperatorTypeName() << " is J0=" << (int)MECalculatorData::JJ0() << " and hence could not couple wave functions with JJ=" << (int)JJ << endl << endl;
		}
        MPI_Abort(MPI_COMM_WORLD, -1);
	}

//	The order of coefficients is given as follows:
// 	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//	TransformTensorStrengthsIntoPP_NN_structure turns that into:
// 	index = type*k0max*rho0max + k0*rho0max + rho0
	interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

	vector<float> vals;	
	vector<size_t> column_indices;
	vector<size_t> row_ptrs;
	row_ptrs.push_back(0);

/**	
	Construct data structures allowing me to transform a wave function with the
	basis states order given by the runParams.GetNdiagWfn()-cyclic distribution into ndiag'-cyclic 
	distribution.
*/	
	unsigned long firstStateId_bra = bra_basis.getFirstStateId();
	unsigned long firstStateId_ket = ket_basis.getFirstStateId();

/*
    Matrix matrix(false);
    Props& p = matrix.getProps();// global and local properties of submatrix with my_row row and my_col col coordinates

    p.globalNRows.setSize(bra_basis.getModelSpaceDim()); //	total number of rows
    p.localNRows.setSize(bra_basis.dim());	// number of local rows

    p.globalNColumns.setSize(ket_basis.getModelSpaceDim()); //  total number of columns
    p.localNColumns.setSize(ket_basis.dim());	// number of local columns

    p.rowOffset    = bra_basis.getFirstStateId();
    p.columnOffset = ket_basis.getFirstStateId();
*/
    cout << "Process " << my_rank << " starts calculation of me" << endl;

    CalculateME(interactionPPNN, interactionPN, bra_basis, ket_basis, 0, my_row, 0, my_col, vals, column_indices, row_ptrs);

//	if there are no matrix elements ==> mascot throw expection "Local number of
//	nonzeros value must be set."
    if (vals.size() == 0)
    {
        cout << "FYI: my_row: " << my_row << " my_col:" << my_col << "\t... all m.e. are vanishing." << endl;
//	store a single zero in matrix ==> this will make mascot library happy, i.e.
//	it won't throw exception
        vals.push_back(0.0);
        column_indices.push_back(0);
    }

    if (my_rank == 0)
    {
        bra_basis.Reshuffle(runParams.GetModelSpace(), 0, 1);
    }

    double b0;
    unsigned long dim_total = bra_basis.getModelSpaceDim();
    const std::vector<std::string>& wfns = runParams.GetWnfs();

    for (size_t i = 0; i < wfns.size(); ++i)
    {
        vector<float> bra_wfn;
/** root reads the entire wave function and orders basis states properly */			
        if (my_rank == 0)
        {
            ReadWfnSection(wfns[i], bra_basis, runParams.GetNdiagWfn(), runParams.GetNdiagBasis(), bra_wfn);
        }
        else
        {
            bra_wfn.resize(dim_total);
        }
        MPI_Bcast((void*)&bra_wfn[0], bra_wfn.size(), MPI_FLOAT, 0, MPI_COMM_WORLD);
        
        vector<float>  ket_wfn(bra_wfn);

		float result_local = bra_x_Observable_x_ket_symmetric(bra_wfn, vals, column_indices, row_ptrs, ket_wfn, bra_basis.getFirstStateId(), ket_basis.getFirstStateId());
		float total(0);
        MPI_Reduce(&result_local, &total, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

/*
        Vector rightVector(false);
        rightVector.getProps().dataTypeId = DataType::FLOAT;
        rightVector.getProps().globalNRows.setSize(dim_total);
        rightVector.getProps().localNRows .setSize(dim_total);
        rightVector.getProps().rowOffset = 0;
        rightVector.setValues(&ket_wfn[0]);
*/
//      vector<float> Ay_vector(dim_total, 0);
/*
        Vector Ay(false);
        Ay.getProps().dataTypeId = DataType::FLOAT;
        Ay.getProps().globalNRows.setSize(dim_total);
        Ay.getProps().localNRows .setSize(dim_total);
        Ay.getProps().rowOffset = 0;
        Ay.setValues(&Ay_vector[0]);
*/
//		matrix.multiply(rightVector, Ay, MPI_COMM_WORLD);
/*
        Vector leftVector(false);
        leftVector.getProps().dataTypeId = DataType::FLOAT;
        leftVector.getProps().globalNRows.setSize(dim_total);
        leftVector.getProps().localNRows .setSize(dim_total);
        leftVector.getProps().rowOffset = 0;
        leftVector.setValues(&bra_wfn[0]);
*/
//      float total(0);
//      leftVector.dotProduct(Ay, &total, DataType::FLOAT);

        if (my_rank == 0)
        {
            if (runParams.GetOperatorType() == CObservablesRunParameters::kE2)
            { 
                if (i == 0) // first expectation value
                {
                    b0 = HoLength_fm(runParams.hw());
                    cout << "hw = " << runParams.hw() << "\tb0=" << b0 << " [fm]." << endl << endl;

                    cout << "J = ";
                    switch (JJ%2)
                    {
                        case 0: cout << (int)JJ/2; break;
                        case 1: cout << (int)JJ << "/2 ";
                    }
                    cout << endl << endl;

                    cout << "Q(JJ) [e fm^2]" << endl;
                }
                cout << CalculateE2(total, JJ, b0) << endl;
            }
            else if (runParams.GetOperatorType() == CObservablesRunParameters::kM1)
            {
                if (i == 0) // first expectation value
                {
                    cout << "J = ";
                    switch (JJ%2)
                    {
                        case 0: cout << (int)JJ/2; break;
                        case 1: cout << (int)JJ << "/2 ";
                    }
                    cout << endl << endl;

                    cout << "M1(JJ) [mu_N]" << endl;
                }
                cout << CalculateM1(total, JJ) << endl;
            }
            else if (runParams.GetOperatorType() == CObservablesRunParameters::kRMS)
            {
                if (i == 0)
                {
                    b0 = HoLength_fm(runParams.hw());
                    cout << "hw=" << runParams.hw() << "MeV\t\t b0=" << b0 << " [fm]." << endl << endl;

                    cout << "J = ";
                    switch (JJ%2)
                    {
                        case 0: cout << (int)JJ/2; break;
                        case 1: cout << (int)JJ << "/2 ";
                    }
                    cout << endl << endl;

                    cout << "mass rms [fm]" << endl;
                }
                double r2_intr_over_b0sq = total;
                cout << b0*sqrt(r2_intr_over_b0sq/A) << endl;
            }
            else // ==> other operator ==> print result and end
            {
                cout << "Result: " << total << endl;
            }
        }
    }

	MPI_Barrier(MPI_COMM_WORLD);

	if (my_rank == 0)
	{
		cout << "Time: " << boost::chrono::duration<double>(boost::chrono::system_clock::now() - start) << endl;
	}

	MPI_Finalize();
}

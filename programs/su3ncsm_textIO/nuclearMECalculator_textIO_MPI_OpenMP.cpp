#include <mpi.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/OperatorLoader.h>

#include <stdexcept>
#include <cmath>
#include <vector>
#include <stack>
#include <ctime>
#include <limits>

using namespace std;

void MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col)
{
	assert(ndiag * (ndiag + 1) / 2 >= my_rank); 

	int executing_process_id(0);
	for (size_t i = 0; i < ndiag; ++i)
	{
		row = 0;
		for (col = i; col < ndiag; ++col, ++row, ++executing_process_id)
		{
			if (my_rank == executing_process_id)
			{
				return;
			}
		}
	}
}

unsigned long CalculateME(
					const CInteractionPPNN& interactionPPNN, const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, const lsu3::CncsmSU3xSU2Basis& ket, 
					const unsigned int ndiag, const unsigned int idiag, const unsigned int jdiag,
					ofstream& fresults)
{
//	global variables
	const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

	unsigned long number_nonzero_me(0);

	#pragma omp parallel reduction(+:number_nonzero_me)
	{
		std::cerr<<"I am "<<omp_get_thread_num()<<std::endl;

//  local variables
	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_J_distr_p;
	unsigned char num_vacuums_J_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	std::vector<MECalculatorData> rmeCoeffsPNPN;

	SU3xSU2::RME identityOperatorRMEPP, identityOperatorRMENN;

	InitializeIdenticalOperatorRME(identityOperatorRMEPP);
	InitializeIdenticalOperatorRME(identityOperatorRMENN);
	
	SingleDistribution distr_ip, distr_in, distr_jp, distr_jn;
	UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
	SU3xSU2_VEC vW_ip, vW_in, vW_jp, vW_jn;

	unsigned char deltaP, deltaN;


	
	int32_t icurrentDistr_p, icurrentDistr_n; 
	int32_t icurrentGamma_p, icurrentGamma_n;

//	loop over (ip, in) pairs
	#pragma omp for
	for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		if (bra.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
//		uint32_t blockFirstRow(firstStateId_I);
//		uint32_t blockFirstRow = bra.firstStateInBlockPosition(ipin_block); // if ipin_block == 0 ==> return position of a first state in idiag segment
		uint32_t blockFirstRow = bra.getFirstStateId() + bra.BlockPositionInSegment(ipin_block); // if ipin_block == 0 ==> return position of a first state in idiag segment

		uint32_t ip = bra.getProtonIrrepId(ipin_block);
		uint32_t in = bra.getNeutronIrrepId(ipin_block);

		SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

		uint16_t aip_max = bra.getMult_p(ip);
		uint16_t ain_max = bra.getMult_n(in);


		vector<vector<float> > vals_local(bra.NumberOfStatesInBlock(ipin_block));
		vector<vector<size_t> > col_ind_local(bra.NumberOfStatesInBlock(ipin_block));

		uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max()); 
		uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max()); 
		
		uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max()); 
		uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max()); 

		uint32_t last_jp(std::numeric_limits<uint32_t>::max());
		uint32_t last_jn(std::numeric_limits<uint32_t>::max());
//	loop over (jp, jn) pairs
		for (unsigned int jpjn_block = (idiag == jdiag) ? ipin_block : 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			if (ket.NumberOfStatesInBlock(jpjn_block) == 0)
			{
				continue;
			}
//			uint32_t  blockFirstColumn((idiag == jdiag) ? blockFirstRow : firstStateId_J);
//			uint32_t  blockFirstColumn = ket.firstStateInBlockPosition(jpjn_block);
			uint32_t  blockFirstColumn = ket.getFirstStateId() + ket.BlockPositionInSegment(jpjn_block);

			uint32_t jp = ket.getProtonIrrepId(jpjn_block);
			uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

			SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
			SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			uint16_t ajp_max = ket.getMult_p(jp);
			uint16_t ajn_max = ket.getMult_n(jn);

			if (jp != last_jp)
			{
				icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
				icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

				if (ilastDistr_p != icurrentDistr_p)
				{
					distr_ip.resize(0); bra.getDistr_p(ip, distr_ip);
					gamma_ip.resize(0); bra.getGamma_p(ip, gamma_ip);
					vW_ip.resize(0); bra.getOmega_p(ip, vW_ip);
				
					distr_jp.resize(0); ket.getDistr_p(jp, distr_jp);
					hoShells_p.resize(0);
					deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p, num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
				}

				if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p)
				{
					if (deltaP <= 4)
					{
						if (!selected_tensorsPP.empty())
						{
							std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsPP.resize(0);

						if (!selected_tensors_p_pn.empty())
						{
							std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_p_pn.resize(0);

						gamma_jp.resize(0); ket.getGamma_p(jp, gamma_jp);
						TransformGammaKet_SelectByGammas(hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensorsPP, selected_tensors_p_pn);
					}
				}
	
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					vW_jp.resize(0); ket.getOmega_p(jp, vW_jp);
					TransformOmegaKet_CalculateRME(distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP, selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
				}
				ilastDistr_p = icurrentDistr_p;
				ilastGamma_p = icurrentGamma_p;
			}

			if (jn != last_jn)
			{
				icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
				icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

				if (ilastDistr_n != icurrentDistr_n)
				{
					distr_in.resize(0); bra.getDistr_n(in, distr_in);
					gamma_in.resize(0); bra.getGamma_n(in, gamma_in);
					vW_in.resize(0); bra.getOmega_n(in, vW_in); 

					distr_jn.resize(0); ket.getDistr_n(jn, distr_jn);
					hoShells_n.resize(0);
					deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n, num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
				}

				if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n)
				{
					if (deltaN <= 4)
					{
						if (!selected_tensorsNN.empty())
						{
							std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsNN.resize(0);

						if (!selected_tensors_n_pn.empty())
						{
							std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_n_pn.resize(0);

						gamma_jn.resize(0); ket.getGamma_n(jn, gamma_jn);
						TransformGammaKet_SelectByGammas(hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn, selected_tensorsNN, selected_tensors_n_pn);
					}
				}

				if (deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					vW_jn.resize(0); ket.getOmega_n(jn, vW_jn);
					TransformOmegaKet_CalculateRME(distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN, selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
				}	

				ilastDistr_n = icurrentDistr_n;
				ilastGamma_n = icurrentGamma_n;
			}

			//	loop over wpn that result from coupling ip x in	
			uint32_t ibegin = bra.blockBegin(ipin_block);
			uint32_t iend = bra.blockEnd(ipin_block);
			uint32_t currentRow = blockFirstRow;
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
				size_t afmax = aip_max*ain_max*omega_pn_I.rho;
				IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));
				
				bool isDiagonalBlock = (idiag == jdiag && ipin_block == jpjn_block); 
				uint32_t currentColumn = (isDiagonalBlock) ? currentRow : blockFirstColumn;
				uint32_t jbegin = (isDiagonalBlock) ? iwpn : ket.blockBegin(jpjn_block);
				uint32_t jend = ket.blockEnd(jpjn_block);
				for (int jwpn = jbegin; jwpn < jend; ++jwpn)
				{
					SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
					size_t aimax = ajp_max*ajn_max*omega_pn_J.rho;
					IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));
	
					if (deltaP + deltaN <= 4)
					{
						Reset_rmeCoeffs(rmeCoeffsPNPN);
						if (in == jn && !rmeCoeffsPP.empty())
						{
//	create structure with < an (lmn mun)Sn ||| 1 ||| an' (lmn mun) Sn> r.m.e.				
							CreateIdentityOperatorRME(w_in, w_jn, ajn_max, identityOperatorRMENN);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [I_{nn} x T_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Proton_x_Identity_MeData(omega_pn_I, omega_pn_J, rmeCoeffsPP, identityOperatorRMENN, rmeCoeffsPNPN);
						}

						if (ip == jp  && !rmeCoeffsNN.empty())
						{
//	create structure with < ap (lmp mup)Sp ||| 1 ||| ap' (lmp mup) Sp> r.m.e.				
							CreateIdentityOperatorRME(w_ip, w_jp, ajp_max, identityOperatorRMEPP);
//	calculate <	{(lmp mup)Sp x (lmn mun)Sn} wf Sf ||| [T_{nn} x I_{pp}] |||{(lmp' mup')Sp' x (lmn' mun')Sn'} wi Si >_{rhot}		 	
							Calculate_Identity_x_Neutron_MeData(omega_pn_I, omega_pn_J, identityOperatorRMEPP, rmeCoeffsNN, rmeCoeffsPNPN);
						}

						if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
						{
							CalculatePNInteractionMeData(interactionPN, omega_pn_I, omega_pn_J, rme_index_p_pn, rme_index_n_pn, rmeCoeffsPNPN);
						}

						if (!rmeCoeffsPNPN.empty())
						{
							if (isDiagonalBlock && iwpn == jwpn)
							{
//								assert(idiag == jdiag);
								CalculateME_Diagonal_UpperTriang_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
							else
							{
								CalculateME_nonDiagonal_Scalar((currentRow - blockFirstRow), afmax, braSU3xSU2basis, currentRow, aimax, ketSU3xSU2basis, currentColumn, rmeCoeffsPNPN, vals_local, col_ind_local);
							}
						}
					}
					currentColumn += aimax*ket.omega_pn_dim(jwpn);
				}
				currentRow += afmax*bra.omega_pn_dim(iwpn);
			}
			blockFirstColumn += ket.NumberOfStatesInBlock(jpjn_block);
			last_jp = jp;
			last_jn = jn;
		}
#pragma omp critical
		{
		for (size_t i = 0; i < vals_local.size(); ++i)
		{
			size_t irow = blockFirstRow + i;
			for (size_t j = 0; j < vals_local[i].size(); ++j, ++number_nonzero_me)
			{
				fresults << (irow + 1) << " " << (col_ind_local[i][j] + 1) << " " << vals_local[i][j] << "\n";
			}
		}
		cout << blockFirstRow << endl;
		}
		blockFirstRow += bra.NumberOfStatesInBlock(ipin_block);
	}
	fresults << number_nonzero_me;

	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);
	Reset_rmeCoeffs(rmeCoeffsPNPN);
//	delete arrays allocated in identityOperatorRME?? structures
	delete []identityOperatorRMEPP.m_rme;
	delete []identityOperatorRMENN.m_rme;
	}
	return number_nonzero_me;
}


int main(int argc,char **argv)
{
   	MPI_Init(&argc, &argv);

	int my_rank, nprocs, my_row, my_col;
	MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);

	if (argc != 2)
	{
		cout << "Name of the file with the model space definition is required input parameter." << endl;
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	unsigned int ndiag = (-1 + sqrt(1 + 8*nprocs))/2; 
	if (fabs(ndiag - (double)(-1.0 + sqrt(1 + 8*nprocs))/2.0) > 1.0e-7)
	{
		if (my_rank == 0)
		{
			cout << "number of processes = " << nprocs << " is wrong: must be qual to ndiag*(ndiga+1)/2, where ndiag is number of diagonal processes." << endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	MapRankToColRow_MFDn_Compatible(ndiag, my_rank, my_row, my_col);

	stringstream block_matrix_output_file_name;
	block_matrix_output_file_name << "matrix_col";
// Since I am saving it to be compatible with MFDn output I need to interchange
// row <----> columns in block coordinates			
	if ((my_row+1) < 10)	// row
	{
		block_matrix_output_file_name << "0";
	}
	block_matrix_output_file_name << (my_row+1) << "_row"; // row is saved as column
	if ((my_col+1) < 10)
	{
		block_matrix_output_file_name << "0";
	}
	block_matrix_output_file_name << (my_col + 1);

	InitSqrtLogFactTables();
	proton_neutron::ModelSpace ncsmModelSpace(argv[1]);

	int Z    = ncsmModelSpace.number_of_protons();
	int N    = ncsmModelSpace.number_of_neutrons();
	int Nmax = ncsmModelSpace.back().N();

	CBaseSU3Irreps baseSU3Irreps(Z, N, Nmax); 

	lsu3::CncsmSU3xSU2Basis bra(ncsmModelSpace, my_row, ndiag);	
	lsu3::CncsmSU3xSU2Basis ket(ncsmModelSpace, my_col, ndiag);	

	stringstream interaction_log_file_name;
	interaction_log_file_name << "interaction_loading_" << my_rank << ".log";
	ofstream interaction_log_file(interaction_log_file_name.str().c_str());

	bool allow_generate_missing_rme_files = (nprocs == 1);
			
	CInteractionPPNN interactionPPNN(baseSU3Irreps, true, interaction_log_file);
	CInteractionPN interactionPN(baseSU3Irreps, allow_generate_missing_rme_files, true, interaction_log_file);
	int A = Z + N;
	const float hw = 15;
	const float lambda = 50;

/** J0 and M0 of the operator are stored internally as: MECalculatorData::JJ0_ and  MECalculatorData::MM0_ and are being read from one-body interaction files */
	try
	{
		COperatorLoader operatorLoader;

		operatorLoader.AddTrel(A, hw);
		operatorLoader.AddVcoul(hw);
		operatorLoader.AddVnn();
		operatorLoader.AddBdB(A, lambda);
		operatorLoader.Load(my_rank, interactionPPNN, interactionPN);
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
       MPI_Abort(MPI_COMM_WORLD, -1);
    }

//	The order of coefficients is given as follows:
// 	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
//	TransformTensorStrengthsIntoPP_NN_structure turns that into:
// 	index = type*k0max*rho0max + k0*rho0max + rho0
	interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

	time_t start,end;
	time(&start);
/**	In order to calculate id of the first state in the bra and ket basis of the block with (my_row, my_col) coordinates
 * one does this:
 * \code	unsigned long firstStateId_bra = std::accumulate(dims.begin(), dims.begin() + my_row, (unsigned long)0); 
 * \code	unsigned long firstStateId_ket = std::accumulate(dims.begin(), dims.begin() + my_col, (unsigned long)0); 
*/			
 	unsigned long firstStateId_bra = bra.getFirstStateId();
 	unsigned long firstStateId_ket = ket.getFirstStateId();

	ofstream fresults(block_matrix_output_file_name.str().c_str());
	fresults.precision(10);

	cout << "Process " << my_rank << " starts calculation of me" << endl;
	unsigned long nme = CalculateME(interactionPPNN, interactionPN, bra, ket, ndiag, my_row, my_col, fresults);
//	unsigned long nme = CalculateME(interactionPPNN, interactionPN, Basis, ndiag, 0, my_row, 0, my_col, fresults, irreps_basis);
	block_matrix_output_file_name << ".header";
	ofstream fheader(block_matrix_output_file_name.str().c_str());
/** MFDn: format: #cols #rows #nme */
	fheader << bra.dim() << " " << ket.dim() << " " << nme << "\n";

	time(&end);
	double dif = difftime (end,start);
	cout << "Resulting time: " << dif << " seconds." << endl;

	MPI_Finalize();
}

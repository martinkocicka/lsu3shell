#include <SU3ME/BaseSU3Irreps.h>
#include <SU3ME/OperatorDataStructure.h>
#include <UNU3SU3/CUNMaster.h>
#include <algorithm>
#include <iostream>

using namespace std;

int main() 
{	
	size_t Z, N;
	size_t Nmax;
    cout << "Enter the number of protons ... ";
    cin >> Z;
    cout << "Enter the number of neutron ... ";
    cin >> N;
    cout << "Enter the maximal model space size Nmax=";
    cin >> Nmax;

	CBaseSU3Irreps BaseSU3Irreps(Z, N, Nmax);
	BaseSU3Irreps.ShowShellOccupation();
	BaseSU3Irreps.ShowInternalDataStructure();
	cout << "Number of all basic irreps = " << BaseSU3Irreps.GetNBaseIrreps() << endl;
}

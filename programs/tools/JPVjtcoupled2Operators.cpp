#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <cmath>
#include <limits>
#include <cstdlib>

using namespace std;

const string sHrelName = "Hrel_2b_nmax8.dat";
const string sTrelName = "Trel_2b_nmax8.dat";
const string sVcoulName = "Vcoul_2b_nmax3_hw25.dat";

template<class T>
bool Negligible(const T v) {return fabs(v) < 1.0e-5;}

void Create(const string& sFileName)
{
	enum Structure {I1 = 0, I2 = 1, I3 = 2, I4 = 3, J = 4, T = 5};
	enum OperatorStructure {TREL = 0, HREL = 1, VCOUL = 2, VPN = 3, VPP = 4, VNN = 5};

	vector<int> I1I2I3I4JT(6);
	vector<double> Values(6);

	string sVName(sFileName);
	sVName += "_decomposition_ready";

	cout << sVName << endl;

	ofstream ofHrel(sHrelName.c_str(), std::ios_base::trunc), ofTrel(sTrelName.c_str(), std::ios_base::trunc), ofVcoul(sVcoulName.c_str(), std::ios_base::trunc), ofV(sVName.c_str(), std::ios_base::trunc);
	ofHrel.precision(std::numeric_limits<double>::digits10);
	ofTrel.precision(std::numeric_limits<double>::digits10);
	ofVcoul.precision(std::numeric_limits<double>::digits10);
	ofV.precision(std::numeric_limits<double>::digits10);

	FILE *inter_file = NULL;
	inter_file=fopen(sFileName.c_str(),"rb");	


	if (!inter_file) 
	{
		std::cerr << "Unable to open file " << sFileName << std::endl;
		std::exit(EXIT_FAILURE);
	}

	size_t nTerms;
	char si1[4] = {0, 0, 0, 0}, si2[4] = {0, 0, 0, 0}, si3[4] = {0, 0, 0, 0}, si4[4] = {0, 0, 0, 0}, tmp;
	// in order to be compatible with JPV Fortran format, where each integer
	// has 3 digits and could start with a whitespace character, we need to use
	// the old school fscanf approach	

	fscanf(inter_file, "%lu", &nTerms);
	fscanf(inter_file, "%c", &tmp);
	cout << nTerms << endl;
	for (size_t i = 0; i < nTerms; ++i)
	{
		fscanf(inter_file,"%3c", &si1[0]);
		fscanf(inter_file,"%3c", &si2[0]);
		fscanf(inter_file,"%3c", &si3[0]);
		fscanf(inter_file,"%3c", &si4[0]);

		I1I2I3I4JT[I1] = atoi(si1);
		I1I2I3I4JT[I2] = atoi(si2);
		I1I2I3I4JT[I3] = atoi(si3);
		I1I2I3I4JT[I4] = atoi(si4);

		fscanf(inter_file,"%u", &I1I2I3I4JT[J]);
		fscanf(inter_file,"%u", &I1I2I3I4JT[T]);

		fscanf(inter_file,"%lf", &Values[TREL]);
		fscanf(inter_file,"%lf", &Values[HREL]);
		fscanf(inter_file,"%lf", &Values[VCOUL]);
		fscanf(inter_file,"%lf", &Values[VPN]);
		fscanf(inter_file,"%lf", &Values[VPP]);
		fscanf(inter_file,"%lf", &Values[VNN]);
		fscanf(inter_file,"%c", &tmp);

		if (I1I2I3I4JT[T] == 0)	//	==> only |PN J> can have |T=0 J> component  
		{
			assert(Values[VPP] == 0 && Values[VNN] == 0);
		}

		if (!Negligible(Values[TREL])) 
		{
			ofTrel 	<< I1I2I3I4JT[I1] << " " << I1I2I3I4JT[I2] << " " << I1I2I3I4JT[I3] << " " << I1I2I3I4JT[I4] << " " << I1I2I3I4JT[J] << " " << I1I2I3I4JT[T] << "\t";
			if (I1I2I3I4JT[T] == 0)	//	==> only |PN J> can have |T=0 J> component  
			{
				ofTrel  << " " << Values[TREL] << " " << 0 << " " << 0 << endl;
			}
			else
			{
				// Trel is charge independent ...
				ofTrel  << " " << Values[TREL] << " " << Values[TREL] <<  " " << Values[TREL] << endl;
			}
		}
		
		if (!Negligible(Values[HREL])) 
		{
			ofHrel 	<< I1I2I3I4JT[I1] << " " << I1I2I3I4JT[I2] << " " << I1I2I3I4JT[I3] << " " << I1I2I3I4JT[I4] << " " << I1I2I3I4JT[J] << " " << I1I2I3I4JT[T] << "\t";
			if (I1I2I3I4JT[T] == 0)
			{
				ofHrel  << " " << Values[HREL] << " " << 0 << " " << 0 << endl; 
			}
			else
			{
				// Hrel is charge independent ...
				ofHrel  << " " << Values[HREL] << " " << Values[HREL] << " " << Values[HREL] << endl; 
			}
		}
		
		if (I1I2I3I4JT[T] != 0 && !Negligible(Values[VCOUL])) 
		{
			// Vcoul has only proton-proton part non vanishing
			ofVcoul	<< I1I2I3I4JT[I1] << " " << I1I2I3I4JT[I2] << " " << I1I2I3I4JT[I3] << " " << I1I2I3I4JT[I4] << " " << I1I2I3I4JT[J] << " " << I1I2I3I4JT[T] << "\t";
			ofVcoul << " " << 0 << " " << Values[VCOUL] << " " << 0 << endl; 
		}

		if (!Negligible(Values[VPN])) {	
			ofV	<< I1I2I3I4JT[I1] << " " << I1I2I3I4JT[I2] << " " << I1I2I3I4JT[I3] << " " << I1I2I3I4JT[I4] << " " << I1I2I3I4JT[J] << " " << I1I2I3I4JT[T] << "\t";
			if (I1I2I3I4JT[T] == 0)	// since Values[PP] = Values[NN] = 0 for T=0
			{
				ofV << Values[VPN] << " " << 0 << " " << 0 << endl; 
			}
			else
			{
				ofV << Values[VPN] << " " << Values[VPP] << " " << Values[VNN] << endl;
			}
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		cout << endl;
		cout << "This program takes as an input file in JPV 'PNfmt' format and generates Trel, Hrel, Vcoul and V_*_decomposition_ready" << endl;
		cout << "Correct usage: split input_filename" << endl;
		return 1;
	}
	else
	{
		Create(argv[1]);
	}
}

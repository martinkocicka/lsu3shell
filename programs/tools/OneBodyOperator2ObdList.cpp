#include <SU3ME/global_definitions.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <fstream>
#include <iostream>

using std::cout;
using std::endl;
using std::ifstream;
using std::fstream;
using std::ofstream;

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		cout << "Usage: "<< argv[0] <<" <filename: one-body operator> <filename: output file>" << endl;
		cout << "where the output file contains a list of quantum numbers of a given operator" << endl;
		return EXIT_FAILURE;
	}

	ifstream one_body_operator(argv[1]);
	if (!one_body_operator)
	{
		cout << "Could not open '" << argv[1] << "' file that contains a one-body operator" << endl;
		return 0;
	}
	ofstream obd_list(argv[2]);
	if (!one_body_operator)
	{
		cout << "Unable to create '" << argv[2] << "' output file." << endl;
		return 0;
	}

	int lm0, mu0, SS2, k0max, LL0, JJ0, total_JJ0, total_MM0;
	int n1, n2, nd, nt;
	int nTensors, nCoeffs;
	SU3xSU2::LABELS tensor_term_labels;
	int num_tensor_terms = 0;
	
	one_body_operator >> total_JJ0 >> total_MM0;

	while (true)
	{
		one_body_operator >> n1 >> n2;
		if (one_body_operator.eof()) {
			break;
		}
		one_body_operator >> nTensors;

//		iterate over tensors in the CTensorGroup in the input file 
		for (int i = 0; i < nTensors; ++i)
		{
			one_body_operator >> lm0 >> mu0 >> SS2 >> LL0 >> JJ0;
			if (JJ0 != total_JJ0)
			{
				cout << "Error one-body operator must have a constant J0 quantum number" << endl;
				return 0;
			}

			k0max = nCoeffs = SU3::kmax(SU3::LABELS(1, lm0, mu0), LL0/2);
			std::vector<float> dCoeffsP(nCoeffs, 0), dCoeffsN(nCoeffs, 0);
			for (int j = 0; j < nCoeffs; ++j)
			{
				one_body_operator >> dCoeffsP[j];
				one_body_operator >> dCoeffsN[j];
			}
			if (std::count_if(dCoeffsP.begin(), dCoeffsP.end(), Negligible) != dCoeffsP.size())
			{
				num_tensor_terms += k0max;	// there is one obd for each possible value of k0
			}
			if (std::count_if(dCoeffsN.begin(), dCoeffsN.end(), Negligible) != dCoeffsN.size())
			{
				num_tensor_terms += k0max;
			}
		}
	}

	obd_list << num_tensor_terms << endl;

	one_body_operator.clear(); // flag eof is set => clear it
	one_body_operator.seekg(0, std::ios::beg);
	one_body_operator >> total_JJ0 >> total_MM0;

	while (true)
	{
		one_body_operator >> n1 >> n2;
//	obd list must have shell of creation operator first		
		if (one_body_operator.eof()) {
			break;
		}

		if (n1 < 0)
		{
			std::swap(n1, n2);
		}
		nd = abs(n1) - 1;
		nt = abs(n2) - 1;

		one_body_operator >> nTensors;

//		iterate over tensors in the CTensorGroup in the input file 
		for (int i = 0; i < nTensors; ++i, ++num_tensor_terms)
		{
			one_body_operator >> lm0 >> mu0 >> SS2 >> LL0 >> JJ0;

			k0max = nCoeffs = SU3::kmax(SU3::LABELS(lm0, mu0), LL0/2);
			std::vector<float> dCoeffsP(nCoeffs), dCoeffsN(nCoeffs);
			for (int j = 0; j < nCoeffs; ++j)
			{
				one_body_operator >> dCoeffsP[j];
				one_body_operator >> dCoeffsN[j];
			}
			if (std::count_if(dCoeffsP.begin(), dCoeffsP.end(), Negligible) != dCoeffsP.size())
			{
				for (int k0 = 0; k0 < k0max; ++k0)
				{
					obd_list << nd << " " << nt << " " << lm0 << " " << mu0 << " " << SS2 << " " << k0 << " " << LL0 << " " << JJ0 << " " << "p" <<  endl;
				}
			}
			if (std::count_if(dCoeffsN.begin(), dCoeffsN.end(), Negligible) != dCoeffsN.size())
			{
				for (int k0 = 0; k0 < k0max; ++k0)
				{
					obd_list << nd << " " << nt << " " << lm0 << " " << mu0 << " " << SS2 << " " << k0 << " " << LL0 << " " << JJ0 << " " << "n" <<  endl;
				}
			}
		}
	}
}

#include <LSU3/std.h>
#include <LSU3/ncsmSU3xSU2Basis.h>

#include <set>
#include <iostream>

using namespace std;

// Calculate total harmonic oscillator energy of nuclei at Nhw = n0hw space.
float getN0(int nprotons, int nneutrons, int n0hw)
{
	// N0 ... harmonic oscillator energy of a valence space configurations
	float N0 = 0;
	std::vector<uint8_t> proton_valence_distr;
	std::vector<uint8_t> neutron_valence_distr;

	for (int n = 0; true; ++n)
	{
		int8_t nferms = (n+1)*(n+2);
		if (nprotons > nferms)
		{
			proton_valence_distr.push_back(nferms);
			nprotons -= nferms;
		}
		else
		{
			proton_valence_distr.push_back(nprotons);
			break;
		}
	}

	for (int n = 0; true; ++n)
	{
		int8_t nferms = (n+1)*(n+2);
		if (nneutrons > nferms)
		{
			neutron_valence_distr.push_back(nferms);
			nneutrons -= nferms;
		}
		else
		{
			neutron_valence_distr.push_back(nneutrons);
			break;
		}
	}

	for (int n = 0; n < proton_valence_distr.size(); ++n)
	{
		N0 += proton_valence_distr[n]*(n + 1.5);
	}
	for (int n = 0; n < neutron_valence_distr.size(); ++n)
	{
		N0 += neutron_valence_distr[n]*(n + 1.5);
	}
//	now we have to add additional energy [n0hw] energy and subtract center-of-mass HO energy
	return (N0 + n0hw) - 1.5;
}

void GenerateLispSpSnSlmmu(	const int nprotons, 
							const int nneutrons, 
							const int n0hw, 
							std::vector<cpp0x::tuple<uint8_t, uint8_t, uint8_t> >& allowedSpSnS, 
							std::vector<cpp0x::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> >& allowedSpSnSlmmu)
{ 
	std::set<cpp0x::tuple<uint8_t, uint8_t, uint8_t> > unique_SpSnS;
	std::set<cpp0x::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> > unique_SpSnSlmmu;
	proton_neutron::ModelSpace ncsmModelSpace(nprotons, nneutrons, n0hw);
	lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);
	const uint32_t number_blocks = basis.NumberOfBlocks();
	for(int iblock = 0; iblock < number_blocks; ++iblock)
	{
		uint32_t ip = basis.getProtonIrrepId(iblock);
		uint32_t in = basis.getNeutronIrrepId(iblock);

		int Sp = (basis.getProtonSU3xSU2(ip)).S2;
		int Sn = (basis.getNeutronSU3xSU2(in)).S2;
		
		uint32_t ibegin = basis.blockBegin(iblock);
		uint32_t iend = basis.blockEnd(iblock);
		for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
		{
			SU3xSU2::LABELS omega_pn(basis.getOmega_pn(ip, in, iwpn));
			unique_SpSnS.insert(cpp0x::make_tuple(Sp, Sn, omega_pn.S2));
			unique_SpSnSlmmu.insert(cpp0x::make_tuple(Sp, Sn, omega_pn.S2, omega_pn.lm, omega_pn.mu));
		}
	}
	
	allowedSpSnS.resize(unique_SpSnS.size());
	allowedSpSnSlmmu.resize(unique_SpSnSlmmu.size());

	std::copy(unique_SpSnS.begin(), unique_SpSnS.end(), allowedSpSnS.begin());
	std::copy(unique_SpSnSlmmu.begin(), unique_SpSnSlmmu.end(), allowedSpSnSlmmu.begin());
}

void Get20Coupling(int r, SU3_VEC& wn)
{
	int n1, n2, n3;
	for (n1 = 2*r; n1 >= 0; n1-=2)
	{
		for (n2 = 0; n2 <= n1; n2 += 2)
		{
			for (n3 = 0; n3 <= n2; n3 += 2)
			{
				if (n1+n2+n3 == 2*r) {
					wn.push_back(SU3::LABELS(1, (n1 - n2), (n2 - n3)));
				}
			}
		}
	}
}

inline float C2SU3(int lm, int mu)
{
	return (2.0/3.0)*(lm*lm + mu*mu + lm*mu + 3*lm + 3*mu);
}

inline float C2Sp3R(int lm, int mu, float N)
{
	return C2SU3(lm, mu)+(1.0/3.0)*N*N - 4.0*N;
}


void ShowBasis(int SSp, int SSn, int SS, int lm, int mu, float n0, int n0hw, int nmaxhw)
{
	for (int nhw = n0hw; nhw <= nmaxhw; nhw += 2)
	{
		cout << nhw << "hw" << endl;
		int r = (nhw - n0hw)/2;
		SU3_VEC wn;
		Get20Coupling(r, wn);
		for (int i = 0; i < wn.size(); ++i)
		{
			SU3_VEC w;
			SU3::Couple( SU3::LABELS(1, lm, mu), wn[i], w);
			for (int j = 0; j < w.size(); ++j)
			{
				float a1 = C2Sp3R(w[j].lm, w[j].mu, (n0 + 2*r));
				float a2 = C2Sp3R(lm, mu, n0);
				float c2 = (1.0/(2.0*sqrt(6.0)))*(a1 - a2);

				cout << n0 << "(" << (int)lm << " " << (int)mu << ") ";
				cout << "(" << (int)wn[i].lm << " " << (int)wn[i].mu << ") ";
				cout << "(" << (int)w[j].lm << " " << (int)w[j].mu << ") " << "\t" << c2 << endl;
			}
		}
	}
}

int main()
{
	int nprotons, nneutrons, n0hw, nmaxhw;
	cout << "Enter number of protons:";
	cin >> nprotons;
	cout << "Enter number of neutrons:";
	cin >> nneutrons;
	cout << "Enter N0hw:";
	cin >> n0hw;

	float n0 = getN0(nprotons, nneutrons, n0hw);

	int32_t SSp, SSn, SS, lm, mu;
	std::vector<cpp0x::tuple<uint8_t, uint8_t, uint8_t> > allowedSpSnS;
	std::vector<cpp0x::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t> > allowedSpSnSlmmu;
	
	GenerateLispSpSnSlmmu(nprotons, nneutrons, n0hw, allowedSpSnS, allowedSpSnSlmmu);

	cout << "Allowed spin components at " << n0hw << "hw space are following:" << endl;
	for (int i = 0; i < allowedSpSnS.size(); ++i)
	{
		SSp = cpp0x::get<0>(allowedSpSnS[i]);
		SSn = cpp0x::get<1>(allowedSpSnS[i]);
		SS  = cpp0x::get<2>(allowedSpSnS[i]);
		cout << i << ":\t" << "\tSp:";
		
		if (SSp%2)
		{
			cout << SSp << "/2";
		}
		else
		{
			cout << SSp/2 << " ";
		}

		if (SSn%2)
		{
			cout << " Sn:" << SSn << "/2 ";
		}
		else
		{
			cout << " Sn:" << SSn/2;
		}

		if (SS%2)
		{
			cout << " S:" << SS << "/2" << endl;
		}
		else
		{
			cout << " S:" << SS/2 << endl;
		}
	}

	uint32_t ispin;
	cin >> ispin;

	if (ispin > allowedSpSnS.size())
	{
		cerr << ispin << " is out of allowed range." << endl;
		return EXIT_FAILURE;
	}

	SSp = cpp0x::get<0>(allowedSpSnS[ispin]);
	SSn = cpp0x::get<1>(allowedSpSnS[ispin]);
	SS  = cpp0x::get<2>(allowedSpSnS[ispin]);

	int isu3 = 0;
	for (int i = 0; i < allowedSpSnSlmmu.size(); ++i)
	{
		if (SSp == cpp0x::get<0>(allowedSpSnSlmmu[i]) && SSn == cpp0x::get<1>(allowedSpSnSlmmu[i]) && SS  == cpp0x::get<2>(allowedSpSnSlmmu[i]))
		{
			lm  = cpp0x::get<3>(allowedSpSnSlmmu[i]);
			mu  = cpp0x::get<4>(allowedSpSnSlmmu[i]);
			cout << isu3 << ":\t" << "(" << lm << " " << mu << ")" << endl;
			isu3++;
		}
	}
	uint32_t ichoice;

	cout << "Choose bandhead:";
	cin >> ichoice;
	if (ichoice > isu3)
	{
		cerr << ichoice  << " is out of allowed range." << endl;
		return EXIT_FAILURE;
	}

	isu3 = 0;
	for (int i = 0; i < allowedSpSnSlmmu.size(); ++i)
	{
		if (SSp == cpp0x::get<0>(allowedSpSnSlmmu[i]) && SSn == cpp0x::get<1>(allowedSpSnSlmmu[i]) && SS  == cpp0x::get<2>(allowedSpSnSlmmu[i]))
		{
			if (isu3 == ichoice)
			{
				lm  = cpp0x::get<3>(allowedSpSnSlmmu[i]);
				mu  = cpp0x::get<4>(allowedSpSnSlmmu[i]);
				break;
			}
			isu3++;
		}
	}

	cout << "Nmax:";
	cin >> nmaxhw;
	
	ShowBasis(SSp, SSn, SS, lm, mu, n0, n0hw, nmaxhw);
}

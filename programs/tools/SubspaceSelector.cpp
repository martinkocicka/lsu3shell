#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
#include <sstream>
#include <LSU3/std.h>


using namespace cpp0x;
using std::vector;
using std::pair;
using std::map;
using std::string;
using std::fstream;
using std::make_pair;

enum {kSortByProb = 0, kSortByRatio = 1};
enum {kSSp = 0, kSSn = 1, kSS = 2, kLm = 3, kMu = 4, kProb = 5, kRatio = 6, kDim = 7};
typedef std::tr1::tuple<int, int, int, int, int, float, float, size_t> INFO;

struct Less_SU3
{
	inline bool operator() (const SU3::LABELS& l, const SU3::LABELS& r) const {return l.C2() < r.C2() || (l.C2() == r.C2() && l < r);}
};

struct SortByProbability
{
	inline bool operator() (const INFO& l, const INFO& r) const 
	{
		return std::tr1::get<kProb>(l) > std::tr1::get<kProb>(r);
	}
};

struct SortByRatio
{
	inline bool operator() (const INFO& l, const INFO& r) const 
	{
		return std::tr1::get<kRatio>(l) > std::tr1::get<kRatio>(r);
	}
};

void ConstructCMInvariantSpSnSlmmuSubspaces(const CncsmSU3Basis& basis, SU3xSU2::IrrepsContainer<IRREPBASIS>& wpn_irreps_container,	std::map<CTuple<int, 5>, size_t>& dimensions)
{
	unsigned int ap, an;
	unsigned char status;
	unsigned long idim = 0;
	unsigned int irrep_dim = 0;
	unsigned int nomega_pn = 0;
	SingleDistribution distr_p, distr_n;
	UN::SU3xSU2_VEC gamma_p, gamma_n;
	SU3xSU2_VEC omega_p, omega_n;
	SU3xSU2::LABELS omega_pn;
	PNConfIterator iter = basis.firstPNConf(0, 1);

	for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
	{
		assert(iter.hasPNOmega());

		status = iter.status();
		switch (status)
		{
			case PNConfIterator::kNewDistr_p: distr_p.resize(0); iter.getDistr_p(distr_p);
			case PNConfIterator::kNewDistr_n: 
				distr_n.resize(0); 
				iter.getDistr_n(distr_n); 
			case PNConfIterator::kNewGamma_p: gamma_p.resize(0); iter.getGamma_p(gamma_p);
			case PNConfIterator::kNewGamma_n: gamma_n.resize(0); iter.getGamma_n(gamma_n);
			case PNConfIterator::kNewOmega_p: omega_p.resize(0); iter.getOmega_p(omega_p); ap = iter.getMult_p();
			case PNConfIterator::kNewOmega_n: omega_n.resize(0); iter.getOmega_n(omega_n); an = iter.getMult_n();
		}
		omega_pn = iter.getCurrentSU3xSU2();
		irrep_dim = wpn_irreps_container.rhomax_x_dim(omega_pn);	// rhomax * dim[(lm mu)S]
		if (irrep_dim == 0) // ==> irrep w_pn does not contain any state with J <= Jcut
		{
			continue;	//	move to the next configuration 
		}
		SU3xSU2::LABELS wp = iter.getProtonSU3xSU2();
		SU3xSU2::LABELS wn = iter.getNeutronSU3xSU2();
		CTuple<int, 5> key;
		key[kSSp] = wp.S2; key[kSSn] = wn.S2; key[kSS] = omega_pn.S2; key[kLm] = omega_pn.lm; key[kMu] = omega_pn.mu;
		dimensions[key] += ap*an*irrep_dim;
	}
}

struct Less
{
	bool operator() (const pair<float, vector<int> >& l,  const pair<float, vector<int> >& r) {return l.first > r.first;}
};


//	This utility helps in deciding what (Nmax+2)hw Sp Sn S (lm+2 mu) subspace include in the truncated model space. 
//	This is based either on the probability of (Nmax) Sp Sn S (lm mu) subspace and/or ratio
//   
//   	prob
//		----
//      dim
//	----------
//  ratio_max 
//
//	prob: probability of (Nmax)hw Sp Sn S (lm mu) subspace
//	dim: dimension of (Nmax+2)hw Sp Sn S (lm+2 mu) subspace
//	ratio_max: the highest prob/dim ratio
//
int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << endl;
	  	cout << "Usage: "; 
		cout << argv[0] << "<filename1: Nmax model space> <ndiag> <eigenstate> <filename2: full Nmax+2 subspace> <0 or 1: sort by prob or ratio>" << endl;
		cout << "filename1: model space in which a given <eigenstates> was evaluated" << endl;
		cout << "filename2: space that contains configurations carrying Nmax+2 ho quanta" << endl;
		cout << "0: resulting table is sorted in decreasing order according to the probability" << endl; 
		cout << "1: position of each Sp Sn S (lm mu) subspace the resulting table is resovled based on (prob/dim)/ratio_max" << endl; 
		cout << "where dim = dim [Sp Sn S (lm+2 mu)], of Nmax+2 model space" << endl; 
		cout << endl;
		return 0;
	}

	int sort_type;
	std::istringstream(argv[5]) >> sort_type;
	if (sort_type != 0 && sort_type != 1)
	{
		cout << "unknown sort type " << sort_type << endl;
		return 0;
	}

	
//	{Sp, Sn, S, lm, mu} quantum numbers of Nmax+2 model space associated with
//	their dimensions
	std::map<CTuple<int, 5>, size_t> nextNhw_dimensions;
	proton_neutron::ModelSpace nextNhwModelSpace(argv[4]);
	CncsmSU3Basis  basis_nextNhw(nextNhwModelSpace);
	SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_nextNhw(basis_nextNhw.GenerateFinal_SU3xSU2Irreps(), nextNhwModelSpace.JJ());
	ConstructCMInvariantSpSnSlmmuSubspaces(basis_nextNhw, wpn_irreps_nextNhw, nextNhw_dimensions);

	unsigned int ndiag;
	std::istringstream(argv[2]) >> ndiag;
	const string wfn_file_name(argv[3]);

	proton_neutron::ModelSpace wfnModelSpace(argv[1]);
	int Nmax = wfnModelSpace.back().N();
	CncsmSU3Basis  basis_full(wfnModelSpace);
	SU3xSU2::IrrepsContainer<IRREPBASIS> wpn_irreps_container(basis_full.GenerateFinal_SU3xSU2Irreps(), wfnModelSpace.JJ());

	if (wfnModelSpace.JJ() != nextNhwModelSpace.JJ())
	{
		cout << "Error: JJ for wfn model space JJ=" << wfnModelSpace.JJ() << " is different from JJ for Nmax+2 subspace JJ=" << nextNhwModelSpace.JJ() << endl;
		return 0;
	}

	cout << "Reading " << wfn_file_name << " that was saved with " << ndiag << " processors." << endl;
	
	std::vector<unsigned long> dims(ndiag, 0);
	CalculateDimAllSections(basis_full, dims, ndiag, wpn_irreps_container);
	cout << "Many-body basis is divided into " << ndiag << " sections." << endl;

	float dcoeff;
//	irreps_projections[i]: contains a map of {Sp, Sn, S, (lm mu)} irreps which
//	are associated with the probability amplitude. Irreps span ncsmModelSpace[i].
	map<CTuple<int, 5>, float> irreps_projections;
#ifdef TEXT_WFN							
	fstream wfn_file(wfn_file_name.c_str());
#else							
	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary);
#endif	

	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return 1;
	}
//	iterate over the segments of the basis [which is split between ndiagonal
//	processes].
	for (size_t idiag = 0; idiag < ndiag; ++idiag)
	{
		unsigned long firstStateId = std::accumulate(dims.begin(), dims.begin() + idiag, (unsigned long)0); 
//		cout << "section " << idiag << "\t\t\tdim=" <<  dims[idiag] << endl;

		int nmax_space_new, nmax_space, amax;
		unsigned int ap, an;
		unsigned char status;
		unsigned long idim = 0;
		unsigned int rhomax_x_dim_wpn = 0;
		SingleDistribution distr_p, distr_n;
		UN::SU3xSU2_VEC gamma_p, gamma_n;
		SU3xSU2_VEC omega_p, omega_n;
		SU3xSU2::LABELS w_pn;
		size_t nstates, number_coeffs_read(0);

		PNConfIterator iter = basis_full.firstPNConf(idiag, ndiag);
		for (iter.rewind(); iter.hasPNConf(); iter.nextPNConf())
		{
			assert(iter.hasPNOmega());

			w_pn             = iter.getCurrentSU3xSU2();
			rhomax_x_dim_wpn = wpn_irreps_container.rhomax_x_dim(w_pn);	// rhomax * dim[(lm mu)S]
			status           = iter.status();

			switch (status)
			{
				case PNConfIterator::kNewDistr_p: distr_p.resize(0); iter.getDistr_p(distr_p);
				case PNConfIterator::kNewDistr_n: distr_n.resize(0); iter.getDistr_n(distr_n); nmax_space_new = iter.nhw();
				case PNConfIterator::kNewGamma_p: gamma_p.resize(0); iter.getGamma_p(gamma_p);
				case PNConfIterator::kNewGamma_n: gamma_n.resize(0); iter.getGamma_n(gamma_n);
				case PNConfIterator::kNewOmega_p: omega_p.resize(0); iter.getOmega_p(omega_p); ap = iter.getMult_p();
				case PNConfIterator::kNewOmega_n: omega_n.resize(0); iter.getOmega_n(omega_n); an = iter.getMult_n();
			}
			amax             = ap*an*w_pn.rho;
			nstates          = ap*an*rhomax_x_dim_wpn;

			if (nstates == 0)
			{
				continue;
			}

			IRREPBASIS irrep_basis(wpn_irreps_container.getSU3xSU2PhysicalBasis(w_pn));
			float wpn_projection = 0.0;
//	vector of { <LJka, |a|^2>, <L'K'k'a', |b|^2>, .... , <L''J''k''a'', |c|^2> }			
			vector<pair<CTuple<int, 4>, float> > states_LJka;
//	The order of loops is extremely important and must be same as the one being
//	used to calculate the matrix elements.
			for (irrep_basis.rewind(); !irrep_basis.IsDone(); irrep_basis.nextL())
			{
				for (int J = irrep_basis.Jmin(); J <= irrep_basis.Jmax(); J += 2)
				{
					for (int k = 0; k < irrep_basis.kmax(); ++k)
					{
						size_t kfLfJId = irrep_basis.getId(k, J);
						for (int a = 0; a < amax; ++a, number_coeffs_read++)
						{

#ifdef TEXT_WFN							
							wfn_file >> dcoeff;
#else							
							wfn_file.read((char*)&dcoeff, sizeof(float));
#endif
//	firstStateId ... Id of the first state in basis for a given idiag
//	idim ... number of states in the prevous irreps we iterated through
							size_t basis_state_id = firstStateId + idim + kfLfJId*amax + a;
//	we should never reach the end of the file before iterating through all
//	possible comfigurations
							if (!wfn_file)
							{
								cout << "End! Last coeff read: " << dcoeff << endl;
							}
		//					assert(wfn_file);
//	the (id+1) of a given state is equal to the number of states that have been
//	so far read from the input file. 
	//						assert((basis_state_id + 1) == number_coeffs_read);
							
							if (!Negligible(dcoeff))
							{
								wpn_projection += dcoeff*dcoeff;
								CTuple<int, 4> JLka;
								JLka[0] = irrep_basis.L(); 
								JLka[1] = J; 
								JLka[2] = k; 
								JLka[3] = a; 
								states_LJka.push_back(make_pair(JLka, dcoeff));
							}
						}
					}
				}
			}
			idim += nstates;
			if (states_LJka.empty())// ==> there is no state in w_pn irrep that would project on the input wave function
			{
				continue;
			}
//	we are interested only in Sp Sn S (l m) subspaces carrying Nmax oscillator
//	quanta
			if (Nmax == iter.nhw())
			{
				CTuple<int, 5> spsnslmu;
				SU3xSU2::LABELS wp(iter.getProtonSU3xSU2()), wn(iter.getNeutronSU3xSU2());
				spsnslmu[kSSp] = wp.S2; spsnslmu[kSSn] = wn.S2; spsnslmu[kSS] = w_pn.S2; spsnslmu[kLm] = w_pn.lm; spsnslmu[kMu] = w_pn.mu;
				irreps_projections[spsnslmu] += wpn_projection;
			}
		}
	}

	map<CTuple<int, 5>, float>::const_iterator it = irreps_projections.begin();
	float ratio_max = 0.0;
	for (; it != irreps_projections.end(); ++it)
	{
		CTuple<int, 5> key(it->first);
		float projection(it->second);
		key[3] += 2;
		size_t dim = nextNhw_dimensions[key];
		float ratio = projection / (float)dim;
		if (ratio > ratio_max)
		{
			ratio_max = ratio;
		}
	}

	it = irreps_projections.begin();
	vector<INFO> vresults; 
	for (; it != irreps_projections.end(); ++it)
	{
		CTuple<int, 5> key(it->first);
		float projection(it->second);
		key[3] += 2;
		size_t dim = nextNhw_dimensions[key];
		float ratio = (projection/(float)dim)/ratio_max;
		INFO element(key[0], key[1], key[2], (key[3]-2), key[4], projection, ratio, dim);
		vresults.push_back(element);
	}

	if (sort_type == kSortByProb)
	{
		std::sort(vresults.begin(), vresults.end(), SortByProbability());
	}
	else if (sort_type == kSortByRatio)
	{
		std::sort(vresults.begin(), vresults.end(), SortByRatio());
	}
	for (int i = 0; i < vresults.size(); ++i)
	{
		INFO element(vresults[i]);
		int Sp = std::tr1::get<kSSp>(element);
		int Sn = std::tr1::get<kSSn>(element);
		int S  = std::tr1::get<kSS>(element);
		int lm = std::tr1::get<kLm>(element);
		int mu = std::tr1::get<kMu>(element);
		float proj = std::tr1::get<kProb>(element);
		float ratio = std::tr1::get<kRatio>(element);
		size_t dim  = std::tr1::get<kDim>(element);

		cout << "Sp=" << Sp << " Sn=" << Sn << " S=" << S << "(" << lm << " " << mu << ")\t" << proj << "\t" << ratio << "\t" << dim << endl;
	}
}

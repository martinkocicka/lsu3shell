#include "SU3ME/CShellConfigurations.h"
#include <UNU3SU3/CUNMaster.h>

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::cin;
using std::vector;

int CalculateNumberOfUNSU3Irreps(size_t Z, size_t N, size_t Nmax)
{
	vector<unsigned> vMaxProtons;
	vector<unsigned> vMaxNeutrons;

	CShellConfigurations ProtonConfs(Nmax, Z);
	CShellConfigurations NeutronConfs(Nmax, N);

	ProtonConfs.GetMaxFermionsShells(vMaxProtons);
	NeutronConfs.GetMaxFermionsShells(vMaxNeutrons);

	uint8_t max_ho_shells = std::max(vMaxProtons.size(), vMaxNeutrons.size());
	vector<unsigned> vMaxNucleons(max_ho_shells, 1);

	for (size_t i = 0; i < max_ho_shells; ++i)
	{
		vMaxNucleons[i] = std::max( (i < vMaxProtons.size()) ? vMaxProtons[i] : 0,  (i < vMaxNeutrons.size()) ? vMaxNeutrons[i] : 0);
	}
	
	int number_UNSU3irreps(0);
	CUNMaster UNMaster;
	for (int n = 0; n < max_ho_shells; ++n)
	{
		for (int A = 1; A <= vMaxNucleons[n]; ++A)
		{
			std::vector<std::pair<SU2::LABEL, UN::SU3_VEC> > SpinIrreps;
			if (A == 1) // single fermion ===> 1(n 0)1/2
			{
				number_UNSU3irreps += 1;
			}
			else if (A == (n+1)*(n+2)) // single fermion ===> 1(n 0)1/2
			{
				number_UNSU3irreps += 1;
			}
			else
			{
				UNMaster.GetAllowedSU3xSU2Irreps(n, A, SpinIrreps);
			}
			for (size_t iS = 0; iS < SpinIrreps.size(); ++iS)
			{
				for (size_t i = 0; i < SpinIrreps[iS].second.size(); ++i)
				{
					number_UNSU3irreps += SpinIrreps[iS].second[i].mult;
				}
			}
		}
	}
//	cout << "Number of single-shell U(N) > SU(3) irreps:" << number_UNSU3irreps << endl;
	return number_UNSU3irreps;
}


int main(int argc,char **argv)
{
	int Z, N, Nmax;

	cout << "Number of protons:";
	cin >> Z;
	cout << "Number of neutrons:";
	cin >> N;
	cout << "Nmax:";
	cin >> Nmax;

	vector<int> results(Nmax+1, 0);
	for (int nmax = Nmax; nmax >= 0; nmax -= 2)
	{
		cout << "Nmax: " << nmax << " ... "; cout.flush();
		results[nmax] = CalculateNumberOfUNSU3Irreps(Z, N, nmax);
		cout << results[nmax] << endl;
	}

	cout << "result = {";
	for (int nmax = Nmax; nmax >= 0; nmax -= 2)
	{
		cout << "{" << nmax << ", " << results[nmax] << "}";
		if ((nmax - 2) < 0)
		{
			cout << "};" << endl;
		}
		else
		{
			cout << ", ";
		}
	}
}

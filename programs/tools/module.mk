$(eval $(begin-module))

################################################################
# unit definitions
################################################################

# module_units_h := 
# module_units_cpp-h := 
# module_units_f := 
module_programs_cpp := \
overlaps \
ncsmSU3xSU2BasisLSU3 \
wfn2ndiag \
L2_LS_B+B_AB00_2SU3Tensors \
AllowedSU3xSU2Lister_main \
CBaseSU3Irreps GenerateL2Me JPVjtcoupled2Operators ListCMinvariantSU3xSU2Subspaces \
JPVjtcoupled2PNjcoupled \
PhaseMatrix ShellOccupancy \
canonicalbasis identicalFermsBasisInfo ncsmSU3xSU2BasisOnTheFly \
physicalbasis \
showVectorInIdenticalFermsBasis su2cgs su39lm su3mult su3so3cg su3u1su2cg su3u6lm \
su3z6lm wigner9j idTonwSkLJ_pn idTonwSkLJ_ppnn  SingleShellHWSGen MultiShellHWSGen \
iteratorTimer  \
pn2hdf5_HDF5 ppnn2hdf5_HDF5 \
ncsmSU3xSU2BasisFastIteration \
TransformObdSU3toSU2 \
Generate_obdlists_nmax_JJ0 \
SubspaceSelector \
OneBodyOperator2ObdList \
Sp3RBasisLister \

# module_programs_f :=
# module_generated  :=
################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))

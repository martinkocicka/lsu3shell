#include <SU3ME/ModelSpaceExclusionRules.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/global_definitions.h>
#include <vector>
#include <stack>
#include <ctime>
using namespace std;

void Print(const CTuple<int, 4>& lmS)
{
	cout << lmS[3] << "(" << lmS[0] << " " << lmS[1] << ")" << lmS[2];
}

void PrintState(const vector<CTuple<int, 4> >& Labels_p, const vector<CTuple<int, 4> >& Labels_n)
{
	cout << "[";
	size_t index;
	size_t nOmegas_p = (Labels_p.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_p; ++i)
	{
		cout << "{";
	}
	Print(Labels_p[0]);
	if (nOmegas_p > 0)
	{
		cout << " x ";
		Print(Labels_p[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_p - 1; ++i)
		{
			Print(Labels_p[index]); cout << " x ";
			Print(Labels_p[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_p[index]);
	}

	cout << "] x ["; 
	size_t nOmegas_n = (Labels_n.size() - 1)/2;
	for (size_t i = 0; i < nOmegas_n; ++i)
	{
		cout << "{";
	}
	Print(Labels_n[0]);
	if (nOmegas_n > 0)
	{
		cout << " x ";
		Print(Labels_n[1]);
		cout << "}";

		index = 2;
		for (size_t i = 0; i < nOmegas_n - 1; ++i)
		{
			Print(Labels_n[index]); cout << " x ";
			Print(Labels_n[index + 1]); cout << "}";
			index += 2;
		}
		Print(Labels_n[index]);
	}
	cout << "]" << endl;
//	cout << "] " << (int)omega_pn.rho << "(" << (int)omega_pn.lm << " " << (int)omega_pn.mu << ")" << (int)omega_pn.S2; 
}

////////////////////////////////////////////////////////////////////////
//	The following set of functions is merely for the output purposes.  I needed
//	to do this quickly, so it is messy ... but seems to work at least good
//	enough for the testing/debbuging rme calculation purposes ...
//
void Set(CTuple<int, 4>& lmS, const UN::SU3xSU2& gamma)
{
	lmS[0] = gamma.lm; lmS[1] = gamma.mu; lmS[2] = gamma.S2; lmS[3] = gamma.mult;
}
void Set(CTuple<int, 4>& lmS, const SU3xSU2::LABELS& omega)
{
	lmS[0] = omega.lm; lmS[1] = omega.mu; lmS[2] = omega.S2; lmS[3] = omega.rho;
}
//	Take gamma and omega and creates a single array of four integers with structure
//
//	Labels = { 	{gamma[0].lm, gamma[0].mu, gamma[0].S2, gamma[0].mult}, 
//				{gamma[1].lm, gamma[1].mu, gamma[1].S2, gamma[1].mult}, 
//				{omega[0].lm, omega[0].mu, omega[0].S2, omega[0].rho}, 
//				{gamma[2].lm, gamma[2].mu, gamma[2].S2, gamma[2].mult}, 
//				{omega[1].lm, omega[1].mu, omega[1].S2, omega[1].rho}, 
//				.
//				.
//				.
//				{omega[#shells - 1].lm, omega[#shells - 1].mu, omega[#shells - 1].S2, omega[#shells - 1].rho}, 
template<class T>
void Output(const T& gamma, const SU3xSU2_VEC& omega, vector<CTuple<int, 4> >& Labels)
{
	CTuple<int, 4> lmS;

	Set(lmS, gamma[0]); Labels.push_back(lmS); 

	if (omega.empty())
	{
		return;
	}

	Set(lmS, gamma[1]); Labels.push_back(lmS);

	for (size_t iomega = 0; iomega < omega.size() - 1; ++iomega)
	{
		Set(lmS, omega[iomega]); Labels.push_back(lmS);
		Set(lmS, gamma[iomega+2]); Labels.push_back(lmS);
	}
	Set(lmS, (omega.back()));
	Labels.push_back(lmS); 
}

void Print( const SingleDistribution& distr_p, const SingleDistribution& distr_n)
{
	cout << "[";
	for (size_t i = 0; i < distr_p.size() - 1; ++i)
	{
		cout << (int)distr_p[i] << " ";
	}
	cout << (int)distr_p.back() << "] x [";

	for (size_t i = 0; i < distr_n.size() - 1; ++i)
	{
		cout << (int)distr_n[i] << " ";
	}
	cout << (int)distr_n.back() << "]" << endl;
}

void Print(UN::SU3xSU2_VEC& gamma_p, SU3xSU2_VEC& omega_p, UN::SU3xSU2_VEC& gamma_n, SU3xSU2_VEC& omega_n)
{
	vector<CTuple<int, 4> > Labels_p;
	vector<CTuple<int, 4> > Labels_n;

	Output(gamma_p, omega_p, Labels_p);
	Output(gamma_n, omega_n, Labels_n);

	PrintState(Labels_p, Labels_n);
}

void IterateOverBasis(const lsu3::CncsmSU3xSU2Basis& basis)
{
	typedef uint32_t GROUP_SIZE;
	typedef uint16_t NUMBER_OF_GROUPS;

	map<GROUP_SIZE, uint16_t> group_statistics;

	uint16_t ap_max, an_max;
	uint16_t irrep_dim(0);
	uint32_t idim(0);
	uint64_t firstStateId = basis.getFirstStateId();

	uint32_t ip;
	uint32_t in;

	SingleDistribution distr_p; 
	SingleDistribution distr_n;
	UN::SU3xSU2_VEC gamma_p; 
	UN::SU3xSU2_VEC gamma_n;
	SU3xSU2_VEC omega_p, omega_n;
	SU3xSU2::LABELS omega_pn;
	
//	loop over (ip, in) pairs
	for (int ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++)
	{
		ip = basis.getProtonIrrepId(ipin_block);
		in = basis.getNeutronIrrepId(ipin_block);

		ap_max = basis.getMult_p(ip);
		an_max = basis.getMult_n(in);

//		cout << "(ip:" << ip << ", in:" << in << ")" << endl;
		
		distr_p.resize(0); basis.getDistr_p(ip, distr_p);
		gamma_p.resize(0); basis.getGamma_p(ip, gamma_p);
		omega_p.resize(0); basis.getOmega_p(ip, omega_p);

		distr_n.resize(0); basis.getDistr_n(in, distr_n); 
		gamma_n.resize(0); basis.getGamma_n(in, gamma_n);
		omega_n.resize(0); basis.getOmega_n(in, omega_n);

//		Print(distr_p, distr_n);
//		Print(gamma_p, omega_p, gamma_n, omega_n);

		group_statistics[basis.NumberOfStatesInBlock(ipin_block)] += 1;

		for (int iwpn = basis.blockBegin(ipin_block); iwpn < basis.blockEnd(ipin_block); ++iwpn)
		{
			omega_pn = basis.getOmega_pn(ip, in, iwpn);
//			cout << (int)omega_pn.rho << "(" << (int)omega_pn.lm << " " << (int)omega_pn.mu << ")" << (int)omega_pn.S2;
			irrep_dim = ap_max*an_max*omega_pn.rho*basis.omega_pn_dim(iwpn);
//			cout << "\t\tid:" << firstStateId + idim << " dim:" << irrep_dim << endl;;
			idim += irrep_dim;
		}
	}

	cout << "We have following distribution of group of states: " << endl;
	for (map<GROUP_SIZE, NUMBER_OF_GROUPS>::iterator it = group_statistics.begin(); it != group_statistics.end(); ++it)
	{
		cout << "group size: " << it->first << "\t #groups: " << it->second << endl;
	}
}

int main(int argc,char **argv)
{
	if (argc != 2 && argc != 4)
	{
		cout << "Usage: " << argv[0] << " <model space definition> [<ndiag> <idiag>]" << endl;
		cout << "Implicitly ndiag=1 and idiag=0." << endl;
		return EXIT_FAILURE;
	}

	int ndiag; 
	int idiag;

	if (argc == 4)
	{
		ndiag = atoi(argv[2]);
 		idiag = atoi(argv[3]);
	}
	else
	{
		ndiag = 1;
		idiag = 0;
	}

	proton_neutron::ModelSpace ncsmModelSpace(argv[1]);
	lsu3::CncsmSU3xSU2Basis  basis(ncsmModelSpace, idiag, ndiag);
//	cout << "Proton irreps" << endl;
//	basis.ShowProtonIrreps();
//	cout << "Neutron irreps" << endl;
//	basis.ShowNeutronIrreps();
	basis.ShowMemoryRequirements();
	for (int i = 0; i < ndiag; ++i)
	{
		cout << "idiag: " << i << " size: " << basis.dim(i) << endl;
	}
	IterateOverBasis(basis);
}

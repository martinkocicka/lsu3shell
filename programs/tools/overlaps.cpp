#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
#include <sstream>

#include <iostream>

using namespace std;

/*
 *
 *
 * Calculate overlaps of a function, <bra_wfn>, which span a certain model space <bra model space>,
 * with list of ket states that span subspace <ket model space> of bra model space.
 *
 *
 */
int main(int argc, char* argv[])
{
	cout.precision(8);
	cout.setf(ios::fixed);
	if (argc != 5)
	{
		cout << "Usage: " << argv[0] << " <bra_wfn [ndiag=1]> <bra model space>  <file with list ket wfns [ndiag=1]> <ket model space>" << endl;
		cout << "Note that bra and ket space must belong to same nuclei and ket space is a subspace of bra space, i.e.  ket \\in bra." << endl;
		return 0;
	}

//	<bra model space> ... argv[2]
	lsu3::CncsmSU3xSU2Basis bra(std::string(argv[2]), 0, 1);
//	<ket model space> ... argv[4]
	lsu3::CncsmSU3xSU2Basis ket(std::string(argv[4]), 0, 1);
//	Check if it makes sense to calculate overlaps	
	if (bra.JJ() != ket.JJ())
	{
		cerr << "Ket model space and bra model space do not have the same J value." << endl;
		return EXIT_FAILURE;
	}
	if (bra.NProtons() != ket.NProtons() || bra.NNeutrons() != ket.NNeutrons())
	{
		cerr << "Ket model space and bra model space describe different nuclei" << endl;
		return EXIT_FAILURE;
	}
	if ((bra.getModelSpaceDim() < ket.getModelSpaceDim()))
	{
		cerr << "Dimension of ket model space is larger then of bra model space ... this indicated that ket model space is not a subspace of bra model space." << endl;
		return EXIT_FAILURE;
	}

	lsu3::CncsmSU3xSU2Basis& J_basis = ket;
	lsu3::CncsmSU3xSU2Basis& I_basis =  bra;

	uint32_t irrep_dim;
	const uint32_t number_ipin_blocks = I_basis.NumberOfBlocks();  // variables starting with i
	const uint32_t number_jpjn_blocks = J_basis.NumberOfBlocks(); // variables starting with j
	std::vector<uint32_t> matching_bra_coeff_begin;
	// this vector contains indices of coefficients in bra wfn. Element at
	// index i, holds index poiting to a first coefficient in bra wfn that has
	// the same set of [U(N) SU(3)]_p x [U(N) SU(3)]_n --> SU(3) quantum
	// numbers. The lenght of following sequence is given my ap_max x an_max x
	// rho_max x dim(SU(3)xSU(2) for given J)
	// The lenght of the vector matching_bra_coeff_begin is thus less equal to
	// dimension of ket space.
	matching_bra_coeff_begin.reserve(J_basis.getModelSpaceDim());
	
//	Is this really needed ?	
//	uint32_t ip(-1), in(-1);
	uint32_t blockFirstRow(0);
//	iterate over [jp jn] blocks of states
	for (unsigned int jpjn_block = 0, ipin_block = 0; jpjn_block < number_jpjn_blocks && ipin_block < number_ipin_blocks; jpjn_block++)
	{
		uint32_t jp = J_basis.getProtonIrrepId(jpjn_block);
		uint32_t jn = J_basis.getNeutronIrrepId(jpjn_block);

//	Iterate from the current position in bigger basis, try to find (jp jn)
//	block. We use the fact that blocks in basis are sorted in an increasing order.
		uint32_t ip(0), in(0);
		for ( ; ipin_block < number_ipin_blocks; ++ipin_block) //ipin_block will be pointing to the next block one step ahead
		{
			ip = I_basis.getProtonIrrepId(ipin_block); 
			in = I_basis.getNeutronIrrepId(ipin_block); 
			if (ip == jp && in == jn)
			{
				break; // blockFirstRow still points at the beggining of the (ip in) block
			}
			// increase index blockFirstRow to point to the first basis state of the next block or to the end of the basis.
 			blockFirstRow += I_basis.NumberOfStatesInBlock(ipin_block);
		}
		// either ip == jp && in == jn or we are at the end, i.e. ipin_block == number_ipin_blocks && blockFirstRow == dim[bra]
		if (ip == jp && in == jn)
		{
//	we found matching pair (jp jn) == (ip in) ==> we will need to calculate its dimension ==> prepare ap_max & an_max
			uint32_t aip_max = I_basis.getMult_p(ip); // must be qual to J_basis.getMult_p(jp)
			uint32_t ain_max = I_basis.getMult_n(in); // must be qual to J_basis.getMult_n(jn)

			uint32_t jbegin = J_basis.blockBegin(jpjn_block);
			uint32_t jend = J_basis.blockEnd(jpjn_block);

			uint32_t ibegin = I_basis.blockBegin(ipin_block);
			uint32_t iend = I_basis.blockEnd(ipin_block);

			bool matching_states_found = false;

			uint32_t ipos_current(blockFirstRow);

			for (uint32_t jwpn = jbegin, iwpn = ibegin; jwpn < jend && iwpn < iend; ++jwpn)
			{
				SU3xSU2::LABELS omega_pn_J(J_basis.getOmega_pn(jp, jn, jwpn));

				for (; iwpn < iend; ++iwpn, ipos_current += irrep_dim)
				{
					SU3xSU2::LABELS omega_pn_I(I_basis.getOmega_pn(ip, in, iwpn));
					irrep_dim = aip_max*ain_max*omega_pn_I.rho*I_basis.omega_pn_dim(iwpn);

					matching_states_found = (omega_pn_J == omega_pn_I);
					if (matching_states_found)
					{
						break;
					}
				}
				if (matching_states_found)
				{
					// matching_bra_coeff_begin[i] contains an index of the
					// first coefficient corresponding with (ip in wpnI) == (jp
					// jn wpnJ) irrep in ket space
					matching_bra_coeff_begin.push_back(ipos_current);
				}
			}
		}
	}


	vector<float> wfn_smaller(J_basis.getModelSpaceDim());
	vector<float> wfn_bigger(I_basis.getModelSpaceDim());

//	fstream wfn_bigger_file(argv[1], std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
//	wfn_bigger_file.seekg (0, std::ios::beg);
	fstream wfn_bigger_file(argv[1], std::ios::in | std::ios::binary);
	wfn_bigger_file.read((char*)&wfn_bigger[0], I_basis.getModelSpaceDim()*sizeof(float));
	if (!wfn_bigger_file)
	{
		cout << "Error could not open bra '" << argv[1] << "' file with wfn." << endl;
		return EXIT_FAILURE;
	}

//	show the content of bra wfn in a mathematica compatible way
/*
	uint32_t imatching_bra_index = 0;
	cout << "wfn_bra={";
	for (unsigned int jpjn_block = 0, ibra_cnt = 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
	{
		uint32_t jp = J_basis.getProtonIrrepId(jpjn_block);
		uint32_t jn = J_basis.getNeutronIrrepId(jpjn_block);

		uint32_t jbegin = J_basis.blockBegin(jpjn_block);
		uint32_t jend = J_basis.blockEnd(jpjn_block);
		for (uint32_t jwpn = jbegin; jwpn < jend; ++jwpn)
		{
			uint32_t ajp_max = J_basis.getMult_p(jp);
			uint32_t ajn_max = J_basis.getMult_n(jn);
			SU3xSU2::LABELS omega_pn_I(J_basis.getOmega_pn(jp, jn, jwpn));
			uint32_t idim = ajp_max*ajn_max*omega_pn_I.rho*J_basis.omega_pn_dim(jwpn);
			uint32_t icurr_bigger  = matching_bra_coeff_begin[imatching_bra_index++];

			for (int j = 0; j < idim; ++j)
			{
				cout << wfn_bigger[icurr_bigger+j];
				if (j+1 == idim && imatching_bra_index == matching_bra_coeff_begin.size())
				{
					cout << "};";
				}
				else
				{
					cout << ", "; 
				}
			}
		}
	}
	cout << endl;
*/
	fstream wfn_list(argv[3]);
	if (!wfn_list)
	{
		cout << "Error could not open list of ket wfns stored in '" << argv[3] << "' file." << endl;
		cout << "Error " << endl;
		return EXIT_FAILURE;
	}

//  prob_results[i] .... |<bra | ket_{i}>|^2 
	vector<float> prob_results;
	vector<string> wfn_file_names;
//	Read ket wfn files and for each calculate its probability in bra wfn	
	do 
	{
		string smaller_file_name;
		wfn_list >> smaller_file_name;
		if (!wfn_list)
		{
			break;
		}
		
//		fstream wfn_smaller_file(smaller_file_name, std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
//		wfn_smaller_file.seekg (0, std::ios::beg);
		fstream wfn_smaller_file(smaller_file_name.c_str(), std::ios::in | std::ios::binary);
		if (!wfn_smaller_file)
		{
			cerr << "Could not open file '" << smaller_file_name << "'!" << endl;
			return EXIT_FAILURE;
		}
		wfn_smaller_file.read((char*)&wfn_smaller[0], J_basis.getModelSpaceDim()*sizeof(float));

//	show the content of  wfn in mathematica compatible way
/*	
		cout << "wfn_smaller" << iter_number << "={ ";
		for (int i = 0; i < wfn_smaller.size()-1; ++i)
		{
			cout << wfn_smaller[i] << ", ";
		}
		cout << wfn_smaller.back() << "};" << endl;
		cout << endl;
*/
		float prob = 0;
		uint32_t imatching_bra_index = 0, icurrent = 0;
		for (unsigned int jpjn_block = 0, ibra_cnt = 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			uint32_t jp = J_basis.getProtonIrrepId(jpjn_block);
			uint32_t jn = J_basis.getNeutronIrrepId(jpjn_block);

			uint32_t jbegin = J_basis.blockBegin(jpjn_block);
			uint32_t jend = J_basis.blockEnd(jpjn_block);
			for (uint32_t jwpn = jbegin; jwpn < jend; ++jwpn)
			{
				SU3xSU2::LABELS omega_pn_I(J_basis.getOmega_pn(jp, jn, jwpn));

				uint32_t icurr_bigger  = matching_bra_coeff_begin[imatching_bra_index++];

				uint32_t ajp_max = J_basis.getMult_p(jp);
				uint32_t ajn_max = J_basis.getMult_n(jn);
				uint32_t idim = ajp_max*ajn_max*omega_pn_I.rho*J_basis.omega_pn_dim(jwpn);

				for (int j = 0; j < idim; ++j, ++icurrent)
				{
					prob += wfn_smaller[icurrent]*wfn_bigger[icurr_bigger + j];
				}
			}
		}
		wfn_file_names.push_back(smaller_file_name);
		prob_results.push_back(prob*prob);
	}
	while(true);

//	Show resulting probabilities ... "
	float total(0);
	for (int i = 0; i < prob_results.size(); ++i)
	{
		cout << wfn_file_names[i] << " prob:" << prob_results[i]*100 << endl;
		total += prob_results[i];
	}
	cout << "total: " << total*100 << endl;
}

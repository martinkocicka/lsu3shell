#include <mpi.h>

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <mascot/Exception.h>
#include <mascot/DataSet.h>
#include <mascot/File.h>

#include <SU3ME/InteractionPPNN.h>

int main(int argc, char* argv[])
{
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <input file> <output file>\n";
        std::exit(1);
    }

    std::string inFileName(argv[1]);
    std::string outFileName(argv[2]);

    std::cout << "Input file name:  " << inFileName  << "\n";
    std::cout << "Output file name: " << outFileName << "\n";

    try {
        mascot::File* file = mascot::File::create(outFileName);

        mascot::DataSet* d_n = file->createChunkedDataSet("n", mascot::DataType::SINT8, 1024, 1024);
        mascot::DataSet* d_nTensors = file->createChunkedDataSet("nTensors", mascot::DataType::UINT32, 1024, 1024);
        mascot::DataSet* d_irholmmuS2 = file->createChunkedDataSet("irholmmuS2", mascot::DataType::UINT8, 1024, 1024);
        mascot::DataSet* d_coeffs = file->createChunkedDataSet("coeffs", mascot::DataType::FLOAT, 1024, 1024);

        std::ifstream f(inFileName.c_str());
        if (!f) {
            std::cerr << "Could not open input file " << inFileName << "\n";
            throw;
        }

        while (true) {
            int8_t n[6];
            int64_t temp;

            // n1, n2, n3, n4, n5, n6
            for (int i = 0; i < 6; ++i) {
                f >> temp;
                n[i] = mascot::safe_numeric_cast<int8_t>(temp);
            }

            if (f.eof())
                break;

            for (int i = 0; i < 6; ++i) 
                d_n->write(n[i]);

            // nTensors
            f >> temp;
            uint32_t nTensors = mascot::safe_numeric_cast<uint32_t>(temp);
            d_nTensors->write(nTensors);

            for (uint32_t iTensor = 0; iTensor < nTensors; ++iTensor) {
                // irho, lm, mu, S2 (3x)
                uint8_t irholmmuS2[12];

                for (int i = 0; i < 12; ++i) {
                    f >> temp;
                    irholmmuS2[i] = mascot::safe_numeric_cast<uint8_t>(temp);
                    d_irholmmuS2->write(irholmmuS2[i]);
                }

                // coeffs
                int nCoeffs = irholmmuS2[8] * SU3::kmax(SU3::LABELS(1, irholmmuS2[9], irholmmuS2[10]), irholmmuS2[11] / 2);
                for (int i = 0; i < nCoeffs; ++i) {
                    float a;
                    f >> a;
                    d_coeffs->write(a);
                }
            }
        }

        file->closeDataSet(d_n);
        file->closeDataSet(d_nTensors);
        file->closeDataSet(d_irholmmuS2);
        file->closeDataSet(d_coeffs);

        mascot::File::close(file);
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << mascot::caughtAt(__FILE__, __LINE__);
    }

    return 0;
}

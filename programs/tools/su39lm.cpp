#include <UNU3SU3/CSU3Master.h>
#include <LookUpContainers/CSU39lm.h>
#include <iostream>
#include <algorithm>

using namespace std;

int main() 
{
	int lm1, mu1, lm2, mu2, lm3, mu3, lm4, mu4, lm12, mu12, lm34, mu34, lm13, mu13, lm24, mu24, lm, mu; 
	size_t nsu39lm;

    cout << "Enter (lm1 mu1) " << endl;
    cin >> lm1 >> mu1;
    cout << "Enter (lm2 mu2) " << endl;
    cin >> lm2 >> mu2;
    cout << "Enter (lm12 mu12) " << endl;
    cin >> lm12 >> mu12;


    cout << "Enter (lm3 mu3) " << endl;
    cin >> lm3 >> mu3;
    cout << "Enter (lm4 mu4) " << endl;
    cin >> lm4 >> mu4;
    cout << "Enter (lm34 mu34) " << endl;
    cin >> lm34 >> mu34;

    cout << "Enter (lm13 mu13) " << endl;
    cin >> lm13 >> mu13;
    cout << "Enter (lm24 mu24) " << endl;
    cin >> lm24 >> mu24;
    cout << "Enter (lm mu) " << endl;
    cin >> lm >> mu;

	SU3::LABELS ir1(lm1, mu1);
	SU3::LABELS ir2(lm2, mu2);
	SU3::LABELS ir3(lm3, mu3);
	SU3::LABELS ir4(lm4, mu4);

	SU3::LABELS ir12(lm12, mu12);
	SU3::LABELS ir34(lm34, mu34);
	SU3::LABELS ir13(lm13, mu13);
	SU3::LABELS ir24(lm24, mu24);

	SU3::LABELS ir(lm, mu);

	int rho12max 	= SU3::mult(ir1, ir2, ir12);
	int rho34max 	= SU3::mult(ir3, ir4, ir34);
	int rho13max 	= SU3::mult(ir1, ir3, ir13);
	int rho24max 	= SU3::mult(ir2, ir4, ir24);
	int rho1234max	= SU3::mult(ir12, ir34, ir);
	int rho1324max	= SU3::mult(ir13, ir24, ir);

	if (!rho12max) 
	{
		cerr << ir1 << " x " << ir2 << " does not couple to " << ir12 << endl;
	}
	if (!rho34max) 
	{
		cerr << ir3 << " x " << ir4 << " does not couple to " << ir34 << endl;
	}
	if (!rho13max) 
	{
		cerr << ir1 << " x " << ir3 << " does not couple to " << ir13 << endl;
	}
	if (!rho24max) 
	{
		cerr << ir2 << " x " << ir4 << " does not couple to " << ir24 << endl;
	}

	if (!rho1234max) 
	{
		cerr << ir12 << " x " << ir34 << " does not couple to " << ir << endl;
	}
	if (!rho1324max) 
	{
		cerr << ir13 << " x " << ir24 << " does not couple to " << ir << endl;
	}

	nsu39lm = rho12max*rho34max*rho13max*rho24max*rho1234max*rho1324max;

	double su39lm[nsu39lm];
	CSU39lm<double> su39lmCalculator(10, 10);
	su39lmCalculator.Get9lm(ir1, ir2, ir12, ir3, ir4, ir34, ir13, ir24, ir, su39lm);
	
	int n1 = rho12max;
	int n2 = n1*rho34max;
	int n3 = n2*rho1234max;
	int n4 = n3*rho13max;
	int n5 = n4*rho24max;
	for (size_t rho12 = 0; rho12 < rho12max; ++rho12)
		for (size_t rho34 = 0; rho34 < rho34max; ++rho34)
			for (size_t rho1234 = 0; rho1234 < rho1234max; ++rho1234)
				for (size_t rho13 = 0; rho13 < rho13max; ++rho13)
					for (size_t rho24 = 0; rho24 < rho24max; ++rho24)
						for (size_t rho1324 = 0; rho1324 < rho1324max; ++rho1324)
						{
							int i = rho12 + rho34*n1 + rho1234*n2 + rho13*n3 + rho24*n4 + rho1324*n5;
							if (i == 0) 
							{
								cout << "{" << ir1 << " " << ir2 << " " << ir12 << " " << rho12+1 << "}" << endl;
								cout << "{" << ir3 << " " << ir4 << " " << ir34 << " " << rho34+1 << "} = " << su39lm[0] << endl;
								cout << "{" << ir13 << " " << ir24 << " " << ir << " " << rho1324+1 << "}" << endl;
								cout << "{" << rho13+1 << "\t" << rho24+1 << "\t" << rho1234+1 << "   }" << endl;
							}
							else 
							{
								cout <<  "                    " << rho12+1 << endl;
								cout <<  "                    " << rho34+1 << " = " << su39lm[i] << endl;
								cout <<  "                    " << rho1324+1 << endl;
								cout << rho13+1 << "\t" << rho24+1 << "\t" << rho1234+1 << endl;
							}
						}

}

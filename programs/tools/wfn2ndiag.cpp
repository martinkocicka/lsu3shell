#include <LSU3/ncsmSU3xSU2Basis.h>

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

bool WfnToNdiagNewBasis(const std::string& wfn_file_name, lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, const int ndiag, std::vector<float>& wfn)
{
//	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
	assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

	std::fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return false;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
	{
		cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
		cout << "The actual size of the file: " << size << " bytes!";
		return false;
	}

	wfn.reserve(nelems); 
	float* wfn_full;
	wfn_full = new float[nelems];
	wfn_file.seekg (0, std::ios::beg);

//	Load wave function generated for ndiag_wfn into a vector that contains
//	order of basis states for ndiag=1.
	uint32_t ipos;
	uint16_t number_of_states_in_block;
	uint32_t number_ipin_blocks = basis.NumberOfBlocks();
//	Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
//		
//	section --> {iblock0, iblock1, ...... }
//		----------------------------------
//		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
//		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
//		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
//		.
//		.
//		.
	for (uint16_t i = 0; i < ndiag_wfn; ++i)
	{
		for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
		{
//	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			
			wfn_file.read((char*)&wfn_full[ipos], number_of_states_in_block*sizeof(float));
		}
	}

	for (uint32_t isection = 0; isection < ndiag; ++isection)
	{
//		This loop iterates over blocks that belong to isection of basis split into ndiag segments
//		I am using the fact that for isection of the basis split into ndiag segment has the
//		following (ip in) blocks: {isection, isection + ndiag, isection + 2ndiag ....}
		for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks; ipin_block += ndiag)
		{
//	obtain position of the block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			wfn.insert(wfn.end(), &wfn_full[ipos], &wfn_full[ipos] + number_of_states_in_block);
		}
	}
//	trim excess memory
	std::vector<float>(wfn).swap(wfn);
	delete []wfn_full;
	return true;
}

int main(int argc,char **argv)
{
	if (argc != 6)
	{
		cout << "Usage: "<< argv[0] <<" <model space filename> <wfn input filename> <ndiag input>  <wfn output filename> <ndiag output>" << endl;
		return EXIT_FAILURE;
	}

	std::string model_space_filename(argv[1]);
	std::string input_wfn(argv[2]);
	int ndiag_input = atoi(argv[3]);
	std::string output_wfn(argv[4]);
	int ndiag_output = atoi(argv[5]);

	if (ndiag_output <= 0 || ndiag_output > 499)
	{
		std::cerr << "Unrealistic value of <ndiag output>:" << ndiag_output << std::endl;
		return EXIT_FAILURE;
	}

	proton_neutron::ModelSpace ncsmModelSpace;
	ncsmModelSpace.Load(model_space_filename);
	lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);
	std::vector<float> wfn;

	wfn.reserve(basis.getModelSpaceDim());
	WfnToNdiagNewBasis(input_wfn, basis, ndiag_input, ndiag_output, wfn);
		
	std::fstream foutput_file(output_wfn.c_str(), std::ios::binary | std::ios::out | std::ios::trunc);
	if (wfn.size() != basis.getModelSpaceDim())
	{
		std::cerr << "Error: the size of model space:" << basis.getModelSpaceDim() << " but wfn has size " << wfn.size() << endl;
		return EXIT_FAILURE;
	}
	foutput_file.write((char*)&wfn[0], wfn.size()*sizeof(float));

	return EXIT_SUCCESS;
}

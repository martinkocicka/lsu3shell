#include <SU3ME/Interaction2SU3tensors.h>
#include <SU3ME/JTCoupled2BMe_Hermitian_SingleOperator.h>

using namespace std;

struct CNoSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return true;
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return true;
	}
} noSelectionRules;

struct Clm_plus_mu_Even
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return true;
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return ((su3ir.lm + su3ir.mu) % 2) == 0;
	}
} CMfreeInteractionSelectionRules;



struct Cinteraction_sel_rules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return ((su3ir.lm + su3ir.mu) % 2) == 0;
	}
	private:
} CInteractionSelectionRules;

struct CVcoul
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 0;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return ((su3ir.lm + su3ir.mu) % 2) == 0;
	}
	private:
} VcoulSelectionRules;


struct CTrelSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}
	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 0;
	}
	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return ((su3ir.lm == 2) && (su3ir.mu == 0)) || ((su3ir.lm == 0) && (su3ir.mu == 2)) || ((su3ir.lm == 0) && (su3ir.mu == 0));
	}
	private:
} TrelSelectionRules;

struct CHrelSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}
	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return Spin == 0;
	}
	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return ((su3ir.lm == 0) && (su3ir.mu == 0));
	}
	private:
} HrelSelectionRules;

/** The input parameters of the code are nmax interaction_file output_file */
int main(int argc, char* argv[])
{
#ifdef USER_FRIENDLY_OUTPUT
	cerr << "You have to comment line with '#define USER_FRIENDLY_OUTPUT' in UNU3SU3basics.h !" << endl;
	return 1;
#endif	
/** \todo nmax could be obtained from input_file */	
	if (argc != 5)
	{
		cout << endl;
	  	cout << "Usage: " << argv[0] << " <Nmax> <valence shell> <file in JPV 'PNfmt' format> <output file with SU3 tensors>" << endl;
		cout << endl;
		cout << endl;
		cout << endl;
		cout << endl;
		cout << "Structure of the outputfile:" << endl;
		cout << "n1 n2 n3 n4 IRf IRi IR0 iS" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 0" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 0" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 0" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 1" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 1" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
		return 0;
	}
	int Nmax = atoi(argv[1]);
	int valence_shell = atoi(argv[2]);
	int nmax = Nmax + valence_shell;

	InitSqrtLogFactTables();
	
	string sFileName(argv[3]);
	string sFileSU3Decomposition(argv[4]);

	size_t nTensorComponents;

	JTCoupled2BMe_Hermitian_SingleOperator<double> JTcoupledRME(sFileName);

//	SwitchHOConvention: 
//	multiplies matrix elements by phase = (-)^{1/2 sum (ni - li)} transforming
//	matrix elements given in HO basis positive at origin into positive at
//	infinity and the other way around.
	JTcoupledRME.SwitchHOConvention();
	int itype;

//	CScalarTwoBodyOperators2SU3Tensors<JTCoupled2BMe_Hermitian_SingleOperator<double> > Decomposer(JTcoupledRME, nmax);
	CScalarTwoBodyOperators2SU3Tensors<JTCoupled2BMe_Hermitian_SingleOperator<double> > Decomposer(JTcoupledRME, Nmax, valence_shell, nmax);
	cout << "What kind of symmetry selection rules do you want to apply?" << endl;
	cout << "1\t no selection rules" << endl;
	cout << "2\t realistic interactions, i.e. (parity conserving, (lm + mu == even)" << endl;
	cout << "3\t Trel, i.e. (parity conserving, S0=0 & (2 0) (0 0) (0 2)" << endl;
	cout << "4\t Vcoul, i.e. (parity conserving, S0=0 & (lm + mu) = even" << endl;
//	cout << "5\t SU(3) and SU(3) scalar operator, i.e. (parity conserving, S0=0 & (0 0), e.g. B+B, rirj" << endl;
	cin >> itype;
	if (itype < 1 || itype > 4) 
	{
		std::cout << itype << " is not a correct option." << endl;
		return 0;
	}

	if (itype == 1)
	{
		nTensorComponents = Decomposer.PerformSU3Decomposition(noSelectionRules, sFileSU3Decomposition);
	}
	else if (itype == 2)
	{
		nTensorComponents = Decomposer.PerformSU3Decomposition(CInteractionSelectionRules, sFileSU3Decomposition);
	}
	else if (itype == 3)
	{
		nTensorComponents = Decomposer.PerformSU3Decomposition(TrelSelectionRules, sFileSU3Decomposition);
	}
	else if (itype == 4)
	{
		nTensorComponents = Decomposer.PerformSU3Decomposition(VcoulSelectionRules, sFileSU3Decomposition);
	}
	cout << "totally " << nTensorComponents << " SU(3) tensor components" << endl;
}

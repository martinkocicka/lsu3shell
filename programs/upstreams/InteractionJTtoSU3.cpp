#include <SU3ME/ScalarOperators2SU3Tensors.h>
#include <SU3ME/Interaction2SU3tensors.h>
#include <SU3ME/global_definitions.h>
#include <SU3NCSMUtils/clebschGordan.h>
#include <UNU3SU3/CSU3Master.h>
#include <UNU3SU3/UNU3SU3Basics.h>

#include <LSU3/std.h>

#include <iostream>
#include <fstream>
#include <map>
#include <tuple>

#define TIMING

using std::vector;
using std::pair;
using std::map;
using std::cout;
using std::endl;
using std::string;
using cpp0x::tuple;

enum {kNA=0, kNB=1, kNC=2, kND=3};
enum {kDoesNotExist = 0xFFFF};

struct CNoSelectionRules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return true;
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return true;
	}
} noSelectionRules;


struct Cinteraction_sel_rules
{
	inline bool operator() (int n1, int n2, int n3, int n4) const
	{
		return ((MINUSto(n1)*MINUSto(n2)) == (MINUSto(n3)*MINUSto(n4))); // return true if parity is conserved
	}

	inline bool operator() (const SU2::LABEL& Spin) const 
	{
		return true;
	}

	inline bool operator() (const SU3::LABELS& su3ir) const 
	{
		return ((su3ir.lm + su3ir.mu) % 2) == 0;
	}
	private:
} CInteractionSelectionRules;


typedef CTuple<uint8_t, 4> NANBNCND;

void Get_nlj(const int index, int& n, int& l, int& j) // Warning: procedure returns angular momentum l, and not 2*l, while for total momentum j = 2*j
{
	SPS2Index::Get_nlj(index, n, l, j);
}


struct JCoupledProtonNeutron2BMe_Hermitian_SingleOperator
{
	typedef double DOUBLE;

	enum ME_DATA_STRUCTURE {kSize,    kI1,       kI2,      kI3,       kI4,       kJ,     kVpp,    kVnn,     kVpn};
	typedef cpp0x::tuple<uint32_t, uint16_t*, uint16_t*, uint16_t*, uint16_t*, uint8_t*, DOUBLE*, DOUBLE*, DOUBLE*> ME_DATA;
	typedef cpp0x::tuple_element<kSize, ME_DATA>::type SIZE_t;
	typedef cpp0x::tuple_element<kI1, ME_DATA>::type I1_t;
	typedef cpp0x::tuple_element<kI2, ME_DATA>::type I2_t;
	typedef cpp0x::tuple_element<kI3, ME_DATA>::type I3_t;
	typedef cpp0x::tuple_element<kI4, ME_DATA>::type I4_t;
	typedef cpp0x::tuple_element<kJ, ME_DATA>::type J_t;
	typedef cpp0x::tuple_element<kVpp, ME_DATA>::type VPP_t;
	typedef cpp0x::tuple_element<kVnn, ME_DATA>::type VNN_t;
	typedef cpp0x::tuple_element<kVpn, ME_DATA>::type VPN_t;


	typedef map<NANBNCND, ME_DATA> INTERACTION_DATA;

	INTERACTION_DATA interaction_data_;

	JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName);

	void SwitchHOConvention();
	void MultiplyByCommonFactor();
	ME_DATA GetMe(NANBNCND nanbncnd);

	void Show();

	inline size_t GetNumberOfOperators() const {return 1;}
	
	template<typename SelectionRules>
	void PerformSU3Decomposition(JCoupledProtonNeutron2BMe_Hermitian_SingleOperator& JCoupledPNME, int Nmax, int valence_shell, int nmax, const string& sOutputFileName);
};

//	This function calculates and returns SU(3) CGs of the following type:
//	<(na 0) 0 *; (nb 0) 0 * || wf * *>.
//
//	For each allowed value la and lb, elements la x lb_max + la contains index of su3cgs vector 
//	whose element contains <(na 0) 0 la, (nb 0) 0 lb || wf 0 min(L0)>_{0}, where
//	min(L0) is the lowest allowed value of L0 for a given irrep wf.
//
//	Order: index = L0 x k0_max + L0 -------> su3cgs[k0, rho0, kf, ki] 
void ComputeSU3CGs_Simple(const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0, vector<uint16_t>& indices, vector<double>& su3cgs)
{
	assert ((wf.lm == 0 && wi.lm == 0) || (wf.mu == 0 && wi.mu == 0));

	su3cgs.resize(0);
	int max_Lf = wf.lm + wf.mu + 1;
	int max_Li = wi.lm + wi.mu + 1;
	int max_L0 = w0.lm + w0.mu + 1;

	double dCG[MAX_K][MAX_K][MAX_K][MAX_K]; // this array is required by su3lib
	memset(dCG, sizeof(dCG), 0);

	vector<uint16_t> temp_indices(max_Lf*max_Li, kDoesNotExist);
	double temp_su3cgs[MAX_K*max_L0]; // max

    int lmf(wf.lm), muf(wf.mu), lmi(wi.lm), mui(wi.mu), lm0(w0.lm), mu0(w0.mu);
	int max_kf, max_ki, max_k0, max_rho;
	int nCGs;

	for (int Lf = 0; Lf < max_Lf; Lf++) // iterate over all Lf in (lmf muf)
	{
		if (!SU3::kmax(wf, Lf))
		{
			continue;
		}
		for (int Li = 0; Li < max_Li; Li++) // iterate over all Li in (lmi mui)
		{
			if (!SU3::kmax(wi, Li))
			{
				continue;
			}
			nCGs = 0;
			for (int L0 = abs(Lf-Li); L0 <= (Lf + Li) && L0 < max_L0; L0++)
			{
				if (!SU3::kmax(w0, L0))
				{
					continue;
				}
#ifndef AIX			
				wu3r3w_(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#else
				wu3r3w (lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#endif
			   	for (int k0 = 0; k0 < max_k0; k0++) // for all values of k0
				{ 
					temp_su3cgs[nCGs++] = dCG[k0][0][0][0]; // store SU(3) wigner coefficient
	    		}
			}
			if (nCGs)
			{
				temp_indices[Lf*max_Li + Li] = su3cgs.size();
				su3cgs.insert(su3cgs.end(), &temp_su3cgs[0], &temp_su3cgs[0] + nCGs);
			}
		}
	}
	temp_indices.swap(indices);
}

//	This function calculates and returns SU(3) CGs of the following type:
//	<wf * *; wi * * || w0 * L0>_{*}, where L0 = 0, 1, 2 ... i.e. I assume
//	it is needed for a two-body scalar (L0=S0) interaction. It could be 
//	easily generalized for a list of L0 values if needed [i.e. for 3-b interactions]
//
//	For each allowed value Lf and Li, elements Lf x Li_max + Li contains index of su3cgs vector 
//	whose element contains <wf 0 Lf, wi 0 Li || w0 0 min(L0)>_{0}, where
//	min(L0) is the lowest allowed value of L0 for a given irrep w0.
//
//	Order: index = k0 x rho0_max x kf_max x ki_max + rho0 x kf_max x ki_max + kf x ki_max + ki -------> su3cgs[k0, rho0, kf, ki] 
void ComputeSU3CGs(const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0, vector<uint16_t>& indices, vector<double>& su3cgs)
{
	su3cgs.clear();
//	NOTE: Lf ranges from 0 up to (lm + mu) ---> number of Lf == max_Lf
	int max_Lf = wf.lm + wf.mu + 1;
	int max_Li = wi.lm + wi.mu + 1;

	double dCG[MAX_K][MAX_K][MAX_K][MAX_K]; // this array is required by su3lib function wu3r3w
	memset(dCG, sizeof(dCG), 0);

	vector<uint16_t> temp_indices(max_Lf*max_Li, kDoesNotExist);
//	Estimation of maximal size of temp_su3cgs:
	double temp_su3cgs[4*MAX_K*MAX_K*MAX_K]; 

    int lmf(wf.lm), muf(wf.mu), lmi(wi.lm), mui(wi.mu), lm0(w0.lm), mu0(w0.mu);
	int max_kf, max_ki, max_k0, max_rho;
	int nCGs;


	for (int Lf = 0; Lf < max_Lf; Lf++) // iterate over all Lf in (lmf muf)
	{
		if (!SU3::kmax(wf, Lf))
		{
			continue;
		}

		for (int Li = 0; Li < max_Li; Li++) // iterate over all Li in (lmi mui)
		{
			if (!SU3::kmax(wi, Li))
			{
				continue;
			}
			nCGs = 0;
			for (int L0 = abs(Lf-Li); L0 <= std::min(2, Lf+Li); L0++)
			{
				if (!SU3::kmax(w0, L0))
				{
					continue;
				}
#ifndef AIX			
				wu3r3w_(lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#else
				wu3r3w (lmf, muf, lmi, mui, lm0, mu0, Lf, Li, L0, max_rho, max_kf, max_ki, max_k0, dCG);
#endif
			   	for (int k0 = 0; k0 < max_k0; k0++) // for all values of k0
				{ 
		    		for (int irho = 0; irho < max_rho; irho++) // for all possible multiplicities rho0
					{
						for (int kf = 0; kf < max_kf; kf++)  // for all values of kf
			   			{
							for (int ki = 0; ki < max_ki; ki++) // for all values of ki
			   		    	{
								temp_su3cgs[nCGs++] = dCG[k0][ki][kf][irho]; // store SU(3) wigner coefficient
	    				    }
			   			}
    	    		}
	    		}
			}
			if (nCGs)
			{
				temp_indices[Lf*max_Li + Li] = su3cgs.size();
				su3cgs.insert(su3cgs.end(), &temp_su3cgs[0], &temp_su3cgs[0] + nCGs);
			}
		}
	}
	temp_indices.swap(indices);
}

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName)
{
	std::ifstream file(sInteractionFileName.c_str(), std::ifstream::binary);
	if (!file)
	{ std::cout << "Could not open file '" << sInteractionFileName << "'!" << std::endl;
		exit (EXIT_FAILURE);
	}
	cout << "Start reading interaction file ... "; cout.flush();

	int na, nb, nc, nd;
	uint32_t n_elems;
	while (true)
	{
		int i1, i2, i3, i4, J;
		DOUBLE Vpp, Vnn, Vpn;
		file.read((char*)&na, sizeof(int));
		file.read((char*)&nb, sizeof(int));
		file.read((char*)&nc, sizeof(int));
		file.read((char*)&nd, sizeof(int));
		file.read((char*)&n_elems, sizeof(uint32_t));

		if (!file) 
		{
			break;
		}

		I1_t vec_i1 = new uint16_t[n_elems]; 
		I2_t vec_i2 = new uint16_t[n_elems]; 
		I3_t vec_i3 = new uint16_t[n_elems]; 
		I4_t vec_i4 = new uint16_t[n_elems]; 
		J_t  vec_J  = new uint8_t[n_elems];
		VPP_t vec_Vpp = new double[n_elems]; 
		VNN_t vec_Vnn = new double[n_elems]; 
		VPN_t vec_Vpn = new double[n_elems]; 

		file.read((char*)vec_i1, n_elems*sizeof(uint16_t));
		file.read((char*)vec_i2, n_elems*sizeof(uint16_t));
		file.read((char*)vec_i3, n_elems*sizeof(uint16_t));
		file.read((char*)vec_i4, n_elems*sizeof(uint16_t));
		file.read((char*)vec_J, n_elems*sizeof(uint8_t));
		file.read((char*)vec_Vpp, n_elems*sizeof(double));
		file.read((char*)vec_Vnn, n_elems*sizeof(double));
		file.read((char*)vec_Vpn, n_elems*sizeof(double));

		NANBNCND nanbncnd;
		nanbncnd[kNA] = na;
		nanbncnd[kNB] = nb;
		nanbncnd[kNC] = nc;
		nanbncnd[kND] = nd;


		INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
		if (it == interaction_data_.end())
		{
			interaction_data_.insert(std::make_pair(nanbncnd, cpp0x::make_tuple(n_elems, vec_i1, vec_i2, vec_i3, vec_i4, vec_J, vec_Vpp, vec_Vnn, vec_Vpn)));
		}
		else
		{
			// This should never happen
			std::cerr << "The same set of {na, nb, nc, nd}={" << na << ", " << nb << ", " << nc << ", " << nc << "} occurs multiple times in the input file '" << sInteractionFileName << "' !!!" << std::endl;
			exit(EXIT_FAILURE);
		}
	}
	cout << "Done" << endl;
}

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::ME_DATA JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::GetMe(NANBNCND nanbncnd)
{
	INTERACTION_DATA::iterator it = interaction_data_.find(nanbncnd);
	if (it != interaction_data_.end())
	{
		return it->second;
	}
	else
	{
		return cpp0x::make_tuple(0, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);
	}
}

// This function multiplies all matrix elements by a factor
// provided by the first line of equation (1) in devel/docs/Recoupling/Recoupling.pdf
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::MultiplyByCommonFactor()
{
	INTERACTION_DATA::iterator it = interaction_data_.begin();
	for (; it != interaction_data_.end(); ++it)
	{
		ME_DATA me_data = it->second;
		SIZE_t size = cpp0x::get<kSize>(me_data);
		I1_t i1 = cpp0x::get<kI1>(me_data);
		I2_t i2 = cpp0x::get<kI2>(me_data);
		I3_t i3 = cpp0x::get<kI3>(me_data);
		I4_t i4 = cpp0x::get<kI4>(me_data);
		J_t  J   = cpp0x::get<kJ>(me_data);
		VPP_t Vpp  = cpp0x::get<kVpp>(me_data);
		VNN_t Vnn  = cpp0x::get<kVnn>(me_data);
		VPN_t Vpn  = cpp0x::get<kVpn>(me_data);

		for (int i = 0; i < size; ++i)
		{
			int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

			Get_nlj(i1[i], na, la, jja);
			Get_nlj(i2[i], nb, lb, jjb);
			Get_nlj(i3[i], nc, lc, jjc);
			Get_nlj(i4[i], nd, ld, jjd);
			JJ = 2*J[i];

			double common_phase = MINUSto(nd + nc - (jjd + jjc)/2)*SQRT(jja + 1)*SQRT(jjb + 1)*SQRT(jjc + 1)*SQRT(jjd + 1)*(JJ + 1);
			//	delta_{na nb}{la lb}{ja jb}{ta tb}
			int iab = (na == nb) && (la == lb) && (jja == jjb);
			//	delta_{nc nd}{lc ld}{jc jd}{tc td}
			int	idc = (nd == nc) && (ld == lc) && (jjd == jjc);

			Vpp[i] *= common_phase*SQRT((iab + 1)*(idc + 1));
			Vnn[i] *= common_phase*SQRT((iab + 1)*(idc + 1));
			Vpn[i] *= common_phase;
		}
	}
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::SwitchHOConvention()
{
	INTERACTION_DATA::iterator it = interaction_data_.begin();
	for (; it != interaction_data_.end(); ++it)
	{
		ME_DATA me_data = it->second;

		SIZE_t size = cpp0x::get<kSize>(me_data);
		I1_t i1 = cpp0x::get<kI1>(me_data);
		I2_t i2 = cpp0x::get<kI2>(me_data);
		I3_t i3 = cpp0x::get<kI3>(me_data);
		I4_t i4 = cpp0x::get<kI4>(me_data);
		VPP_t Vpp  = cpp0x::get<kVpp>(me_data);
		VNN_t Vnn  = cpp0x::get<kVnn>(me_data);
		VPN_t Vpn  = cpp0x::get<kVpn>(me_data);

		for (int i = 0; i < size; ++i)
		{
			int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

			Get_nlj(i1[i], na, la, jja);
			Get_nlj(i2[i], nb, lb, jjb);
			Get_nlj(i3[i], nc, lc, jjc);
			Get_nlj(i4[i], nd, ld, jjd);

			int phase = MINUSto((na - la + nb - lb + nc - lc + nd - ld)/2);

			Vpp[i] *= phase;
			Vnn[i] *= phase;
			Vpn[i] *= phase;
		}
	}
}

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::Show()
{
	cout << "na   nb   nc   nd" << endl;
	INTERACTION_DATA::iterator it = interaction_data_.begin();
	for (; it != interaction_data_.end(); ++it)
	{
		NANBNCND nanbncnd = it->first;
		ME_DATA me_data = it->second;

		cout << (int)nanbncnd[kNA] << "    " << (int)nanbncnd[kNB] << "    " << (int)nanbncnd[kNC] << "    " << (int)nanbncnd[kND] << endl;

		SIZE_t size = cpp0x::get<kSize>(me_data);
		I1_t i1 = cpp0x::get<kI1>(me_data);
		I2_t i2 = cpp0x::get<kI2>(me_data);
		I3_t i3 = cpp0x::get<kI3>(me_data);
		I4_t i4 = cpp0x::get<kI4>(me_data);
		J_t  J   = cpp0x::get<kJ>(me_data);
		VPP_t Vpp  = cpp0x::get<kVpp>(me_data);
		VNN_t Vnn  = cpp0x::get<kVnn>(me_data);
		VPN_t Vpn  = cpp0x::get<kVpn>(me_data);

		for (int i = 0; i < size; ++i)
		{
			int na, la, jja, nb, lb, jjb, nc, lc, jjc, nd, ld, jjd, JJ;

			Get_nlj(i1[i], na, la, jja);
			Get_nlj(i2[i], nb, lb, jjb);
			Get_nlj(i3[i], nc, lc, jjc);
			Get_nlj(i4[i], nd, ld, jjd);
			cout << "\t\t";
/*			
			cout << i1[i] << " " << i2[i] << " " << i3[i] << " " << i4[i] << " J:" << (int)J[i] << "\t\t\t";
*/			

			cout << la << " " << lb << " " << lc << " " << ld << "\t\t";
			cout << (int)J[i] << "\t\t" << jja << "/2 " << jjb << "/2 " << jjc << "/2 " << jjd << "/2\t\t";

			cout << Vpp[i] << " " << Vnn[i] << " " << Vpn[i] << endl;
		}
	}
}

void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell, const int maxShell, vector<NANBNCND>& ho_shells_combinations)
{
	ho_shells_combinations.resize(0);

	NANBNCND na_nb_nc_nd;
	for (int na = 0; na <= maxShell; ++na)
	{
		for (int nb = 0; nb <= maxShell; ++nb)
		{
			int adxad_Nhw = 0;
			if (na > valence_shell)
			{
				adxad_Nhw += (na - valence_shell);
			}
			if (nb > valence_shell)
			{
				adxad_Nhw += (nb - valence_shell);
			}
			if (adxad_Nhw > Nmax)
			{
				continue;
			}
			na_nb_nc_nd[kNA] = na;
			na_nb_nc_nd[kNB] = nb;

			for (int nc = 0; nc <= maxShell; ++nc)
			{
				for (int nd = 0; nd <= maxShell; ++nd)
				{
					int taxta_Nhw = 0;
					if (nc > valence_shell)
					{
						taxta_Nhw += (nc - valence_shell);
					}
					if (nd > valence_shell)
					{
						taxta_Nhw += (nd - valence_shell);
					}
					if (taxta_Nhw > Nmax)
					{
						continue;
					}
					na_nb_nc_nd[kNC] = nc;
					na_nb_nc_nd[kND] = nd;
					ho_shells_combinations.push_back(na_nb_nc_nd);	//	in such case we do not need 
				}
			}
		}
	}
	// shrink vector to its proper size to save some memory	
	vector<NANBNCND>(ho_shells_combinations.begin(), ho_shells_combinations.end()).swap(ho_shells_combinations);
}

double GetWigner9j(int a2, int b2, int c2, int f2, int g2, int h2, int j2)
{
	static std::map<CTuple<int, 2>, double> wig9js;
	static CTuple<int, 2> key;

//	set key
	key[0] = (a2 << 24) | (b2 << 16) | (c2 << 8) | f2;
	key[1] =              (g2 << 16) | (h2 << 8) | j2;

	std::map<CTuple<int, 2>, double>::const_iterator wig9j_to_be_found = wig9js.find(key);
	if (wig9j_to_be_found == wig9js.end())
	{
		double d = wigner9j(a2, b2, c2, 1, 1, f2, g2, h2, j2);
		wig9js.insert(std::make_pair(key, d));
		return d;
	}
	else
	{
		return wig9j_to_be_found->second;
	}
}

double GetWigner6j(int a2, int b2, int c2, int d2, int e2, int f2)
{
	static std::map<CTuple<int, 2>, double> wig6js;
	static CTuple<int, 2> key;

//	set key
	key[0] = (a2 << 24) | (b2 << 16) | (c2 << 8) | d2;
	key[1] =                           (e2 << 8) | f2;

	std::map<CTuple<int, 2>, double>::const_iterator wig6j_to_be_found = wig6js.find(key);
	if (wig6j_to_be_found == wig6js.end())
	{
		double d = wigner6j(a2, b2, c2, d2, e2, f2);
		wig6js.insert(std::make_pair(key, d));
		return d;
	}
	else
	{
		return wig6j_to_be_found->second;
	}
}

const int SfSiS0[6][3] =  {{0, 0, 0}, {1, 1, 0}, {0, 1, 1}, {1, 0, 1}, {1, 1, 1},      {1, 1, 2}};
const double PiSfSiS0[6] = {   1,         3,         3,         3,    sqrt(27),         sqrt(45)};

void ComputeTensorStrenghts(	const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0, 
								const uint32_t size, const uint16_t* i1, const uint16_t* i2, const  uint16_t* i3, const uint16_t* i4, const uint8_t* J, const double* Vpp, const double* Vnn, const double* Vpn,
								const uint16_t* lalb_indices, const uint16_t* ldlc_indices, const uint16_t* lfli_indices, 
								const double* abf_cgSU3, const double* dci_cgSU3, const double* fi0_cgSU3, 
								std::vector<double>* results)
{
	int iab_cgs, idc_cgs0, index_LfLi;
	int na, nb, nc, nd, la, lb, lc, ld, jja, jjb, jjc, jjd, JJ;
//	Lf_max: number of possible values Lf can have in wf irrep.	
	int Lf_max(wf.lm + wf.mu + 1);
//	Li_max: number of possible values Li can have in wi irrep.	
	int Li_max(wi.lm + wi.mu + 1);
	int ki_max, kf_max, k0_max;
	int rho0_max(w0.rho);
	int phase;
	double phaseSU2;

	double su3cg_sum[MAX_K*MAX_K];	// index = (k0*rho0_max + rho0) ---->  su3cg_sum[k0, rho0]

//	loop over {na la ja nb lb jb nc lc jc nd ld jd J} ---> {Vpp, Vnn, Vpn}
	for (int i = 0; i < size; i++)
	{
/*		
		cout << "i1:" << (int) i1i2i3i4J[ilabel] << " ";
		cout << "i2:" << (int) i1i2i3i4J[ilabel+1] << " ";
		cout << "i3:" << (int) i1i2i3i4J[ilabel+2] << " ";
		cout << "i4:" << (int) i1i2i3i4J[ilabel+3] << " ";
		cout << "J:" << (int) i1i2i3i4J[ilabel+4] << endl;
*/
		Get_nlj(i1[i], na, la, jja);
		Get_nlj(i2[i], nb, lb, jjb);
		Get_nlj(i3[i], nc, lc, jjc);
		Get_nlj(i4[i], nd, ld, jjd);
		JJ =  2*J[i];

		iab_cgs = lalb_indices[la*(nb+1) + lb]; // abf_cgSU3[iab_cgs] ---> <(na 0) 0 la; (nb 0) 0 lb || wf * *>_{0}
		if (iab_cgs == kDoesNotExist)
		{
			continue;
		}
		idc_cgs0 = ldlc_indices[ld*(nc+1) + lc]; // dci_cgSU3[idc_cgs] ---> <(0 nd) 0 ld; (0 nc) 0 lc || wi * *>_{0}
		if (idc_cgs0 == kDoesNotExist)
		{
			continue;
		}

		for (int Lf = abs(la-lb); (Lf <= (la+lb) && Lf < Lf_max); ++Lf, iab_cgs += kf_max)
		{
			kf_max = SU3::kmax(wf, Lf);
			if (!kf_max)
			{
				continue;
			}

			for (int Li = abs(ld-lc), idc_cgs = idc_cgs0; (Li <= (ld+lc) && Li < Li_max); ++Li, idc_cgs += ki_max)
			{
				ki_max = SU3::kmax(wi, Li);
				if (!ki_max)
				{
					continue;
				}

				index_LfLi = lfli_indices[Lf*Li_max + Li]; // fi0_cgSU3[index_LfLi] ---> <wf * Lf; wi * Li || w0 * S0min>_{*}
				if (index_LfLi == kDoesNotExist)
				{
					continue;
				}

//				for (int S0 = 0; S0 <= 2; ++S0, index_LfLi += k0_max*rho0_max*kf_max*ki_max)
				for (int S0 = abs(Lf-Li); S0 <= 2 && S0 <= (Lf+Li); ++S0, index_LfLi += k0_max*rho0_max*kf_max*ki_max)
				{
					k0_max = SU3::kmax(w0, S0);
					if (!k0_max)
					{
						continue;
					}

					for (int k0 = 0, ik0rho0 = 0, j = 0; k0 < k0_max; k0++)
					{
						for (int rho0 = 0; rho0 < rho0_max; rho0++, ik0rho0++)
						{
							su3cg_sum[ik0rho0] = 0;
							for (int kf = 0; kf < kf_max; kf++)
							{
								double sum_ki_result(0);
								for (int ki = 0; ki < ki_max; ki++, j++)
								{
									sum_ki_result += fi0_cgSU3[index_LfLi+j] * dci_cgSU3[idc_cgs+ki];
								}
								su3cg_sum[ik0rho0] += sum_ki_result * abf_cgSU3[iab_cgs+kf];
							}
						}
					}

//					S0 == 0 ==> int iS = 0
//					S0 == 1 ==> int iS = 2;
//					S0 == 2 ==> int iS = 5;
					for (int iS = ((S0 == 1)*2 + (S0 == 2)*5); S0 == SfSiS0[iS][2] && iS < 6; ++iS)
					{
						int Sf = SfSiS0[iS][0];
						int Si = SfSiS0[iS][1];
						if (!SU2::mult(2*Lf, 2*Sf, JJ)) // wigner9j(la, lb, Lf, 1, 1, Sf, ja, jb, J) = 0
						{
							continue;
						}
						if (!SU2::mult(2*Li, 2*Si, JJ)) // wigner9j(ld, lc, Li, 1, 1, Si, jd, jc, J) = 0
						{
							continue;
						}
						phaseSU2 = PiSfSiS0[iS]*MINUSto(Sf + S0 + Li)*SQRT(2*Li+1)*SQRT(2*Lf+1);
						phaseSU2*= GetWigner9j(2*la, 2*lb, 2*Lf, 2*Sf, jja, jjb, JJ)*GetWigner9j(2*ld, 2*lc, 2*Li, 2*Si, jjd, jjc, JJ)*GetWigner6j(2*Lf, 2*Li, 2*S0, 2*Si, 2*Sf, JJ);
/*
						cout << "Sf:" << Sf << " Si:" << Si << " S0:" << S0 << " Lf:" << Lf << " Li:" << Li << " ime:" << ime << "\t phaseSU2:" << phaseSU2 << " su3cg_sum[0]:" << su3cg_sum[0] << " vpn:" << vpn;
						cout <<"\t\t\ttot:" << phaseSU2*su3cg_sum[0]*vpn << endl;
*/
						for (int k0 = 0, ik0rho0 = 0, j = 0; k0 < k0_max; ++k0)
						{
							for (int rho0 = 0; rho0 < rho0_max; ++rho0, ik0rho0++, j += 3)
							{
								results[iS][j]   += phaseSU2*su3cg_sum[ik0rho0]*Vpp[i];
								results[iS][j+1] += phaseSU2*su3cg_sum[ik0rho0]*Vnn[i];
								results[iS][j+2] += phaseSU2*su3cg_sum[ik0rho0]*Vpn[i];
							}
						}
					}
				}
			}
		}
	}
}

void ComputeTensorStrenghts2(	const SU3::LABELS& wf, const SU3::LABELS& wi, const SU3::LABELS& w0, 
								const uint32_t size, const uint16_t* i1, const uint16_t* i2, const  uint16_t* i3, const uint16_t* i4, const uint8_t* J, const double* Vpp, const double* Vnn, const double* Vpn,
								const uint16_t* lalb_indices, const uint16_t* ldlc_indices, const uint16_t* lfli_indices, 
								const double* abf_cgSU3, const double* dci_cgSU3, const double* fi0_cgSU3, 
								std::vector<double>* results)
{
	int index_LfLi;
	int na, nb, nc, nd, la, lb, lc, ld, jja, jjb, jjc, jjd, JJ;
//	Lf_max: number of possible values Lf can have in wf irrep.	
	int Lf_max(wf.lm + wf.mu + 1);
//	Li_max: number of possible values Li can have in wi irrep.	
	int Li_max(wi.lm + wi.mu + 1);
	int ki_max, kf_max, k0_max;
	int rho0_max(w0.rho);
	int phase;
	double phaseSU2;

	uint32_t nthreads = size;

//	loop over {na la ja nb lb jb nc lc jc nd ld jd J} ---> {Vpp, Vnn, Vpn}
	for (int ithread = 0; ithread < nthreads; ithread++)
	{
/*		
		cout << "i1:" << (int) i1i2i3i4J[ilabel] << " ";
		cout << "i2:" << (int) i1i2i3i4J[ilabel+1] << " ";
		cout << "i3:" << (int) i1i2i3i4J[ilabel+2] << " ";
		cout << "i4:" << (int) i1i2i3i4J[ilabel+3] << " ";
		cout << "J:" << (int) i1i2i3i4J[ilabel+4] << endl;
*/
		Get_nlj(i1[ithread], na, la, jja);
		Get_nlj(i2[ithread], nb, lb, jjb);
		Get_nlj(i3[ithread], nc, lc, jjc);
		Get_nlj(i4[ithread], nd, ld, jjd);
		JJ =  2*J[ithread];

		int iab_cgs0 = lalb_indices[la*(nb+1) + lb]; // abf_cgSU3[iab_cgs] ---> <(na 0) 0 la; (nb 0) 0 lb || wf * *>_{0}
		int idc_cgs0 = ldlc_indices[ld*(nc+1) + lc]; // dci_cgSU3[idc_cgs] ---> <(0 nd) 0 ld; (0 nc) 0 lc || wi * *>_{0}

		for (int iS = 0; iS < 6; iS++)
		{
			int S0 = SfSiS0[iS][2];
			k0_max = SU3::kmax(w0, S0);

			float local_results[2*16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

			if (idc_cgs0 != kDoesNotExist && iab_cgs0 != kDoesNotExist && k0_max)
			{
				int Sf = SfSiS0[iS][0];
				int Si = SfSiS0[iS][1];

				for (int Lf = abs(la-lb), iab_cgs = iab_cgs0; (Lf <= (la+lb) && Lf < Lf_max); ++Lf, iab_cgs += kf_max)
				{
					kf_max = SU3::kmax(wf, Lf);
					if (!kf_max)
					{
						continue;
					}

					if (!SU2::mult(2*Lf, 2*Sf, JJ)) // wigner9j(la, lb, Lf, 1, 1, Sf, ja, jb, J) = 0
					{
						continue;
					}
					double wig1 = GetWigner9j(2*la, 2*lb, 2*Lf, 2*Sf, jja, jjb, JJ);

					for (int Li = abs(ld-lc), idc_cgs = idc_cgs0; (Li <= (ld+lc) && Li < Li_max); ++Li, idc_cgs += ki_max)
					{
						ki_max = SU3::kmax(wi, Li);
						if (!ki_max)
						{
							continue;
						}
						if (S0 < abs(Lf-Li) || S0 > (Lf+Li))
						{
							continue;
						}
						if (!SU2::mult(2*Li, 2*Si, JJ)) // wigner9j(ld, lc, Li, 1, 1, Si, jd, jc, J) = 0
						{
							continue;
						}

						index_LfLi = lfli_indices[Lf*Li_max + Li]; // fi0_cgSU3[index_LfLi] ---> <wf * Lf; wi * Li || w0 * S0min>_{*}
						if (index_LfLi == kDoesNotExist)
						{
							continue;
						}

						// at this momnet index_LfLi points to first SU(3) CG with S0min, which may be less then S0
						// ==> we need to move it so it points to SU(3) CG corresponding to S0
						for (int s0 = abs(Lf-Li); s0 < S0; s0++)
						{
							index_LfLi += SU3::kmax(w0, s0)*rho0_max*kf_max*ki_max; 
						}

						phaseSU2 = PiSfSiS0[iS]*MINUSto(Sf + S0 + Li)*SQRT(2*Li+1)*SQRT(2*Lf+1);
						phaseSU2*= wig1*GetWigner9j(2*ld, 2*lc, 2*Li, 2*Si, jjd, jjc, JJ)*GetWigner6j(2*Lf, 2*Li, 2*S0, 2*Si, 2*Sf, JJ);

						for (int i = 0, j = 0; i < rho0_max*k0_max; ++i)
						{
							double su3cg_sum = 0;
							for (int kf = 0; kf < kf_max; kf++)
							{
								double sum_ki_result(0);
								for (int ki = 0; ki < ki_max; ki++, j++)
								{
									sum_ki_result += fi0_cgSU3[index_LfLi+j] * dci_cgSU3[idc_cgs+ki];
								}
								su3cg_sum += sum_ki_result * abf_cgSU3[iab_cgs+kf];
							}
							local_results[i] += phaseSU2*su3cg_sum;
						}
					} // Li
				} // Lf
			} // if (idc_cgs0 != kDoesNotExist && iab_cgs0 != kDoesNotExist && k0_max)

			for (int j = 0, i = 0; j < k0_max*rho0_max; j++, i += 3)
			{
				results[iS][i]   += (double)local_results[j]*Vpp[ithread];
				results[iS][i+1] += (double)local_results[j]*Vnn[ithread];
				results[iS][i+2] += (double)local_results[j]*Vpn[ithread];
			}
		} // iS
	} // ithread
}



template<typename SelectionRules>
void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::PerformSU3Decomposition(JCoupledProtonNeutron2BMe_Hermitian_SingleOperator& JCoupledPNME, int Nmax, int valence_shell, int nmax, const string& sOutputFileName)
{
#ifdef TIMING	
	boost::chrono::system_clock::time_point start;
	boost::chrono::duration<double> total_duration_SU3_1(0),total_duration_SU3_2(0), total_duration_SU3_3(0), total_duration_transformation(0), duration;
#endif

	std::ofstream outfile(sOutputFileName.c_str());
	if (!outfile)
	{
		std::cerr << "Unable to open output file '" << sOutputFileName << "'" << std::endl;
		exit(EXIT_FAILURE);
	}
	outfile.precision(10);

	SelectionRules IsIncluded;

	enum {kA = 0, kB = 1, kC = 2, kD = 3};

	vector<uint16_t> lalb_indices; // (la, lb): lalb_indices[la*lb_max + lb] ---> <(na 0) la; (nb 0) lb || wf * *>
	vector<uint16_t> ldlc_indices; // (ld, lc): lalb_indices[ld*lc_max + lc] ---> <(0 nd) ld; (0 nc) lc || wi * *>
	vector<uint16_t> lfli_indices;// (Lf, Li): lflf_indices[Lf*Li_max + Li] ---> {<wf * Lf; wi * Li || w0 * L0=0>_{*}, <wf * Lf; wi * Li || w0 * L0=1>_{*}, <wf * Lf; wi * Li || w0 * L0=2>_{*} }

	vector<double> abf_cgSU3; // <(na 0) * (nb 0) * || (lm mu)f *>
	vector<double> dci_cgSU3; // <(0 nd) * (0 nc) * || (lm mu)i *>
	vector<double> fi0_cgSU3; // <wf * wi * || w0 * L0={0,1,2}>_{*}

//	
	abf_cgSU3.reserve(1024);
	dci_cgSU3.reserve(1024);
	fi0_cgSU3.reserve(1024);

	vector<NANBNCND> ho_shells_combinations;
	GenerateAllCombinationsHOShells(Nmax, valence_shell, nmax, ho_shells_combinations);

	SU3::LABELS ir[4] = {SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0), SU3::LABELS(0, 0)};

	SU3_VEC FinalIrreps;	//	list of (lmf muf) irreps resulting from (na 0) x (nb 0)
	SU3_VEC InitIrreps;		//	list of (lmi mui) irreps resulting from (0 nd) x (0 nc)

	FinalIrreps.reserve(nmax + 1); // (na 0) x (nb 0) ---> max(na, nb) SU(3) irreps
	InitIrreps.reserve(nmax + 1); 
//	iterate over {na, nb, nc, nd} HO shell quantum numbers
	for (int ishells = 0; ishells < ho_shells_combinations.size(); ++ishells)
	{
		ir[kA].lm = ho_shells_combinations[ishells][kA];
		ir[kB].lm = ho_shells_combinations[ishells][kB];
		ir[kC].mu = ho_shells_combinations[ishells][kC];
		ir[kD].mu = ho_shells_combinations[ishells][kD];

		int n1 = ir[kA].lm;
		int n2 = ir[kB].lm;
		int n3 = ir[kD].mu;
		int n4 = ir[kC].mu;

		if (!IsIncluded(n1, n2, n3, n4)) // operator does not have this combination of n1 n2 n3 n4
		{
			continue;
		}

		double* meV;
		uint16_t* labels;
		int me_phase;
//		Obtain a set of matrix elements {<na* nb*|| {Vpp, Vnn, Vpn} || nc* nd*>}
		ME_DATA me_data = JCoupledPNME.GetMe(ho_shells_combinations[ishells]);

		SIZE_t size = cpp0x::get<kSize>(me_data);
//		cout << "size:" << size << endl;
		I1_t i1 = cpp0x::get<kI1>(me_data);
		I2_t i2 = cpp0x::get<kI2>(me_data);
		I3_t i3 = cpp0x::get<kI3>(me_data);
		I4_t i4 = cpp0x::get<kI4>(me_data);
		J_t  J   = cpp0x::get<kJ>(me_data);
		VPP_t Vpp  = cpp0x::get<kVpp>(me_data);
		VNN_t Vnn  = cpp0x::get<kVnn>(me_data);
		VPN_t Vpn  = cpp0x::get<kVpn>(me_data);

		if (size == 0)
		{
			continue;
		}

		std::cout << ir[kA] << " " << ir[kB] << " " << ir[kC] << " " << ir[kD] << "\n";
	
		FinalIrreps.clear(); InitIrreps.clear();
		SU3::Couple(ir[kA], ir[kB], FinalIrreps);	//	(na 0) x (nb 0) -> rho=1{(lmf muf)}
		SU3::Couple(ir[kD], ir[kC], InitIrreps);	//	(0 nd) x (0 nc) -> rho=1{(lmi mui)}	

//	iterate over (lmf muf) irreps		
		for (int iif = 0; iif < FinalIrreps.size(); ++iif)
		{
			ComputeSU3CGs_Simple(ir[kA], ir[kB], FinalIrreps[iif], lalb_indices, abf_cgSU3);

//	iterate over (lmi mui) irreps		
			for (int ii = 0; ii < InitIrreps.size(); ++ii)
			{
				ComputeSU3CGs_Simple(ir[kD], ir[kC], InitIrreps[ii], ldlc_indices, dci_cgSU3);

				SU3_VEC Irreps0;
				SU3::Couple(FinalIrreps[iif], InitIrreps[ii], Irreps0); // (lmf muf) x (lmi mui) -> rho_{0} (lm0 mu0)
//	iterate over rho(lmu0 mu0)
				for (int i0 = 0; i0 < Irreps0.size(); ++i0)
				{
					if (!IsIncluded(Irreps0[i0])) // Irrep0 does not have tensorial character of the operator
					{
						continue;
					}

					cout << FinalIrreps[iif] << "\t" << InitIrreps[ii] << "\t" << Irreps0[i0]; cout.flush();

#ifdef TIMING	
					start = boost::chrono::system_clock::now();
#endif					
					ComputeSU3CGs(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], lfli_indices, fi0_cgSU3);
#ifdef TIMING	
					duration = boost::chrono::system_clock::now() - start;
					total_duration_SU3_3 += duration;
					cout << "\t" << duration; cout.flush();
#endif					
/*	
 *	Scalar opetator [i.e. J0=0] has L0==S0. Since we are dealing with
 *	a two-body interaction ==> S0 = 0, 1, or 2.
 *	S0=0 appears 2 times: Sf=0 x Si=0 --> S0=0 and Sf=1 x Si=1 --> S0=0
 *	==> 2 x SU3::kmax(w0, L0=0) x rho0_max x 3
 *
 *	S0=1 appears 3 times: Sf=1 x Si=0 --> S0=1; Sf=0 x Si=1 --> S0=0; Sf=1 x Si=1 ---> S0=1
 *	==> 3 x SU3::kmax(w0, L0=1) x rho0_max x 3
 *
 *	S0=2 appears 1 times: Sf=1 x Si=1 --> S0=2
 *	==> 1 x SU3::kmax(w0, L0=1) x rho0_max x 3
 *
 *	Two body interaciton has L0=S0 and  can be either 0, 1, or 2.
 *
 */	
//					int nresults = (2*(SU3::kmax(Irreps0[i0], 0)) + 3*SU3::kmax(Irreps0[i0], 1) + SU3::kmax(Irreps0[i0], 2))*Irreps0[i0].rho*3;
//	Order of elements in results: results[iS, k0, rho0, type], where iS = 0 ... 6 [= number of Sf Si S0 quantum numbers for two-body interaction]
//	index = iS x k0_max x rho0_max x 3 + k0 x rho0_max x 3 + rho0 x 3 + type
					std::vector<double> results[6];
					results[0].resize(3*SU3::kmax(Irreps0[i0], 0)*Irreps0[i0].rho);
					results[1].resize(3*SU3::kmax(Irreps0[i0], 0)*Irreps0[i0].rho);
					results[2].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[3].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[4].resize(3*SU3::kmax(Irreps0[i0], 1)*Irreps0[i0].rho);
					results[5].resize(3*SU3::kmax(Irreps0[i0], 2)*Irreps0[i0].rho);
/*
					ComputeTensorStrenghts(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], \
											meV, labels, number_me_triplets, me_phase, \
											&lalb_indices[0], &ldlc_indices[0], &lfli_indices[0], \
											&abf_cgSU3[0], &dci_cgSU3[0], &fi0_cgSU3[0], \
											results);
*/

#ifdef TIMING	
					start = boost::chrono::system_clock::now();
#endif					
					ComputeTensorStrenghts2(FinalIrreps[iif], InitIrreps[ii], Irreps0[i0], \
											size, i1, i2, i3, i4, J, Vpp, Vnn, Vpn, \
											&lalb_indices[0], &ldlc_indices[0], &lfli_indices[0], \
											&abf_cgSU3[0], &dci_cgSU3[0], &fi0_cgSU3[0], \
											results);
#ifdef TIMING	
					duration = boost::chrono::system_clock::now() - start;
					total_duration_transformation += duration;
					cout << "\t" << duration;
#endif					
					cout << endl;

					for (int iS = 0; iS < 6; ++iS)
					{
						int k0_max = SU3::kmax(Irreps0[i0], SfSiS0[iS][2]);
						if (!k0_max)
						{
							continue;
						}

						if (std::count_if(results[iS].begin(), results[iS].end(), Negligible) == 3*k0_max*Irreps0[i0].rho)
						{
							continue;
						}

						outfile << n1 << " " << n2 << " " << n3 << " " << n4;
						outfile << "\t\t" << FinalIrreps[iif] << " " << 2*SfSiS0[iS][0] << "\t" << InitIrreps[ii] << " " << 2*SfSiS0[iS][1] << "\t" << Irreps0[i0];
						outfile << " " << 2*SfSiS0[iS][2] << "\n";
						for (int k0 = 0, j = 0; k0 < k0_max; k0++)
						{
							for (int rho0 = 0; rho0 < Irreps0[i0].rho; ++rho0, j += 3)
							{
								if (Negligible(results[iS][j]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << results[iS][j] << " ";
								}

								if (Negligible(results[iS][j + 1]))
								{
									outfile << 0 << " ";
								}
								else
								{
									outfile << results[iS][j + 1] << " ";
								}

								if (Negligible(results[iS][j + 2]))
								{
									outfile << 0 << "\n";
								}
								else
								{
									outfile << results[iS][j + 2] << "\n";
								}
							}
						}
					}
				}
			}
		}
	}
#ifdef TIMING	
	cout << "Computing <wf wi || w0>:" << total_duration_SU3_3 << "\t" << "\t Transformation:" << total_duration_transformation << endl;
#endif
}

int main(int argc, char* argv[])
{
#ifdef USER_FRIENDLY_OUTPUT
	cerr << "You have to comment line with '#define USER_FRIENDLY_OUTPUT' in UNU3SU3basics.h !" << endl;
	return 1;
#endif	
/** \todo nmax could be obtained from input_file */	
	if (argc != 5)
	{
		cout << endl;
	  	cout << "Usage: " << argv[0] << " <Nmax> <valence shell> <file in proton-neutron J-coupled format> <output file with SU3 tensors>" << endl;
		cout << endl;
		cout << endl;
		cout << endl;
		cout << endl;
		cout << "Structure of the outputfile:" << endl;
		cout << "n1 n2 n3 n4 IRf IRi IR0 iS" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 0" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 0" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 0" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 0 k0 = 1" << endl;
		cout << "a_pp a_nn a_pn \t rho0 = 1 k0 = 1" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
		cout << " . " << endl;
		cout << " . " << endl;
		cout << "a_pp a_nn a_pn \t rho0 = rho0max k0 = 1" << endl;
		return 0;
	}

	int Nmax = atoi(argv[1]);
	int valence_shell = atoi(argv[2]);
	int nmax = Nmax + valence_shell;

//	Constructor of CBlocks calls blocks_() function of su3lib which is required
//	for the latter to work properly
	CBlocks blocks;	
	InitSqrtLogFactTables();
	
	string sFileName(argv[3]);
	string sFileSU3Decomposition(argv[4]);

	JCoupledProtonNeutron2BMe_Hermitian_SingleOperator JCoupledPNME(sFileName);

//	JCoupledPNME.Show();

	JCoupledPNME.SwitchHOConvention();
//	Multiply each matrix element by factor that depends on its quantum numbers
	JCoupledPNME.MultiplyByCommonFactor();


	JCoupledPNME.PerformSU3Decomposition<Cinteraction_sel_rules>(JCoupledPNME, Nmax, valence_shell, nmax, sFileSU3Decomposition);

	return EXIT_SUCCESS;
}

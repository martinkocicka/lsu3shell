$(eval $(begin-module))

################################################################
# unit definitions
################################################################

# module_units_h := 
# module_units_cpp-h := 
# module_units_f := 
module_programs_cpp := GenerateRME Interaction2SU3tensors_main RecoupleSU3Interaction RecoupleSU3InteractionVcoul EMObservables2SU3Tensors Interaction_proton_neutron_Jcoupled_2SU3tensors \
InteractionJTtoSU3

# module_programs_f :=
# module_generated :=

################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))

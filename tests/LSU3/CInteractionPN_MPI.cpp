#include <mpi.h>

#include <iostream>

#include <LSU3/CInteractionPN_MPI.h>

using namespace lsu3;

int main(int argc, char* argv[])
{
    int iProc, nProcs;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &iProc); 

    CInteractionPN cpn;

    for (int k = 1; k < argc; ++k)
        cpn.readFile(argv[k], MPI_COMM_WORLD);

    cpn.finalize(MPI_COMM_WORLD);

    CInteractionPN::Map map_p, map_n;

    cpn.get_map_p(map_p, MPI_COMM_WORLD);
    cpn.get_map_n(map_n, MPI_COMM_WORLD);

    MPI_Finalize();

    return 0;
}

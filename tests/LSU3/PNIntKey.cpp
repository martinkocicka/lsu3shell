#include <iostream>

#include <LSU3/types.h>

using namespace lsu3;

int main()
{
    HoShellsn1n2 nanb, ncnd;
    LmMuS2 lmmus2_p, lmmus2_n;

    get_n1(nanb) = -1;
    get_n2(nanb) =  3;

    get_lm(lmmus2_p) = 0;
    get_mu(lmmus2_p) = 1;
    get_S2(lmmus2_p) = 2;

    get_n1(ncnd) =  2;
    get_n2(ncnd) = -4;

    get_lm(lmmus2_n) = 1;
    get_mu(lmmus2_n) = 2;
    get_S2(lmmus2_n) = 3;

    LmMuS2 lmmus2;
    get_lm(lmmus2) = 5;
    get_mu(lmmus2) = 6;
    get_S2(lmmus2) = 2;

    NNLmMuS2 nnlmmus2_1, nnlmmus2_2;
    get_n1n2(nnlmmus2_1)   = nanb;
    get_LmMuS2(nnlmmus2_1) = lmmus2_p;

    get_n1n2(nnlmmus2_2)   = ncnd;
    get_LmMuS2(nnlmmus2_2) = lmmus2_n;

    PNIntKey key;
    get_NNLmMuS2_p(key) = nnlmmus2_1;
    get_NNLmMuS2_n(key) = nnlmmus2_2;
    get_LmMuS2(key)     = lmmus2;

    PNIntKey key2, key3;
    key2 = key;
    key3 = key;

    get_nc(key2) = -1;

    bool test;

    test = (key2 == key);
    test = (key2 < key);

    test = (key3 == key);
    test = (key3 < key);
    
    std::cout << (int)get_na(key)  << std::endl;
    std::cout << (int)get_nb(key)  << std::endl;
    std::cout << (int)get_lmp(key) << std::endl;
    std::cout << (int)get_mup(key) << std::endl;
    std::cout << (int)get_S2p(key) << std::endl;

    std::cout << (int)get_nc(key) << std::endl;
    std::cout << (int)get_nd(key) << std::endl;
    std::cout << (int)get_lmn(key) << std::endl;
    std::cout << (int)get_mun(key) << std::endl;
    std::cout << (int)get_S2n(key) << std::endl;

    std::cout << (int)get_lm0(key) << std::endl;
    std::cout << (int)get_mu0(key) << std::endl;
    std::cout << (int)get_S20(key) << std::endl;

    return 0;
}

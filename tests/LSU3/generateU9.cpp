#include <boost/mpi.hpp>
#include <boost/chrono.hpp> 

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <stdexcept>
#include <cmath>
#include <vector>
#include <stack>

#include <map>

#include <boost/archive/binary_oarchive.hpp> 
#include <boost/serialization/map.hpp>
#include <boost/serialization/array.hpp>

#include "U9U6U3tests.h"

using namespace std;

void GetUnique9lm( 	const CInteractionPN& interactionPN,
				const SU3xSU2::LABELS& bra, 
				const SU3xSU2::LABELS& ket,
				const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_p,
				const std::vector<std::pair<SU3xSU2::RME*, unsigned int> >& rme_index_n, 
				U9LABELS& key,
				U9LIST& counter)
{
	size_t nTensors;
	SU3xSU2::LABELS* tensor_labels;
	TENSOR_STRENGTH* coeffs;
	WigEckSU3SO3CGTable** su3so3cgTables;

	for (size_t iproton = 0; iproton < rme_index_p.size(); ++iproton)
	{
		for (size_t ineutron = 0; ineutron < rme_index_n.size(); ++ineutron)
		{
			nTensors = interactionPN.GetTensorsCoefficientsLabels(rme_index_p[iproton].second, rme_index_n[ineutron].second, tensor_labels, su3so3cgTables, coeffs);
			for (size_t irrep = 0; irrep < nTensors; ++irrep)
			{
				if (SU2::mult(ket.S2, tensor_labels[irrep].S2, bra.S2) && SU3::mult(ket, tensor_labels[irrep], bra)) 
				{
					key[6] = rme_index_p[iproton].first->Tensor;
					key[7] = rme_index_n[ineutron].first->Tensor;
					key[8] = SU3::LABELS(1, tensor_labels[irrep].lm, tensor_labels[irrep].mu);
					U9LIST::iterator it = counter.find(key);
					if (it == counter.end())
					{
						uint32_t n = SU3::mult(key[0], key[1], key[2]);
						n *= SU3::mult(key[6], key[7], key[8]);
						n *= SU3::mult(key[3], key[4], key[5]);
						n *= SU3::mult(key[0], key[6], key[3]);
						n *= SU3::mult(key[1], key[7], key[4]);
						n *= SU3::mult(key[2], key[8], key[5]);
						counter[key] = make_pair(n, 1);
					}
					else
					{
						it->second.second += 1;
					}
				}
			}
		}
	}
}

void GenerateU9(	const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					const unsigned int idiag, 
					const unsigned int jdiag,
					ofstream& output_file)
{
	vector<unsigned char> hoShells_n, hoShells_p;
	
	std::vector<CTensorGroup*> tensorGroupsPP, tensorGroupsNN;
	std::vector<CTensorGroup_ada*> tensorGroups_p_pn, tensorGroups_n_pn;

	vector<int> phasePP, phaseNN, phase_p_pn, phase_n_pn;

	unsigned char num_vacuums_J_distr_p;
	unsigned char num_vacuums_J_distr_n;
	std::vector<std::pair<CRMECalculator*, CTensorGroup::COEFF_DOUBLE*> > selected_tensorsPP, selected_tensorsNN;
	std::vector<std::pair<CRMECalculator*, unsigned int> > selected_tensors_p_pn, selected_tensors_n_pn;

	std::vector<RmeCoeffsSU3SO3CGTablePointers> rmeCoeffsPP, rmeCoeffsNN;
	std::vector<std::pair<SU3xSU2::RME*, unsigned int> > rme_index_p_pn, rme_index_n_pn;

	SingleDistribution distr_ip, distr_in, distr_jp, distr_jn;
	UN::SU3xSU2_VEC gamma_ip, gamma_in, gamma_jp, gamma_jn;
	SU3xSU2_VEC vW_ip, vW_in, vW_jp, vW_jn;

	unsigned char deltaP, deltaN;

	const uint32_t number_ipin_blocks = bra.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket.NumberOfBlocks();

	int32_t icurrentDistr_p, icurrentDistr_n; 
	int32_t icurrentGamma_p, icurrentGamma_n;

	U9LIST counter;
	U9LABELS key;

	for (unsigned int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		cout << "#ipin: " << ipin_block << "\ttotal #ipin: " << number_ipin_blocks << endl;

		if (bra.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
		uint32_t ip = bra.getProtonIrrepId(ipin_block);
		uint32_t in = bra.getNeutronIrrepId(ipin_block);

		SU3xSU2::LABELS w_ip(bra.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(bra.getNeutronSU3xSU2(in));

		uint16_t aip_max = bra.getMult_p(ip);
		uint16_t ain_max = bra.getMult_n(in);

		uint16_t ilastDistr_p(std::numeric_limits<uint16_t>::max()); 
		uint16_t ilastDistr_n(std::numeric_limits<uint16_t>::max()); 
		
		uint32_t ilastGamma_p(std::numeric_limits<uint32_t>::max()); 
		uint32_t ilastGamma_n(std::numeric_limits<uint32_t>::max()); 

		uint32_t last_jp(std::numeric_limits<uint32_t>::max());
		uint32_t last_jn(std::numeric_limits<uint32_t>::max());
//	loop over (jp, jn) pairs
		for (unsigned int jpjn_block = (idiag == jdiag) ? ipin_block : 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			if (ket.NumberOfStatesInBlock(jpjn_block) == 0)
			{
				continue;
			}
			uint32_t jp = ket.getProtonIrrepId(jpjn_block);
			uint32_t jn = ket.getNeutronIrrepId(jpjn_block);

			SU3xSU2::LABELS w_jp(ket.getProtonSU3xSU2(jp));
			SU3xSU2::LABELS w_jn(ket.getNeutronSU3xSU2(jn));

			uint16_t ajp_max = ket.getMult_p(jp);
			uint16_t ajn_max = ket.getMult_n(jn);

			if (jp != last_jp)
			{
				icurrentDistr_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
				icurrentGamma_p = ket.getIndex_p<lsu3::CncsmSU3xSU2Basis::kGamma>(jp);

				if (ilastDistr_p != icurrentDistr_p)
				{
					distr_ip.resize(0); bra.getDistr_p(ip, distr_ip);
					gamma_ip.resize(0); bra.getGamma_p(ip, gamma_ip);
					vW_ip.resize(0); bra.getOmega_p(ip, vW_ip);
				
					distr_jp.resize(0); ket.getDistr_p(jp, distr_jp);
					hoShells_p.resize(0);
					deltaP = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_ip, gamma_ip, vW_ip, distr_jp, hoShells_p, num_vacuums_J_distr_p, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn);
				}

				if (ilastGamma_p != icurrentGamma_p || ilastDistr_p != icurrentDistr_p)
				{
					if (deltaP <= 4)
					{
						if (!selected_tensorsPP.empty())
						{
							std::for_each(selected_tensorsPP.begin(), selected_tensorsPP.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsPP.resize(0);

						if (!selected_tensors_p_pn.empty())
						{
							std::for_each(selected_tensors_p_pn.begin(), selected_tensors_p_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_p_pn.resize(0);

						gamma_jp.resize(0); ket.getGamma_p(jp, gamma_jp);
						TransformGammaKet_SelectByGammas(hoShells_p, distr_jp, num_vacuums_J_distr_p, nucleon::PROTON, phasePP, tensorGroupsPP, phase_p_pn, tensorGroups_p_pn, gamma_ip, gamma_jp, selected_tensorsPP, selected_tensors_p_pn);
					}
				}
	
				if (deltaP <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsPP);
					Reset_rmeIndex(rme_index_p_pn);

					vW_jp.resize(0); ket.getOmega_p(jp, vW_jp);
					TransformOmegaKet_CalculateRME(distr_jp, gamma_ip, vW_ip, gamma_jp, num_vacuums_J_distr_p, selected_tensorsPP, selected_tensors_p_pn, vW_jp, rmeCoeffsPP, rme_index_p_pn);
				}
				ilastDistr_p = icurrentDistr_p;
				ilastGamma_p = icurrentGamma_p;
			}

			if (jn != last_jn)
			{
				icurrentDistr_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
				icurrentGamma_n = ket.getIndex_n<lsu3::CncsmSU3xSU2Basis::kGamma>(jn);

				if (ilastDistr_n != icurrentDistr_n)
				{
					distr_in.resize(0); bra.getDistr_n(in, distr_in);
					gamma_in.resize(0); bra.getGamma_n(in, gamma_in);
					vW_in.resize(0); bra.getOmega_n(in, vW_in); 

					distr_jn.resize(0); ket.getDistr_n(jn, distr_jn);
					hoShells_n.resize(0);
					deltaN = TransformDistributions_SelectByDistribution(interactionPPNN, interactionPN, distr_in, gamma_in, vW_in, distr_jn, hoShells_n, num_vacuums_J_distr_n, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn);
				}

				if (ilastGamma_n != icurrentGamma_n || ilastDistr_n != icurrentDistr_n)
				{
					if (deltaN <= 4)
					{
						if (!selected_tensorsNN.empty())
						{
							std::for_each(selected_tensorsNN.begin(), selected_tensorsNN.end(), CTensorGroup::DeleteCRMECalculatorPtrs());
						}
						selected_tensorsNN.resize(0);

						if (!selected_tensors_n_pn.empty())
						{
							std::for_each(selected_tensors_n_pn.begin(), selected_tensors_n_pn.end(), CTensorGroup_ada::DeleteCRMECalculatorPtrs());
						}
						selected_tensors_n_pn.resize(0);

						gamma_jn.resize(0); ket.getGamma_n(jn, gamma_jn);
						TransformGammaKet_SelectByGammas(hoShells_n, distr_jn, num_vacuums_J_distr_n, nucleon::NEUTRON, phaseNN, tensorGroupsNN, phase_n_pn, tensorGroups_n_pn, gamma_in, gamma_jn, selected_tensorsNN, selected_tensors_n_pn);
					}
				}

				if (deltaN <= 4)
				{
					Reset_rmeCoeffs(rmeCoeffsNN);
					Reset_rmeIndex(rme_index_n_pn);

					vW_jn.resize(0); ket.getOmega_n(jn, vW_jn);
					TransformOmegaKet_CalculateRME(distr_jn, gamma_in, vW_in, gamma_jn, num_vacuums_J_distr_n, selected_tensorsNN, selected_tensors_n_pn, vW_jn, rmeCoeffsNN, rme_index_n_pn);
				}	

				ilastDistr_n = icurrentDistr_n;
				ilastGamma_n = icurrentGamma_n;
			}

			//	loop over wpn that result from coupling ip x in	
			uint32_t ibegin = bra.blockBegin(ipin_block);
			uint32_t iend = bra.blockEnd(ipin_block);
			for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
			{
				SU3xSU2::LABELS omega_pn_I(bra.getOmega_pn(ip, in, iwpn));
				size_t afmax = aip_max*ain_max*omega_pn_I.rho;
				IRREPBASIS braSU3xSU2basis(bra.Get_Omega_pn_Basis(iwpn));
				
				bool isDiagonalBlock = (idiag == jdiag && ipin_block == jpjn_block); 
				uint32_t jbegin = (isDiagonalBlock) ? iwpn : ket.blockBegin(jpjn_block);
				uint32_t jend = ket.blockEnd(jpjn_block);
				for (int jwpn = jbegin; jwpn < jend; ++jwpn)
				{
					SU3xSU2::LABELS omega_pn_J(ket.getOmega_pn(jp, jn, jwpn));
					size_t aimax = ajp_max*ajn_max*omega_pn_J.rho;
					IRREPBASIS ketSU3xSU2basis(ket.Get_Omega_pn_Basis(jwpn));
	
					if (deltaP + deltaN <= 4)
					{
						if (!rme_index_p_pn.empty() && !rme_index_n_pn.empty())
						{
							key[0] = SU3::LABELS(1, w_jp.lm, w_jp.mu);
							key[1] = SU3::LABELS(1, w_jn.lm, w_jn.mu);
							key[2] = SU3::LABELS(1, omega_pn_J.lm, omega_pn_J.mu);

							key[3] = SU3::LABELS(1, w_ip.lm, w_ip.mu);
							key[4] = SU3::LABELS(1, w_in.lm, w_in.mu);
							key[5] = SU3::LABELS(1, omega_pn_I.lm, omega_pn_I.mu);
							GetUnique9lm(interactionPN, omega_pn_I, omega_pn_J, rme_index_p_pn, rme_index_n_pn, key, counter);
						}
					}
				}
			}
			last_jp = jp;
			last_jn = jn;
		}
	}
	Reset_rmeCoeffs(rmeCoeffsPP);
	Reset_rmeCoeffs(rmeCoeffsNN);

//	Print some information about 9-(l m) coefficients that are needed for
//	calculation of PN rmes
	uint32_t ncoeffs(0);
	uint32_t max_ncoeffs(0);
	for (U9LIST::iterator it = counter.begin(); it != counter.end(); ++it)
	{
		ncoeffs += it->second.first;
		if (it->second.first > max_ncoeffs)
		{
			max_ncoeffs = it->second.first;
		}
	}

	cout << "Number of unique 9lm for PN interaction: " << counter.size() << endl;
	cout << "Number of 9(lm) coeffs: " << ncoeffs << endl;
	cout << "Maximal size:" << max_ncoeffs << endl;

	boost::archive::binary_oarchive oa(output_file);
	oa << counter;

}

int main(int argc,char **argv)
{
 	boost::mpi::environment env(argc, argv);

 	boost::mpi::communicator mpi_comm_world;
	if (mpi_comm_world.size() > 1)
	{
		mpi_comm_world.abort(EXIT_FAILURE);
	}

	if (argc != 6)
	{
		cout << "Usage: "<< argv[0] <<" <file name with run parameters> <resulting U9 file name> <ndiag> <iblock> <jblock>" << endl;
		return EXIT_FAILURE;
	}

	unsigned int ndiag = atoi(argv[3]);
	int idiag = atoi(argv[4]);
	int jdiag = atoi(argv[5]);
	int my_rank(0); 

	InitSqrtLogFactTables();

	CRunParameters run_params;

	try
	{
		run_params.LoadRunParameters(argv[1]);
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
	   return EXIT_FAILURE;
	}

	lsu3::CncsmSU3xSU2Basis ket(run_params.GetModelSpace(), jdiag, ndiag);	
	lsu3::CncsmSU3xSU2Basis bra(run_params.GetModelSpace(), idiag, ndiag);	

	ofstream interaction_log_file("/dev/null");

	CBaseSU3Irreps baseSU3Irreps(run_params.Z(), run_params.N(), run_params.Nmax()); 

	bool log_is_on = false;
	bool generate_missing_rme = false;	

	CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);
	CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on, interaction_log_file);

	try 
	{
		run_params.LoadInteractionTerms(my_rank, interactionPPNN, interactionPN);
	}
	catch (const std::logic_error& e) 
	{ 
	   std::cerr << e.what() << std::endl;
	   mpi_comm_world.abort(-1);
    }

	ofstream output_file(argv[2], std::ios::binary |  std::ios::out | std::ios::trunc);  
	if (!output_file)
	{
		cerr << "Could not open '" << argv[2] << "' file for resulting 9lm symbols" << endl;
		return EXIT_FAILURE;
	}

	GenerateU9(interactionPPNN, interactionPN, bra, ket, idiag, jdiag, output_file);
	return EXIT_SUCCESS;
}

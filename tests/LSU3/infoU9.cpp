#include "U9U6U3tests.h"
#include <LookUpContainers/CSU39lm.h>

#include <iostream>
#include <fstream>

#include <boost/archive/binary_iarchive.hpp> 
#include <boost/serialization/map.hpp>
#include <boost/serialization/array.hpp>

using std::cout;
using std::endl;

void ShowData(U9LIST& u9list, bool show_content)
{
	typedef uint32_t NUMBER_COEFFS;
	typedef uint32_t FREQUENCY;
	std::map<NUMBER_COEFFS, FREQUENCY> statistics;
	
	CSU39lm<double> su3lib(1000000, 1000000);
	uint32_t ncoeffs(0);

	if (show_content)
	{
		U9LABELS u9Labels;
		cout << "ir1 ir2 ir12 ir3 ir4 ir34 ir13 ir24 ir1234 \t #occurences  \t#coeffs\t {coeffs}" << endl;

		for (U9LIST::iterator it = u9list.begin(); it != u9list.end(); ++it)
		{
			int nsu39lm = it->second.first;
			ncoeffs += nsu39lm;
			statistics[nsu39lm] += 1;

			u9Labels = it->first;

			cout << "(" << (int)u9Labels[0].lm << " " << (int)u9Labels[0].mu << ") ";
			cout << "(" << (int)u9Labels[6].lm << " " << (int)u9Labels[6].mu << ") ";
			cout << "(" << (int)u9Labels[3].lm << " " << (int)u9Labels[3].mu << ") ";
			cout << "(" << (int)u9Labels[1].lm << " " << (int)u9Labels[1].mu << ") ";
			cout << "(" << (int)u9Labels[7].lm << " " << (int)u9Labels[7].mu << ") ";
			cout << "(" << (int)u9Labels[4].lm << " " << (int)u9Labels[4].mu << ") ";
			cout << "(" << (int)u9Labels[2].lm << " " << (int)u9Labels[2].mu << ") ";
			cout << "(" << (int)u9Labels[8].lm << " " << (int)u9Labels[8].mu << ") ";
			cout << "(" << (int)u9Labels[5].lm << " " << (int)u9Labels[5].mu << ") ";
			cout << "\t" << it->second.second << "\t" << nsu39lm << "\t";
			double su39lm[nsu39lm];
			Get9lmWithout6lmCache(u9Labels[0], u9Labels[6], u9Labels[3], u9Labels[1], u9Labels[7], u9Labels[4], u9Labels[2], u9Labels[8], u9Labels[5], su39lm);
			for (int i = 0; i < nsu39lm;++i)
			{
				cout << su39lm[i] << " ";
			}
			cout << endl;
		}
	}
	else
	{
		for (U9LIST::iterator it = u9list.begin(); it != u9list.end(); ++it)
		{
			uint32_t nsu39lm = it->second.first;
			statistics[nsu39lm] += 1;
			ncoeffs += nsu39lm;
		}
	}

	cout << "U6 coeffs size\t frequency" << endl;
	for (std::map<NUMBER_COEFFS, FREQUENCY>::iterator it = statistics.begin(); it != statistics.end(); ++it)
	{
		cout << it->first << "\t\t" << it->second << "\t" << 100.0*(it->second/(1.0*u9list.size())) << "%" << endl;
	}
	cout << "#9-(l m): " << u9list.size() << endl;
	cout << "total size of all coeffs: " << ncoeffs << endl;
}


int main(int argc,char **argv)
{
	if (argc != 3)
	{
		cout << "Usage: "<< argv[0] <<" <U9 file name> <1 or 0>" << endl;
		cout << "1: list content of file with U9 coefficients" << endl;
		return EXIT_FAILURE;
	}

	std::ifstream input_file(argv[1],  std::ios::binary);
	if (!input_file)
	{
		std::cerr << "Could not open '" << argv[1] << "' input file with 9-(lm mu) coefficients" << endl;
		return EXIT_FAILURE;
	}

	U9LIST u9list;
	boost::archive::binary_iarchive ia(input_file);

	ia  >> u9list;

	bool list_content = (argv[2][0] == '1');

	ShowData(u9list, list_content);

	return EXIT_SUCCESS;
}
